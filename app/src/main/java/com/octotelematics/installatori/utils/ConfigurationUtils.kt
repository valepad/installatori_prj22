package com.octotelematics.installatori.utils

object ConfigurationUtils {

    enum class DashboardSections {
        HOME,
        DASHBOARD,
        CALENDAR
    }

    enum class ProfileSections (val title: String) {
        ACCOUNT("Account"),
        TUTORIAL("Tutorials"),
        CRASHES("Crashes"),
        TERMS("Terms & Conditions"),
        SETTINGS("Settings")
    }

    val listDashboardTabBar = mutableListOf(DashboardSections.HOME, DashboardSections.DASHBOARD,DashboardSections.CALENDAR)


    val listProfileSections = mutableListOf(ProfileSections.ACCOUNT, ProfileSections.TUTORIAL, ProfileSections.CRASHES, ProfileSections.TERMS, ProfileSections.SETTINGS)

    //FINGERPRINT
    val FINGERPRINT = true
}