package com.octotelematics.installatori.utils

import android.app.Application
import android.content.Context
import com.octo.core.DDSDK
import com.octo.core.distracted_driving.DDDistraction
import com.octo.core.tag.DDBLE
import com.octotelematics.installatori.BuildConfig


class CustomApplication : Application() {

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    companion object{
        private var mContext: Context? = null
        var bleDelegateCustom: CustomBLEDelegate? = null

        fun getContext(): Context? {
            return mContext
        }

        fun setContext(mContext: Context?) {
            this.mContext = mContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        val distraction=DDDistraction.ALL
        if (BuildConfig.FLAVOR.equals(AppUtils.APP_AS_SENSOR_FLAVOR)) {
            val configuration = null
            val eventHandler = NotificationsEventHandler.newInstance(this)
            val notification = CustomNotifications(this, eventHandler)
            DDSDK.setup(this, configuration, eventHandler, notification)
            DDDistraction.setup(this, distraction)
        }
        else if (BuildConfig.FLAVOR.equals(AppUtils.APP_AS_A_TAG)){
            val configuration = null
            val eventHandler = NotificationsEventHandler.newInstance(this)
            val notification = CustomNotifications(this, eventHandler)
            DDSDK.setup(this, configuration, eventHandler, notification)
            bleDelegateCustom = CustomBLEDelegate(this)
            DDBLE.setup(this, DDBLE.Mode.TAG, false,"http://management.uat-iot-hub.octotelematics.net/", DDBLE.ALL, bleDelegateCustom!!)
            DDDistraction.setup(this, distraction)
        }
    }

}