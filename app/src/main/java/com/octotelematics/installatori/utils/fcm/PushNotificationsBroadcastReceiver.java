package com.octotelematics.installatori.utils.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by iLabora on 13.09.2017.
 */

public class PushNotificationsBroadcastReceiver extends BroadcastReceiver {

    PushNotificationsInterface pushNotificationsInterface;

    public PushNotificationsBroadcastReceiver(PushNotificationsInterface pushNotificationsInterface) {
        this.pushNotificationsInterface = pushNotificationsInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        pushNotificationsInterface.onReceive(intent);
    }
}
