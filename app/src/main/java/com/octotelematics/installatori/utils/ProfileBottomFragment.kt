package com.octotelematics.installatori.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.R

import com.octotelematics.installatori.fragments.profile.AccountFragment


import com.octotelematics.installatori.uicore.base.BaseActivity
import kotlinx.android.synthetic.main.fragment_home.*


class ProfileBottomFragment @SuppressLint("ValidFragment") constructor(val baseActivity: BaseActivity) : BottomSheetDialogFragment() {
    private var inflateView: View? = null
    var accept = false

    private var dialog: BottomSheetDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


            setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogThemeGrey)


    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
          return  dialog!!

    }
    var voucherItem: VouchersItem? = null

//TODO PROVA CON TABLAYOUT
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (arguments != null) {
          if(  arguments!!.getString("from") == "profile") {
              inflateView = inflater.inflate(R.layout.fragment_profile, container, false)
              val nameprofile = inflateView!!.findViewById<TextView>(R.id.nameprofile)
              val accountLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_account)
              val settingsLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_settings)
              val anagraphicLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_anagrafic)
              val contanctLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_contacts)
              val timetablesLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_timetables)
              val profilesenabledLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_enable_profiles)
              val infoLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_infostructure)
              val inventaryLayout = inflateView!!.findViewById<ConstraintLayout>(R.id.profile_tab_inventory)
              if (PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) != null
              ) {
                  voucherItem = PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) as VouchersItem
              }
              nameprofile.text = voucherItem?.user?.clientFirstName+"\n"+voucherItem?.user?.clientLastName
              accountLayout.setOnClickListener {
                  baseActivity.addFragment(AccountFragment(), true, true, R.id.container) }
              settingsLayout.setOnClickListener {  dismiss()  }
              anagraphicLayout.setOnClickListener {   dismiss() }
              contanctLayout.setOnClickListener {   dismiss() }
              timetablesLayout.setOnClickListener {  dismiss()  }
              profilesenabledLayout.setOnClickListener {  dismiss()  }
              infoLayout.setOnClickListener {   dismiss() }
              inventaryLayout.setOnClickListener {  dismiss()  }
          }
        }

        return inflateView
    }




}