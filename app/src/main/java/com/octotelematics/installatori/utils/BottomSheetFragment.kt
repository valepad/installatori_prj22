package com.octotelematics.installatori.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.main.HomeFragment

import com.octotelematics.installatori.fragments.adapters.PlateListAdapter
import com.octotelematics.installatori.fragments.interfaces.OnItemClickPlate
import com.octotelematics.installatori.fragments.profile.ContactUsFragment


import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_ITEM
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_LIST


class BottomSheetFragment @SuppressLint("ValidFragment") constructor(
    private val baseFragment: BaseFragment?,
    private val dashboard: HomeFragment?

    ) :

    BottomSheetDialogFragment() {
    private var inflateView: View? = null
    var accept = false

    private var dialog: BottomSheetDialog? = null


    private var adapter: PlateListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments!!.getString("from") == "plate")
            setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogThemeGrey)
        else
            setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
          return  dialog!!

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (arguments != null) {

            if (arguments!!.getString("from") == "plate") {
                inflateView = inflater.inflate(R.layout.fragment_plate_selection, container, false)
                val prefs: SharedPreferences =
                    context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)

                val voucherList = prefs.getString(PREF_VOUCHER_LIST, "")

                if (voucherList != null && voucherList.isNotEmpty()) {
                    var gson = Gson()
                    var vouchers: Vouchers = gson.fromJson(voucherList, Vouchers::class.java)
                    //filtro i voucher della response per tipi
                    var vouchersFiltered: MutableList<VouchersItem?> = ArrayList()

                    for (i in vouchers) {
                        if (i.voucherStatus == AppUtils.VoucherStatus.CREATED.name || i.voucherStatus == AppUtils.VoucherStatus.REACTIVATED.name
                            || i.voucherStatus == AppUtils.VoucherStatus.SUSPENDED.name ||i.voucherStatus == AppUtils.VoucherStatus.DRAFT.name
                            ||i.voucherStatus==AppUtils.VoucherStatus.CREATED_NGP.name) {

                            vouchersFiltered?.add(i)

                        }
                    }
                    val scrollplate = inflateView!!.findViewById<ScrollView>(R.id.scrollplate)

                    if (vouchersFiltered != null) {
                        val json = Gson().toJson(vouchersFiltered)
                        var vouchers: Vouchers = gson.fromJson(json, Vouchers::class.java)
                        setPlateAdapter(vouchers)
                    }
                    if(vouchers.size>2){
                        scrollplate.layoutParams.height= 600
                        scrollplate.requestLayout()
                    } else{
                        scrollplate.layoutParams.height= 300
                        scrollplate.requestLayout()
                    }
                }

                val contattaci = inflateView!!.findViewById<TextView>(R.id.contattaci)
                val addvoucher = inflateView!!.findViewById<TextView>(R.id.addvoucher)
                addvoucher.setOnClickListener {
                    dismiss()


                }
                contattaci.setOnClickListener {
                   baseFragment!!.addFragment(ContactUsFragment(), true, true, R.id.container)
                    dismiss()
                    /*     val bottomAlert = BottomSheetAssistance(baseFragment)
                         if (fragmentManager != null) {
                             val args = Bundle()
                             args.putString("from", "assistance")
                             bottomAlert.arguments = args

                             bottomAlert.isCancelable = true
                             bottomAlert.show(fragmentManager!!, bottomAlert.tag)
                         }*/
                }
            }
            if (arguments!!.getString("from") == "distraction") {
                inflateView = inflater.inflate(R.layout.fragment_distraction_bottom, container, false)
                val proceed = inflateView!!.findViewById<TextView>(R.id.next_btn_insert_info_car)

                val close = inflateView!!.findViewById<TextView>(R.id.close)
                proceed.setOnClickListener {
                    dismiss()
                }
                close.setOnClickListener {
                    dismiss()
                }
            }

            // notNow.setOnClickListener((View v) -> dismiss());
            // locationButton.setOnClickListener((View view) -> enableLocationService());
        }
        return inflateView
    }

    var voucherItem: VouchersItem? = null
    var plate: String? = null

    private fun setPlateAdapter(plateResponse: Vouchers) {
        if (PreferencesHelper.getObjValue(
                context!!,
                "voucherItem",
                VouchersItem::class.java
            ) != null
        ) {
            voucherItem = PreferencesHelper.getObjValue(
                context!!,
                "voucherItem",
                VouchersItem::class.java
            ) as VouchersItem

            if (voucherItem?.motor?.plate != null) {
                plate = voucherItem?.motor?.plate
            } else {
                plate = "- - -"
            }
        }

        val baseActivity = activity as BaseActivity
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val recyclerView: RecyclerView? = inflateView?.findViewById(R.id.recyclerViewPlates)
        recyclerView?.layoutManager = layoutManager
        adapter = plateResponse?.let {
            PlateListAdapter(
                it,
                plate,
                object : OnItemClickPlate {
                    override fun onClick(position: Int) {
                        //copiato da dd azzurina. da testare
                        if (it[position].voucherStatus == AppUtils.VoucherStatus.DRAFT.name) {
                            PreferencesHelper.setStringValue(
                                context!!,
                                PreferencesHelper.PREF_VOUCHER_FK,
                                it[position].voucherId
                            )

                            dismiss()
                        }else if(it[position].voucherStatus == AppUtils.VoucherStatus.CREATED.name){
                            PreferencesHelper.setObjValue(context!!, PREF_VOUCHER_ITEM, it[position])
                            PreferencesHelper.setStringValue(
                                context!!,
                                "voucherID",
                                it[position].idVoucher
                            )
                            if (BuildConfig.FLAVOR == AppUtils.APP_AS_UI_FLAVOR) {
                                PreferencesHelper.setStringValue(
                                    baseActivity,
                                    PreferencesHelper.PREF_VOUCHER_ID,
                                    it[position]!!.idVoucher
                                )
                            }else{
                                PreferencesHelper.setStringValue(context!!,
                                    PreferencesHelper.PREF_DEVICE_ID,  it[position].device?.deviceId)
                            }
                            dashboard!!.loadData()
                            dismiss()
                        }


                    }
                }
            )
        }
        recyclerView?.adapter = adapter
    }


}