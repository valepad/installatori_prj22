package com.octotelematics.installatori.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.octo.core.DDSDK

class CustomBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (DDSDK.isRegistered()) {
            Log.e("BroadcastReceiver", "It's already registered")
            DDSDK.start(context!!)
        }
        else {
            Log.e("BroadcastReceiver", "It's not registered")
        }
    }
}