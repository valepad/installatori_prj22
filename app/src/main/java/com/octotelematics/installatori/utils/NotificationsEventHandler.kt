package com.octotelematics.installatori.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.octo.core.DDEventHandler
import com.octo.core.DDTagDiscardableConnection
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_SHOW_DIALOG_GPS


class NotificationsEventHandler(context: Context) : DDEventHandler {

    val mContext: Context = context
    var dialog: DialogUtils? = null


    companion object{
        fun newInstance(context: Context): NotificationsEventHandler {
            val notification = NotificationsEventHandler(context)
            return notification
        }
    }
    override fun onAuthFailed() {
        val handlerThread = HandlerThread("MyHandlerThread")
        handlerThread.start()
        val looper: Looper = handlerThread.getLooper()
        val handler = Handler(looper)
        handler.post(Runnable(){
            dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
                .title("ERRORE")
                .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
                .content("OnAuthFailed - Failure to start framework")
                .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
                .positiveText("Close")
                .show()
        })
        Log.e("onAuthFailed","onAuthFailed")
    }

    override fun onBatteryLow() {
        Log.e("onBatteryLow","onBatteryLow")
    }

    override fun onGpsRequired() {
        Log.e("onGpsRequired","onGpsRequired")
        val showDialogGPS = PreferencesHelper.getBooleanValue(mContext, PREF_SHOW_DIALOG_GPS)
        if (!showDialogGPS){
            PreferencesHelper.setBooleanValue(mContext, PREF_SHOW_DIALOG_GPS, true)
            dialog = DialogUtils.Builder(dialog,CustomApplication.getContext() as Activity)
                .title("ATTENZIONE")
                .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
                .content("Attiva il gps per registrare i viaggi!")
                .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
                .positiveText("Close")
                .show()
        }
    }

   /* override fun onMissingActivityRecognitionPermission() {
        ActivityCompat.requestPermissions(
            CustomApplication.getContext() as Activity,
            arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), 99);
    }*/

    override fun onMissingActivityRecognitionPermission() {
        ActivityCompat.requestPermissions(
            CustomApplication.getContext() as Activity,
            arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), 99);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ContextCompat.checkSelfPermission( CustomApplication.getContext() as Activity,
                    Manifest.permission.ACTIVITY_RECOGNITION)
                === PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
            }else {
                ActivityCompat.requestPermissions(
                    CustomApplication.getContext() as Activity,
                    arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), 99);
            }
        };
    }

    override fun onMissingLocationPermission() {
        ActivityCompat.requestPermissions(
            CustomApplication.getContext() as Activity, arrayOf(
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            22
        )

        isPermissionNotGranted(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun isPermissionNotGranted(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            CustomApplication.getContext() as Activity,
            permission
        ) != PackageManager.PERMISSION_GRANTED
    }

    override fun onMissingPhonePermission() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE)
            != PackageManager.PERMISSION_GRANTED) {
            // We do not have this permission. Let's ask the user
        }
        Log.e("MISION","onMissingPhonePermission")
    }

    override fun onTripStarted() {
        CustomNotifications.newIstance(mContext, NotificationsEventHandler(mContext))
        Log.e("STARTED","PARTITO IL VIAGGIO")
    }

    override fun onTripStopped() {
        CustomNotifications.newIstance(mContext, NotificationsEventHandler(mContext))
        Log.e("STOPPED", "STOPPATO IL VIAGGIO")
    }

    override fun willConnectToTag(tagConnection: DDTagDiscardableConnection) {

    }
}