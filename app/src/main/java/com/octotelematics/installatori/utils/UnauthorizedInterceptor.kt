package com.octotelematics.installatori.utils

import com.octotelematics.installatori.network.utils.CustomInterceptor
import com.octotelematics.installatori.uicore.base.BaseActivity
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class UnauthorizedInterceptor(var activity: BaseActivity, var type: String?) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val response: Response =
            CustomInterceptor(type).intercept(chain)
        if (response.code == 401) { // Magic is here ( Handle the error as your way )
            AppUtils.logout(activity)
        }
        return response
    }
}
