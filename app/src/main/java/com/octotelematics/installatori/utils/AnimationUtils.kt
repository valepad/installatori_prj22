package com.octotelematics.installatori.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.view.View

object AnimationUtils {
    fun showPopupWindow(popupContainer: View, fade: Boolean, move: Boolean) {
        val yAnimator = setupYAnimator(popupContainer, 0F, if (move) 500 else 0)
        val fadeAnimator = setupFadeAnimator(popupContainer, 1.0f, if (fade) 500 else 0)
        val set = AnimatorSet()
        if (fade && move) {
            set.playTogether(yAnimator, fadeAnimator)
        } else {
            set.playSequentially(yAnimator, fadeAnimator)
        }
        set.start()
    }

    fun hidePopupWindow(popupContainer: View) {
        hidePopupWindow(popupContainer, true, true)
    }

    fun hidePopupWindow(popupContainer: View, fade: Boolean, move: Boolean) {
        val yAnimator = setupYAnimator(popupContainer, popupContainer.height.toFloat(), if (move) 500 else 0)
        val fadeAnimator = setupFadeAnimator(popupContainer, 0.0f, if (fade) 500 else 0)
        val set = AnimatorSet()
        if (fade && move) {
            set.playTogether(yAnimator, fadeAnimator)
        } else {
            set.playSequentially(fadeAnimator, yAnimator)
        }
        set.start()
    }

    //region Trends
    fun trendsTutorialIn(view: View) {
        val xAnimator = setupXAnimator(view, 0F, 250)
        val fadeAnimator = setupFadeAnimator(view, 1.0f, 250)
        val set = AnimatorSet()
        set.playTogether(xAnimator, fadeAnimator)
        set.start()
    }


    fun trendsTutorialOut(view: View) {
        val xAnimator = setupXAnimator(view, view.width.toFloat(), 250)
        val fadeAnimator = setupFadeAnimator(view, 0.0f, 250)
        val set = AnimatorSet()
        set.playTogether(xAnimator, fadeAnimator)
        set.start()
    }


    fun trendsLabelIn(context: Context, label: View): AnimatorSet {

        val d = context.resources.displayMetrics.density

        val labelWidthDp = 35
        val labelHeightDp = 25
        val labelWidthPx = (labelWidthDp * d).toInt()
        val labelHeightPx = (labelHeightDp * d).toInt() // margin in pixels

        val widthAnimator = setupWidthAnimator(label, labelWidthPx, 250)
        val heightAnimator = setupHeightAnimator(label, labelHeightPx, 250)

        val set = AnimatorSet()
        set.playTogether(widthAnimator, heightAnimator)
        set.start()

        return set
    }

    fun trendsLabelOut(context: Context, label: View): AnimatorSet {

        val widthAnimator = setupWidthAnimator(label, 0, 250)
        val heightAnimator = setupHeightAnimator(label, 0, 250)

        val set = AnimatorSet()
        set.playTogether(widthAnimator, heightAnimator)
        set.start()

        return set
    }

    fun trendsChart(context: Context, label: View, value: Float?): AnimatorSet {

        val d = context.resources.displayMetrics.density

        val chartAnimator = ValueAnimator.ofInt(label.height, (value!! * d * 17f).toInt())  //170dp
        chartAnimator.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Int
            val layoutParams = label.layoutParams
            layoutParams.height = `val`
            label.layoutParams = layoutParams
        }

        val set = AnimatorSet()
        set.play(chartAnimator)
        set.start()

        return set
    }
    //endregion

    /**
     * PRIVATE METHODS
     */

    private fun setupXAnimator(viewToAnimate: View, finalX: Float, duration: Int): ValueAnimator {
        val animator = ValueAnimator.ofFloat(viewToAnimate.x, finalX)
        animator.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Float
            viewToAnimate.x = `val`
        }
        animator.duration = duration.toLong()

        return animator
    }

    private fun setupYAnimator(viewToAnimate: View, finalY: Float, duration: Int): ValueAnimator {
        val animator = ValueAnimator.ofFloat(viewToAnimate.y, finalY)
        animator.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Float
            viewToAnimate.y = `val`
        }
        animator.duration = duration.toLong()

        return animator
    }

    private fun setupHeightAnimator(
        viewToAnimate: View,
        finalHeight: Int,
        duration: Int
    ): ValueAnimator {
        val animator = ValueAnimator.ofInt(viewToAnimate.measuredHeight, finalHeight)
        animator.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Int
            val layoutParams = viewToAnimate.layoutParams
            layoutParams.height = `val`
            viewToAnimate.layoutParams = layoutParams
        }
        animator.duration = duration.toLong()

        return animator
    }

    private fun setupWidthAnimator(
        viewToAnimate: View,
        finalWidth: Int,
        duration: Int
    ): ValueAnimator {
        val animator = ValueAnimator.ofInt(viewToAnimate.measuredWidth, finalWidth)
        animator.addUpdateListener { valueAnimator ->
            val `val` = valueAnimator.animatedValue as Int
            val layoutParams = viewToAnimate.layoutParams
            layoutParams.width = `val`
            viewToAnimate.layoutParams = layoutParams
        }
        animator.duration = duration.toLong()

        return animator
    }

    private fun setupFadeAnimator(
        viewToAnimate: View,
        fadeValue: Float,
        duration: Int
    ): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(viewToAnimate, "alpha", fadeValue)
        animator.duration = duration.toLong()

        return animator
    }

    private fun setupDelayedFadeAnimator(
        viewToAnimate: View,
        fadeValue: Float,
        duration: Int,
        delay: Long
    ): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(viewToAnimate, "alpha", fadeValue)
        animator.duration = duration.toLong()
        animator.startDelay = delay
        return animator
    }

    private fun setupTranslationYAnimator(
        viewToAnimate: View,
        finalY: Float,
        duration: Int
    ): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(viewToAnimate, "translationY", finalY)
        animator.setDuration(duration.toLong())

        return animator
    }
}