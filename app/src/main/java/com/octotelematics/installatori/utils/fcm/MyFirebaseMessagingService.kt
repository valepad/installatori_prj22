package com.octotelematics.installatori.utils.fcm

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

import android.text.TextUtils
import android.util.ArrayMap
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.octotelematics.installatori.network.DDNetworkClient
import APIRepository
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.BuildConfig.TSC_ID
import com.octotelematics.installatori.main.MainActivity

import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by iLabora on 10.08.2017.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var notificationUtils: NotificationUtils? = null
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        // Saving reg id to shared preferences
        storeRegId(s)

        // sending reg id to your server
//        sendRegistrationToServer(refreshedToken);
        updateToken(s)
    }

    private fun storeRegId(token: String) {
        val pref = getFirebasePreferences(applicationContext)
        val editor = pref.edit()
        editor.putString(FirebaseConfig.PREF_REG_ID, token)
        editor.putBoolean(FirebaseConfig.PREF_SENT, false)
        editor.apply()
    }

    @SuppressLint("CheckResult")
    private fun updateToken(token: String) {
        if (DDNetworkClient.instance != null && DDNetworkClient.instance.apiService != null) {
            val jsonParamsToken: MutableMap<String?, Any?> =
                ArrayMap()
            jsonParamsToken["deviceType"] = "Android"
            jsonParamsToken["token"] = token
            val body = ConvertRequestBody.responseBodyReturn(jsonParamsToken)
            APIRepository(applicationContext).accessPushToken(
                TSC_ID,
                body,
                object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        Log.e("DEBUG", "ON FAILURE = " + t)
                        //   baseFragment!!.hideProgress()
                    }

                    override fun onResponse(
                        call: retrofit2.Call<ResponseBody>,
                        response: retrofit2.Response<ResponseBody>) {
                        if (response.isSuccessful) {
                            if (applicationContext != null) {
                                FirebaseMessaging.getInstance().isAutoInitEnabled = true
                                val pref = MyFirebaseMessagingService.getFirebasePreferences(applicationContext!!)
                                val edit = pref.edit()
                                edit.putBoolean(FirebaseConfig.PREF_SENT, true)
                                edit.apply()
                                Log.d("OctoLog", "WITH THE PUSH TOKEN IT IS ALL RIGHT")
                            }
                        }
                        //baseFragment!!. hideProgress()
                    }

                }
            )

        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage == null) return

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            handleNotification(remoteMessage.notification!!.title,remoteMessage.notification!!.body)
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            try {
                val json = JSONObject(remoteMessage.data.toString())
                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e("fcm", e.message)
            }
        }
    }

    private fun handleNotification(title:String?,message: String?) {
        if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            /*
            Intent pushNotification = new Intent(FirebaseConfig.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            */
            handleDataMessage(title,message)
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private fun handleDataMessage(json: JSONObject) {
        try {
            val data = json.getJSONObject("data")
            val title = data.getString("title")
            val message = data.getString("message")
            val isBackground = data.getBoolean("is_background")
            val imageUrl = data.getString("image")
            val timestamp = data.getString("timestamp")
            val payload = data.getJSONObject("payload")

            // TODO: 2019-11-05 decomment after client tests
            if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
                // app is in foreground, broadcast the push message
                val pushNotification = Intent(FirebaseConfig.PUSH_NOTIFICATION)
                pushNotification.putExtra("message", message)
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
            } else {
                // app is in background, show the notification in notification tray
                val resultIntent = Intent(applicationContext, MainActivity::class.java)
                resultIntent.putExtra("message", message)

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(
                        applicationContext,
                        title,
                        message,
                        timestamp,
                        resultIntent
                    )
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(
                        applicationContext,
                        title,
                        message,
                        timestamp,
                        resultIntent,
                        imageUrl
                    )
                }
            }
        } catch (e: JSONException) {
            Log.e("", e.message)
        } catch (e: Exception) {
            Log.e("", e.message)
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(
        context: Context,
        title: String?,
        message: String?,
        timeStamp: String,
        intent: Intent
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent,
        imageUrl: String
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    private fun handleDataMessage(title:String?,message: String?) {
        try {
            // app is in background, show the notification in notification tray
            val resultIntent = Intent(applicationContext, MainActivity::class.java)
            resultIntent.putExtra("message", message)
            resultIntent.putExtra("title", title)

            // TODO: 2019-11-05 decomment after client tests
            showNotificationMessage(applicationContext, title, message, "", resultIntent)
        } catch (e: Exception) {
            Log.e("", e.message)
        }
    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.simpleName
        fun getFirebasePreferences(applicationContext: Context): SharedPreferences {
            return applicationContext.getSharedPreferences(FirebaseConfig.SHARED_PREF, 0)
        }
    }
}