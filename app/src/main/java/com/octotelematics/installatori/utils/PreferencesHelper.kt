package com.octotelematics.installatori.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import java.lang.reflect.Type
import java.util.*

object PreferencesHelper {
    private const val APP_SETTINGS = "APP_SETTINGS"
    const val DIGITAL_DRIVER = "DigitalDriver"
    const val FINGER_DATA = "FingerData"

    // PREFERENCES KEYS
    const val TRIP_RECORDING = "TRIP_RECORDING"
    const val SHOW_REJECTED_TRIP = "SHOW_REJECTED_TRIP"
    const val PUSH_ACTIVE = "PUSH_ACTIVE"
    const val FINGERPRINT_DISABLE = "FINGERPRINT_DISABLE"

    const val PREF_DEVICE_ID = "deviceId"
    const val PREF_OLD_PASSWORD = "oldPassword"
    const val PREF_EMAIL_USER = "emailUser"
    const val REMEMBERME = "ememberme"
    const val PREF_USER_ID = "userId"
    const val PREF_BADGES = "badges"
    const val PREF_VOUCHER_ID = "voucherID"
    const val PREF_BADGES_ACHIEVED = "badgesAchieved"
    const val PREF_DD_TOKEN = "ddToken"
    const val PREF_VOUCHER_FK = "voucherFk"

    const val PREF_OLD_PASSWORD_TEMPORARY = "oldPasswordTemporary"
    const val PREF_VOUCHER_LIST = "voucherList"
    const val PREF_PACKAGE_ID_SELECTED = "packageIdSelected"
    const val PREF_NEW_PASSWORD = "newPassword"

    const val PREF_CLIENT_NAME = "clientName"
    const val PREF_CLIENT_SURNAME = "clientSurname"
    const val PREF_PHONE = "phone"
    const val PREF_DATE_SAVED = "dateSaved"
    const val PREF_FIRST_PLAYGROUND_ACCESS = "firstPlaygroundAccess"

    const val PREF_PLATE = "plate"
    const val PREF_VIN = "vin"
    const val PREF_DATE_CAR = "dateCar"
    const val PREF_BRAND = "brand"
    const val PREF_MODEL = "model"
    const val PREF_COLOR = "color"

    const val PREF_TOKEN_ACCESS = "tokenAccess"
    const val PREF_CODE_COLOR = "codeColor"

    const val PREF_ACCESS_TOKEN = "accessToken"

    const val PREF_NAME = "name"
    const val PREF_SURNAME = "surname"
    const val PREF_FISCAL_CODE = "fiscalCode"
    const val PREF_DATE = "date"
    const val PREF_TEMPLATE = "template"
    const val PREF_LEADERBOARDENABLE = "leaderboardenable"
    const val PREF_SHOW_RECEIVED_CHALLENGES = "showReceivedChallenges"

    const val PREF_SEEN_FRIENDS = "seenFriends"
    const val PREF_SEEN_CHALLENGE = "seenchallenge"

    const val PREF_LATITUDE_CAR = "latitudeCar"
    const val PREF_LONGITUDE_CAR = "longitudeCar"
    const val PREF_TIMESTAMP_CAR = "timestampCar"

    const val PREF_VOUCHER_ITEM = "voucherItem"
    const val PREF_SERIAL_NUMBER = "serialNumber"
    const val PREF_VOUCHER_GARAGE = "voucherGarage"

    const val PREF_CURRENT_VOUCHER = "currentVoucher"
    const val PREF_CURRENT_USER = "currentUser"
    const val PREF_CURRENT_PLAYER = "currentPlayer"

    const val PREF_FRIEND_LIST = "friendList"

    const val PREF_CURRENT_PLAYER_ID = "currentPlayerID"

    const val ISTIMETAB = "ISTIMETAB"
    const val ISTHEREDATA = "istheredata"
    const val DAY = "day"
    const val YEAR = "year"
    const val MONTH = "month"
    const val WEEK = "week"

    const val DAY_CHART_TIME = "day_time"
    const val NIGHT_CHART_TIME = "night_time"
    const val TOTAL_HOURS = "total_hours"
    const val DAY_CHART_DISTANCE = "day_distance"
    const val NIGHT_CHART_DISTANCE = "night_distance"

    const val HIGHWAY_DISTANCE = "highway_distance"
    const val INTERSTATE_DISTANCE = "interstate_distance"
    const val CITY_DISTANCE = "city_distance"

    const val HIGHWAY_TIME = "highway_time"
    const val INTERSTATE_TIME = "interstate_time"
    const val CITY_TIME = "city_time"

    const val PREF_DURATION = "duration"
    const val PREF_KM = "km"
    const val PREF_JOURNEYS = "journeys"

    const val PREF_DRIVING_SCORE = "drivingScore"
    const val PREF_LEVELS = "levels"
    const val PREF_LEVELSLIST = "levelslist"

    const val PREF_SERIAL_NUMBER_TO_SHOW = "serialNumberToShow"
    const val PREF_IS_FROM_TAG_FRAGMENT = "isFromTagFragment"

    const val PREF_REFRESH_TRIP_LIST = "refreshTripList"
    const val PREF_SHOW_DIALOG_GPS = "showDialogGPS"

    const val PREF_SMARTDIAG_IMEI = "smartdiagImei"
    const val PREF_SMARTDIAG_SERIAL = "smartdiagSerial"
    const val PREF_FROM_DETAIL_TAG = "detailTag"


    const val PREF_LANGUAGE = "language"


    const val PREF_EMAIL_FINGER = "emailFinger"
    const val PREF_PSW_FINGER = "pswFinger"
    const val PREF_FINGER_DISABLED = "fingerDisabled"


    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            DIGITAL_DRIVER,
            Context.MODE_PRIVATE
        )
    }

    fun getBooleanValue(context: Context, keyName: String): Boolean {
        return getSharedPreferences(context)
            .getBoolean(keyName, false)
    }

    fun setBooleanValue(
        context: Context,
        keyName: String,
        value: Boolean
    ) {
        val editor =
            getSharedPreferences(context).edit()
        editor.putBoolean(keyName, value)
        editor.commit()
    }

    fun getObjValue(context: Context, keyName: String, classModel: Objects): Any? {
        val gson = Gson()
        val json: String? = getSharedPreferences(context)
            .getString(keyName, null)
        val obj: Objects = gson.fromJson(json, classModel::class.java)
        return obj
    }

    fun getObjValue(context: Context, keyName: String, type: Type): Any? {
        val gson = Gson()
        val json: String = getSharedPreferences(context)
            .getString(keyName, null)
            ?: return null
        return gson.fromJson<Objects?>(json, type)
    }

    fun setObjValue(
        context: Context,
        keyName: String,
        value: Any?
    ) {
        val gson = Gson()
        val json = gson.toJson(value)
        val editor =
            getSharedPreferences(context).edit()
        editor.putString(keyName, json)
        editor.commit()
    }

    fun getStringValue(context: Context, keyName: String): String? {
        return getSharedPreferences(context)
            .getString(keyName, null)
    }

    fun setStringValue(
        context: Context,
        keyName: String,
        value: String?
    ) {
        val editor =
            getSharedPreferences(context).edit()
        editor.putString(keyName, value)
        editor.commit()
    } // other getters/setters

    fun getIntValue(context: Context, keyName: String): Int? {
        return getSharedPreferences(context)
            .getInt(keyName, -1)
    }

    fun setIntValue(
        context: Context,
        keyName: String,
        value: Int
    ) {
        val editor =
            getSharedPreferences(context).edit()
        editor.putInt(keyName, value)
        editor.commit()
    } // other getters/setters

    fun getLongValue(context: Context, keyName: String): Long? {
        return getSharedPreferences(context)
            .getLong(keyName, -1)
    }

    fun setLongValue(
        context: Context,
        keyName: String,
        value: Long
    ) {
        val editor =
            getSharedPreferences(context).edit()
        editor.putLong(keyName, value)
        editor.commit()
    }

    fun hasValue(
        context: Context,
        keyName: String
    ): Boolean {
        return getSharedPreferences(context).contains(keyName)
    }

    fun removeValue(
        context: Context,
        keyName: String
    ) {
        val editor =
            getSharedPreferences(context).edit()
        editor.remove(keyName)
        editor.commit()
    }

    fun removeAll(context: Context) {
        val editor =
            getSharedPreferences(context).edit()
        editor.clear()
        editor.commit()
    }

    // methods with different SharedPreferences
    private fun getSharedPreferences(context: Context, sharedName: String): SharedPreferences {
        return context.getSharedPreferences(
            sharedName,
            Context.MODE_PRIVATE
        )
    }

    fun getBooleanValue(context: Context, sharedName: String, keyName: String): Boolean {
        return getSharedPreferences(context, sharedName)
            .getBoolean(keyName, false)
    }

    fun setBooleanValue(
        context: Context,
        sharedName: String,
        keyName: String,
        value: Boolean
    ) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.putBoolean(keyName, value)
        editor.commit()
    }

    fun getObjValue(
        context: Context,
        sharedName: String,
        keyName: String,
        classModel: Objects
    ): Any? {
        val gson = Gson()
        val json: String? = getSharedPreferences(context, sharedName)
            .getString(keyName, null)
        val obj: Objects = gson.fromJson(json, classModel::class.java)
        return obj
    }

    fun getObjValue(context: Context, sharedName: String, keyName: String, type: Type): Any? {
        val gson = Gson()
        val json: String = getSharedPreferences(context, sharedName)
            .getString(keyName, null)
            ?: return null
        return gson.fromJson<Objects?>(json, type)
    }

    fun setObjValue(
        context: Context,
        sharedName: String,
        keyName: String,
        value: Any?
    ) {
        val gson = Gson()
        val json = gson.toJson(value)
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.putString(keyName, json)
        editor.commit()
    }

    fun getStringValue(context: Context, sharedName: String, keyName: String): String? {
        return getSharedPreferences(context, sharedName)
            .getString(keyName, null)
    }

    fun setStringValue(
        context: Context,
        sharedName: String,
        keyName: String,
        value: String?
    ) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.putString(keyName, value)
        editor.commit()
    } // other getters/setters

    fun getIntValue(context: Context, sharedName: String, keyName: String): Int? {
        return getSharedPreferences(context, sharedName)
            .getInt(keyName, -1)
    }

    fun setIntValue(
        context: Context,
        sharedName: String,
        keyName: String,
        value: Int
    ) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.putInt(keyName, value)
        editor.commit()
    } // other getters/setters

    fun getLongValue(context: Context, sharedName: String, keyName: String): Long? {
        return getSharedPreferences(context, sharedName)
            .getLong(keyName, -1)
    }

    fun setLongValue(
        context: Context,
        sharedName: String,
        keyName: String,
        value: Long
    ) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.putLong(keyName, value)
        editor.commit()
    }

    fun hasValue(
        context: Context,
        sharedName: String,
        keyName: String
    ): Boolean {
        return getSharedPreferences(context, sharedName).contains(keyName)
    }

    fun removeValue(
        context: Context,
        sharedName: String,
        keyName: String
    ) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.remove(keyName)
        editor.commit()
    }

    fun removeAll(context: Context, sharedName: String) {
        val editor =
            getSharedPreferences(context, sharedName).edit()
        editor.clear()
        editor.commit()
    }
}