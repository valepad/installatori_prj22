package com.octotelematics.installatori.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.octo.core.DDEventHandler
import com.octo.core.notification.NotificationBuilder
import com.octotelematics.installatori.R

class CustomNotifications(context: Context, ddEventHandler: DDEventHandler) :
    NotificationBuilder(context, ddEventHandler) {

    var mContext: Context = context

    companion object{
        fun newIstance(context: Context, ddEventHandler: DDEventHandler): CustomNotifications{
            val notifications: CustomNotifications = CustomNotifications(context, ddEventHandler)
            return notifications
        }
    }

    override fun getNotificationPreferences(): NotificationPreferences {
        return NotificationPreferences("Digital Driver 2.0", icon= R.drawable.ic_foreground_notification_icon,onGoingText = "Trip Started", idleText = "Trip Stopped", locationSettingsText= "Settings Lost!!!!")

    }

    override fun handleIdle() {
        super.handleIdle()
        Log.d("handleIdle", "handleIdle NOTIFICA")
    }

    override fun handleTrip() {
        super.handleTrip()
        showStartTripNotification()
        Log.d("handleTrip", "handleTrip NOTIFICA")
    }

    override fun handleTripStopped(tripId: Long) {
        super.handleTripStopped(tripId)
        showStopTripNotification(tripId)
        Log.d("handleTripStopped", "handleTripStopped NOTIFICA")
    }

    private fun showStartTripNotification() {
        val pm: PackageManager = mContext.getPackageManager()
        val appIntent = pm.getLaunchIntentForPackage(mContext.getPackageName()) ?: return
        appIntent.action = Intent.ACTION_MAIN
        appIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        appIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        //PendingIntent pIntent = PendingIntent.getService(this, 0, appIntent, 0);
        val pIntent = PendingIntent.getActivity(mContext, 0, appIntent, 0)
        val notificationManager =
            mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                ?: return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "octo-trip-stop",
                "octo-trip-stop",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(mContext, "octo-trip-stop")
        notificationBuilder.setChannelId("octo-trip-stop")
        notificationBuilder.setContentTitle("Digital Driver 2.0")
        notificationBuilder.setStyle(
            NotificationCompat.BigTextStyle()
                .bigText( //mContext.getString(R.string.notifications_tripStartedAtBody, new Date(System.currentTimeMillis()).toString()))
                    "Trip Started"
                )
        )
        notificationBuilder.setContentText( //mContext.getString(R.string.notifications_tripStartedAtBody, new Date(System.currentTimeMillis()).toString())
            "Trip Started"
        )
        notificationBuilder.setSmallIcon(R.drawable.ic_foreground_notification_icon)
        notificationBuilder.setAutoCancel(false)
        notificationBuilder.setLocalOnly(true)
        notificationBuilder.setContentIntent(pIntent)
        notificationBuilder.setDefaults(Notification.DEFAULT_ALL)
        notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        val notification: Notification = notificationBuilder.build()
        notificationManager.notify(
            (System.currentTimeMillis() % 1000000000).toInt(),
            notification
        )
    }

    private fun showStopTripNotification(tripId: Long) {
        val pm: PackageManager = mContext.getPackageManager()
        val appIntent = pm.getLaunchIntentForPackage(mContext.getPackageName()) ?: return
        appIntent.putExtra("tripID", tripId.toString() + "")
        appIntent.action = Intent.ACTION_MAIN
        appIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        appIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        //PendingIntent pIntent = PendingIntent.getService(this, 0, appIntent, 0);
        val pIntent = PendingIntent.getActivity(mContext, 0, appIntent, 0)
        val notificationManager = mContext.getSystemService(
            Context.NOTIFICATION_SERVICE
        ) as NotificationManager
            ?: return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "octo-trip-stop", "octo-trip-stop",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(mContext, "octo-trip-stop")
        notificationBuilder.setChannelId("octo-trip-stop")
        notificationBuilder.setContentTitle("Digital Driver 2.0")
        notificationBuilder.setStyle(
            NotificationCompat.BigTextStyle()
                .bigText( //mContext.getString(R.string.notifications_tripStoppedAtBody, new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.getDefault()).format(new Date(System.currentTimeMillis())))
                    "Trip Stopped"
                )
        )
        notificationBuilder.setContentText( //mContext.getString(R.string.notifications_tripStoppedAtBody, new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.getDefault()).format(new Date(System.currentTimeMillis())))
            "Trip Stopped"
        )
        notificationBuilder.setSmallIcon(R.drawable.ic_foreground_notification_icon)
        notificationBuilder.setAutoCancel(false)
        notificationBuilder.setLocalOnly(true)
        notificationBuilder.setContentIntent(pIntent)
        notificationBuilder.setDefaults(Notification.DEFAULT_ALL)
        notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        val notification: Notification = notificationBuilder.build()
        notificationManager.notify(
            (System.currentTimeMillis() % 1000000000).toInt(),
            notification
        )
    }
}