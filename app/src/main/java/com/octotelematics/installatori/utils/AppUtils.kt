package com.octotelematics.installatori.utils

import android.app.Activity
import android.content.Context
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.views.DDTextView

import com.octotelematics.installatori.R
import com.octotelematics.installatori.main.LoginFragment


object AppUtils {

    const val APP_AS_SENSOR_FLAVOR = "AaaS"
    const val APP_AS_UI_FLAVOR = "AaaUI"
    const val APP_AS_A_TAG = "AaaTag"

    const val DEFAULT_LANGUAGE = "EN"



    enum class VoucherStatus(name: String) {
        DRAFT("DRAFT"),
        SUBMIT("SUBMIT"),
        CREATED_NGP("CREATED_NGP"),
        CREATED("CREATED"),
        REACTIVATED("REACTIVATED"),
        SUSPENDED("SUSPENDED")
    }

    enum class Terms(name: String) {

        TERMSANDCONDITIONS("TERMS_AND_CONDITIONS"),
        PRIVACYPOLICY("PRIVACYPOLICY")
    }
     enum class TemplateVouchers(name: String) {
        templateTAG("templateTAG"),
        templateAAAS("templateAAAS"),
        templateUI("templateUI")
    }





    fun isUsingMiles(context: Context): Boolean {
        //TODO: DA IMPLEMENTARE
        return false
    }

    fun loadImage(url: String?, image: ImageView) {
        Glide.with(image.context) //passing context
            .load(url) //passing your url to load image.
            .placeholder(R.drawable.icon_default) //this would be your default image (like default profile or logo etc). it would be loaded at initial time and it will replace with your loaded image once glide successfully load image using url.
            //in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
            .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
            // when image (url) will be loaded by glide then this face in animation help to replace url image in the place of placeHolder (default) image.
            //.centerInside()//this method help to fit image into center of your ImageView
            .fitCenter()
            .into(image)
    }

    fun makeToast(activity: Activity, text: Int) {
        try {
            activity.runOnUiThread {
                Toast.makeText(activity, text, Toast.LENGTH_SHORT)
            }
        } catch (e: Exception) {

        }
    }

    fun makeToast(activity: Activity, text: String) {
        try {
            activity.runOnUiThread {
                Toast.makeText(activity, text, Toast.LENGTH_SHORT)
            }
        } catch (e: Exception) {

        }
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {

                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    textPaint.color = textPaint.linkColor
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            val startIndexOfLink = this.text.toString().indexOf(link.first)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    fun DDTextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {

                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    textPaint.color = textPaint.linkColor
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            val startIndexOfLink = this.text.toString().indexOf(link.first)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    fun isValidTrip(context: Context): Boolean? {
        var value: Boolean? = null
        if (!PreferencesHelper.getBooleanValue(context, PreferencesHelper.SHOW_REJECTED_TRIP)) {
            value = true
        }
        return value
    }

    fun logout(activity: BaseActivity) {
        try {
            PreferencesHelper.removeAll(activity)
            activity.replaceFragment(LoginFragment(), false, R.id.container)
        } catch (e: Exception) {
        }
    }
}