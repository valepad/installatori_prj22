package com.octotelematics.installatori.utils

import android.app.Activity
import android.content.Context
import android.util.Log
import com.octo.core.tag.BleDelegate
import com.octo.core.tag.DDBleDFUState
import com.octo.shared.model.BLE
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_IS_FROM_TAG_FRAGMENT

class CustomBLEDelegate(context: Context) : BleDelegate {
    val mContext: Context = context
    var dialog: DialogUtils? = null
    private var listenerCustom: onTagChangedListener? = null
    private var listenerProgresFirmware: onFirmwareChangedListener? = null
    private var listenerFailedFirmware: onFirmwareFailedListener? = null

    interface onTagChangedListener {
        fun setOnChangedValueTag()
    }

    interface onFirmwareChangedListener {
        fun setOnChangedValueFirmwareProgress(value:Int)
    }

    interface onFirmwareFailedListener{
        fun setOnChangedFailedListener(boolean: Boolean)
    }

    fun onTagChangedObject(){
        listenerCustom = null
    }

    fun setCustomObjectListener(listener: onTagChangedListener?) {
        this.listenerCustom = listener
    }

    fun setCustomFirmwareListener(listener: onFirmwareChangedListener?) {
        this.listenerProgresFirmware = listener
    }

    fun setCustomFirmwareFailedListener(listener: onFirmwareFailedListener?) {
        this.listenerFailedFirmware = listener
    }



    override fun batteryLow(ble: BLE, level: Int) {
        TODO("Not yet implemented")
    }

    override fun bluetoothDisabled() {
        dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
            .title("ATTENZIONE")
            .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
            .content("Bluetooth is disabled!")
            .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
            .positiveText("Close")
            .show()
    }

    override fun bluetoothPermissionRequired() {
        dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
            .title("ATTENTIONE")
            .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
            .content("Accept GPS Permission to continue!")
            .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
            .positiveText("OK")
            .show()
    }

    override fun fotaEnd(ble: BLE, status: Boolean) {
        Log.e("fotaEnd","fotaEnd")
        if (status){
            dialog?.dismiss()
            dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
                .title(mContext.resources.getString(R.string.digital_driver))
                .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
                .content(mContext.resources.getString(R.string.firmware_updated))
                .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
                .positiveText("Close")
                .show()
        }else {
            Log.e("fotaEnd", "status is$status")
            dialog?.dismiss()
            dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
                .title(mContext.resources.getString(R.string.digital_driver))
                .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
                .content(mContext.resources.getString(R.string.firmware_error_update))
                .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
                .positiveText("Close")
                .show()
        }

        if (this.listenerFailedFirmware != null){
            listenerFailedFirmware!!.setOnChangedFailedListener(status)
        }
    }



    override fun unsupportedCrashes(ble: BLE) {

    }




    override fun fotaStart(ble: BLE) {
        Log.e("fota","fotaStart")
        /*dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
            .title(mContext.resources.getString(R.string.updating_firmware))
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
            .setProgressColor(mContext.resources.getColor(R.color.colorPrimary))
            .progress()
            .show()*/
    }

    override fun fotaStatusChanged(ble: BLE, state: DDBleDFUState) {
        if(state is DDBleDFUState.progress){
            var progress = state as DDBleDFUState.progress
            progress.value
            if (this.listenerProgresFirmware != null){
                listenerProgresFirmware!!.setOnChangedValueFirmwareProgress(progress.value)
            }
        }
        Log.e("fotaStatusChanged", "fotaStatusChanged: $state")
    }

    override fun tagConnectionChanges(ble: BLE, connected: Boolean) {
        Log.e("tagConnectionChanges","tagConnectionChanges " + connected)
        if (PreferencesHelper.getBooleanValue(mContext, PREF_IS_FROM_TAG_FRAGMENT)){
            if (this.listenerCustom != null){
                listenerCustom!!.setOnChangedValueTag()
            }
        }
    }


    override fun updateAvailable(ble: BLE, version: String) {
        Log.e("updateAvailable","update is Available")
    }

    /*override fun unsupportedFirmwareVersion() {
        Log.e("unsupportedFirmwareV","unsupportedFirmwareVersion")
        dialog = DialogUtils.Builder(dialog, CustomApplication.getContext() as Activity)
            .title("ATTENZIONE")
            .setTitleColor(mContext.resources.getColor(R.color.colorPrimary))
            .content("Firmware version is unsupported!")
            .setMessageColor(mContext.resources.getColor(R.color.colorPrimary))
            .positiveText("Close")
            .show()
    }*/
}