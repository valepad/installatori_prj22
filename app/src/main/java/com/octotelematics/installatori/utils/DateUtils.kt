package com.octotelematics.installatori.utils

import android.content.Context
import android.text.TextUtils
import com.octotelematics.installatori.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DEFAULT_DATE_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    const val DEFAULT_DATE_FORMAT_3 = "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"

    fun getJourneyDate(context: Context, timestamp: Long?, timezone: String?): String {
        if (timestamp == null || timezone == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000
        cal.timeZone = TimeZone.getTimeZone(timezone)
        if (AppUtils.isUsingMiles(context)) {
            val pattern = "MMM dd,YYYY, h:mm a"
            val date = SimpleDateFormat(pattern)
            return date.format(timestamp * 1000)
        } else {
            return (if (cal.get(Calendar.DAY_OF_MONTH) < 10) "0" else "") + cal.get(Calendar.DAY_OF_MONTH) + "." +
                    (if (cal.get(Calendar.MONTH) < 9) "0" else "") + (cal.get(Calendar.MONTH) + 1) + "." +
                    cal.get(Calendar.YEAR) + " - " +
                    (if (cal.get(Calendar.HOUR_OF_DAY) < 10) "0" else "") + cal.get(Calendar.HOUR_OF_DAY) + ":" +
                    (if (cal.get(Calendar.MINUTE) < 10) "0" else "") + cal.get(Calendar.MINUTE)
        }
    }

    fun getJourneyDateRefused(context: Context, timestamp: Long?, timezone: String?): String {
        if (timestamp == null || timezone == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000
        cal.timeZone = TimeZone.getTimeZone(timezone)
        if (AppUtils.isUsingMiles(context)) {
            val pattern = "MMM dd,YYYY"
            val date = SimpleDateFormat(pattern)
            return date.format(timestamp * 1000)
        } else {
            return (if (cal.get(Calendar.DAY_OF_MONTH) < 10) "0" else "") + cal.get(Calendar.DAY_OF_MONTH) + "." +
                    (if (cal.get(Calendar.MONTH) < 9) "0" else "") + (cal.get(Calendar.MONTH) + 1) + "." +
                    cal.get(Calendar.YEAR)
        }
    }

    fun getJourneyDate(context: Context, timestamp: Long?, timezone: TimeZone): String {
        if (timestamp == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000
        cal.timeZone = timezone

        return (if (cal.get(Calendar.DAY_OF_MONTH) < 10) "0" else "") + cal.get(Calendar.DAY_OF_MONTH) + "." +
                (if (cal.get(Calendar.MONTH) < 9) "0" else "") + (cal.get(Calendar.MONTH) + 1) + "." +
                cal.get(Calendar.YEAR) + " - " +
                (if (cal.get(Calendar.HOUR_OF_DAY) < 10) "0" else "") + cal.get(Calendar.HOUR_OF_DAY) + ":" +
                (if (cal.get(Calendar.MINUTE) < 10) "0" else "") + cal.get(Calendar.MINUTE)
    }

    fun getJourneyTime(context: Context, timestamp: Long?, timezone: String?): String {
        if (timestamp == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000
        if (timezone == null)
            cal.timeZone = TimeZone.getTimeZone("UTC")
        else
            cal.timeZone = TimeZone.getTimeZone(timezone)
        if (!AppUtils.isUsingMiles(context)) {
            return (if (cal.get(Calendar.HOUR_OF_DAY) < 10) "0" else "") + cal.get(Calendar.HOUR_OF_DAY) + ":" +
                    (if (cal.get(Calendar.MINUTE) < 10) "0" else "") + cal.get(Calendar.MINUTE)
        } else {
            val dateFormat = SimpleDateFormat("hh.mm aa")
            val date = Date(timestamp * 1000)
            return dateFormat.format(date)
        }
    }


    fun getJourneyTime(context: Context, timestamp: Long?, timezone: TimeZone?): String {
        if (timestamp == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000
        if (timezone == null)
            cal.timeZone = TimeZone.getTimeZone("UTC")
        else
            cal.timeZone = timezone
        return (if (cal.get(Calendar.HOUR_OF_DAY) < 10) "0" else "") + cal.get(Calendar.HOUR_OF_DAY) + ":" +
                (if (cal.get(Calendar.MINUTE) < 10) "0" else "") + cal.get(Calendar.MINUTE)
    }

    fun getMinutesFromSeconds(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60
        val secondsLeft = seconds - minutes * 60

        var result = minutes.toString() + "m:"
        if (secondsLeft < 10) {
            result += "0"
        }
        result += secondsLeft.toString() + "s"
        return result
    }

    fun getMinutes(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60


        return minutes.toString() + "m"
    }

    fun getDaysHoursMinutesFromSeconds(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60
        val secondsLeft = seconds - minutes * 60
        val hours = minutes / 60
        val minutesLeft = minutes - hours * 60
        val days = hours / 24
        val hoursLeft = hours - days * 24

        var result: String
        if (days > 0) {
            result = days.toString() + "d:"
            if (hoursLeft < 10) {
                result += "0"
            }
            result += hoursLeft.toString() + "h:"
            if (minutesLeft < 10) {
                result += "0"
            }
            result += minutesLeft.toString() + "m"
        } else if (hours > 0) {
            result = hours.toString() + "h:"
            if (minutesLeft < 10) {
                result += "0"
            }
            result += minutesLeft.toString() + "m:"
            if (secondsLeft < 10) {
                result += "0"
            }
            result += secondsLeft.toString() + "s"
        } else {
            result = minutes.toString() + "m:"
            if (secondsLeft < 10) {
                result += "0"
            }
            result += secondsLeft.toString() + "s"
        }
        return result
    }

    fun getHoursMinutesFromSeconds(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60
        val secondsLeft = seconds - minutes * 60
        val hours = minutes / 60
        val minutesLeft = minutes - hours * 60

        var result = ""

        if (hours < 10) {
            result += "0"
        }
        result += "$hours h "
        if (minutesLeft < 10) {
            result += "0"
        }
        result += "$minutesLeft m"

        return result
    }

    fun getDaysHoursFromSeconds(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60
        val secondsLeft = seconds - minutes * 60
        val hours = minutes / 60
        val minutesLeft = minutes - hours * 60
        val days = hours / 24
        val hoursLeft = hours - days * 24

        var result: String

        if (duration == 0L) {
            result = "0"
        } else {

            if (days > 0) {
                result = days.toString() + "d:"
                if (hoursLeft < 10) {
                    result += "0"
                }
                result += hoursLeft.toString() + "h"
            } else if (hours > 0) {
                result = hours.toString() + "h:"
                if (minutesLeft < 10) {
                    result += "0"
                }
                result += minutesLeft.toString() + "m"
            } else {
                result = minutes.toString() + "m:"
                if (secondsLeft < 10) {
                    result += "0"
                }
                result += secondsLeft.toString() + "s"
            }
        }
        return result
    }

    fun getDaysHoursFromSecondsDifferentLayout(context: Context, duration: Long?): String {
        if (duration == null) {
            return context.resources.getString(R.string.na)
        }
        val seconds = duration.toInt()
        val minutes = seconds / 60
        val secondsLeft = seconds - minutes * 60
        val hours = minutes / 60
        val minutesLeft = minutes - hours * 60
        val days = hours / 24
        val hoursLeft = hours - days * 24

        var result: String
        if (days > 0) {
            result = "$days d "
            if (hoursLeft < 10) {
                result += "0"
            }
            result += "$hoursLeft h "
        } else if (hours > 0) {
            result = "$hours h "
            if (minutesLeft < 10) {
                result += "0"
            }
            result += "$minutesLeft m "
        } else {
            result = "$minutes m "
            if (secondsLeft < 10) {
                result += "0"
            }
            result += "$secondsLeft s "
        }
        return result
    }

    fun getDefaultDateFormat(context: Context, timestamp: Long?): String {
        if (timestamp == null) {
            return context.resources.getString(R.string.na)
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp * 1000

        return (if (cal.get(Calendar.DAY_OF_MONTH) < 10) "0" else "") + cal.get(Calendar.DAY_OF_MONTH) + "-" +
                (if (cal.get(Calendar.MONTH) < 10) "0" else "") + cal.get(Calendar.MONTH) + "-" +
                cal.get(Calendar.YEAR) + " " +
                (if (cal.get(Calendar.HOUR_OF_DAY) < 10) "0" else "") + cal.get(Calendar.HOUR_OF_DAY) + ":" +
                (if (cal.get(Calendar.MINUTE) < 10) "0" else "") + cal.get(Calendar.MINUTE)

    }

    fun formatTrendsDate(threeWeeksAgo: String, today: String): String {
        return "$threeWeeksAgo - $today"
    }

    fun changeDateFormat(date: String, oldFormat: String, newFormat: String): String? {
        val input = SimpleDateFormat(oldFormat, Locale.getDefault())
        val output = SimpleDateFormat(newFormat, Locale.getDefault())
        try {
            val oneWayTripDate = input.parse(date)
            return output.format(oneWayTripDate)
        } catch (e: Exception) {
        }

        return null
    }

    fun convertDateFormat(dateValue: String, oldPattern: String, newPattern: String): String {
        if (!TextUtils.isEmpty(dateValue)) {
            val odf = SimpleDateFormat(oldPattern, Locale.US)
            val ndf = SimpleDateFormat(newPattern, Locale.getDefault())
            var result = ""
            try {
                val date = odf.parse(dateValue)
                result = ndf.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return result
        } else {
            return ""
        }
    }

    fun getDate(date: String, dateFormat: String): Date? {
        try {
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault())
            return sdf.parse(date)
        } catch (e: Exception) {
        }

        return null
    }

    fun getDateString(date: Date, dateFormat: String): String? {
        try {
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault())
            return sdf.format(date)
        } catch (e: Exception) {
        }

        return null
    }
}