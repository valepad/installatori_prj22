package com.octotelematics.installatori.utils

import android.content.Context
import android.util.ArrayMap
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.DDNetworkClientTokenISAM
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.network.utils.CustomInterceptor
import okhttp3.RequestBody

object NetworkUtils {

    // parameters
    const val PARAMETER_OLD_PASSWORD = "oldPassword"
    const val PARAMETER_NEW_PASSWORD = "newPassword"

    const val PARAMETER_CLIENT_ID = "client_id"
    const val PARAMETER_CLIENT_SECRET = "client_secret"
    const val PARAMETER_GRANT_TYPE = "grant_type"
    const val PARAMETER_SCOPE = "scope"

    const val GRANT_TYPE = "password"
    const val SCOPE = "openid"

    const val PARAMETER_DEVICE_TYPE = "deviceType"
    const val PARAMETER_TOKEN = "token"

    const val PARAMETER_VOUCHER_STATUS = "voucherStatus"
    const val PARAMETER_GATEWAY_SCOPES = "gatewayScopes"
    const val PARAMETER_ID_TOKEN = "idToken"
    const val PARAMETER_EXPIRY = "expiry"


    // token side
    const val GRANT_TYPE_TOKEN = "client_credentials"

    interface ManageLoginInterface {
        fun loginSuccess()
        fun temporarPassword()
        fun genericError()
        fun error(title: String, message: String)
    }

    private var tempDialog: DialogUtils? = null

    fun setupDDNetworkClient(context: Context) {
        try {
            DDNetworkClient.instance.getInstance(
                context,
                BuildConfig.BASE_URL,
                "",
                BuildConfig.BASE_URL_TOKEN,
                GRANT_TYPE,
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                UnauthorizedInterceptor(context as BaseActivity, null)
            )

        } catch (e: Exception) {
        }
    }

    fun setupDDNetworkClientDashboard(context: Context) {
        try {
            DDNetworkClient.instance.getInstance(
                context,
                BuildConfig.BASE_URL,
                "",
                BuildConfig.BASE_URL_TOKEN,
                GRANT_TYPE,
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                UnauthorizedInterceptor(context as BaseActivity, "dashboard")
            )

        } catch (e: Exception) {
        }
    }

    fun setupDDNetworkClientNoUnauthorized(context: Context) {
        try {
            DDNetworkClient.instance.getInstance(
                context,
                BuildConfig.BASE_URL,
                "",
                BuildConfig.BASE_URL_TOKEN,
                GRANT_TYPE,
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                CustomInterceptor(null)
            )

        } catch (e: Exception) {
        }
    }

    fun setupDDNetworkClientToken(context: Context) {
        try {
            DDNetworkClientTokenISAM.instance.getInstance(
                context,
                BuildConfig.BASE_URL,
                "",
                BuildConfig.BASE_URL_TOKEN_ISAM,
                GRANT_TYPE_TOKEN,
                BuildConfig.CLIENT_ID_TOKEN,
                BuildConfig.CLIENT_SECRET_TOKEN
            )

        } catch (e: Exception) {
        }
    }

    fun getBodyAccessToken(): RequestBody {
        val jsonParamsToken: MutableMap<String?, Any?> =
            ArrayMap()
        jsonParamsToken[PARAMETER_CLIENT_ID] = BuildConfig.CLIENT_ID_TOKEN
        jsonParamsToken[PARAMETER_CLIENT_SECRET] = BuildConfig.CLIENT_SECRET_TOKEN
        jsonParamsToken[PARAMETER_GRANT_TYPE] = GRANT_TYPE_TOKEN
        jsonParamsToken[PARAMETER_SCOPE] = "digital-contract-service-v1.devices-app.subscriptions digital-contract-service-v1.users.create-user digital-contract-service-v1.terms-and-conditions digital-contract-service-v1.users.change-password digital-contract-service-v1.users.reset-password"
        return ConvertRequestBody.responseBodyReturn(jsonParamsToken)
    }

}