package com.octotelematics.installatori.utils

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.octotelematics.installatori.R

import kotlinx.android.synthetic.main.custom_alert_single.*


class CustomAlertSingle(val title: String?, val mess: String?,val textbutton:String?) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

      //  dialog!!.window?.setBackgroundDrawableResource(R.drawable.bg_rounded) //to give rounded corner
        return inflater.inflate(R.layout.custom_alert_single, container, false)
    }

    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.35).toInt()
        dialog!!.window?.setLayout(width, height)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val button = view.findViewById<TextView>(R.id.button_alert)
        if (title != null) {
 title_alert.text=title
        }
        if (mess != null) {
           subtitle_alert.text=mess

        }
        if(textbutton!=null){
            button_alert.text=textbutton
        }
        button.setOnClickListener {
            dismiss()
        }
    }

}