package com.octotelematics.installatori.utils.fcm;
/**
 * Created by iLabora on 10.08.2017.
 */

//public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
//
//    public MyFirebaseInstanceIDService() {
//        super();
//    }
//
//    @Override
//    public void onTokenRefresh() {
//        super.onTokenRefresh();
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//
//        // Saving reg id to shared preferences
//        storeRegId(refreshedToken);
//
//        // sending reg id to your server
////        sendRegistrationToServer(refreshedToken);
//
//        updateToken(refreshedToken);
//        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        /*Intent registrationComplete = new Intent(FirebaseConfig.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("token", refreshedToken);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);*/
//
//        FirebaseMessaging.getInstance().subscribeToTopic(FirebaseConfig.TOPIC_GLOBAL);
//    }
//
//    private void storeRegId(String token) {
//        SharedPreferences pref = getFirebasePreferences(getApplicationContext());
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString(FirebaseConfig.PREF_REG_ID, token);
//        editor.putBoolean(FirebaseConfig.PREF_SENT, false);
//        editor.apply();
//    }
//
//    public static SharedPreferences getFirebasePreferences(Context applicationContext) {
//        return applicationContext.getSharedPreferences(FirebaseConfig.SHARED_PREF, 0);
//    }
//
//    @SuppressLint("CheckResult")
//    private void updateToken(String token) {
//        OctoPreferences preferences = new OctoPreferences(getApplicationContext());
//        String auth = preferences.getAuthHeader();
//        if (auth != null && !auth.isEmpty()) {
//            PushTokenUpdateRequest ptur = new PushTokenUpdateRequest("ANDROID", token);
//            RestClient.getInstance(getApplicationContext()).updatePushToken(ptur, auth, LocaleUtils.getLanguage(getApplicationContext()))
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(() -> {
//                        if (getApplicationContext() != null) {
//                            SharedPreferences pref = getFirebasePreferences(getApplicationContext());
//                            SharedPreferences.Editor edit = pref.edit();
//                            edit.putBoolean(FirebaseConfig.PREF_SENT, true);
//                            edit.apply();
//                        }
//                    }, throwable -> { });
//        }
//    }
//}