package com.octotelematics.installatori.utils.fcm;

import android.content.Intent;

/**
 * Created by iLabora on 13.09.2017.
 */

public interface PushNotificationsInterface {

    void onReceive(Intent action);
}
