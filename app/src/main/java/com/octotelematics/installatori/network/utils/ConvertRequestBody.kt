package com.octotelematics.installatori.network.utils

import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject




class ConvertRequestBody{
    companion object{
        fun responseBodyReturn(response: Any): RequestBody{
            var gson: Gson = Gson()
            var json = gson.toJson(response)
            var body: RequestBody = RequestBody.create(
                "application/json; charset=utf-8".toMediaTypeOrNull(),
                JSONObject(json).toString()
            )
            return body
        }
    }
}