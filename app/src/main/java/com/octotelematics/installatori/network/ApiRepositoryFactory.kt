package com.octotelematics.installatori.network

import APIRepository
import android.content.Context
import com.octotelematics.installatori.BuildConfig

import it.apps.androidpoc.repository.ApiRepositoryMocked
import java.security.AccessControlContext

/** Factory per il repository in grado per gestire al meglio anche l'ambiente Mock
 *
 */

class ApiRepositoryFactory(val context: Context) {

    private val repositoryImpl = APIRepository(context)
    private val repositoryMocked = ApiRepositoryMocked(context)

    //TODO MOCKED DATA
    fun getRepository() = if (true)
        repositoryMocked
    else
        repositoryImpl
}