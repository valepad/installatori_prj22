package com.octotelematics.installatori.network

import com.google.gson.annotations.SerializedName

data class ResponseToken(
    @SerializedName("access_token")
    var access_token: String,

    @SerializedName("token_type")
    var token_type: String,

    @SerializedName("expires_in")
    var expires_in: Int,

    @SerializedName("id_token")
    var id_token: String,

    @SerializedName("accessToken")
    var accessToken: String
)