package com.octotelematics.installatori.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface APIServiceTokenISAM {
    /*@Headers("TSC_ID: LVM")
    @POST("exchangeIDToken")
    fun loginExchangeToken(
        @Header("Authorization") token: String,
        @Body body: RequestBody
    ) : Call<ResponseBody>*/

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("token")
    fun loginISAM(
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("grant_type") grant_type: String,
        @Field("scope") scope: String,
        @Field("username") username: String,
        @Field("password") password: String
    ) : Call<ResponseBody>
}