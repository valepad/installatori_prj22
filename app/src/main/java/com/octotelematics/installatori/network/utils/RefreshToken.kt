package com.octotelematics.installatori.network.utils

import android.util.Log
import com.google.gson.Gson
import com.octotelematics.installatori.network.APIServiceToken
import com.octotelematics.installatori.network.DDNetworkClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RefreshToken {

    var token: String? = null

    companion object {
        val instance = RefreshToken()
    }

    /*fun returnToken() : String?{
        return token
    }*/

    fun refreshToken(returnToken: ReturnToken) {
        val service : APIServiceToken? = DDNetworkClient.instance.apiServiceToken

        service?.let {
            var call: Call<ResponseBody> = it.getToken(
                DDNetworkClient.grantType,
                DDNetworkClient.clientId,
                DDNetworkClient.clientSecret
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ){
                    val statusCode = response.code()
                    val customResponse = NetworkUtis.manageResponse(response)
                    val responseFormat =
                        Gson().fromJson(customResponse.body.toString(), com.octotelematics.installatori.network.ResponseToken::class.java)
                    Log.e("--->Token", responseFormat.access_token)
                    Log.e("--->Type", responseFormat.token_type)
                    Log.e("--->Expire", responseFormat.expires_in.toString())
                    Log.e("---Code", statusCode.toString())
                    token = responseFormat.token_type + " " + responseFormat.access_token
                    returnToken.returnToken(token!!)
                }

                override fun onFailure(
                    call: Call<ResponseBody>,
                    t: Throwable?
                ) { // Log error here since request failed
                }
            })
        }
    }
}