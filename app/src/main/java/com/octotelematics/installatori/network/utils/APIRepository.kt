import com.octotelematics.installatori.network.utils.RetryableCallback

import android.content.Context
import android.util.Log
import com.octotelematics.installatori.network.DDNetworkClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

open class APIRepository(context: Context) {

    var context = context
    fun <T> enqueueWithRetry(call: Call<T>, callback: Callback<T>) {
        call.enqueue(object : RetryableCallback<T>(call, context) {
            override fun onFinalResponse(call: Call<T>, response: Response<T>) {
                Log.e("APIHelper", "reached onFinalResponse")
                callback.onResponse(call, response)
            }

            override fun onFinalFailure(call: Call<T>, t: Throwable) {
                Log.e("APIHelper", "reached onFinalFailure")
                callback.onFailure(call, t)
            }
        })
    }

    //DD2.0 (NGP SERVICES)
    fun loginISAMToken(
        client_id: String,
        client_secret: String,
        grant_type: String,
        scope: String,
        username: String,
        password: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiServiceTokenISAM!!.loginISAM(
                client_id,
                client_secret,
                grant_type,
                scope,
                username,
                password
            )
        enqueueWithRetry(call, callback)
    }

    fun loginAPIGWExchangeToken(
        headerToken: String,
        tscId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.loginExchangeToken(
                headerToken,
                tscId,
                body
            )
        enqueueWithRetry(call, callback)
    }
    fun setBcall(
        deviceId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setBcall(deviceId, body)
        enqueueWithRetry(call, callback)
    }

    fun getVouchers(
        tscId: String,
        lang: String,
        template: String,
        email: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getVouchers(
                tscId,
                lang,
                template,
                email
            )
        enqueueWithRetry(call, callback)
    }

    fun getCrashes(
        tscId: String,
        lang: String,
        voucherId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getCrashes(
                tscId,
                lang,
                voucherId
            )
        enqueueWithRetry(call, callback)
    }

    fun getCrashes(
        tscId: String,
        lang: String,
        voucherId: String?,
        valid: Boolean?,
        type: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getCrashes(
                tscId,
                lang,
                voucherId,
                valid,
                type
            )
        enqueueWithRetry(call, callback)
    }

    fun getCrashesPosition(
        tscId: String,
        lang: String,
        crashId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getCrashesDetails(
                tscId,
                lang,
                crashId
            )
        enqueueWithRetry(call, callback)
    }

    fun getScoringsOverall(
        voucherId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getScoringsOverall(
                voucherId
            )
        enqueueWithRetry(call, callback)
    }

    fun getTripEventScores(
        tripId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getTripEventScores(
                tripId
            )
        enqueueWithRetry(call, callback)
    }

    fun getCarFinderPosition(
        deviceId: String?,
        lastPosition:Boolean?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getCarFinderPosition(
                deviceId,lastPosition
            )
        enqueueWithRetry(call, callback)
    }


    fun getTargetArea(
        deviceId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getTargetArea(
                deviceId
            )
        enqueueWithRetry(call, callback)
    }

    fun getSpeedLimit(
        deviceId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getSpeedLimit(
                deviceId
            )
        enqueueWithRetry(call, callback)
    }

    fun setTargetArea(
        id: Int,
        deviceId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setTargetArea(id, deviceId, body)
        enqueueWithRetry(call, callback)
    }
    fun setFeedback(
        lang: String,
        tscId: String?,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setFeedback(lang, tscId, body)
        enqueueWithRetry(call, callback)
    }

    fun setTargetAreaFirstTime(
        id: Int,
        deviceId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setTargetAreaFirsTime(id, deviceId, body)
        enqueueWithRetry(call, callback)
    }

    fun setSpeedLimit(
        deviceId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setSpeedLimit(deviceId, body)
        enqueueWithRetry(call, callback)
    }

    fun setSpeedLimitFirstTime(
        deviceId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.setSpeedLimitFirstTime(deviceId, body)
        enqueueWithRetry(call, callback)
    }


    fun getCodeSdk(
        deviceId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getCodeSdk(
                deviceId
            )
        enqueueWithRetry(call, callback)
    }

    // GAMIFICATION
    fun getBadgeByTSC(
        lang: String,
        tscId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getBadgeByTSC(
                lang,
                tscId
            )
        enqueueWithRetry(call, callback)
    }

    fun getBadgesAchieved(
        tscId: String,
        lang: String,
        voucherID: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getBadgesAchieved(
                tscId,
                lang,
                voucherID
            )
        enqueueWithRetry(call, callback)
    }

    fun getPlayerLevels(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPlayerLevels(
                tscId,
                lang
            )
        enqueueWithRetry(call, callback)
    }

    fun getAvatars(
        tscId: String,
        lang: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
        DDNetworkClient.instance.apiService!!.getAvatars(
            tscId,
            lang,
            tscId
        )
        enqueueWithRetry(call, callback)
    }


    // ------ PLAYERS ---------
    fun getPlayer(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPlayer(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun updatePlayer(
        tscId: String,
        lang: String,
        userId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.updatePlayer(
                tscId,
                lang,
                userId,
                body
            )
        enqueueWithRetry(call, callback)
    }

    fun createPlayer(
        tscId: String,
        lang: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.createPlayer(
                tscId,
                lang,
                body
            )
        enqueueWithRetry(call, callback)
    }

    fun findPlayerByUsername(
        tscId: String,
        lang: String,
        username: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.findPlayerByUsername(
                tscId,
                lang,
                username
            )
        enqueueWithRetry(call, callback)
    }

    // ------ CHALLENGES ---------

    fun getChallengeTypes(
        tscId: String,
        lang: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengeTypes(
                tscId,
                lang
                )
        enqueueWithRetry(call, callback)
    }

    fun getRankingGlobal(
        tscId: String,
        lang: String,
        page: Int,
        size: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getRankingGlobal(
                tscId,
                lang,
                page,
                size
            )
        enqueueWithRetry(call, callback)
    }

    fun getPersonalRankingGlobal(
        tscId: String,
        lang: String,
        page: Int,
        size: Int,
        position: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPersonalRankingGlobal(
                tscId,
                lang,
                page,
                size,
                position
            )
        enqueueWithRetry(call, callback)
    }

    fun getRankingFriends(
        tscId: String,
        lang: String,
        userId: String,
        page: Int,
        size: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getRankingFriends(
                tscId,
                lang,
                userId,
                page,
                size
            )
        enqueueWithRetry(call, callback)
    }

    fun getPersonalRankingFriends(
        tscId: String,
        lang: String,
        userId: String,
        page: Int,
        size: Int,
        position: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPersonalRankingFriends(
                tscId,
                lang,
                userId,
                page,
                size,
                position
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengesByUserId(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengesByUserId(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun createChallenge(
        tscId: String,
        lang: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.createChallenge(
                tscId,
                lang,
                body
            )
        enqueueWithRetry(call, callback)
    }

    fun acceptChallenge(
        tscId: String,
        lang: String,
        challengeId: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.acceptChallenge(
                tscId,
                lang,
                challengeId
            )
        enqueueWithRetry(call, callback)
    }

    fun rejectChallenge(
        tscId: String,
        lang: String,
        challengeId: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.rejectChallenge(
                tscId,
                lang,
                challengeId
            )
        enqueueWithRetry(call, callback)
    }

    fun retireChallenge(
        tscId: String,
        lang: String,
        challengeId: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.retireChallenge(
                tscId,
                lang,
                challengeId
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengeOngoing(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengeOngoing(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengePending(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengePending(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengeSent(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengeSent(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengeReceived(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengeReceived(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getChallengeFinish(
        tscId: String,
        lang: String,
        userId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getChallengeFinish(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    // ------ FRIENDS ---------
    fun getFriendsList(
        tscId: String,
        lang: String,
        userId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getFriendsList(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getSentFriendsList(
        tscId: String,
        lang: String,
        userId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getSentFriendsList(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun getPendingFriendsList(
        tscId: String,
        lang: String,
        userId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPendingFriendsList(
                tscId,
                lang,
                userId
            )
        enqueueWithRetry(call, callback)
    }

    fun acceptFriendship(
        tscId: String,
        lang: String,
        friendshipId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.acceptFriendship(
                tscId,
                lang,
                friendshipId
            )
        enqueueWithRetry(call, callback)
    }

    fun rejectFriendship(
        tscId: String,
        lang: String,
        friendshipId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.rejectFriendship(
                tscId,
                lang,
                friendshipId
            )
        enqueueWithRetry(call, callback)
    }

    fun sendFriendship(
        tscId: String,
        lang: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.sendFriendship(
                tscId,
                lang,
                body
            )
        enqueueWithRetry(call, callback)
    }


    // ACCESS TOKEN
    fun getAccessToken(
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getAccessTokenRegistration(body)
        enqueueWithRetry(call, callback)
    }

    fun getPackageList(
        tscId: String,
        template: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPackageList(
                tscId,
                template
            )
        enqueueWithRetry(call, callback)
    }

    // VALEO API

    // OVERALL SCORE
    fun getOverallScore(voucherId: String, callback: Callback<ResponseBody>) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getOverallScore(voucherId)
        enqueueWithRetry(call, callback)
    }

    fun getOverallScore(
        from: String,
        to: String,
        endUserContractId: String,
        deviceId: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getOverallScore(
            from,
            to,
            endUserContractId,
            deviceId
        )
        enqueueWithRetry(call, callback)
    }

    // SCORING

    fun getScoringsByMonitoredObjId(
        monitoredObjId: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getScoringsByMonitoredObjId(
                monitoredObjId
            )
        enqueueWithRetry(call, callback)
    }

    fun getScorings(
        voucherId: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getScorings(voucherId)
        enqueueWithRetry(call, callback)
    }

    fun getScorings(
        voucherId: String, type: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getScorings(voucherId, type)
        enqueueWithRetry(call, callback)
    }

    fun getScorings(
        deviceId: String,
        from: String,
        to: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getScorings(
            deviceId,
            from,
            to
        )
        enqueueWithRetry(call, callback)
    }

    fun getScorings(
        endUserId: String,
        voucherId: String,
        monitoredObjId: String,
        deviceId: String,
        type: String,
        limit: String,
        from: Date,
        to: Date, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getScorings(
            endUserId,
            voucherId,
            monitoredObjId,
            deviceId,
            type,
            limit,
            from,
            to
        )
        enqueueWithRetry(call, callback)
    }

    // TRIPS

    fun getTrip(
        tripId: String,
        pathParam: String,
        voucherId: String,
        endUserId: String,
        monitoredObjId: String,
        deviceId: String,
        from: Date,
        to: Date,
        overKey: String,
        reverse: String,
        pageSize: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTrips(
            tripId,
            pathParam,
            voucherId,
            endUserId,
            monitoredObjId,
            deviceId,
            from,
            to,
            overKey,
            reverse,
            pageSize
        )
        enqueueWithRetry(call, callback)
    }

    fun getTrip(
        tripId: String,
        pathParam: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTrips(
            tripId,
            pathParam
        )
        enqueueWithRetry(call, callback)
    }

    fun getTripPositions(
        tripId: String,
        withPoints: Int,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTripPositions(
            tripId,
            withPoints
        )
        enqueueWithRetry(call, callback)
    }

    fun getTripEvents(
        tripId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTripEvents(
            tripId
        )
        enqueueWithRetry(call, callback)
    }

    fun updateTripStatus(
        tripId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.updateTripStatus(
            tripId,
            body
        )
        enqueueWithRetry(call, callback)
    }

    fun getTripWithQueryParam(
        voucherId: String?,
        endUserId: String?,
        monitoredObjId: String?,
        deviceId: String?,
        from: String?,
        to: String?,
        overKey: String?,
        reverse: String?,
        valid: Boolean?,
        pageSize: Int?, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getTripsWithQueryParam(
                voucherId,
                endUserId,
                monitoredObjId,
                deviceId,
                from,
                to,
                overKey,
                reverse,
                valid,
                pageSize
            )
        enqueueWithRetry(call, callback)
    }

    fun getTripScorings(
        tripId: String,
        endUserId: String,
        deviceId: String,
        referenceDate: String,
        from: Date,
        to: Date,
        limit: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTripScorings(
            tripId,
            endUserId,
            deviceId,
            referenceDate,
            from,
            to,
            limit
        )
        enqueueWithRetry(call, callback)
    }

    // TRENDS

    fun getTrends(
        voucherId: String?,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getTrends(
            voucherId
        )
        enqueueWithRetry(call, callback)
    }

    fun getWeeklyTrends(
        voucherId: String,
        type: String,
        from: String?,
        to: String?, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getWeeklyTrends(
            voucherId,
            type,
            from,
            to
        )
        enqueueWithRetry(call, callback)
    }

    fun getBehaviourTrends(

        type: String,
        from: String?,
        to: String?, endUserId: String, callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getBehaviourTrends(

            type,
            from,
            to,
            endUserId
        )
        enqueueWithRetry(call, callback)
    }

    // USERS
    fun createUser(
        headerAuthorization: String,
        tscId: String,
        bodyUser: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.createUser(
            headerAuthorization,
            tscId,
            bodyUser
        )
        enqueueWithRetry(call, callback)
    }

    fun createUser(
        headerAuthorization: String,
        tscId: String,
        e2EtrackingId: String,
        eventId: String,
        creationTime: String,
        bodyUser: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.createUser(
            headerAuthorization,
            tscId,
            e2EtrackingId,
            eventId,
            creationTime,
            bodyUser
        )
        enqueueWithRetry(call, callback)
    }

    fun changeTemporaryPsw(
        authorization: String,
        tscId: String,
        email: String?,
        bodyEmailUser: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.changeTemporaryPsw(
            authorization,
            tscId,
            email,
            bodyEmailUser
        )
        enqueueWithRetry(call, callback)
    }

    fun changeTemporaryPsw(
        authorization: String,
        tscId: String,
        email: String?,
        e2EtrackingId: String,
        eventId: String,
        creationTime: String,
        bodyEmailUser: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.changeTemporaryPsw(
            authorization,
            tscId,
            email,
            e2EtrackingId,
            eventId,
            creationTime,
            bodyEmailUser
        )
        enqueueWithRetry(call, callback)
    }

    fun getProfile(
        email: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.getProfile(
            email
        )
        enqueueWithRetry(call, callback)
    }

    fun accessPushToken(
        tscId: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.accessTokenPush(
            tscId,
            body
        )
        enqueueWithRetry(call, callback)
    }

    fun deletePushtoken(
        tscId: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.deleteTokenPush(
            tscId
        )
        enqueueWithRetry(call, callback)
    }

    fun setUserStatus(
        status: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.setUserStatus(
            status
        )
        enqueueWithRetry(call, callback)
    }

    fun resetPassword(
        authorization: String,
        tscId: String,
        email: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.resetPassword(
            authorization,
            tscId,
            email
        )
        enqueueWithRetry(call, callback)
    }

    fun resetPassword(
        authorization: String,
        tscId: String,
        email: String,
        e2EtrackingId: String,
        eventId: String,
        creationTime: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.resetPassword(
            authorization,
            tscId,
            email,
            e2EtrackingId,
            eventId,
            creationTime
        )
        enqueueWithRetry(call, callback)
    }

    fun voucherAll(
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.vouchersAll()
        enqueueWithRetry(call, callback)
    }

    fun voucherBulkRequest(
        tscId: String,
        email: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.voucherBulkRequest(
            tscId,
            email,
            body
        )
        enqueueWithRetry(call, callback)
    }

    fun getUserVouchersByMail(
        email: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getUserVouchersByMail(
                email
            )
        enqueueWithRetry(call, callback)
    }

    fun getUserVouchersByMailAndTSC(
        tscId: String,
        email: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getUserVouchersByMailAndTSC(
                tscId,
                email
            )
        enqueueWithRetry(call, callback)
    }

    fun getTec(
        tscId: String,
        lang: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getTeC(
                tscId,
                lang
            )
        enqueueWithRetry(call, callback)
    }
    fun getFAQ(
        tscId: String,
        lang: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getFAQ(
                tscId,
                lang
            )
        enqueueWithRetry(call, callback)
    }

    fun getPrivacypolicy(
        tscId: String,
        lang: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.getPrivacypolicy(
                tscId,
                lang
            )
        enqueueWithRetry(call, callback)
    }

    fun updateBulk(
        tscId: String,
        voucherFk: String,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.updateBulk(
            tscId,
            voucherFk,
            body
        )
        enqueueWithRetry(call, callback)
    }

    fun unsubscribe(
        tscId: String,
        voucherFk: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.unsubscribe(
            tscId,
            voucherFk
        )
        enqueueWithRetry(call, callback)
    }

    fun createVoucher(
        appAsUi: String,
        device: String,
        deviceConfiguration: String,
        deviceStatus: String,
        idContractType: String,
        messageBatchSize: String,
        ngpDeviceId: String,
        ngpUserId: String,
        ngpUserStatus: String,
        sensors: List<Any>,
        servicesList: List<String>,
        ssnnin: String,
        voucherStatus: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.createVoucher(
            appAsUi,
            device,
            deviceConfiguration,
            deviceStatus,
            idContractType,
            messageBatchSize,
            ngpDeviceId,
            ngpUserId,
            ngpUserStatus,
            sensors,
            servicesList,
            ssnnin,
            voucherStatus
        )
        enqueueWithRetry(call, callback)
    }

    fun updateVoucher(
        id: String,
        appAsUi: String,
        device: String,
        deviceConfiguration: String,
        deviceStatus: String,
        idContractType: String,
        messageBatchSize: String,
        ngpDeviceId: String,
        ngpUserId: String,
        ngpUserStatus: String,
        sensors: List<Any>,
        servicesList: List<String>,
        ssnnin: String,
        voucherStatus: String,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> = DDNetworkClient.instance.apiService!!.updateVoucher(
            id,
            appAsUi,
            device,
            deviceConfiguration,
            deviceStatus,
            idContractType,
            messageBatchSize,
            ngpDeviceId,
            ngpUserId,
            ngpUserStatus,
            sensors,
            servicesList,
            ssnnin,
            voucherStatus
        )
        enqueueWithRetry(call, callback)
    }

    // GENERIC METHODS
    fun genericGet(
        url: String?,
        queries: Map<String, String>,
        headers: Map<String, String>,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.genericGet(url, queries, headers)
        enqueueWithRetry(call, callback)
    }

    fun genericPost(
        url: String?,
        queries: Map<String, String>,
        headers: Map<String, String>,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.genericPost(url, queries, headers, body)
        enqueueWithRetry(call, callback)
    }

    fun genericPut(
        url: String?,
        queries: Map<String, String>,
        headers: Map<String, String>,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.genericPut(url, queries, headers, body)
        enqueueWithRetry(call, callback)
    }

    fun genericDelete(
        url: String?,
        queries: Map<String, String>,
        headers: Map<String, String>,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.genericDelete(url, queries, headers, body)
        enqueueWithRetry(call, callback)
    }

    fun genericPatch(
        url: String?,
        queries: Map<String, String>,
        headers: Map<String, String>,
        body: RequestBody,
        callback: Callback<ResponseBody>
    ) {
        var call: Call<ResponseBody> =
            DDNetworkClient.instance.apiService!!.genericPatch(url, queries, headers, body)
        enqueueWithRetry(call, callback)
    }
}


/*
class APIRepository(private val context: Context, private val apiInterface: APIService) :
    BaseRepository() {

    // VALEO API

    // OVERALL SCORE
    fun getOverallScore(authCode: String, voucherId: String): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getOverallScore(authCode, voucherId) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getOverallScore(
        authCode: String,
        from: String,
        to: String,
        endUserContractId: String,
        deviceId: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = {
                apiInterface.getOverallScore(
                    authCode,
                    from,
                    to,
                    endUserContractId,
                    deviceId
                )
            },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    // SCORING
    suspend fun getScoringsByMonitoredObjId(
        authCode: String,
        monitoredObjId: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getScoringsByMonitoredObjId(authCode, monitoredObjId) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getScorings(authCode: String, voucherId: String): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getScorings(authCode, voucherId) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getScorings(
        authCode: String,
        voucherId: String,
        type: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getScorings(authCode, voucherId, type) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getScorings(
        authCode: String,
        deviceId: String,
        from: String,
        to: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getScorings(authCode, deviceId, from, to) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getScorings(
        authCode: String,
        endUserId: String,
        voucherId: String,
        monitoredObjId: String,
        deviceId: String,
        type: String,
        limit: String,
        from: Date,
        to: Date
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = {
                apiInterface.getScorings(
                    authCode,
                    endUserId,
                    voucherId,
                    monitoredObjId,
                    deviceId,
                    type,
                    limit,
                    from,
                    to
                )
            },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    // TRIPS
    suspend fun getTrip(
        tripId: String,
        pathParam: String,
        authCode: String,
        voucherId: String,
        endUserId: String,
        monitoredObjId: String,
        deviceId: String,
        from: Date,
        to: Date,
        overKey: String,
        reverse: String,
        pageSize: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = {
                apiInterface.getTrips(
                    tripId,
                    pathParam,
                    authCode,
                    voucherId,
                    endUserId,
                    monitoredObjId,
                    deviceId,
                    from,
                    to,
                    overKey,
                    reverse,
                    pageSize
                )
            },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }


    suspend fun getTrip(
        tripId: String,
        pathParam: String,
        authCode: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getTrips(tripId, pathParam, authCode) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getTripWithQueryParam(
        authCode: String,
        voucherId: String,
        endUserId: String,
        monitoredObjId: String,
        deviceId: String,
        from: Date,
        to: Date,
        overKey: String,
        reverse: String,
        pageSize: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = {
                apiInterface.getTripsWithQueryParam(
                    authCode,
                    voucherId,
                    endUserId,
                    monitoredObjId,
                    deviceId,
                    from,
                    to,
                    overKey,
                    reverse,
                    pageSize
                )
            },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun getTripScorings(
        authCode: String,
        tripId: String,
        endUserId: String,
        deviceId: String,
        referenceDate: String,
        from: Date,
        to: Date,
        limit: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = {
                apiInterface.getTripScorings(
                    authCode,
                    tripId,
                    endUserId,
                    deviceId,
                    referenceDate,
                    from,
                    to,
                    limit
                )
            },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    // TRENDS
    suspend fun getTrends(
        authCode: String,
        voucherId: String,
        type: String
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.getTrends(authCode, voucherId, type) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }


    // GENERIC METHODS
    suspend fun genericGet(
        url: String?,
        headers: Map<String, String>,
        queries: Map<String, String>
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.genericGet(url, headers, queries) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun genericPost(
        url: String?,
        headers: Map<String, String>,
        queries: Map<String, String>,
        body: RequestBody
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.genericPost(url, headers, queries, body) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun genericPut(
        url: String?,
        headers: Map<String, String>,
        queries: Map<String, String>,
        body: RequestBody
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.genericPut(url, headers, queries, body) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }

    suspend fun genericDelete(
        url: String?,
        headers: Map<String, String>,
        queries: Map<String, String>,
        body: RequestBody
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.genericDelete(url, headers, queries, body) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }


    suspend fun genericPatch(
        url: String?,
        headers: Map<String, String>,
        queries: Map<String, String>,
        body: RequestBody
    ): Response<ResponseBody>? {
        return safeApiCall(
            //await the result of deferred type
            call = { apiInterface.genericPatch(url, headers, queries, body) },
            error = context.getString(R.string.generic_error)
            //convert to mutable list
        )
    }
}
*/