package com.octotelematics.installatori.network.utils

import com.octotelematics.installatori.network.DDNetworkClient
import okhttp3.Interceptor
import okhttp3.Response

class CustomInterceptor(type: String?) : Interceptor {

    //private var mContext: Context = context
    private var type: String? = type

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .build()

        val okHttpVersion = System.getProperty("http.agent")
        var appVersion = "0.0.0"

        /*try {
            val pInfo = mContext.packageManager.getPackageInfo(mContext.packageName, 0)
            appVersion = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("version", e.message)
        }*/

        //String userAgent = MainApplication.getInstance(context).getPreferences().getAnything("userAgent") + okHttpVersion;

        val requestBuilder = original.newBuilder()
            .header("Content-Type", "application/json")
            .header("X-Platform", "Android")
            //.header("X-App-Name", appName)
            .header("User-Agent", okHttpVersion!!)
            .header("X-App-Version", appVersion)
            .header("Accept", "*/*")
            .url(url)

        if (DDNetworkClient.token != null && !url.toUrl().toString().contains("getAccessToken") && type == "dashboard"){
            requestBuilder.addHeader("Authorization", "Bearer " + DDNetworkClient.token!!)
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}