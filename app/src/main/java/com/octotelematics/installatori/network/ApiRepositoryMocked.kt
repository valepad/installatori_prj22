package it.apps.androidpoc.repository

import APIRepository
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

/** La classe che rappresenta il repository con chiamate Mockate
 * tutti i file mock sono nella cartella "assets/mock"
 */
class ApiRepositoryMocked(context: Context) : APIRepository(context) {


 //  override suspend fun callLogin(callback: NetworkCallback<LoginResponse>) = withContext(Dispatchers.IO) {
 //      val jsonString = AssetsUtils.getJsonFromAssets("mock/login.json")
 //      val bodyType = object : TypeToken<ListaUtentiResponse>() {}.type
 //      val body = Gson().fromJson<LoginResponse>(jsonString, bodyType)
 //      Timer().schedule(object : TimerTask() {
 //          override fun run() {
 //              callback.onSuccess(body)
 //          }
 //      }, 3000)
 //      return@withContext
 //  }



}