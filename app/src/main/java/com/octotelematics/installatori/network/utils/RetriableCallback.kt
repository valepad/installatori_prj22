package com.octotelematics.installatori.network.utils

import android.content.Context
import android.widget.Toast
import retrofit2.Callback;
import retrofit2.Call;
import retrofit2.Response;

abstract class RetryableCallback<T>(call : Call<T>,context: Context) : Callback<T> {

    val TAG : String? = "RetryableCallback::class.simpleName"
    var call : Call<T> = call
    var context = context

    var attempts = 0

    override fun onResponse(call : Call<T>, response: Response<T> ) {
        if (!NetworkUtis.isCallSuccess(response)) {
            /*if (response.code() == 401) {
                Log.e("RetryableCallback", "401, NOT AUTHENTICATED")
                try{
                    // fa la chiamata per generare un nuovo token
                    RefreshToken.instance.refreshToken(object: ReturnToken {
                        override fun returnToken(token: String) {
                            // setto il token
                            DDNetworkClient.token = token
                            // ripete la chiamata
                            retryCall()
                        }

                    })

                } catch (e: Exception) {
                    // do something with the exception
                }
            }*/
            onFinalResponse(call, response)
        } else {
            onFinalResponse(call, response) // no need to do any retry, pass the response and the call to the final callback
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        /*Log.e("RetryableCallback", t.message)
        if (t.message != null && t.message!!.contains("Server", false)) { // if error contains some keyword, retry the request as well. This is just an example to show you can call retry from either success or failure.
            retryCall()
        } else*/
        onFinalFailure(call, t) // if not, finish the call as a failure
    }

    abstract fun onFinalResponse(call: Call<T>, response: Response<T>)

    abstract fun onFinalFailure(call: Call<T>, t: Throwable)

    fun retryCall() {
        attempts++
        if (attempts >= 3) {
            call.clone().cancel()
            Toast.makeText(context, "NOT AUTHENTICATED", Toast.LENGTH_LONG)
        } else {
            call.clone().enqueue(this) // clone the original call and enqueue it for retry
        }
    }

}

