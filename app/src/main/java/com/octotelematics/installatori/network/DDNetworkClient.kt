package com.octotelematics.installatori.network

import android.content.Context
import com.octotelematics.installatori.BuildConfig
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory


import okhttp3.Interceptor
import okhttp3.OkHttpClient.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class DDNetworkClient {
    var apiService: APIService? = null
    var apiServiceToken: APIServiceToken? = null
    var apiServiceTokenISAM: APIServiceTokenISAM? = null

    companion object {
        val instance = DDNetworkClient()

        lateinit var context: Context
        lateinit var baseUrl: String
        lateinit var appName: String
        lateinit var baseUrlToken: String

        var token: String? = null

        var clientId : String = ""
        var clientSecret: String = ""
        var grantType: String = ""
        lateinit var interceptioObj: Interceptor

    }

    fun resetAPIService(){
        apiService = null
    }

    fun getInstance(context: Context, baseUrl: String, appName: String, baseUrlToken: String, grantType: String ,clientId: String, clientSecret: String, interceptioObj: Interceptor): APIService? {
        if (apiService == null)
            RestClient(context, baseUrl, appName, baseUrlToken,grantType, clientId, clientSecret, interceptioObj)
        return apiService
    }

    fun RestClient(context: Context, baseUrl: String, appName: String,baseUrlToken: String,grantType: String, clientId: String, clientSecret: String, interceptioObj: Interceptor) {

        Companion.context = context
        Companion.baseUrl = baseUrl
        Companion.appName = appName
        Companion.baseUrlToken = baseUrlToken

        Companion.clientId = clientId
        Companion.clientSecret = clientSecret
        Companion.grantType = grantType

        val okHttpClientBuilder = Builder()

        val loggingInterceptor = HttpLoggingInterceptor()

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        okHttpClientBuilder
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(45, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                /* val original = chain.request()
                 val originalHttpUrl = original.url

                 val url = originalHttpUrl.newBuilder()
                     .build()

                 val okHttpVersion = System.getProperty("http.agent")
                 var appVersion = "0.0.0"

                 try {
                     val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                     appVersion = pInfo.versionName
                 } catch (e: PackageManager.NameNotFoundException) {
                     Log.e("version", e.message)
                 }

                 //String userAgent = MainApplication.getInstance(context).getPreferences().getAnything("userAgent") + okHttpVersion;

                 val requestBuilder = original.newBuilder()
                     .header("Content-Type", "application/json")
                     .header("X-Platform", "Android")
                     //.header("X-App-Name", appName)
                     .header("User-Agent", okHttpVersion!!)
                     .header("X-App-Version", appVersion)*/
                // .header("Accept", "*/*")
                /* .url(url)

              if (token != null && !url.toUrl().toString().contains("getAccessToken")){
                  requestBuilder.addHeader("Authorization", "Bearer " + token!!)
              }

              val request = requestBuilder.build()
              chain.proceed(request)*/
                interceptioObj.intercept(chain)
            }
        // we are creating a networking client using OkHttp and add our authInterceptor.

        //Only Debug HttpInterceptor
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(loggingInterceptor)
            okHttpClientBuilder.addNetworkInterceptor(StethoInterceptor())
        }

        fun getRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(okHttpClientBuilder.build())
                .build()
        }

        fun getRetrofitTokenISAM(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrlToken)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(okHttpClientBuilder.build())
                .build()
        }

        apiServiceTokenISAM = getRetrofitTokenISAM().create(APIServiceTokenISAM::class.java)

        apiService = getRetrofit().create(APIService::class.java)

        apiServiceToken = getRetrofit().create(APIServiceToken::class.java)
    }

}
