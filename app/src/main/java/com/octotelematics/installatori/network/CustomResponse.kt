package com.octotelematics.installatori.network

import org.json.JSONObject

data class CustomResponse (
    var code : Int,
    var body : JSONObject
)