package com.octotelematics.installatori.network.utils

interface ReturnToken {
     fun returnToken(token:String)
}