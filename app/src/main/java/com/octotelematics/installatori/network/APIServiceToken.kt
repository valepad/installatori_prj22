package com.octotelematics.installatori.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface APIServiceToken {
    @FormUrlEncoded
    @POST("getAccessToken")
    fun getToken(
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id : String,
        @Field("client_secret") client_secret: String
    ) : Call<ResponseBody>
}