package com.octotelematics.installatori.network

import org.json.JSONArray

data class CustomResponseArray (
    var code : Int,
    var body : JSONArray
)