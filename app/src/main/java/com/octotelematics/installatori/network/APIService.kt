package com.octotelematics.installatori.network

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface APIService {


    //DD2.0 REQUEST
    @POST("gateway/security/exchangeIDToken")
    fun loginExchangeToken(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @GET("gateway/DigitalContractService/api/vouchers/getUserVouchersByMailAndTSC/{userEmail}")
    fun getVouchers(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Header("template") template: String,
        @Path("userEmail", encoded = false) userEmail: String
    ): Call<ResponseBody>

    //CRASHES
    @GET("gateway/CrashService/2.0/crashes/")
    fun getCrashes(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Query("voucherId") voucherId: String?
    ): Call<ResponseBody>

    @GET("gateway/CrashService/2.0/crashes/")
    fun getCrashes(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Query("voucherId") voucherId: String?,
        @Query("valid") valid: Boolean?,
        @Query("type") type: String?
    ): Call<ResponseBody>

    @GET("gateway/CrashService/2.0/crashes/{crashId}")
    fun getCrashesDetails(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("crashId") crashId: String
    ): Call<ResponseBody>


    @GET("gateway/UbiServices/2.0/overall/scores")
    fun getScoringsOverall(
        @Query("voucherId") voucherId: String?
    ): Call<ResponseBody>

    @GET("gateway/IotServices/2.0/positions")
    fun getCarFinderPosition(
        @Query("deviceId") deviceId: String?,
        @Query("lastPosition") lastPosition: Boolean?
    ): Call<ResponseBody>


    @GET("gateway/UbiServices/2.0/trips/{tripId}/scores")
    fun getTripEventScores(
        @Path("tripId") tripId: String?
    ): Call<ResponseBody>

    @GET("gateway/VASServices/1.0/device/{{deviceId}}/targetArea")
    fun getTargetArea(
        @Path("deviceId") deviceId: String?
    ): Call<ResponseBody>


    @GET("gateway/VASServices/1.0/device/{{deviceId}}/speedLimit")
    fun getSpeedLimit(
        @Path("deviceId") deviceId: String?
    ): Call<ResponseBody>

    @PUT("gateway/VASServices/1.0/device/{{deviceId}}/targetArea/{{id}}")
    fun setTargetArea(
        @Path("id") id: Int,
        @Path("deviceId") deviceId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

  @POST("gateway/DigitalContractService/api/feedback")
    fun setFeedback(
      @Header("lang") lang: String,
      @Header("tscID") tscID: String?,
      @Body body: RequestBody
    ): Call<ResponseBody>


    @POST("gateway/VASServices/1.0/device/{{deviceId}}/targetArea/{{id}}")
    fun setTargetAreaFirsTime(
        @Path("id") id: Int,
        @Path("deviceId") deviceId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>


    @PUT("gateway/VASServices/1.0/device/{{deviceId}}/speedLimit")
    fun setSpeedLimit(
        @Path("deviceId") deviceId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @POST("gateway/VASServices/1.0/device/{{deviceId}}/speedLimit")
    fun setSpeedLimitFirstTime(
        @Path("deviceId") deviceId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @GET("gateway/IoTHubRegistrationServices/1.0/devices/{deviceId}/registrationCode")
    fun getCodeSdk(
        @Path("deviceId") deviceId: String?
    ): Call<ResponseBody>

    // GAMIFICATION
    @GET("gateway/GamificationService/1.0/api/badge_conf/getBadgesByTsc/{tscID}")
    fun getBadgeByTSC(
        @Header("lang") lang: String,
        @Path("tscID") tscID: String?
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/badge_conf/getBadgesAchieved")
    fun getBadgesAchieved(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Header("VoucherFk") voucherFk: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/player/getPlayerLevels")
    fun getPlayerLevels(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/avatar/getAvatarByTsc/{tscID}")
    fun getAvatars(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("tscID") tscID: String
    ): Call<ResponseBody>

    //------ PLAYER --------
    @GET("gateway/GamificationService/1.0/api/player/get_player/{userId}")
    fun getPlayer(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @PUT("gateway/GamificationService/1.0/api/player/updatePlayer/{userId}")
    fun updatePlayer(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/player/create")
    fun createPlayer(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/player/get_playerByUsername/{username}")
    fun findPlayerByUsername(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("username") username: String
    ): Call<ResponseBody>

    //------ CHALLENGES --------
    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeTypes")
    fun getChallengeTypes(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/ranking/a1d8E000001pKj4QAE")
    fun getRankingGlobal(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/ranking/a1d8E000001pKj4QAE")
    fun getPersonalRankingGlobal(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("position") position: Int
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/ranking/a1d8E000001pKj4QAE/{userId}")
    fun getRankingFriends(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String,
        @Query("page") page: Int,
        @Query("size") size: Int

    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/ranking/a1d8E000001pKj4QAE/{userId}")
    fun getPersonalRankingFriends(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String,
        @Query("page") page: Int,
        @Query("size") size: Int,
        @Query("position") position: Int

    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeByUserId/{userId}")
    fun getChallengesByUserId(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/challenge/create")
    fun createChallenge(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/challenge/accept/{challengeId}")
    fun acceptChallenge(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("challengeId") challengeId: Int
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/challenge/reject/{challengeId}")
    fun rejectChallenge(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("challengeId") challengeId: Int
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/challenge/retire/{challengeId}")
    fun retireChallenge(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("challengeId") challengeId: Int
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeOngoing/{userId}")
    fun getChallengeOngoing(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengePending/{userId}")
    fun getChallengePending(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeSent/{userId}")
    fun getChallengeSent(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeReceived/{userId}")
    fun getChallengeReceived(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/challenge/getChallengeFinish/{userId}")
    fun getChallengeFinish(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String
    ): Call<ResponseBody>

    @POST("gateway/RoadSafety/1.0/assistance/{deviceId}")
    fun setBcall(
        @Path("deviceId") deviceId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>
    //------ FRIENDS --------
    @GET("gateway/GamificationService/1.0/api/friends/getListFriends/{userId}")
    fun getFriendsList(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String?
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/friends/getListFriendsSent/{userId}")
    fun getSentFriendsList(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String?
    ): Call<ResponseBody>

    @GET("gateway/GamificationService/1.0/api/friends/getListFriendsPending/{userId}")
    fun getPendingFriendsList(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("userId") userId: String?
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/friends/acceptFriendship/{friendshipId}")
    fun acceptFriendship(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("friendshipId") friendshipId: String?
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/friends/rejectFriendship/{friendshipId}")
    fun rejectFriendship(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Path("friendshipId") friendshipId: String?
    ): Call<ResponseBody>

    @POST("gateway/GamificationService/1.0/api/friends/sendFriendship")
    fun sendFriendship(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String,
        @Body body: RequestBody
    ): Call<ResponseBody>


    // ACCESS TOKEN
    @POST("invoke/pub.apigateway.oauth2/getAccessToken")
    fun getAccessTokenRegistration(
        @Body body: RequestBody
    ): Call<ResponseBody>


    @GET("gateway/DigitalContractService/1.0/api/device_app/subscriptions")
    fun getPackageList(
        @Header("TscId") tscId: String,
        @Header("template") template: String
    ): Call<ResponseBody>

    // VALEO REQUEST

    // OVERALL SCORE
    @GET("overallScore")
    fun getOverallScore(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("endUserContractId") endUserContractId: String,
        @Query("deviceId") deviceId: String
    ): Call<ResponseBody>

    @GET("overallScore")
    fun getOverallScore(
        @Query("endUserContractId") endUserContractId: String
    ): Call<ResponseBody>

    // SCORINGS
    @GET("scorings")
    fun getScorings(
        @Query("voucherId") voucherId: String
    ): Call<ResponseBody>

    @GET("scorings")
    fun getScoringsByMonitoredObjId(
        @Query("monitoredObjId") monitoredObjId: String
    ): Call<ResponseBody>

    @GET("scorings")
    fun getScorings(
        @Query("deviceId") deviceId: String,
        @Query("from") from: String,
        @Query("to") to: String
    ): Call<ResponseBody>

    @GET("scorings")
    fun getScorings(
        @Query("voucherId") voucherId: String,
        @Query("type") type: String
    ): Call<ResponseBody>

    fun getScorings(
        @Query("endUserId") endUserId: String,
        @Query("voucherId") voucherId: String,
        @Query("monitoredObjId") monitoredObjId: String,
        @Query("deviceId") deviceId: String,
        @Query("type") type: String,
        @Query("limit") limit: String,
        @Query("from") from: Date,
        @Query("to") to: Date
    ): Call<ResponseBody>

    // TRIPS
    @GET("trips/{tripId}{pathParam}")
    fun getTrips(
        @Path("tripId") tripId: String,
        @Path("pathParam") pathParam: String,
        @Query("voucherId") voucherId: String,
        @Query("endUserId") endUserId: String,
        @Query("monitoredObjId") monitoredObjId: String,
        @Query("deviceId") deviceId: String,
        @Query("from") from: Date,
        @Query("to") to: Date,
        @Query("overKey") overKey: String,
        @Query("reverse") reverse: String,
        @Query("pageSize") pageSize: String
    ): Call<ResponseBody>

    @GET("trips/{tripId}{pathParam}")
    fun getTrips(
        @Path("tripId") tripId: String,
        @Path("pathParam") pathParam: String
    ): Call<ResponseBody>

    @GET("gateway/UbiServices/2.0/trips/{tripId}/positions")
    fun getTripPositions(
        @Path("tripId") tripId: String,
        @Query("withPoints") withPoints: Int
    ): Call<ResponseBody>

    @GET("gateway/UbiServices/2.0/trips/{tripId}/events")
    fun getTripEvents(
        @Path("tripId") tripId: String
    ): Call<ResponseBody>

    @PUT("gateway/UbiServices/2.0/trips/{tripId}/confirmation")
    fun updateTripStatus(
        @Path("tripId") tripId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    //Get Trip with queryParams
    @GET("gateway/UbiServices/2.0/trips")
    fun getTripsWithQueryParam(
        @Query("voucherId") voucherId: String?,
        @Query("endUserId") endUserId: String?,
        @Query("monitoredObjId") monitoredObjId: String?,
        @Query("deviceId") deviceId: String?,
        @Query("from") from: String?,
        @Query("to") to: String?,
        @Query("overKey") overKey: String?,
        @Query("reverse") reverse: String?,
        @Query("valid") valid: Boolean?,
        @Query("pageSize") pageSize: Int?
    ): Call<ResponseBody>

    //Get TripScorings
    @GET("tripScoring")
    fun getTripScorings(
        @Query("tripId") tripId: String,
        @Query("endUserId") endUserId: String,
        @Query("deviceId") deviceId: String,
        @Query("referenceDate") referenceDate: String,
        @Query("from") from: Date,
        @Query("to") to: Date,
        @Query("limit") limit: String
    ): Call<ResponseBody>

    // TRENDS
    @GET("gateway/UbiServices/2.0/statistics/overallHabits")
    fun getTrends(
        @Query("voucherId") voucherId: String?
    ): Call<ResponseBody>

    @GET("gateway/UbiServices/2.0/scorings")
    fun getWeeklyTrends(
        @Query("voucherId") voucherId: String,
        @Query("type") type: String,
        @Query("from", encoded = true) from: String?,
        @Query("to", encoded = true) to: String?
    ): Call<ResponseBody>

    @GET("gateway/UbiServices/2.0/statistics/behaviours")
    fun getBehaviourTrends(
        @Query("aggregationType") type: String,
        @Query("from", encoded = true) from: String?,
        @Query("to", encoded = true) to: String?,
        @Query("endUserID") voucherId: String
    ): Call<ResponseBody>

    // USERS
    @Headers(
        "e2ETrackingId: prova",
        "creationTime: 2020-05-08T00:00:00",
        "eventId: prova",
        "eventDetailId: prova",
        "Host: uat-api.octotelematics.net:56861"
    )
    @POST("/gateway/DigitalContractService/api/users/createUser")
    fun createUser(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Body bodyUser: RequestBody
    ): Call<ResponseBody>

    @Headers(
        "eventDetailId: prova",
        "Host: uat-api.octotelematics.net:56861"
    )
    @POST("/gateway/DigitalContractService/api/users/createUser")
    fun createUser(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Header("e2ETrackingId") e2EtrackingId: String,
        @Header("eventId") eventId: String,
        @Header("creationTime") creationTime: String,
        @Body bodyUser: RequestBody
    ): Call<ResponseBody>

    @Headers(
        "e2ETrackingId: test",
        "creationTime: 2020-05-08T00:00:00",
        "eventId: test",
        "eventDetailId: test",
        "consumerId: app"
    )
    @POST("/gateway/DigitalContractService/api/users/changePassword")
    fun changeTemporaryPsw(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Header("email") email: String?,
        @Body bodyEmailPsw: RequestBody
    ): Call<ResponseBody>

    @Headers(
        "eventDetailId: test",
        "consumerId: app"
    )
    @POST("/gateway/DigitalContractService/api/users/changePassword")
    fun changeTemporaryPsw(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Header("email") email: String?,
        @Header("e2ETrackingId") e2EtrackingId: String,
        @Header("eventId") eventId: String,
        @Header("creationTime") creationTime: String,
        @Body bodyEmailPsw: RequestBody
    ): Call<ResponseBody>

    @GET("api/user/profile")
    fun getProfile(
        @Query("email") email: String
    ): Call<ResponseBody>

    @POST("gateway/DigitalContractService/api/device_app")
    fun accessTokenPush(
        @Header("TscId") tscId: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @DELETE("gateway/DigitalContractService/api/device_app")
    fun deleteTokenPush(
        @Header("TscId") tscId: String

    ): Call<ResponseBody>

    @PUT("api/user/setUserStatus")
    fun setUserStatus(
        @Query("status") status: String
    ): Call<ResponseBody>

    @Headers(
        "e2ETrackingId: prova",
        "eventId: prova",
        "eventDetailId: prova",
        "consumerId: app",
        "creationTime: 2020-04-08T00:00:00.000"
    )
    @POST("gateway/DigitalContractService/api/users/resetPassword")
    fun resetPassword(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Header("email") email: String
    ): Call<ResponseBody>

    @Headers(
        "eventDetailId: prova",
        "consumerId: app"
    )
    @POST("gateway/DigitalContractService/api/users/resetPassword")
    fun resetPassword(
        @Header("Authorization") token: String,
        @Header("TscId") tscId: String,
        @Header("email") email: String,
        @Header("e2ETrackingId") e2EtrackingId: String,
        @Header("eventId") eventId: String,
        @Header("creationTime") creationTime: String
    ): Call<ResponseBody>

    @PUT("gateway/DigitalContractService/api/vouchers/unsubscribe/{voucherFk}")
    fun unsubscribe(
        @Header("TscId") tscId: String,
        @Path("voucherFk") voucherFk: String
    ): Call<ResponseBody>

    @GET("api/vouchers/vouchersAll")
    fun vouchersAll(
    ): Call<ResponseBody>

    @Headers("consumerId: app")
    @POST("gateway/DigitalContractService/api/vouchers/voucherBulkRequests")
    fun voucherBulkRequest(
        @Header("TscId") tscId: String,
        @Header("email") email: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @GET("api/vouchers/getUserVouchersByMail")
    fun getUserVouchersByMail(
        @Query("email") email: String
    ): Call<ResponseBody>

    @Headers("consumerId: app")
    @GET("DigitalContractService/api/vouchers/getUserVouchersByMailAndTSC/{userEmail}")
    fun getUserVouchersByMailAndTSC(
        @Header("TscId") tscId: String,
        @Path("userEmail") email: String

    ): Call<ResponseBody>

    @GET("DigitalContractService/api/privacy")
    fun getPrivacypolicy(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String

    ): Call<ResponseBody>

    @GET("gateway/DigitalContractService/api/termsAndConditions")
    fun getTeC(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String
    ): Call<ResponseBody>

    @GET("gateway/DigitalContractService/api/faq")
    fun getFAQ(
        @Header("TscId") tscId: String,
        @Header("lang") lang: String
    ): Call<ResponseBody>


    @Headers("consumerId: app")
    @PUT("gateway/DigitalContractService/api/vouchers/voucherBulkUpdate/{voucherFk}")
    fun updateBulk(
        @Header("TscId") tscId: String,
        @Path("voucherFk") voucherFk: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @PUT("api/vouchers/unsubscribe")
    fun unsubscribe(
        @Header("TscId") tscId: String,
        @Query("voucherId") voucherId: String,
        @Query("tsc") tsc: String
    ): Call<ResponseBody>

    @POST("api/vouchers/ngp")
    fun createVoucher(
        @Query("appAsUi") appAsUi: String,
        @Query("device") device: String,
        @Query("deviceConfiguration") deviceConfiguration: String,
        @Query("deviceStatus") deviceStatus: String,
        @Query("idContractType") idContractType: String,
        @Query("messageBatchSize") messageBatchSize: String,
        @Query("ngpDeviceId") ngpDeviceId: String,
        @Query("ngpUserId") ngpUserId: String,
        @Query("ngpUserStatus") ngpUserStatus: String,
        @Query("sensors") sensors: List<Any>,
        @Query("servicesList") servicesList: List<String>,
        @Query("ssnnin") ssnnin: String,
        @Query("voucherStatus") voucherStatus: String
    ): Call<ResponseBody>

    @PUT("api/ngp/{id}")
    fun updateVoucher(
        @Path("id") id: String,
        @Query("appAsUi") appAsUi: String,
        @Query("device") device: String,
        @Query("deviceConfiguration") deviceConfiguration: String,
        @Query("deviceStatus") deviceStatus: String,
        @Query("idContractType") idContractType: String,
        @Query("messageBatchSize") messageBatchSize: String,
        @Query("ngpDeviceId") ngpDeviceId: String,
        @Query("ngpUserId") ngpUserId: String,
        @Query("ngpUserStatus") ngpUserStatus: String,
        @Query("sensors") sensors: List<Any>,
        @Query("servicesList") servicesList: List<String>,
        @Query("ssnnin") ssnnin: String,
        @Query("voucherStatus") voucherStatus: String
    ): Call<ResponseBody>


    // GENERIC REQUESTS
    @GET
    fun genericGet(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>,
        @QueryMap queries: Map<String, String>
    ): Call<ResponseBody>

    @POST
    fun genericPost(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>,
        @QueryMap queries: Map<String, String>,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @PUT
    fun genericPut(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>,
        @QueryMap queries: Map<String, String>,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @DELETE
    fun genericDelete(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>,
        @QueryMap queries: Map<String, String>,
        @Body body: RequestBody
    ): Call<ResponseBody>

    @PATCH
    fun genericPatch(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>,
        @QueryMap queries: Map<String, String>,
        @Body body: RequestBody
    ): Call<ResponseBody>
}
