package com.octotelematics.installatori.network.utils

import com.octotelematics.installatori.network.CustomResponse
import com.octotelematics.installatori.network.CustomResponseArray
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Response

class NetworkUtis {

    companion object{
        fun manageResponse(response: Response<ResponseBody>) : CustomResponse {
            if (response.isSuccessful) {
                if (response.code() == 201){
                    var json = JSONObject(response.body()!!.string())
                    var customResponse = CustomResponse(response.code(), json)
                    return customResponse
                }else {
                    var json = JSONObject(response.body()!!.string())
                    var customResponse = CustomResponse(response.code(), json)
                    return customResponse
                }

            } else {
                var json: JSONObject
                try {
                    json = JSONObject(response.errorBody()!!.string())
                } catch (e: Exception) {
                    json = JSONObject("{}")
                }
                var customResponse = CustomResponse(response.code(), json)
                return customResponse
            }
        }

        fun manageResponseArray(response: Response<ResponseBody>) : CustomResponseArray {
            if (response.isSuccessful) {
                var json = JSONArray(response.body()!!.string())
                var customResponse = CustomResponseArray(response.code(), json)
                return customResponse

            } else {
                val jo = JSONArray()
                try {
                    var jsonObj = JSONObject(response.errorBody()!!.string())
                    var customResponseObj = CustomResponse(response.code(), jsonObj)
                    jo.put(customResponseObj)
                } catch (e: Exception) {}
                var customResponse = CustomResponseArray(response.code(), jo)
                return customResponse
            }
        }

        fun isCallSuccess(response: Response<*>): Boolean {
            val code = response.code()
            return code in 200..399
        }
    }
}