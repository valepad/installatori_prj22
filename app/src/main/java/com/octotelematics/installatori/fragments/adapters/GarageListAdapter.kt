package com.octotelematics.installatori.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.interfaces.OnItemClickVoucher
import kotlinx.android.synthetic.main.item_cars_garage.view.*

class GarageListAdapter(
    private val vouchersList: Vouchers,
    private var context: Context,
    onItemTouchListener: OnItemClickVoucher
) : RecyclerView.Adapter<GarageViewHolder>() {

    private var mContext: Context = context
    private var nameTemporary: String? = null
    private var onItemTouchListener: OnItemClickVoucher? = onItemTouchListener
    var selected: MutableList<RelativeLayout>? = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GarageViewHolder {
        return GarageViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_cars_garage,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return vouchersList.size
    }

    override fun onBindViewHolder(holder: GarageViewHolder, position: Int) {
        val mVoucherList = vouchersList[position]
        /*
        if (mVoucherList.voucherId != null){
            holder.voucherText.text = mVoucherList.voucherId
        }else {
            holder.voucherText.text = "null"
        }
        */

        var text = mVoucherList.voucherId
        if (mVoucherList.motor?.plate != null && mVoucherList.motor?.plate!!.isNotEmpty()) {
            text = mVoucherList.motor!!.plate
        }

        if (text == null || text.isEmpty()) {
            text = mVoucherList.id.toString()
        }

        holder.voucherStatus.text = text

        holder.voucherText.text = mVoucherList.motor!!.vehicleMake+" "+mVoucherList.motor!!.vehicleModel

        selected!!.add(holder.itemContainerVoucher)

        holder.itemContainerVoucher.setOnClickListener {
            //holder.itemContainerPackage.setBackgroundColor(mContext!!.resources.getColor(R.color.white_opacity))
            onItemTouchListener!!.onClick(
                mVoucherList,
                holder.itemContainerVoucher,
                selected
            )

        }
    }


    /* override fun onBindViewHolder(holder: VoucherViewHolder, position: Int) {
         val mPackageList = packageList[position]
         holder.packageText.text = mPackageList.name

         selected!!.add(holder.itemContainerPackage)

         holder.itemContainerPackage.setOnClickListener {
             //holder.itemContainerPackage.setBackgroundColor(mContext!!.resources.getColor(R.color.white_opacity))
             onItemTouchListener!!.onClick(
                 mPackageList.packageId,
                 holder.itemContainerPackage,
                 selected
             )
         }
     }*/
}

class GarageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var voucherText: TextView = itemView.findViewById(R.id.car_name)

    var voucherStatus: TextView = itemView.findViewById(R.id.car_text)

    var itemContainerVoucher: RelativeLayout = itemView.item_layout_car

}