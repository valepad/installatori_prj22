package com.octotelematics.installatori.fragments.profile

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.octotelematics.installatori.R
import com.octotelematics.installatori.main.LoginFragment
import kotlinx.android.synthetic.main.fragment_biometric.*


class BiometricFragment : DialogFragment() {

    var activity: LoginFragment? = null

    companion object {
        fun newInstance(activity: LoginFragment): BiometricFragment {
            val f = BiometricFragment()
            f.activity = activity
            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_biometric, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        biometricLayout()
        logoutCancelButton.setOnClickListener {
            dismiss()

        }
    }
    fun biometricLayout(){
        fingerText.visibility=View.VISIBLE
        fingerSuccess.visibility=View.GONE
        fingerError.visibility=View.GONE
    }
fun biometricSuccess(){
    fingerText.visibility=View.GONE
    fingerSuccess.visibility=View.VISIBLE
    fingerError.visibility=View.GONE
}
    fun biometricError() {
      //  biometricFeedback.setCardBackgroundColor(ContextCompat.getColor(context!!, R.color.rusty_red))
        fingerText.visibility=View.GONE
        fingerSuccess.visibility=View.GONE
        fingerError.visibility=View.VISIBLE
    }
}
