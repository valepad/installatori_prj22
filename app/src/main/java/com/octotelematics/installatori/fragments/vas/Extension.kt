package com.octotelematics.installatori.fragments.vas


import android.content.Context
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import androidx.core.content.ContextCompat

var context: Context? = null


fun addMarkerToMap(map:GoogleMap,latitude:Double,longitude:Double,iconImage:Int,zoomCam:Float?,v1:Float,v2:Float, contextBase: Context) {
    val latLng = LatLng(latitude,longitude)
    context = contextBase

   val markerOptions =  if (v1!= -1f && v2 != -1f){
       MarkerOptions().position(latLng).anchor(v1, v2).icon(BitmapDescriptorFactory.fromResource(iconImage))
    }else{
       MarkerOptions().position(latLng).anchor(0.5F, 0.5F).icon(BitmapDescriptorFactory.fromResource(iconImage))
   }

    map.apply {
        addMarker(markerOptions)
        zoomCam?.let {
            moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoomCam))
        }
    }
}

fun addRadiusArea(latitude:Double,longitude:Double,map:GoogleMap,color:Int,radiusValue:Int):Circle{
    val latLng = LatLng(latitude,longitude)

     return map.addCircle(
        CircleOptions()
            .strokeColor(
                ContextCompat.getColor(context!!,color))
            .center(latLng)
            .fillColor( ContextCompat.getColor(context!!,color))
            .radius(radiusValue.toDouble())
            .strokeWidth(0f)
    )
}


