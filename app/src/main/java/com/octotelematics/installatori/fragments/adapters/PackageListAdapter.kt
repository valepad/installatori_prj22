package com.octotelematics.installatori.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.interfaces.OnItemClickPackage
import com.octotelematics.installatori.models.packageListResponse.packageListResponse

import kotlinx.android.synthetic.main.item_list_package.view.*

class PackageListAdapter(
    private val packageList: packageListResponse,
    private var context: Context,
    onItemTouchListener: OnItemClickPackage
) : RecyclerView.Adapter<PackageViewHolder>() {

    private var mContext: Context = context
    private var nameTemporary: String? = null
    private var onItemTouchListener: OnItemClickPackage? = onItemTouchListener
    var selected: MutableList<RelativeLayout>? = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageViewHolder {
        return PackageViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list_package,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return packageList.size
    }

    override fun onBindViewHolder(holder: PackageViewHolder, position: Int) {
        val mPackageList = packageList[position]
        holder.packageText.text = mPackageList.name

        selected!!.add(holder.itemContainerPackage)

        holder.itemContainerPackage.setOnClickListener {
            //holder.itemContainerPackage.setBackgroundColor(mContext!!.resources.getColor(R.color.white_opacity))


            onItemTouchListener!!.onClick(
                mPackageList.packageId,
                holder.itemContainerPackage,
                selected
            )

        }
    }
}

class PackageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var packageText: TextView = itemView.findViewById(
        R.id.packageText
    )

    var itemContainerPackage: RelativeLayout = itemView.itemContainerPackage

}