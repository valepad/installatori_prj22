package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.SnapHelper
import com.google.gson.Gson
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentCarDetailsBinding
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_car_details.*


class CarDetailsFragment : BaseFragment() {

    lateinit var car: String
    lateinit var binding: FragmentCarDetailsBinding
    private var adapter: VouchersItem? = null
    var snapHelper: SnapHelper? = null
    lateinit var carList: ArrayList<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCarDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        car = PreferencesHelper.getStringValue(requireContext(), "voucherGarage").toString()
        val gson = Gson()
        val carConverted = gson.fromJson(car, VouchersItem::class.java)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(carConverted.motor?.vehicleMake.toString() + " " + carConverted.motor?.vehicleModel.toString() , true,R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
        //setting all the details in the texts
        settingTexts(carConverted)
        car_recycler_contact_us.setOnClickListener{
            addFragment(ContactUsFragment(), true, true, R.id.container)
        }
    }

    private fun settingTexts(carModel: VouchersItem) {
        binding.caDetailLicense.text = carModel.motor!!.plate
        binding.caDetailVin.text = carModel.motor!!.vehicleVIN
        binding.caDetailDate.text = carModel.motor!!.vehicleRegistrationDate
        binding.caDetailBrand.text = carModel.motor!!.vehicleMake
        binding.caDetailModel.text = carModel.motor!!.vehicleModel
        binding.caDetailColor.text = carModel.motor!!.colour
    }
}