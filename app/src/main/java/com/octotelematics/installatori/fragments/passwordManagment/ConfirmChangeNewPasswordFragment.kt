package com.octotelematics.installatori.fragments.passwordManagment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.octotelematics.installatori.R
import com.octotelematics.installatori.main.LoginFragment
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_confirm_change_new_password.*

class ConfirmChangeNewPasswordFragment(val trim: String) : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_change_new_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideProgress()
        sentat.text=getString(R.string.confirm_change_message)+" "+trim
        val activityBase = activity as BaseActivity
        activityBase.showToolbar(false)
        confirm_change_btn.setOnClickListener(View.OnClickListener {
            val manager: FragmentManager = activity!!.supportFragmentManager
            manager.popBackStack()
            changeFragment(LoginFragment(), R.id.container)
        })
    }

}