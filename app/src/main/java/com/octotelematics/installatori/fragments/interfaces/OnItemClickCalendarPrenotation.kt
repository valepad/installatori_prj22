package com.octotelematics.installatori.fragments.interfaces


interface OnItemClickCalendarPrenotation {
    fun onClick(position: Int)
}