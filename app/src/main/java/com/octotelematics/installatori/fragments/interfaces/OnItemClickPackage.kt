package com.octotelematics.installatori.fragments.interfaces

import android.widget.RelativeLayout

interface OnItemClickPackage {
    fun onClick(packageValue: String, relative: RelativeLayout?, packages: MutableList<RelativeLayout>?)
}