package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import APIRepository
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.models.userRegistration.TermsPolicy
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_legal_notes.*
import okhttp3.ResponseBody
import retrofit2.Callback


class LegalNotesFragment : BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_legal_notes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.legal_notes_title),
            true,
            R.drawable.ic_back_white,
            R.drawable.ic_octo_logo_small,
            face
        )
setupTermsText()
    }

    fun setupTermsText() {

        APIRepository(activity!!).getTec(
            BuildConfig.TSC_ID,
            PreferencesHelper.getStringValue(context!!, PreferencesHelper.PREF_LANGUAGE)
                ?: AppUtils.DEFAULT_LANGUAGE,
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                    Log.e("DEBUG", "ON FAILURE = " + t)
                    hideProgress()
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val customResponse = NetworkUtis.manageResponse(response)
                        Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                        var gson = Gson()
                        val response = gson.fromJson(
                            customResponse.body.toString(), TermsPolicy::class.java
                        )
                        texttec.text = response.termsAndConditionsText
                    }
                    hideProgress()
                }

            }
        )

    }
}