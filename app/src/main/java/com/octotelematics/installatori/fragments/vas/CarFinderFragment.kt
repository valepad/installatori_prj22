package com.octotelematics.installatori.fragments.vas

import APIRepository
import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.octotelematics.installatori.R
import com.octotelematics.installatori.models.carFinder.carFinderPosition
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.MapFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.ConfigurationUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_car_finder.*
import okhttp3.ResponseBody
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*


class CarFinderFragment : MapFragment(), OnMapReadyCallback {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var googleMap: GoogleMap? = null
    private var mapReady = false
    private var latitude: Double? = null
    private var longitudeCar: String? = null
    private var latitudeCar: String? = null
    private var progress: DialogUtils? = null
    private var timestampCar: String? = null
    var timeCarFinder: String? = null
    var dialog: DialogUtils? = null
    var myPosition: LatLng? = null
    private var circlePosition: Circle? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_car_finder, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkGPS()
        var voucherItem: VouchersItem?=null
        if( PreferencesHelper.getObjValue(context!!, "currentVoucher", VouchersItem::class.java)!=null){
            voucherItem = PreferencesHelper.getObjValue(context!!, "currentVoucher", VouchersItem::class.java) as VouchersItem

        }

        if(voucherItem!=null && !voucherItem?.motor?.vehicleModel.isNullOrEmpty()&&!voucherItem?.motor?.vehicleMake.isNullOrEmpty()&&!voucherItem?.motor?.plate.isNullOrEmpty())
        {
            mycar.text=voucherItem?.motor?.vehicleMake+" "+voucherItem?.motor?.vehicleModel

        }


            setupCarFinder(savedInstanceState)

 setupToolbar()

    }


    fun setupCarFinder(savedInstanceState: Bundle?) {
        var prefs: SharedPreferences =
                context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)

        val deviceId = prefs.getString("deviceId", null)
       APIRepository(activity!!).getCarFinderPosition(
                deviceId.toString(),
                true,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        Log.e("DEBUG", "ON FAILURE = " + t)
                        hideProgress()
                    }

                    override fun onResponse(
                            call: retrofit2.Call<ResponseBody>,
                            response: retrofit2.Response<ResponseBody>
                    ) {


                        if (response.isSuccessful) {
                            val customResponse = NetworkUtis.manageResponse(response)
                            Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                            var gson = Gson()
                            val response = gson.fromJson(
                                    customResponse.body.toString(),
                                    carFinderPosition::class.java
                            )
                    direction_btn.backgroundTintList=resources.getColorStateList(R.color.mint)
                            direction_btn.isEnabled=true
                                if (activity != null) {
                                    PreferencesHelper.setStringValue(
                                            activity!!,
                                            "latitudeCar",
                                            response.latitude.toString()
                                    )
                                    PreferencesHelper.setStringValue(
                                            activity!!,
                                            "longitudeCar",
                                            response.longitude.toString()
                                    )
                                    PreferencesHelper.setStringValue(
                                            activity!!,
                                            "timestampCar",
                                            response.timestamp
                                    )
                                }

                            initmap(savedInstanceState)
                        }else {
                            direction_btn.backgroundTintList=resources.getColorStateList(R.color.grey_text)
                            direction_btn.isEnabled=false
                            if(response.code() != 404 && response.code() != 401) {
                                dialog = DialogUtils.Builder(dialog, activity!!)
                                    .title("ERRORE")
                                    .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                    .content("Generic Error")
                                    .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                    .positiveText("Close")
                                    .show()
                            }
                        }
                        hideProgress()
                    }

                }
        )

    }

    fun initmap(savedInstanceState: Bundle?) {
        latitudeCar = PreferencesHelper.getStringValue(context!!, "latitudeCar")
        longitudeCar = PreferencesHelper.getStringValue(context!!, "longitudeCar")
        timestampCar = PreferencesHelper.getStringValue(context!!, "timestampCar")
        if (latitudeCar != "" && longitudeCar != "" && timestampCar != "" && latitudeCar != null && longitudeCar != null && timestampCar != null) {
            val geocoder: Geocoder
            val addresses: List<Address>
            geocoder = Geocoder(context!!, Locale.getDefault())
            addresses = geocoder.getFromLocation(latitudeCar!!.toDouble(), longitudeCar!!.toDouble(), 1)
            var address = addresses.get(0).getAddressLine(0)
            if (address != null) {
                address_car.text = address
            }

            try {
                val isoFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                isoFormat.timeZone = TimeZone.getTimeZone("GMT+0");
                val dateStartGTM = isoFormat.parse(timestampCar)
                val formatter = SimpleDateFormat("HH:mm - dd-MM-yyyy", Locale.getDefault())
                timeCarFinder = formatter.format(dateStartGTM)
            } catch (e: java.lang.Exception) {
                timeCarFinder = "-"
            }

        } else {
           latitudeCar = "0.0"
            longitudeCar = "0.0"
            timeCarFinder = "-"
            dialog = DialogUtils.Builder(dialog, activity!!)
                    .title("ERRORE")
                    .setTitleColor(resources.getColor(R.color.dark_blue_text))
                    .content("Position Car not found!")
                    .setMessageColor(resources.getColor(R.color.dark_blue_text))
                    .positiveText("Close")
                    .show()
        }
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) === PackageManager.PERMISSION_GRANTED
        ) {
            /* val gpsLocation = MapFragment().getLocationMap(2000L,10F, context!!)
             val latitude = gpsLocation!!.latitude
             val longi = gpsLocation!!.longitude
             val builder = LatLngBounds.Builder()
             val myPosition = LatLng(latitude, longi)
             val markerOptions = MarkerOptions()
             markerOptions.position(myPosition)
             markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_pin))

             builder.include(myPosition)
             googleMap!!.addMarker(markerOptions)
             Log.e("--->", "icao")*/
        } else {
            ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    1
            )
            checkGPS()
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        mapFragment = childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment?
        mapFragment!!.onCreate(savedInstanceState)
        mapFragment!!.getMapAsync(this)


        updateTime.text = "Updated: $timeCarFinder"

        direction_btn.setOnClickListener(View.OnClickListener {
            val uri = "http://maps.google.com/maps?saddr=" + latitude + "," + longi + "&daddr=" + latitudeCar + "," + longitudeCar
            val intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent)
        })

    }

    fun checkGPS(){
        val lm: LocationManager = activity!!.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled: Boolean = false;
        var network_enabled: Boolean = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (ex: Exception) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (ex: Exception) {
        }

        if (!gps_enabled && !network_enabled) {
            val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
            alertDialogBuilder.setMessage(Html.fromHtml("<font color='#000000'>Il GPS non è attivo sul tuo dispositivo. Vuoi attivarlo?</font>"))
                .setCancelable(false)
                .setPositiveButton(Html.fromHtml("<font color='#000000'>Vai alle impostazioni per attivare il GPS</font>"),
                    DialogInterface.OnClickListener { dialog, id ->
                        val callGPSSettingIntent = Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS
                        )
                        startActivity(callGPSSettingIntent)
                    })
            alertDialogBuilder.setNegativeButton(Html.fromHtml("<font color='#000000'>Cancella</font>"),
                DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
            val alert: AlertDialog = alertDialogBuilder.create()
            alert.show()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e("--->", "ciao")
        if (mapFragment != null) {
            mapFragment!!.getMapAsync(this)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        if (latitudeCar != "" && longitudeCar != "" && timestampCar != "" && latitudeCar != null && longitudeCar != null && timestampCar != null) {
            this.googleMap = googleMap
            var gpsLocation = getLocationMap(20L, 10F, context!!)
            val builder = LatLngBounds.Builder()
            val myPosition = LatLng(latitudeCar!!.toDouble(), longitudeCar!!.toDouble())
            this.googleMap!!.uiSettings.isZoomControlsEnabled = true
            val markerOptions = MarkerOptions()
            markerOptions.position(myPosition)

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pincarfinder))

            builder.include(myPosition)
            googleMap!!.addMarker(markerOptions)
            val bounds = builder.build()
            this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 15.0f))
            if (gpsLocation != null) {
                latitude = gpsLocation?.latitude
                longi = gpsLocation?.longitude

                var myRealPosition = LatLng(gpsLocation?.latitude, gpsLocation?.longitude)

                val btnMyLocation: ImageView = view!!.findViewById(R.id.image_button_position)
                var cameraUpdate: CameraUpdate? = null

                btnMyLocation.setOnClickListener(View.OnClickListener {
                    if (latitude != null && longi != null) {
                        val latLng = LatLng(
                            latitude!!, longi!!
                        )
                        cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 13.0f)
                        googleMap.animateCamera(cameraUpdate)
                    }
                })

                val markerOptions = MarkerOptions()
                markerOptions.position(myRealPosition)
                markerOptions.anchor(0.5F, 0.5F)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ovalposition))
                builder.include(myRealPosition)


                googleMap!!.addMarker(markerOptions)
                circlePosition = addRadiusAreaMyPosition(gpsLocation?.latitude, gpsLocation?.longitude, googleMap, R.color.slate_transparent_green,
                        (0.5 * 1000).toInt()
                )
            }
            /* val width = resources.displayMetrics.widthPixels
             val height = resources.displayMetrics.heightPixels
             val padding = (width * 0.25).toInt()
             val cu = CameraUpdateFactory.newLatLngBounds(bounds, width,height,padding)
              googleMap.animateCamera(cu)
              this.googleMap!!.animateCamera(
                  CameraUpdateFactory.newLatLngZoom(
                      LatLng(
                          latitude!!,
                          longi
                      ), 80.0f
                  )
              )*/
            Log.e("--->", "icao")
        } else {
            this.googleMap = googleMap
            var gpsLocation = getLocationMap(20L, 10F, context!!)
            val builder = LatLngBounds.Builder()
            address_car.text = "- - -"
            //updateTime.text = resources.getString(R.string.dato_non_disponibile_car_position)
            updateTime.text = "Updated: - - -"
            direction_btn.alpha = 0.5F
            direction_btn.isEnabled = false
            if (gpsLocation != null) {
                latitude = gpsLocation?.latitude
                longi = gpsLocation?.longitude

                var myRealPosition = LatLng(gpsLocation?.latitude, gpsLocation?.longitude)

                val markerOptions = MarkerOptions()
                markerOptions.position(myRealPosition)
                markerOptions.anchor(0.5F, 0.5F)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_position))

                builder.include(myRealPosition)

                val btnMyLocation: ImageView = view!!.findViewById(R.id.image_button_position)
                var cameraUpdate: CameraUpdate? = null

                btnMyLocation.setOnClickListener(View.OnClickListener {
                    if (latitude != null && longi != null) {
                        val latLng = LatLng(
                            latitude!!, longi!!
                        )
                        cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 13.0f)
                        googleMap.animateCamera(cameraUpdate)
                    }
                })


                googleMap!!.addMarker(markerOptions)
                circlePosition = addRadiusAreaMyPosition(gpsLocation?.latitude, gpsLocation?.longitude, googleMap, R.color.slate_transparent_green,
                        (0.5 * 1000).toInt()
                )
                this.googleMap!!.uiSettings.isZoomControlsEnabled = true
                this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(myRealPosition, 13.0f))
            }else {
                myPosition = LatLng(0.0, 0.0)
               /* dialog = DialogUtils.Builder(dialog, activity!!)
                        .title("ERRORE")
                        .setTitleColor(resources.getColor(R.color.dark_blue_text))
                        .content("Impossibile recuperare la tua posizione! Attiva il gps e riprova!")
                        .setMessageColor(resources.getColor(R.color.dark_blue_text))
                        .positiveText("Close")
                        .show()*/
            }
            this.googleMap!!.uiSettings.isZoomControlsEnabled = true
          /*  val markerOptions = MarkerOptions()
            markerOptions.position(myPosition!!)
            val markerIcon = MarkerHelper.getMarkerIconFromDrawable(resources.getDrawable(R.drawable.circle_drawable))
            markerOptions.icon(markerIcon)

            builder.include(myPosition)
            googleMap!!.addMarker(markerOptions)
            val bounds = builder.build()
            this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 15.0f))
            if (gpsLocation != null) {
                latitude = gpsLocation?.latitude
                longi = gpsLocation?.longitude
            }*/
        }
    }


    lateinit var baseActivity: BaseActivity
    fun setupToolbar() {
        val baseActivity = activity as BaseActivity
        baseActivity.showToolbar(true)
        baseActivity.showBottomNavigation(true)



        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.findmycar),
            true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face
        )

    }

    }

    fun addRadiusAreaMyPosition(latitude: Double, longitude: Double, map: GoogleMap, color: Int, radiusValue: Int):Circle{
        val latLng = LatLng(latitude, longitude)

        return map.addCircle(
                CircleOptions()
                        .strokeColor(Color.parseColor("#32091f2c"))
                        .center(latLng)
                        .fillColor(Color.parseColor("#32091f2c"))
                        .radius(radiusValue.toDouble())
                        .strokeWidth(0f)
        )
    }

