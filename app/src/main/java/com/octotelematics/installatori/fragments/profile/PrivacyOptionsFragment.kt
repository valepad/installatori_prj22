package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.R



class PrivacyOptionsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_privacy_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(resources.getString(R.string.profiling_privacy_opt), true,R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
    }

//TODO ADD PRIVACY LOGIC
}