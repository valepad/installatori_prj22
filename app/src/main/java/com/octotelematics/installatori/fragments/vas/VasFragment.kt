package com.octotelematics.installatori.fragments.vas


import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment

import com.octotelematics.installatori.R

import kotlinx.android.synthetic.main.fragment_vas.*

/**
 * A simple [Fragment] subclass.
 */
class VasFragment() : BaseFragment() {

    lateinit var baseActivity: BaseActivity
    var permissionAsked: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar()
        carFinder.setOnClickListener {
            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) === PackageManager.PERMISSION_GRANTED
            ) {
               addFragment(CarFinderFragment(), true, true, R.id.container)
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    1
                )
                permissionAsked = true
                val lm: LocationManager = activity!!.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                var gps_enabled: Boolean = false;
                var network_enabled: Boolean = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (ex: Exception) {
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (ex: Exception) {
                }

                if (!gps_enabled && !network_enabled) {
                    val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
                    alertDialogBuilder.setMessage("Il GPS non è attivo sul tuo dispositivo. Vuoi attivarlo?")
                        .setCancelable(false)
                        .setPositiveButton("Vai alle impostazioni per attivare il GPS",
                            DialogInterface.OnClickListener { dialog, id ->
                                val callGPSSettingIntent = Intent(
                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                                )
                                startActivity(callGPSSettingIntent)
                            })
                    alertDialogBuilder.setNegativeButton("Cancella",
                        DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                    val alert: AlertDialog = alertDialogBuilder.create()
                    alert.show()
                }
            }

        }


        areaInteresse.setOnClickListener(View.OnClickListener {
            addFragment(GeofenceFragment(),true,true, R.id.container)
        })
    }

    override fun onResume() {
        super.onResume()
        if (permissionAsked){
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) === PackageManager.PERMISSION_GRANTED
        ) {
           addFragment(CarFinderFragment(), true, true, R.id.container)
            permissionAsked = false
        }
            }
    }

    fun setupToolbar() {
        val baseActivity = activity as BaseActivity
        baseActivity.showToolbar(true)
        baseActivity.showBottomNavigation(true)



        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.services),
            true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face
        )

    }

}
