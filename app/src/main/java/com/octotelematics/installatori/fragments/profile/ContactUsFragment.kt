package com.octotelematics.installatori.fragments.profile

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.R
import kotlinx.android.synthetic.main.fragment_contact_us.*


class ContactUsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val baseActivity = activity as BaseActivity
        baseActivity.showToolbar(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")

        baseActivity.showToolbar(false)
        callus.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    activity!!, arrayOf(android.Manifest.permission.CALL_PHONE),
                    1
                )
            }else {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+39020090093"))
                startActivity(intent)
            }

        }
        callsupport.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {


                ActivityCompat.requestPermissions(
                    activity!!, arrayOf(android.Manifest.permission.CALL_PHONE),
                    1
                )
            }else {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+39020090080"))
                startActivity(intent)
            }

        }

        openemail.setOnClickListener {
            val i = Intent(Intent.ACTION_SEND)
            i.type = "message/rfc822"
            i.putExtra(Intent.EXTRA_EMAIL, arrayOf("assistance@octo.com"))

            try {
                startActivity(Intent.createChooser(i, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
                Toast.makeText(
                    context,
                    "There are no email clients installed.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        arrowBackforgot.setOnClickListener {
            baseActivity.onBackPressed()
        }
        // baseActivity.showDarkToolbar()
        // baseActivity.setDarkToolbar(resources.getString(R.string.profiling_contact_us), true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
    }

}



