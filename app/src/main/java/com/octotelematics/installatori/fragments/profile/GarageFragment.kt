package com.octotelematics.installatori.fragments.profile

import APIRepository
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentGarageBinding
import com.octotelematics.installatori.fragments.adapters.GarageListAdapter
import com.octotelematics.installatori.fragments.interfaces.OnItemClickVoucher
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import okhttp3.ResponseBody
import retrofit2.Callback


class GarageFragment : BaseFragment() {

    private lateinit var binding: FragmentGarageBinding
    private var adapter: GarageListAdapter? = null
    private var vouchers: Vouchers? = null
    var snapHelper: SnapHelper? = null
    private var email: String? = null
    private lateinit var refresh: SwipeRefreshLayout



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGarageBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(resources.getString(R.string.profiling_garage), true,R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
        refresh = binding.garageRecyclerRefresh

        email = PreferencesHelper.getStringValue(context!!, PreferencesHelper.PREF_EMAIL_USER) ?: ""

        getAllVouchers(email!!)
//SETTING UP THE RECYCLER


        refresh.setOnRefreshListener {
            refresh.isRefreshing = false
            getAllVouchers(email!!)
        }
        val voucherList = PreferencesHelper.getStringValue(
            context!!,
            PreferencesHelper.PREF_VOUCHER_LIST
        )
        if (!voucherList.isNullOrEmpty()) {
            if (refresh.isRefreshing) {
                getAllVouchers(email!!)
            }
            var gson = Gson()
            var vouchers: Vouchers = gson.fromJson(voucherList, Vouchers::class.java)
            if (vouchers != null) {
                setVoucherAdapter(vouchers)
            } else {
                //DO CALL TO GET VOUCHERS
                getAllVouchers(email!!)
            }
        } else {
            //DO CALL TO GET VOUCHERS
            getAllVouchers(email!!)
        }

        //bindings
        binding.garageViewCloseBtn.setOnClickListener {
            binding.garageHelpView.visibility = View.GONE

        }

    }

    private fun getAllVouchers(email: String) {
        APIRepository(context!!).getVouchers(
            BuildConfig.TSC_ID,
            PreferencesHelper.getStringValue(
                context!!,
                PreferencesHelper.PREF_LANGUAGE
            ) ?: AppUtils.DEFAULT_LANGUAGE,
            PreferencesHelper.getStringValue(
                context!!,
                PreferencesHelper.PREF_TEMPLATE
            ) ?: "",
            email.trim(),
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val customResponse = NetworkUtis.manageResponseArray(response)
                        var gson = Gson()
                        vouchers =
                            gson.fromJson(customResponse.body.toString(), Vouchers::class.java)
                        if (context != null) {
                            PreferencesHelper.setStringValue(
                                context!!,
                                PreferencesHelper.PREF_VOUCHER_LIST,
                                customResponse.body.toString()
                            )
                        }

                        if (customResponse.body.toString() != "[]") {
                            var gson = Gson()
                            var vouchers: Vouchers =
                                gson.fromJson(customResponse.body.toString(), Vouchers::class.java)

                            setVoucherAdapter(vouchers)
                        } else {
                            Toast.makeText(requireContext(), "lista vuota", Toast.LENGTH_SHORT)
                                .show()
                            //addFragment(InfoCarRegistrationFragment(),true,true,R.id.container)
                        }
                    } else {
                        Toast.makeText(requireContext(), "errore", Toast.LENGTH_SHORT).show()
                    }
                }

            }
        )
    }

    private fun setVoucherAdapter(voucherList: Vouchers) {
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val recyclerView: RecyclerView = binding.garagaRecycler
        snapHelper?.attachToRecyclerView(recyclerView)
        recyclerView.layoutManager = layoutManager
        if (activity != null) {
            adapter = voucherList.let {
                GarageListAdapter(it, activity!!, object : OnItemClickVoucher {
                    override fun onClick(
                        voucher: VouchersItem,
                        relative: RelativeLayout?,
                        packages: MutableList<RelativeLayout>?
                    ) {
                        val gson = Gson()
                        val json = gson.toJson(voucher)
                        var bundle = bundleOf("car" to json)

                        PreferencesHelper.setStringValue(requireContext(), "voucherGarage", json)
                        //replaceFragment(CarRecyclerFragment(), false, R.id.container)
                        addFragment(CarDetailsFragment(), true, true, R.id.container)

                    }
                }
                )
            }
            recyclerView.adapter = adapter
            vouchers = null
        }
    }
}