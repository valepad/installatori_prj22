package com.octotelematics.conTe.fragments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.profile.RoadAssistanceDialog
import com.octotelematics.installatori.utils.DateUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_roadside_assistance.*

import java.util.*


class RoadsideAssistanceFragment : BaseFragment() {



    var dialog: DialogUtils? = null
    lateinit var baseActivity: BaseActivity
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_roadside_assistance, container, false)
    }
var latitude:Double?=null
var longitude:Double?=null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseActivity = activity as BaseActivity
        var baseActivity = activity as BaseActivity
        baseActivity.showToolbar(true)
        baseActivity.showBottomNavigation(true)


        baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.roadside),
            true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face
        )


        val lm: LocationManager = activity!!.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled: Boolean = false;
        var network_enabled: Boolean = false;
        PreferencesHelper.setBooleanValue(context!!, "positiongot", false)

        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.CALL_PHONE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            callPermission()

        } else {
            /*val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + resources.getString(R.string.number_road_assistance)))
            startActivity(intent)*/
        }

        emergencyCall.setOnClickListener {
            sendCurrentLocation()
            //callnow.visibility = View.VISIBLE

            val roadAssistanceDialog: RoadAssistanceDialog = RoadAssistanceDialog.Builder(getActivity(), this, RoadAssistanceDialog.DialogType.LOADING, BaseActivity.instance).setOnTryAgainListener { sendCurrentLocation() }.build()
            roadAssistanceDialog.show()
            roadAssistanceDialog.type = RoadAssistanceDialog.DialogType.SUCCESS
        }

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (ex: Exception) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (ex: Exception) {
        }

        if (!gps_enabled && !network_enabled) {
           /* dialog = DialogUtils.Builder(dialog, activity!!)
                    .title("ERRORE")
                    .setTitleColor(resources.getColor(R.color.colorPrimary))
                    .content("Impossibile recuperare la tua posizione! Attiva il gps e riprova!")
                    .setMessageColor(resources.getColor(R.color.colorPrimary))
                    .positiveText("Close")
                    .show()*/
        }else{


        }

        callnow.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.CALL_PHONE
                    ) !== PackageManager.PERMISSION_GRANTED
            ) {
                callPermission()

            } else {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + resources.getString(R.string.number_road_assistance)))
                startActivity(intent)
            }



        }
       /* if(PreferencesHelper.getBooleanValue(context!!, "positiongot")){
            callnow.visibility=View.VISIBLE
        }       else{
            callnow.visibility=View.GONE
        }
*/
    }
fun callPermission(){
    ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf<String>(Manifest.permission.CALL_PHONE),
            1)

}
    open fun sendCurrentLocation() {

        var prefs: SharedPreferences =
            context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
        val deviceId = prefs.getString("deviceId", null)
        val c = Calendar.getInstance()
        var eventDate = ""
        DateUtils.getDateString(c.time, DateUtils.DEFAULT_DATE_FORMAT_3).let {
            eventDate = it!!
        }
       // formatter.timeZone = TimeZone.getTimeZone("GMT+1")
       // val dateFormatted: String = formatter.format(c)
        val jsonParamsToken: MutableMap<String?, Any?> = ArrayMap()
        val tsLong = System.currentTimeMillis() / 1000
        val ts = tsLong.toString()
        jsonParamsToken["alarmId"] = ts
        jsonParamsToken["eventDate"] = eventDate
        jsonParamsToken["type"] = "SOS_APP"

        val bodyToken = ConvertRequestBody.responseBodyReturn(jsonParamsToken)

   /*     APIRepository(context!!).setBcall(
                deviceId!!,
                bodyToken,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        Log.e("DEBUG", "ON FAILURE = " + t)
                        //roadAssistanceDialog.type = RoadAssistanceDialog.DialogType.FAIL
                       // callnow.visibility = View.VISIBLE

                    }

                    override fun onResponse(
                            call: retrofit2.Call<ResponseBody>,
                            response: retrofit2.Response<ResponseBody>
                    ) {

                        if (response.isSuccessful) {


                        } else {
                            try {
                                //callnow.visibility = View.GONE
                            } catch (e: Exception) {
                            }
                            /*roadAssistanceDialog.type = RoadAssistanceDialog.DialogType.FAIL
                            callnow.visibility = View.VISIBLE
                            if (response.code() != 404 && response.code() != 401) {
                                NetworkUtilsApp.customDialog = CustomDialog.Builder(NetworkUtilsApp.customDialog, context)
                                        .title(R.string.oops)
                                        .content(R.string.spiancenti_error_generico)
                                        .positiveText(R.string.ok)
                                        .setImage(R.drawable.controls_negative)
                                        .show()
                            }*/
                        }
                    }

                }
        )*/








    }

}
