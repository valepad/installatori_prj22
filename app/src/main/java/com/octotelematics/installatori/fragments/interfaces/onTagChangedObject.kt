package com.octotelematics.installatori.fragments.interfaces

class onTagChangedObject {
    private var listener: onTagChangedListener? = null
    // Step 1 - This interface defines the type of messages I want to communicate to my owner
    interface onTagChangedListener {
        fun setOnChangedValueTag()
    }

    fun onTagChangedObject(){
        listener = null
    }

    fun setCustomObjectListener(listener: onTagChangedListener?) {
        this.listener = listener
    }
}