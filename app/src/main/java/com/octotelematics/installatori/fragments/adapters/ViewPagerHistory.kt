package com.octotelematics.installatori.fragments.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

class ViewPagerHistory(fragmentManager: FragmentManager?, lifecycle: Lifecycle?, viewpager: ViewPager2, context: Context) :
    FragmentStateAdapter(fragmentManager!!, lifecycle!!) {
    private var viewpager: ViewPager2 = viewpager
    private var context: Context = context
    var feedsList: List<Fragment>? = null
    override fun getItemCount(): Int {
        return feedsList!!.size
    }

    override fun createFragment(position: Int): Fragment {
        return feedsList!![position]
    }

    fun getTripListFragment():Fragment{
        return feedsList!![0]
    }
    /*var feedsList: List<Fragment>? = null
    override fun getItem(position: Int): Fragment {
        return feedsList!![position]
    }

    override fun getCount(): Int {
        return feedsList!!.size
    }*/
}