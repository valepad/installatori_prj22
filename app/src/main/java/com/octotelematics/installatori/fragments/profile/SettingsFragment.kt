package com.octotelematics.installatori.fragments.profile

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.infinum.goldfinger.Goldfinger
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentSettingsBinding

import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.ConfigurationUtils
import kotlinx.android.synthetic.main.fragment_settings.*

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : BaseFragment() {

    var dialog: DialogUtils? = null
    lateinit var baseActivity: BaseActivity
    private var progress: DialogUtils? = null
    lateinit var binding: FragmentSettingsBinding


    private lateinit var prefs: SharedPreferences
    private var ddToken: String? = null
    private var registrationRequestCode: String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(resources.getString(R.string.profiling_settings), true,R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
        if (BuildConfig.FLAVOR == AppUtils.APP_AS_UI_FLAVOR) {
            binding.settingsPersmission.visibility=View.GONE
        }else{
            binding.settingsPersmission.visibility=View.VISIBLE

        }

        binding.settingsPersmission.setOnClickListener {

        }
        binding.settingsPrivacyOpt.setOnClickListener {
            addFragment(PrivacyOptionsFragment(), true, true, R.id.container)
        }
/*
        manageToolbar()

        baseActivity.showBottomNavigation(true)

        if (FLAVOR.equals(AppUtils.APP_AS_SENSOR_FLAVOR) || FLAVOR.equals(AppUtils.APP_AS_A_TAG)) {
            layoutTripRecording.visibility = View.VISIBLE
        } else {
            layoutTripRecording.visibility = View.GONE
        }

        // TRIP RECORDING
        switchTripRecording.setImageResource(
                if (PreferencesHelper.getBooleanValue(
                                baseActivity,
                                PreferencesHelper.TRIP_RECORDING
                        )
                ) R.drawable.switch_on else R.drawable.switch_off
        )

        switchTripRecording.setOnClickListener(View.OnClickListener {
            var value = !PreferencesHelper.getBooleanValue(baseActivity, PreferencesHelper.TRIP_RECORDING)
            switchTripRecording.setImageResource(if (value) R.drawable.switch_on else R.drawable.switch_off)
            PreferencesHelper.setBooleanValue(baseActivity, PreferencesHelper.TRIP_RECORDING, value)

            if (value) {
                // initialize framework
                getCodeSdk()
            } else {
                // unsubsribe framework
                DDTripDetector.disable()
            }
        })

        // TRIP REJECTED
        switchTripRejected.setImageResource(
                if (PreferencesHelper.getBooleanValue(
                                baseActivity,
                                PreferencesHelper.SHOW_REJECTED_TRIP
                        )
                ) R.drawable.switch_on else R.drawable.switch_off
        )

        switchTripRejected.setOnClickListener(View.OnClickListener {
            var value = !PreferencesHelper.getBooleanValue(baseActivity, PreferencesHelper.SHOW_REJECTED_TRIP)
            switchTripRejected.setImageResource(if (value) R.drawable.switch_on else R.drawable.switch_off)
            PreferencesHelper.setBooleanValue(baseActivity, PreferencesHelper.SHOW_REJECTED_TRIP, value)
        })
*/
        // FINGERPRINT
        val goldfinger: Goldfinger = Goldfinger.Builder(context).build()

        if (ConfigurationUtils.FINGERPRINT && goldfinger.hasEnrolledFingerprint()) {
            fingerPrintLayout.visibility = View.VISIBLE

        } else {
            fingerPrintLayout.visibility = View.GONE

        }
        val fingerDataEdit: SharedPreferences.Editor = context!!.getSharedPreferences("FingerData", Context.MODE_PRIVATE).edit()
        val fingerDataRead: SharedPreferences = context!!.getSharedPreferences("FingerData", Context.MODE_PRIVATE)
        val isFingerEnabled=!fingerDataRead.getBoolean("fingerDisabled", false)
        switchFingerPrint.setImageResource(

                if (isFingerEnabled) {
                    R.drawable.switch_on
                } else {
                    R.drawable.switch_off
                }
        )
        switchFingerPrint.setOnClickListener(View.OnClickListener {
            var value = !fingerDataRead.getBoolean("fingerDisabled", false)
            switchFingerPrint.setImageResource(if (value) R.drawable.switch_off else R.drawable.switch_on)
            fingerDataEdit.putBoolean("fingerDisabled", value)
            fingerDataEdit.apply()
        })


    }


/*
    private fun getCodeSdk() {

        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, activity!!)
                .title(R.string.loading)
                .content("Please wait!")
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(resources.getColor(R.color.colorPrimary))
                .setProgressColor(resources.getColor(R.color.colorPrimary))
                .progress()
        progress = builder.show()

        prefs = activity!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
        val deviceId = prefs.getString("deviceId", null)
        APIRepository(activity!!).getCodeSdk(
                deviceId,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        Log.e("DEBUG", "ON FAILURE = " + t)
                        progress?.dismiss()
                    }

                    override fun onResponse(
                            call: retrofit2.Call<ResponseBody>,
                            response: retrofit2.Response<ResponseBody>
                    ) {

                        if (response.isSuccessful) {
                            val customResponse = NetworkUtis.manageResponse(response)
                            Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                            var gson = Gson()
                            val response = gson.fromJson(
                                    customResponse.body.toString(),
                                    RegistrationCode::class.java
                            )
                            registrationRequestCode = response.registrationRequestCode

                            val handlerThread = HandlerThread("MyHandlerThread")
                            handlerThread.start()
                            val looper: Looper = handlerThread.getLooper()
                            val handler = Handler(looper)
                            handler.post(Runnable() {
                                DDSDK.register(deviceId.toString(), false, "https://uat-api.octotelematics.net:56861", registrationRequestCode!!, "http://management.uat-iot-hub.octotelematics.net") { status ->

                                    Log.e("DDSDK.register-->STATUS", status.toString())
                                    progress?.dismiss()
                                    if (status) {
                                        if (context != null) {
                                            DDSDK.start(context!!)
                                        } else {
                                            Log.e("DASHBOARD", "CONTEXT = NULL")
                                        }
                                    }
                                }

                            })
                        } else {
                            progress?.dismiss()
                        }
                    }

                }
        )
    }*/

}
