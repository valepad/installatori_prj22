package com.octotelematics.installatori.fragments.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.interfaces.OnItemClickPlate
import com.octotelematics.installatori.utils.AppUtils


class PlateListAdapter(
    private val imgList: Vouchers,
    val plate: String?,
    onItemTouchListener: OnItemClickPlate
) : RecyclerView.Adapter<PlateListViewHolder>() {

    private var onItemTouchListener: OnItemClickPlate? = onItemTouchListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlateListViewHolder {
        return PlateListViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_plate,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return imgList.size
    }

    override fun onBindViewHolder(holder: PlateListViewHolder, position: Int) {
        val mImgList = imgList[position]
        holder.inprogress.visibility=View.GONE
        if(mImgList.voucherStatus==AppUtils.VoucherStatus.DRAFT.name){
          holder.configure.visibility = View.VISIBLE

      }
      else if(mImgList.voucherStatus==AppUtils.VoucherStatus.CREATED.name){
          if (plate == mImgList.motor?.plate) {
              holder.selectorActive.visibility = View.VISIBLE

          }else{
              holder.selectorActive.visibility = View.GONE

          }
      }else if(mImgList.voucherStatus==AppUtils.VoucherStatus.CREATED_NGP.name){
          holder.inprogress.visibility=View.VISIBLE
      }



        if (mImgList.motor!!.vehicleModel != null &&mImgList.motor?.vehicleMake != null ) {
            holder.model.text  = mImgList.motor?.vehicleMake +" "+mImgList.motor?.vehicleModel
        }
        holder.plate.text = mImgList.motor?.plate
        if(mImgList.voucherStatus!=AppUtils.VoucherStatus.CREATED_NGP.name){
        holder.layoutPlate.setOnClickListener {
            onItemTouchListener!!.onClick(position)
            holder.selectorActive.visibility = View.VISIBLE
           }
        }
    }
}

class PlateListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var model: TextView = itemView.findViewById(
        R.id.model
    )
    var plate: TextView = itemView.findViewById(
        R.id.plateNum
    )
    var layoutPlate: LinearLayout = itemView.findViewById(R.id.plateLayout)

    var selectorActive: FrameLayout = itemView.findViewById(R.id.selector)
    var configure: TextView = itemView.findViewById(R.id.configure)
    var inprogress: TextView = itemView.findViewById(R.id.inprogress)
}