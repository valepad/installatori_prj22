package com.octotelematics.installatori.fragments.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager.widget.ViewPager

class ViewPagerPairingCalibration(fragmentManager: FragmentManager?, lifecycle: Lifecycle?, viewpager: ViewPager, context: Context) :
    FragmentStatePagerAdapter(fragmentManager!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var viewpager: ViewPager = viewpager
    private var context: Context = context
    var feedsList: List<Fragment>? = null

    override fun getItem(position: Int): Fragment {
        return feedsList!!.get(position)
    }

    override fun getCount(): Int {
        return feedsList!!.size
    }
}