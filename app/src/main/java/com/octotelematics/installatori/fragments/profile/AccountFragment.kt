package com.octotelematics.installatori.fragments.profile

import APIRepository
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.octotelematics.installatori.BuildConfig.TSC_ID
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentAccountBinding
import com.octotelematics.installatori.main.LoginFragment
import com.octotelematics.installatori.fragments.passwordManagment.ChangePasswordFragment

import com.octotelematics.installatori.models.voucherBulkResponseCreate.User
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.fcm.FirebaseConfig
import com.octotelematics.installatori.utils.fcm.MyFirebaseMessagingService
import com.octotelematics.installatori.utils.fcm.MyFirebaseMessagingService.Companion.getFirebasePreferences
import kotlinx.android.synthetic.main.fragment_account.*
import okhttp3.ResponseBody
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 */
class AccountFragment : BaseFragment() {

    var dialog: DialogUtils? = null
    lateinit var baseActivity: BaseActivity
    private var progress: DialogUtils? = null

    private lateinit var binding: FragmentAccountBinding

    lateinit var currentUser: User
    lateinit var currentVoucher: VouchersItem

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAccountBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val baseActivity = activity as BaseActivity

        binding.accountPassword.setOnClickListener {
            changeFragment(ChangePasswordFragment(), R.id.container)
        }
        // setupNetwork()

        baseActivity.showBottomNavigation(true)
        try {
            currentUser =
                PreferencesHelper.getObjValue(baseActivity, "currentUser", User::class.java) as User
        } catch (e: Exception) {

        }
        try {
            currentUser = PreferencesHelper.getObjValue(
                requireContext(),
                "currentUser",
                User::class.java
            ) as User
            currentVoucher = PreferencesHelper.getObjValue(
                requireContext(),
                "currentVoucher",
                VouchersItem::class.java
            ) as VouchersItem
            setValues()
        } catch (e: Exception) {

        }
        binding.passwordLayout.setOnClickListener {
            addFragment(ChangePasswordFragment(), true, true, R.id.container)
        }



         account_contact_us.setOnClickListener {
            addFragment(ContactUsFragment(), true, true, R.id.container)

        }
        binding.accountLogoutBtn.setOnClickListener(View.OnClickListener {
            dialog = DialogUtils.Builder(dialog, activity!!)
                .title(R.string.confirmLogOut)
                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                .content(R.string.reallyLogOut)
                //.setMessageColor(resources.getColor(R.color.blue_text_dd))
                .positiveText(R.string.log_out)
                .negativeText(R.string.cancel)
                .onPositive(View.OnClickListener {
                    dialog!!.dismiss()
                    deleteToken()


                  PreferencesHelper.setStringValue(context!!, "email", "")
                    PreferencesHelper.setStringValue(context!!, "clientName", "")
                    PreferencesHelper.setStringValue(context!!, "clientSurname", "")
                    PreferencesHelper.setLongValue(context!!, "deviceId", 0)


                    val settings = context!!.getSharedPreferences(
                        "DigitalDriver",
                        Context.MODE_PRIVATE
                    )

                    for (i in 0 until activity?.supportFragmentManager!!.backStackEntryCount) {
                        activity?.supportFragmentManager!!.popBackStack()
                    }
                    val pref: SharedPreferences =
                        MyFirebaseMessagingService.getFirebasePreferences(activity!!)
                    val edit = pref.edit()
                    edit.remove(FirebaseConfig.PREF_REG_ID)
                    edit.remove(FirebaseConfig.PREF_SENT)
                    edit.apply()
                    FirebaseMessaging.getInstance().isAutoInitEnabled = false
                    Thread {
                        try {
                            // Remove InstanceID initiate to unsubscribe all topic
                            // TODO: May be a better way to use FirebaseMessaging.getInstance().unsubscribeFromTopic()
                            FirebaseInstanceId.getInstance().deleteInstanceId()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }.start()
                    PreferencesHelper.removeValue(context!!, PreferencesHelper.PREF_VOUCHER_ID)
                    /*Toast.makeText(context!!,  "disabled push",
                            Toast.LENGTH_LONG).show()*/
                    changeFragment(LoginFragment(), R.id.container)
                    settings.edit().clear().commit()
                    PreferencesHelper.removeValue(context!!, "seenFriends")
                }).show()
        })


    }

    @SuppressLint("CheckResult")
    fun deleteToken() {
        APIRepository(context!!).deletePushtoken(
            TSC_ID,
            object : retrofit2.Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                    Log.e("DEBUG", "ON FAILURE = " + t)
                    //   baseFragment!!.hideProgress()
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        if (context != null) {
                            val pref = getFirebasePreferences(context!!)
                            val edit = pref.edit()
                            edit.putBoolean(FirebaseConfig.PREF_SENT, true)
                            edit.apply()
                            /*   Toast.makeText(context!!,  "unregistered push",
                                       Toast.LENGTH_LONG).show()*/
                            Log.d("OctoLog", "WITH THE PUSH TOKEN IT IS ALL RIGHT")
                        }
                    }
                    //baseFragment!!. hideProgress()
                }

            }
        )


    }


    fun setValues() {

        val type =
            Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        val typeNormal =
            Typeface.createFromAsset(activity!!.assets, "fonts/solomon_book.ttf")
/*
        nameLabel.typeface = typeNormal
        surnameLabel.typeface = typeNormal
        birthdayLabel.typeface = typeNormal
        phoneLabel.typeface = typeNormal

        nameValue.typeface = type
        surnameValue.typeface = type
        birthdayValue.typeface = type
        phoneValue.typeface = type

        userId.typeface = typeNormal
        userIdValue.typeface = type
        logOutText.typeface = typeNormal
        ChangePswText.typeface = typeNormal

 */

        binding.accountName.text = currentUser.clientFirstName
        binding.accountSurname.text = currentUser.clientLastName
        binding.accountEmail.text = currentUser.clientEmail
        //TODO PASSWORD MISSING SINCE CALL DOESNT RETURN IT YET
        // birthdayValue.text = if (currentUser.birthDate != null && currentUser.birthDate!!.isNotEmpty()) getBirthday() else ""
        // phoneValue.text = currentUser.clientPhone
        //  userIdValue.text = currentUser.clientEmail
        // car_plateValue.text = currentVoucher!!.motor!!.plate
        // constructorValue.text = currentVoucher!!.motor!!.vehicleMake
        // modelValue.text = currentVoucher!!.motor!!.vehicleModel
        //    codiceFiscaleValue.text = currentUser!!.ssnnin.toString()
        //   device_type_value.text = currentVoucher.device?.deviceType

        /*  when (BuildConfig.FLAVOR) {
              AppUtils.APP_AS_UI_FLAVOR -> {
                  device_id_value.text = currentVoucher.device?.deviceId
                  device_id_layout.visibility = View.VISIBLE
                  divider_devideid.visibility = View.VISIBLE
              }
              AppUtils.APP_AS_A_TAG -> {
                  if (!currentVoucher.device?.sensors!!.isEmpty())
                      sensor_id_value.text = currentVoucher.device?.sensors?.get(0)!!.sensorId
                  sensor_id_layout.visibility = View.VISIBLE
                  dividerSensor.visibility = View.VISIBLE

              }
          }

         */


    }

/*
    fun getBirthday(): String {
        if (currentUser.birthDate != null) {
            val defaultFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ" //In which you need put here
            val sdf = SimpleDateFormat(defaultFormat, Locale.US)
            var myCalendar = Calendar.getInstance()
            myCalendar!!.time = sdf.parse(currentUser.birthDate)
            val myFormat = "dd/MM/yy" //In which you need put here
            val mySdf = SimpleDateFormat(myFormat, Locale.US)
            return mySdf.format(myCalendar?.time)
        } else {
            return ""
        }
    }

    fun setupNetwork() {
        var prefs = activity!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
        var ddToken = prefs.getString("ddToken", "")
        var voucherID = prefs.getString("voucherID", "")

        DDNetworkClient.token = ddToken
        DDNetworkClient.instance.resetAPIService()
        DDNetworkClient.instance.getInstance(
                activity!!.applicationContext,
                "https://uat-api.octotelematics.net:56861/gateway/",
                "",
                "https://oid-dev.octotelematics.com:8889/mga/sps/oauth/oauth20/",
                "password",
                "fs7u2Ie7ydFkR1c1D1E3",
                "NsU7FaFOwTAaAH12G6fP",
                object : Interceptor {
                    @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val response: Response = CustomInterceptor("dashboard").intercept(chain)
                        Log.d("MyApp", "Code : " + response.code)
                        return if (response.code === 401) {
                            if (activity != null) {

                                progress?.dismiss()

                                val editor: SharedPreferences.Editor =
                                        activity!!.getSharedPreferences(
                                                "DigitalDriver",
                                                Context.MODE_PRIVATE
                                        ).edit()
                                editor.putString("ddToken", "")
                                editor.apply()
                                for (i in 0 until activity?.supportFragmentManager!!.backStackEntryCount) {
                                    activity?.supportFragmentManager!!.popBackStack()
                                }
                                changeFragment(BaseOverviewFragment(false), R.id.container)
                            }
                            response
                        } else response
                    }
                }


        )
    }*/

}
