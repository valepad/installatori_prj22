package com.octotelematics.installatori.fragments.interfaces

import android.widget.RelativeLayout
import com.octotelematics.installatori.models.vouchers.VouchersItem

interface OnItemClickVoucher {
        fun onClick(voucher: VouchersItem, relative: RelativeLayout?, packages: MutableList<RelativeLayout>?)
}