package com.octotelematics.installatori.fragments.adapters

import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.octotelematics.installatori.R
import com.octotelematics.installatori.models.misc.FaqResponse


class FaqAdapter(private val data: FaqResponse) :
    RecyclerView.Adapter<FaqAdapterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqAdapterViewHolder {
        return FaqAdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_faq,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: FaqAdapterViewHolder, position: Int) {
        val mData = data!![position]
        holder.titlefaq.text = mData.title
        holder.descriptionfaq.text = mData.description
        holder.clickfaq.setOnClickListener {
            if (holder.descriptionfaq.visibility == View.VISIBLE) {

                TransitionManager.beginDelayedTransition( holder.click_container, AutoTransition())
                holder.descriptionfaq.visibility = View.GONE

                holder.plusminus.text = "+"

            } else {
                TransitionManager.beginDelayedTransition(holder.click_container, AutoTransition())

                holder.descriptionfaq.visibility = View.VISIBLE
                holder.plusminus.text = "-"

            }
        }
    }
}

class FaqAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var titlefaq: TextView = itemView.findViewById(
        R.id.titlefaq
    )
    var descriptionfaq: TextView = itemView.findViewById(
        R.id.descriptionfaq
    )
    var clickfaq: FrameLayout = itemView.findViewById(
        R.id.clickfaq
    )
    var plusminus: TextView = itemView.findViewById(
        R.id.plusminus
    )
    var click_container: CardView = itemView.findViewById(
        R.id.click_container
    )
}
