package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentHelpCentreBinding


class HelpCentreFragment : BaseFragment() {
    lateinit var binding: FragmentHelpCentreBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHelpCentreBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(resources.getString(R.string.profiling_help_centre), true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)

        binding.centreFaq.setOnClickListener {
            addFragment(FAQFragment(), true, true, R.id.container)
        }

        binding.centreContactUs.setOnClickListener {
            addFragment(ContactUsFragment(), true, true, R.id.container)
        }

        binding.centreFeedback.setOnClickListener {
            addFragment(GiveUsFeedbackFragment(), true, true, R.id.container)
        }

        binding.centreRate.setOnClickListener {
//TODO ADD CLICK

        }
    }

}