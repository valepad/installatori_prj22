package com.octotelematics.installatori.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.interfaces.OnItemClickVoucher
import com.octotelematics.installatori.utils.AppUtils
import kotlinx.android.synthetic.main.item_list_voucher.view.*

class VoucherListAdapter(
    private val vouchersList: Vouchers,
    private var context: Context,
    onItemTouchListener: OnItemClickVoucher
) : RecyclerView.Adapter<VoucherViewHolder>() {

    private var mContext: Context = context
    private var nameTemporary: String? = null
    private var onItemTouchListener: OnItemClickVoucher? = onItemTouchListener
    var selected: MutableList<RelativeLayout>? = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoucherViewHolder {
        return VoucherViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list_voucher,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return vouchersList.size
    }

    override fun onBindViewHolder(holder: VoucherViewHolder, position: Int) {
        val mVoucherList = vouchersList[position]
        /*
        if (mVoucherList.voucherId != null){
            holder.voucherText.text = mVoucherList.voucherId
        }else {
            holder.voucherText.text = "null"
        }
        */

        var text = mVoucherList.voucherId
        if (mVoucherList.motor?.plate != null && mVoucherList.motor?.plate!!.isNotEmpty()) {
            text = mVoucherList.motor!!.plate
        }

        if (text == null || text.isEmpty()) {
            text = mVoucherList.id.toString()
        }

        holder.voucherText.text = text

        if (mVoucherList.voucherStatus.equals("ERROR_NGP")) {
            holder.voucherStatus.visibility = View.GONE
            holder.backround.visibility = View.VISIBLE
            holder.backround.text = "ERROR"
            holder.backround.backgroundTintList =context.resources.getColorStateList(R.color.rusty_red)

        } else {
            if (mVoucherList.voucherStatus.equals("CREATED_NGP")) {
                holder.voucherStatus.visibility = View.GONE
                holder.backround.visibility = View.VISIBLE
                holder.backround.text = "IN PROGRESS"
                holder.backround.backgroundTintList =context.resources.getColorStateList(R.color.mint)


            } else {
                holder.voucherStatus.visibility = View.VISIBLE
                holder.backround.visibility = View.GONE
                holder.voucherStatus.text = mVoucherList.voucherStatus
            }
        }



        holder.carname.text =
            mVoucherList.motor!!.vehicleMake + " " + mVoucherList.motor!!.vehicleModel
        selected!!.add(holder.itemContainerVoucher)
        if (mVoucherList.voucherStatus == AppUtils.VoucherStatus.CREATED.name || mVoucherList.voucherStatus == AppUtils.VoucherStatus.REACTIVATED.name ||
            mVoucherList.voucherStatus == AppUtils.VoucherStatus.SUSPENDED.name || mVoucherList.voucherStatus == AppUtils.VoucherStatus.DRAFT.name
        ) {
            holder.itemContainerVoucher.setOnClickListener {
                //holder.itemContainerPackage.setBackgroundColor(mContext!!.resources.getColor(R.color.white_opacity))
                onItemTouchListener!!.onClick(
                    mVoucherList,
                    holder.itemContainerVoucher,
                    selected
                )
            }
        }
    }


    /* override fun onBindViewHolder(holder: VoucherViewHolder, position: Int) {
         val mPackageList = packageList[position]
         holder.packageText.text = mPackageList.name

         selected!!.add(holder.itemContainerPackage)

         holder.itemContainerPackage.setOnClickListener {
             //holder.itemContainerPackage.setBackgroundColor(mContext!!.resources.getColor(R.color.white_opacity))
             onItemTouchListener!!.onClick(
                 mPackageList.packageId,
                 holder.itemContainerPackage,
                 selected
             )
         }
     }*/
}

class VoucherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var voucherText: TextView = itemView.findViewById(
        R.id.voucherText
    )

    var voucherStatus: TextView = itemView.findViewById(
        R.id.voucherStatus
    )
    var carname: TextView = itemView.findViewById(
        R.id.carname
    )
    var backround: TextView = itemView.findViewById(
        R.id.backround
    )

    var itemContainerVoucher: RelativeLayout = itemView.itemContainerVoucher

}