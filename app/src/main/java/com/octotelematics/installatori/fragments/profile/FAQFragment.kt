package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.google.gson.Gson

import APIRepository
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.adapters.FaqAdapter
import com.octotelematics.installatori.models.misc.FaqResponse
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import okhttp3.ResponseBody
import retrofit2.Callback


class FAQFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f_a_q, container, false)
    }
    private var adapter: FaqAdapter? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //TODO ADD THE RECYCLERVIEW
        val baseActivity = activity as BaseActivity

        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.help_centre_faq),
            true,
            R.drawable.ic_back_white,
            R.drawable.ic_octo_logo_small,
            face
        )
        getFaq()
    }

    fun getFaq() {

        APIRepository(activity!!).getFAQ(
            BuildConfig.TSC_ID,
            PreferencesHelper.getStringValue(context!!, PreferencesHelper.PREF_LANGUAGE)
                ?: AppUtils.DEFAULT_LANGUAGE,
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                    Log.e("DEBUG", "ON FAILURE = " + t)
                    hideProgress()
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val customResponse = NetworkUtis.manageResponseArray(response)
                        Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                        var gson = Gson()
                        val response = gson.fromJson(
                            customResponse.body.toString(), FaqResponse::class.java
                        )
                         setFaqAdapter(response)
                    }
                    hideProgress()
                }

            }
        )

    }
    fun setFaqAdapter(list: FaqResponse) {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val recyclerView: RecyclerView? = view?.findViewById(R.id.faqlist)
        val snapHelper: SnapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)
        recyclerView?.layoutManager = layoutManager
        adapter = list?.let {
            FaqAdapter(it)
        }
        recyclerView?.adapter = adapter
    }
}