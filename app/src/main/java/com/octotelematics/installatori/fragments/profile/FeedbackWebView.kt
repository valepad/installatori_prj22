package com.octotelematics.installatori.fragments.profile

import android.os.Bundle
import android.util.ArrayMap
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import APIRepository
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_feedback_web_view.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Callback


class FeedbackWebView() : Fragment() {
    private lateinit var webView: WebView
    private var callCounter = 0
    var dialog: DialogUtils? = null
    var numButton:Int?=null
    private var progress: DialogUtils? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var listButton = ArrayList<TextView>()

        listButton.add(button0)
        listButton.add(button1)
        listButton.add(button2)
        listButton.add(button3)
        listButton.add(button4)
        listButton.add(button5)
        listButton.add(button6)
        listButton.add(button7)
        listButton.add(button8)
        listButton.add(button9)
        listButton.add(button10)

        for (i in listButton) {
            i.setOnClickListener {
                buttonlogic(i, listButton)
            }
        }
        // mostro la prima schermata con i voti
        layoutNumbers.visibility = View.VISIBLE
        message.visibility = View.GONE
        done.visibility = View.GONE
        feedback_btn.isEnabled = false
        feedback_btn.alpha = 0.5f
        feedback_btn.setOnClickListener {
            //mostro la seconda con la text area
            if (layoutNumbers.visibility == View.VISIBLE) {
                layoutNumbers.visibility = View.GONE
                message.visibility = View.VISIBLE

            }
            else{
              //mostro  la conferma
              if(message.visibility==View.VISIBLE){
                  val jsonDetail: MutableMap<String?, Any?> =
                      ArrayMap()
                  jsonDetail["text"] = editMesagge.text.toString()
                  jsonDetail["vote"] =  numButton
                  val body = ConvertRequestBody.responseBodyReturn(jsonDetail)
                  sendFeedbackCall(body)
              }else{
                  val baseActivity = activity as BaseActivity
                  baseActivity.onBackPressed()
                  baseActivity.onBackPressed()
              }
            }

        }

    }
    fun showprogressLoading(){
        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
            .title(R.string.loading)
            .content("Please wait!")
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(resources.getColor(R.color.mint))
            .setProgressColor(resources.getColor(R.color.mint))
            .progress()
        progress = builder.show()
    }
    fun checkProgress() {
        callCounter--
        if (callCounter == 0) {
            progress?.dismiss()
        }
    }
        fun sendFeedbackCall(body: RequestBody){
            showprogressLoading()
     APIRepository(activity!!).setFeedback(
         PreferencesHelper.getStringValue(context!!, PreferencesHelper.PREF_LANGUAGE) ?: AppUtils.DEFAULT_LANGUAGE,
         BuildConfig.TSC_ID,
         body,
         object : Callback<ResponseBody> {
             override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                 checkProgress()
             }

             override fun onResponse(
                 call: retrofit2.Call<ResponseBody>,
                 response: retrofit2.Response<ResponseBody>
             ) {

                 try {
                     if (response.isSuccessful) {
                         progress!!.dismiss()
                         message.visibility=View.GONE
                         done.visibility=View.VISIBLE
                         feedback_btn.setText(resources.getString(R.string.backtohome))

                         checkProgress()
                     } else {
                         checkProgress()

                         if (response.code() != 401) {
                             dialog = DialogUtils.Builder(dialog, activity!!)
                                 .title("ERRORE")
                                 .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                 .content("Errore nella chiamata!")
                                 .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                 .positiveText("Close")
                                 .show()
                         }
                     }
                 } catch (e: java.lang.Exception) {
                 }
             }
         }
     )
 }

    private fun buttonlogic(buttonSelected: TextView, listButton: ArrayList<TextView>) {
       numButton=buttonSelected.text.toString().toInt()
        for (b in listButton) {
            feedback_btn.isEnabled = true
            feedback_btn.alpha = 1f
            if (b != buttonSelected) {
                b.backgroundTintList = resources.getColorStateList(R.color.grey_text_feedback)
                b.setTextColor(resources.getColorStateList(R.color.dark_blue_text))
            } else {
                b.setTextColor(resources.getColorStateList(R.color.white))
                buttonSelected.backgroundTintList = resources.getColorStateList(R.color.mint)
            }


        }
    }
}