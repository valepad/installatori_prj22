package com.octotelematics.installatori.fragments.interfaces


interface OnItemClickPlate {
    fun onClick(position: Int)
}