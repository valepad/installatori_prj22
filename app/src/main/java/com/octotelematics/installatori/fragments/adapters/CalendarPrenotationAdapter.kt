package com.octotelematics.installatori.fragments.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.interfaces.OnItemClickCalendarPrenotation
import com.octotelematics.installatori.models.calendar.PrenotationItem


class CalendarPrenotationAdapter(
    val prenotations: MutableList<PrenotationItem?>,
    onItemTouchListener: OnItemClickCalendarPrenotation
) : RecyclerView.Adapter<CalendarPrenotationViewHolder>() {

    private var onItemTouchListener: OnItemClickCalendarPrenotation? = onItemTouchListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarPrenotationViewHolder {
        return CalendarPrenotationViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_calendar_prenotation,
                    parent,
                    false
                )
        )
    }



    override fun getItemCount(): Int {
      return  prenotations.size
    }
    override fun onBindViewHolder(holder: CalendarPrenotationViewHolder, position: Int) {
        val prenotation = prenotations[position]
        holder.date.text=prenotation?.data
        holder.status.text=prenotation?.status
        holder.date.text=prenotation?.data

    }


}
class CalendarPrenotationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var hours: TextView = itemView.findViewById(
        R.id.hours
    )
    var status: TextView = itemView.findViewById(
        R.id.status
    )
    var date: TextView = itemView.findViewById(
        R.id.date
    )

}