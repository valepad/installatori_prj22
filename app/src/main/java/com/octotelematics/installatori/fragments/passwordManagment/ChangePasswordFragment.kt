package com.octotelematics.installatori.fragments.passwordManagment

import APIRepository
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.ArrayMap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.google.android.material.chip.Chip
import com.google.gson.Gson

import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentChangePasswordBinding
import com.octotelematics.installatori.models.accessTokenResponse.AccessToken
import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.DateUtils
import com.octotelematics.installatori.utils.NetworkUtils
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_EMAIL_USER
import kotlinx.android.synthetic.main.fragment_change_password.*
import okhttp3.ResponseBody
import retrofit2.Callback
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * A simple [Fragment] subclass.
 */
class ChangePasswordFragment : BaseFragment() {

    private lateinit var binding: FragmentChangePasswordBinding
    private lateinit var oldPassText: EditText
    private lateinit var newPassText: EditText
    private lateinit var newConfPassText: EditText
    private lateinit var chipChar: Chip
    private lateinit var chipUpper: Chip
    private lateinit var chipSymbol: Chip
    private lateinit var chipLower: Chip
    private lateinit var chipNumber: Chip
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChangePasswordBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(resources.getString(R.string.profiling_change_password), true,R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)
        chipChar = view.findViewById(R.id.choose_new_pass_chip_char)
        chipUpper = view.findViewById(R.id.choose_new_pass_chip_uppercase)
        chipSymbol = view.findViewById(R.id.choose_new_pass_chip_symbol)
        chipLower = view.findViewById(R.id.choose_new_pass_chip_lowercase)
        chipNumber = view.findViewById(R.id.choose_new_pass_chip_number)

//        baseActivity.showToolbar(false)
  //      baseActivity.showBottomNavigation(false)
        val type =
            Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        val typeNormal =
            Typeface.createFromAsset(activity!!.assets, "fonts/solomon_book.ttf")

        oldPassText = binding.changePasswordOldPassText
        newPassText = binding.changePasswordNewPassText
        newConfPassText = binding.changePasswordConfirmNewPassText

        setUpOnListeners()

        binding.changePassBtn.setOnClickListener {
            changePassBtn()
        }

    }


    private fun setUpOnListeners() {

  /*      newConfPassText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                binding.changePassBtn.isEnabled =
                    isPasswordValid(newConfPassText.text.toString(), newPassText.text.toString())
            }

        })*/

        change_password_confirm_new_pass_text.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                isPasswordValid(s.toString())
                change_pass_btn.isEnabled =
                    chipChar.isChecked and chipLower.isChecked and chipNumber.isChecked and chipSymbol.isChecked and chipUpper.isChecked
            }
        })
    }

    fun checkRegex(password: String, regexExp: String): Boolean {
        val regExpn = (regexExp)
        val inputStr: CharSequence = password
        val pattern: Pattern = Pattern.compile(regExpn)
        val matcher: Matcher = pattern.matcher(inputStr)
        return matcher.matches()
    }
    private fun isPasswordValid(newPwd: String): Boolean {
        val regExpLength = ("(?=.{8,})")
        chipChar.isChecked = newPwd.length >= 8
        val regExpUpp = (".*[A-Z].*")
        chipUpper.isChecked = checkRegex(newPwd, regExpUpp)
        val regExpNum = (".*[0-9]+.*")
        chipNumber.isChecked = checkRegex(newPwd, regExpNum)
        val regExpLower = (".*[a-z]+.*")
        chipLower.isChecked = checkRegex(newPwd, regExpLower)
        val regExpSpecialChar = (".*[!&^%\$#@()/,.]+.*")
        chipSymbol.isChecked = checkRegex(newPwd, regExpSpecialChar)
        return true
    }

   /* private fun isPasswordValid(newPwd: String, newConfPwd: String): Boolean {

        val pwLenght = newPwd.length >= 8

        val pwUpper = checkRegex(newPwd, ".*[A-Z].*")

        val pwNum = checkRegex(newPwd, ".*[0-9]+.*")

        val pwLower = checkRegex(newPwd, ".*[a-z]+.*")

        val pwChar = checkRegex(newPwd, ".*[!&^%\$#@()/,.]+.*")

        val pwEqual = newPwd.equals(newConfPwd)

        return (pwLenght && pwUpper && pwNum && pwLower && pwChar && pwEqual)
    }
*/ private var progress: DialogUtils? = null
    private fun changePassBtn() {

        val oldPsw =   PreferencesHelper.getStringValue(context!!, PreferencesHelper.FINGER_DATA,PreferencesHelper.PREF_PSW_FINGER
        )

        /* val jsonHeaders: MutableMap<String?, Any?> =
                 ArrayMap()
             jsonHeaders["E2ETrackingId"] = "test-27022020-1224"
             jsonHeaders["EventId"] = "test-27022020-1224"
             jsonHeaders["EventDetailId"] = "test-27022020-1224"
             jsonHeaders["ConsumerId"] = "app"
             jsonHeaders["CreationTime"] = "2020-06-15T18:13:38"
             val jsonProfile: MutableMap<String?, Any?> =
                 ArrayMap()*/
        //jsonProfile["UserID"] = "oct-a0000010314.uat"
        //jsonProfile["Email"] = email
        progress = DialogUtils.showProgressDialog(progress, context as BaseActivity)
        val jsonDetail: MutableMap<String?, Any?> =
            ArrayMap()
       jsonDetail["oldPassword"] = change_password_old_pass_text.text.toString()
       jsonDetail["newPassword"] = newPassText.getText().toString()
       /* jsonProfile["Details"] = jsonDetail
        val jsonParams: MutableMap<String?, Any?> =
            ArrayMap()
        jsonParams["Header"] = jsonHeaders
        jsonParams["Profile"] = jsonProfile*/
       val bodyEmailPsw = ConvertRequestBody.responseBodyReturn(jsonDetail)
       progress!!.show()
       DDNetworkClient.instance.resetAPIService()
       DDNetworkClient.instance.resetAPIService()
       NetworkUtils.setupDDNetworkClient(activity!!)
       var dialog: DialogUtils? = null
       val bodyToken = NetworkUtils.getBodyAccessToken()
       APIRepository(context!!).getAccessToken(
           bodyToken,
           object: Callback<ResponseBody> {
               override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                   Log.e("DEBUG", "ON FAILURE = " + t)
                   dialog = DialogUtils.Builder(dialog, context!!)
                       .title("ERRORE")
                       .setTitleColor(context!!.resources.getColor(R.color.dark_blue_text))
                       .content("Errore nella chiamata!")
                       .setMessageColor(context!!.resources.getColor(R.color.dark_blue_text))
                       .positiveText("Close")
                       .show()
               }

               override fun onResponse(
                   call: retrofit2.Call<ResponseBody>,
                   response: retrofit2.Response<ResponseBody>
               ) {

                   val customResponse = NetworkUtis.manageResponse(response)
                   Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                   if (customResponse.code < 400){
                       var gson = Gson()
                       val prefes: SharedPreferences =
                           context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
                       val email = prefes.getString(PREF_EMAIL_USER, "")
                       val tokenResponse = gson.fromJson(customResponse.body.toString(), AccessToken::class.java)
                       val tokenRegistration = tokenResponse.access_token
                       APIRepository(context!!).changeTemporaryPsw(
                                        "Bearer $tokenRegistration",
                                        BuildConfig.TSC_ID,
                                        email ?: "",
                                        System.currentTimeMillis().toString(),
                                        System.currentTimeMillis().toString(),
                                        DateUtils.getDateString(
                                            Date(System.currentTimeMillis()),
                                            "yyyy-MM-dd'T'HH:mm:ss"
                                        )!!,
                                        bodyEmailPsw,
                                        object : Callback<ResponseBody> {
                                            override fun onFailure(
                                                call: retrofit2.Call<ResponseBody>,
                                                t: Throwable
                                            ) {
                                                progress!!.dismiss()
                                                dialog = DialogUtils.Builder(dialog, context!!)
                                                    .title("")
                                                    .setTitleColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                    .content("Password aggiornata")
                                                    .setMessageColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                    .positiveText("Close")
                                                    .show()


                                            }

                                            override fun onResponse(
                                                call: retrofit2.Call<ResponseBody>,
                                                response: retrofit2.Response<ResponseBody>
                                            ) {
                                                if (response.isSuccessful) {
                                                    progress!!.dismiss()
                                                    dialog = DialogUtils.Builder(dialog, context!!)
                                                        .title("ERRORE")
                                                        .setTitleColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                        .content("Password aggiornata")
                                                        .setMessageColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                        .positiveText("Close")
                                                        .show()
                                                    val baseActivity = activity as BaseActivity
                            AppUtils.logout(baseActivity)

                                                } else {
                                                    var errorMessage:String?=null
                                                    val errorresponse = NetworkUtis.manageResponse(response)
                                                    if (errorresponse.body.has("errorMessage")) {
                                                        errorMessage = errorresponse.body.getString("errorMessage")
                                                    }
                                                    progress!!.dismiss()
                                                    dialog = DialogUtils.Builder(dialog, context!!)
                                                        .title("ERRORE")
                                                        .setTitleColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                        .content(errorMessage)
                                                        .setMessageColor(context!!.resources.getColor(R.color.dark_blue_text))
                                                        .positiveText("Close")
                                                        .show()

                                                }
                                            }
                                        })
                                } else {
                       progress!!.dismiss()
                       dialog = DialogUtils.Builder(dialog, context!!)
                           .title("ERRORE")
                           .setTitleColor(context!!.resources.getColor(R.color.dark_blue_text))
                           .content("Errore nella chiamata!")
                           .setMessageColor(context!!.resources.getColor(R.color.dark_blue_text))
                           .positiveText("Close")
                           .show()
                                }
                            }
                        })
                    // this may be extremely wrong but ok

                }




}
