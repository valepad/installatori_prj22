package com.octotelematics.installatori.fragments.profile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.octotelematics.conTe.fragments.RoadsideAssistanceFragment;
import com.octotelematics.installatori.R;


public class RoadAssistanceDialog implements View.OnClickListener {
    private Context context;
    private Activity activity;
    private ImageView ivContent;
    private TextView btPositive;
    private Button btNegative;
    private TextView tvCancel;
    private TextView tvTitle;
    private TextView tvContent;
    //    private ProgressBar progressBar;
    private ImageView ivLoading;
    private LinearLayout layoutMessage;
    private LinearLayout layoutProgress;
    private AlertDialog alertDialog;
    private Builder builder;
    private int durationTimeout = 15;
    private DialogType type;
    private RoadsideAssistanceFragment roadfrag;
    private CountDownTimer countDownTimer = new CountDownTimer(durationTimeout * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            setType(DialogType.FAIL);
        }
    };

    public RoadAssistanceDialog(Context context, Builder builder, Activity activity) {
        this.context = context;
        this.builder = builder;
        this.activity = activity;
        type = builder.getType();
        roadfrag = builder.roadfrag;

        alertDialog = setupLayout();
        setContentLayout(type);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

    public DialogType getType() {
        return type;
    }

    public void setType(DialogType type) {
        this.type = type;
        builder.setType(type);
        countDownTimer.cancel();
        if (alertDialog.isShowing())
            alertDialog.dismiss();
        setContentLayout(type);
        alertDialog.show();
    }

    public Builder getBuilder() {
        return builder;
    }

    public void show() {
        if (alertDialog != null)
            alertDialog.show();
    }

    private AlertDialog setupLayout() {
        View rootView = LayoutInflater.from(context).inflate(R.layout.dialog_road_assistance, null);
        layoutMessage = rootView.findViewById(R.id.layout_message);
        layoutProgress = rootView.findViewById(R.id.layout_progress);
        ivContent = rootView.findViewById(R.id.iv_content);
//        progressBar = rootView.findViewById(R.id.progress_bar);
        ivLoading = rootView.findViewById(R.id.iv_loading);
        btPositive = rootView.findViewById(R.id.bt_positive);
        btNegative = rootView.findViewById(R.id.bt_negative);
        tvCancel = rootView.findViewById(R.id.tv_cancel);
        tvTitle = rootView.findViewById(R.id.tv_title);
        tvContent = rootView.findViewById(R.id.tv_content);
        tvCancel.setOnClickListener(this);
        btPositive.setOnClickListener(this);
        btNegative.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(rootView);
        return builder.create();
    }

    private void setContentLayout(DialogType type) {
        if (type == DialogType.LOADING) {
            layoutProgress.setVisibility(View.VISIBLE);
            layoutMessage.setVisibility(View.GONE);
            btNegative.setVisibility(View.VISIBLE);
            btPositive.setVisibility(View.GONE);
            tvCancel.setVisibility(View.GONE);
            RotateAnimation rotateAnimation = new RotateAnimation(0, 360,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(1500);
            rotateAnimation.setRepeatCount(Animation.INFINITE);
            rotateAnimation.setInterpolator(new LinearInterpolator());
            ivLoading.startAnimation(rotateAnimation);
            countDownTimer.start();
        }
       /* else if (type == DialogType.FAIL) {
            layoutProgress.setVisibility(View.GONE);
            layoutMessage.setVisibility(View.VISIBLE);
            btPositive.setVisibility(View.VISIBLE);
            btNegative.setVisibility(View.GONE);
            tvCancel.setVisibility(View.VISIBLE);
            ivContent.setImageDrawable(context.getDrawable(R.drawable.ic_fail_gps));
            btPositive.setText("Riprova");
            tvTitle.setText("Non siamo riusciti a trovarti");
            tvContent.setText("Non siamo riusciti a localizzare il tuo veicolo, assicurati che i servizi di localizzazione siano attivi e riprova\n");
        } */
        else if (type == DialogType.SUCCESS) {
            layoutProgress.setVisibility(View.GONE);
            layoutMessage.setVisibility(View.VISIBLE);
            btPositive.setVisibility(View.VISIBLE);
            btNegative.setVisibility(View.VISIBLE);
            tvCancel.setVisibility(View.GONE);
            ivContent.setImageDrawable(context.getDrawable(R.drawable.group_14));
            btPositive.setText(context.getResources().getString(R.string.callnow));
            tvTitle.setText(context.getResources().getString(R.string.titlepopupbcall));
            tvContent.setText(context.getResources().getString(R.string.messagepopupbcall));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_positive:
                countDownTimer.cancel();
                alertDialog.dismiss();
            /*    if (btPositive.getText().equals("Riprova") && getType() == DialogType.FAIL) {
                    if (builder.getOnTryAgainListener() != null)
                        builder.getOnTryAgainListener().onTryAgain();
                } else {
*/
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + alertDialog.getContext().getResources().getString(R.string.number_road_assistance)));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        context.startActivity(intent);
                    }else {
                        roadfrag.callPermission();
                    }



             //   }
                break;
            case R.id.bt_negative:
                countDownTimer.cancel();
                alertDialog.dismiss();
                if (builder.getOnCancelListener() != null && getType() == DialogType.LOADING) {
                    builder.getOnCancelListener().onCancel();
                }
                break;

            case R.id.tv_cancel:
                countDownTimer.cancel();
                alertDialog.dismiss();
                break;
        }
    }


    public enum DialogType {LOADING, SUCCESS, FAIL}

    public interface OnTryAgainListener {
        void onTryAgain();
    }

    public interface OnCancelListener {
        void onCancel();
    }

    public static class Builder {
        private Context context;
        private DialogType type;
        private Activity activity;
        private OnTryAgainListener onTryAgainListener;
        private OnCancelListener onCancelListener;
        private RoadsideAssistanceFragment roadfrag;

        public Builder(Context context, RoadsideAssistanceFragment roadfrag, DialogType type, Activity activity) {
            this.context = context;
            this.type = type;
            this.activity = activity;
            this.roadfrag = roadfrag;
        }

        public DialogType getType() {
            return type;
        }

        public Builder setType(DialogType type) {
            this.type = type;
            return this;
        }

        public RoadAssistanceDialog build() {
            return new RoadAssistanceDialog(context, this, activity);
        }

        public RoadAssistanceDialog show() {
            RoadAssistanceDialog dialog = new RoadAssistanceDialog(context, this, activity);
            dialog.show();
            return dialog;
        }

        OnTryAgainListener getOnTryAgainListener() {
            return onTryAgainListener;
        }

        public Builder setOnTryAgainListener(OnTryAgainListener listener) {
            onTryAgainListener = listener;
            return this;
        }

        public OnCancelListener getOnCancelListener() {
            return onCancelListener;
        }

        public Builder setOnCancelListener(OnCancelListener listener) {
            this.onCancelListener = listener;
            return this;
        }
    }
}
