package com.octotelematics.installatori.fragments.profile

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentGiveUsFeedbackBinding


class GiveUsFeedbackFragment : BaseFragment() {
    lateinit var binding: FragmentGiveUsFeedbackBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGiveUsFeedbackBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        val baseActivity = activity as BaseActivity

        //toolbar
        baseActivity.setDarkToolbar(resources.getString(R.string.feedback_toolbar), true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face)


        binding.feedbackBtn.setOnClickListener {
            addFragment(FeedbackWebView(), true, true, R.id.container)
        }
    }
}
