package com.octotelematics.installatori.fragments.vas

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import com.octotelematics.installatori.models.geofence.PoiItem
import com.octotelematics.installatori.models.geofence.PoiRequestItem
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.R
import kotlinx.android.synthetic.main.fragment_geofence.*

class GeofenceFragment : BaseFragment(),
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener,
    SeekBar.OnSeekBarChangeListener{
     private var progress: DialogUtils? = null
    private val MAX_KM = 20
    private val METERS_IN_KM = 1000
    private var DEFAULT_LATITUDE = 42.094395
    private var DEFUALT_LONGITUDE = 13.098726

    var dialog: DialogUtils? = null

    private var retrievePoiObj: PoiItem? = null
    private var isGetTargetArea: Boolean? = null

    private var mGoogleMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var locationRequest: LocationRequest? = null
    private var userLocation: Location? = null
    private var isFirstPermissionRequest: Boolean = true

    private var poiCircle: Circle? = null
    private var selectedPoiLatLng: LatLng? = null
    private var boolSelectedLatLng: Boolean = false
    private var isGeoActive: Boolean = false
    private var radiusValue: Int = 50
    private var isGeoSwitchActive = false
    private var firstTargetArea: Boolean = false
    private var poiLatitude:Double? = null
    private var poiLongitude:Double? = null
    private var baseActivity: BaseActivity? = null
    var mapFragment: SupportMapFragment? = null
    var dialogUtils: DialogUtils? = null
    var isCheck: Boolean? = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        baseActivity = activity as BaseActivity
        baseActivity?.showBottomNavigation(false)

        mGoogleApiClient = GoogleApiClient.Builder(baseActivity!!)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10000)

        return inflater.inflate(R.layout.fragment_geofence, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("onViewCreated","***")


        baseActivity = activity as BaseActivity
        baseActivity!!.showToolbar(true)
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity!!.setDarkToolbar(
           "Target Area",
            true, R.drawable.ic_back_white, R.drawable.ic_octo_logo_small, face
        )



        baseActivity!!.showBottomNavigation(true)

        rectanglegeo.setOnClickListener { }
        sub_container.setOnClickListener { }
        seekbar.setOnSeekBarChangeListener(this)
        seekbar.max = MAX_KM
        save.alpha = 0.3F


        mapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment!!.onCreate(savedInstanceState)
        mapFragment!!.getMapAsync { map ->
            val defaultLatLng = LatLng(DEFAULT_LATITUDE,DEFUALT_LONGITUDE)
            map.uiSettings.isRotateGesturesEnabled = false
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLatLng,11F))
            this.mGoogleMap = map

            val btnMyLocation: ImageView = view.findViewById(R.id.image_button_position)
            var cameraUpdate: CameraUpdate? = null

            btnMyLocation.setOnClickListener(View.OnClickListener { // TODO Auto-generated method stub
                if (userLocation != null) {
                    val latLng = LatLng(
                        userLocation!!.latitude, userLocation!!.longitude
                    )
                    cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12f)
                    map.animateCamera(cameraUpdate)
                }
            })


            loaderGetData()
            getTargetArea()

            map.setOnMarkerClickListener { marker ->
                if (userLocation!=null) {
                    if (marker.position.latitude == userLocation!!.latitude && marker.position.longitude == userLocation!!.longitude) {

                        map.clear()
                        save.alpha = 1F
                        save.isEnabled = true
                        poiLatitude = userLocation!!.latitude
                        poiLongitude = userLocation!!.longitude
                        val markerLatLng = LatLng(poiLatitude!!,poiLongitude!!)
                        selectedPoiLatLng = markerLatLng

                        addMarkerToMap(map, userLocation!!.latitude, userLocation!!.longitude, R.drawable.custom_thumb_darkblue, null, -1F, -1F, context!!)
                        addMarkerToMap(mGoogleMap!!, userLocation!!.latitude, userLocation!!.longitude, R.drawable.custom_thumb_darkblue, null, 0.5F, 0.5F, context!!)

                       /* poiCircle = addRadiusArea(
                            userLocation!!.latitude,
                            userLocation!!.longitude,
                            map,
                            R.color.slate_transparent,
                            radiusValue
                        )*/

                        isCheck = true
                        if (isCheck!!){
                            setBottomLayoutOn()
                            switchGeofence.setImageDrawable(resources.getDrawable(R.drawable.button_toggle_on))
                        }
                    }
                }
                false
            }


            map.setOnMapClickListener { clickLatLng ->
                map.clear()
                save.alpha = 1F
                save.isEnabled = true
                selectedPoiLatLng = clickLatLng

                poiLatitude = clickLatLng.latitude
                poiLongitude = clickLatLng.longitude

                if (userLocation != null) {

                    userLocation?.let { addMarkerToMap(map, it.latitude, it.longitude, R.drawable.my_position, null, -1F, -1F, context!!)
                    }

                   /* poiCircle = addRadiusArea(
                            userLocation!!.latitude,
                            userLocation!!.longitude,
                            map,
                            R.color.slate_transparent_green,
                            50

                    )*/
                }

                addMarkerToMap(map, clickLatLng.latitude, clickLatLng.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F, context!!)
                poiCircle = addRadiusArea(clickLatLng.latitude, clickLatLng.longitude, map, R.color.slate_transparent, radiusValue)

                isCheck = true
                if (isCheck!!){
                    setBottomLayoutOn()
                    switchGeofence.setImageDrawable(resources.getDrawable(R.drawable.button_toggle_on))
                }
            }

            switchGeofence.setOnClickListener { view ->

                Log.e("switchGeofence","***")
               // isGeoSwitchActive =
               // isGeoActive = isActive
                isCheck = !isCheck!!
                isGeoSwitchActive = isCheck!!
                         isGeoActive = isCheck!!
                setTargetArea()

                if(switchGeofence.isPressed){
                    save.alpha = 1F
                    save.isEnabled = true
                }


                if(isCheck!!){
                    setBottomLayoutOn()
                }else{
                    if(retrievePoiObj != null){
                        if (retrievePoiObj!!.active == false) {
                            setBottomLayoutOff(true)
                        } else {
                            setBottomLayoutOff(false)
                        }
                    }
                }
            }
        }



        save.setOnClickListener {
            if (save.alpha==0.3F){
                save.isEnabled = false
            }else{
                save.alpha = 0.3F
                try {
                    loaderSetData()
                    setTargetArea()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }

    }


    fun refreshUI(){
        if(!isGeoSwitchActive) {
            isGeoActive = false
            boolSelectedLatLng = true
            Log.e("geo non attivo.-switch","***")

            seekbar.thumb  = resources.getDrawable(R.drawable.custom_thumb_off)
            saveOpacity.visibility = View.VISIBLE
            save.visibility = View.GONE
            mGoogleMap?.clear()
            userLocation?.let {
                addMarkerToMap(
                        mGoogleMap!!,
                        it.latitude,
                        it.longitude,
                        R.drawable.my_position,
                        null,
                        -1f,
                        -1f,
                        context!!
                )
            }


            if(selectedPoiLatLng!=null){
                addMarkerToMap(mGoogleMap!!, selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F, context!!
                )

                /* poiCircle = addRadiusArea(
                     selectedPoiLatLng!!.latitude,
                     selectedPoiLatLng!!.longitude,
                     map,
                     R.color.slate_transparent,
                     radiusValue * 100
                 )*/

            }else{
                if (retrievePoiObj != null) {
                    retrievePoiObj!!.let {
                        addMarkerToMap(mGoogleMap!!, it.latitude, it.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F, context!!)

                        /*  poiCircle = addRadiusArea(
                              it.latitude,
                              it.longitude,
                              map,
                              R.color.slate_transparent,
                              retrievePoiObj!![0].radius * 100
                          )*/

                    }
                }
            }



        }else{
            // mGoogleMap?.clear()
            isGeoActive = true
            Log.e("geo attivo.-switch","***")
            if (saveOpacity != null){
            saveOpacity.visibility = View.GONE
            save.visibility = View.VISIBLE
            seekbar.thumb = resources.getDrawable(R.drawable.custom_thumb)
            save.alpha = 0.3F
            userLocation?.let {
                addMarkerToMap(mGoogleMap!!, it.latitude, it.longitude, R.drawable.my_position, null, -1f, -1f, context!!
                )
            }

            if (selectedPoiLatLng!=null && boolSelectedLatLng) {

                addMarkerToMap(mGoogleMap!!, selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F, context!!)

                poiCircle = addRadiusArea(
                    selectedPoiLatLng!!.latitude,
                    selectedPoiLatLng!!.longitude,
                    mGoogleMap!!,
                    R.color.slate_transparent,
                    radiusValue
                )
            }
            }else{
                boolSelectedLatLng = true
                /*  userLocation?.let {
                      addMarkerToMap(
                          map,
                          it.latitude,
                          it.longitude,
                          R.drawable.markergeofence,
                          null,
                          0.5F,
                          0.5F,
                          context!!
                      )
                      poiCircle = addRadiusArea(
                          it.latitude,
                          it.longitude,
                          map,
                          R.color.slate_transparent,
                          1
                      )
                  }*/
                //if (retrievePoiObj != null) {
                // retrievePoiObj!!.let {
                /* addMarkerToMap(
                     map,
                     it.latitude,
                     it.longitude,
                     R.drawable.markergeofence,
                     null,
                     0.5F,
                     0.5F,
                     context!!
                 )*/
                // }
                //}
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e("onResume","***")

        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("non ci sono i permessi","***")

            if (isFirstPermissionRequest) {
                ActivityCompat.requestPermissions(baseActivity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
                Log.e("chiedi i permessi","***")

                isFirstPermissionRequest = false
            }
        } else {

            Log.e("i permessi ci sono","***")

            val locationManager =
                baseActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps()
                Log.e("gps spento","***")

            } else {
                mGoogleApiClient?.connect()
            }
        }

    }

    private fun handleNewLocation(location: Location) {
        Log.e("handleNewLocation", "***")

        userLocation = location

        val lat = location.latitude
        val long = location.longitude

        val latLngUser = LatLng(lat,long)
        val markerUserTemp = MarkerOptions().position(latLngUser).anchor(0.5f, 0.5f)

        mGoogleMap?.clear()
        mGoogleMap?.let { map ->


            if(userLocation!=null){
                Log.e("userLocation non è null","***")
                addMarkerToMap(map, userLocation!!.latitude, userLocation!!.longitude, R.drawable.my_position, null, -1f, -1f,context!!)
               /* poiCircle = addRadiusArea(lat, long, map, R.color.slate_transparent_green, 50)*/
               // map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngUser,11.0f))
            }else{
                 // markerUserTemp
                    val m = map.addMarker(markerUserTemp)
                    m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.start_end_point_drawable))
                    //map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngUser,11.0f))

                Log.e("userLocation  è null","***")
            }


            userLocation?.let {

                addMarkerToMap(map, it.latitude, it.longitude,R.drawable.my_position, null, -1f, -1f,context!!)
            }


            if (!isGeoSwitchActive){
                isGeoActive = false

                Log.e("geofence non attivo", "***")

                if (selectedPoiLatLng!=null){
                    addMarkerToMap(map, selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F,context!!)
                   // poiCircle = addRadiusArea(selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, map, R.color.slate_transparent, radiusValue)
                }else{

                   /* retrievePoiObj?.let {
                        Log.e("poi not null ","***")
                        poiLatitude?.let {lat->
                            poiLongitude?.let {long->
                                Log.e("popolati","***")
                                addMarkerToMap(map, lat,long, R.drawable.markergeofence, null, 0.5F, 0.5F,context!!)
                                poiCircle = addRadiusArea(lat, long, map, R.color.slate_transparent, radiusValue * 100)
                            }
                        }

                    }*/
                }

            }else{
                isGeoActive = true
                Log.e("geofence attivo", "***")

                if(selectedPoiLatLng!=null){
                    addMarkerToMap(map, selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, R.drawable.markergeofence, null, 0.5F, 0.5F,context!!)
                    poiCircle = addRadiusArea(selectedPoiLatLng!!.latitude, selectedPoiLatLng!!.longitude, map, R.color.slate_transparent, radiusValue)

                }else{
                    retrievePoiObj?.let {
                        poiLatitude?.let{ lat->
                            poiLongitude?.let {long->
                                addMarkerToMap(map, lat,long, R.drawable.markergeofence, null, 0.5F, 0.5F,context!!)
                                poiCircle = addRadiusArea(lat, long, map, R.color.slate_transparent, radiusValue)
                            }
                        }
                    }
                }

            }
        }
    }

    override fun onConnected(p0: Bundle?) {

        Log.e("onConnected", "***")

        val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            Log.e("location è null","***")

            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                locationRequest,
                this
            )
        } else {
            handleNewLocation(location)
        }
    }

    override fun onLocationChanged(locationChanged: Location?) {
        if (locationChanged != null) {
            handleNewLocation(locationChanged)
        }
    }

    override fun onProgressChanged(seekbar: SeekBar?, progress: Int, p2: Boolean) {
        if (progress == 0){
            km_value.text = "1"
            radiusValue = (1 * METERS_IN_KM)
            poiCircle?.radius = radiusValue.toDouble()
        }else {
            km_value.text = progress.toString()
            radiusValue = (progress * METERS_IN_KM)
            poiCircle?.radius = radiusValue.toDouble()
        }
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        save.alpha = 1F
        save.isEnabled = true
    }

    private fun setBottomLayoutOn(){
        //description_target.text = getText(R.string.seekbar_label)
        seekbar.setProgressDrawable(resources.getDrawable(R.drawable.custom_seekbar))
        seekbar.setOnTouchListener{ view: View, motionEvent: MotionEvent -> false }
        seekbar.setProgressDrawable(resources.getDrawable(R.drawable.custom_seekbar))
        seekbar.thumb  = resources.getDrawable(R.drawable.custom_thumb)
        tv_km_opacity.visibility = View.GONE
        save.visibility = View.VISIBLE
        saveOpacity.visibility = View.GONE
        switchGeofence.setImageDrawable(resources.getDrawable(R.drawable.button_toggle_on))
    }

    /**
     * isFirstAccess:
     * se l'utente non ha ancora settato un poi viene mostrato il messaggio A
     * se ha già settato un punto di interesse viene mostrato il messaggio B
     */
    private fun setBottomLayoutOff(isFirstAccess:Boolean){
        Log.e("setBottomLayoutOff","***")
       /* description_target.text = if(isFirstAccess){
            getText(R.string.seekbar_label_first)
        }else{
            getText(R.string.seekbar_label)
        }*/

        seekbar.setProgressDrawable(resources.getDrawable(R.drawable.custom_seekbar_off))
        seekbar.thumb  = resources.getDrawable(R.drawable.custom_thumb_off)
        seekbar.setOnTouchListener { view: View, motionEvent: MotionEvent -> true }
        tv_km_opacity.visibility = View.VISIBLE
        save.visibility = View.GONE
        saveOpacity.visibility = View.VISIBLE
        switchGeofence.setImageDrawable(resources.getDrawable(R.drawable.btn_toggle_off))
    }

    @SuppressLint("CheckResult")
    private fun setTargetArea(){

        if(retrievePoiObj != null){

            var poiItem = PoiRequestItem(isGeoActive,1,poiLatitude!!,poiLongitude!!,"Both",radiusValue / 10,51)

            var prefs: SharedPreferences =
                context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
            val deviceId = prefs.getString("deviceId", null)

            val bodyToken = ConvertRequestBody.responseBodyReturn(poiItem)
              progress = DialogUtils.showProgressDialog(progress, context as BaseActivity)
  /*          APIRepository(context!!).setTargetArea(
                retrievePoiObj!!.id,
                deviceId.toString(), //TODO: ADD DYNAMIC DEVICEID
                bodyToken,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        Log.e("DEBUG", "ON FAILURE = " + t)
                     progress?.dismiss()
                        activity!!.onBackPressed()
                    }

                    override fun onResponse(
                        call: retrofit2.Call<ResponseBody>,
                        response: retrofit2.Response<ResponseBody>
                    ) {

                        if (!response.isSuccessful){
                            val customResponse = NetworkUtis.manageResponse(response)
                            Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                            dialog = DialogUtils.Builder(dialog, activity!!)
                                .title("Error")
                                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                .content("Server Error")
                                .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                .positiveText("Close")
                                .show()
                         progress?.dismiss()
                        }else {
                           // getTargetArea()
                            refreshUI()
                         progress?.dismiss()
                        }
                    }

                }
            )
*/
        }else {
            if (userLocation!!.latitude != null && userLocation!!.longitude != null) {
                var poiItem = PoiRequestItem(isGeoActive, 1, userLocation!!.latitude , userLocation!!.longitude, "Both", radiusValue / 10, 51)
                retrievePoiObj = PoiItem(isGeoActive, 1, false, userLocation!!.latitude ,userLocation!!.longitude, "Both", radiusValue / 10, 51)
                var prefs: SharedPreferences =
                    context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)
                val deviceId = prefs.getString("deviceId", null)

                val bodyToken = ConvertRequestBody.responseBodyReturn(poiItem)
                  progress = DialogUtils.showProgressDialog(progress, context as BaseActivity)
         /*       APIRepository(context!!).setTargetAreaFirstTime(
                    1,
                    deviceId.toString(), //TODO: ADD DYNAMIC DEVICEID
                    bodyToken,
                    object : Callback<ResponseBody> {
                        override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                            Log.e("DEBUG", "ON FAILURE = " + t)
                         progress?.dismiss()()
                            activity!!.onBackPressed()
                        }

                        override fun onResponse(
                            call: retrofit2.Call<ResponseBody>,
                            response: retrofit2.Response<ResponseBody>
                        ) {

                            if (!response.isSuccessful) {
                                val customResponse = NetworkUtis.manageResponse(response)
                                Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                             progress?.dismiss()()
                                retrievePoiObj = null
                                firstTargetArea = false
                            } else {
                                refreshUI()
                               progress?.dismiss()
                            }
                        }

                    }
                )*/
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun getTargetArea(){
        Log.e("getTargetArea start","***")
        var prefs: SharedPreferences =
            context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE)

        val deviceId = prefs.getString("deviceId", null)
   /*     APIRepository(activity!!).getTargetArea(
            deviceId.toString(),
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                    Log.e("DEBUG", "ON FAILURE = " + t)
                 progress?.dismiss()
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {


                    if (response.isSuccessful) {
                        val customResponse = NetworkUtis.manageResponse(response)
                        Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                        var gson = Gson()
                        var retrievePoi = gson.fromJson(
                            customResponse.body.toString(),
                            PoiItem::class.java
                        )

                        isGetTargetArea = true
                        retrievePoiObj = retrievePoi
                        val markerLatLng = LatLng(retrievePoi.latitude,retrievePoi.longitude)
                        selectedPoiLatLng = markerLatLng
                        boolSelectedLatLng = false
                        if (retrievePoi != null) {
                            if (retrievePoi.inProgress) {
                                opacity_progress_view.visibility = View.VISIBLE
                                seekbar.setEnabled(false)
                                switchGeofence.isEnabled = false
                                save.isEnabled = false
                                save.text = resources.getString(R.string.in_progress)
                            } else {
                                opacity_progress_view.visibility = View.GONE
                                seekbar.setEnabled(true)
                                switchGeofence.isEnabled = true
                                save.isEnabled = true
                                save.text = resources.getString(R.string.savearea)
                            }

                            if (retrievePoi.longitude != null && retrievePoi.latitude != null && retrievePoi.radius != null) {
                                firstTargetArea = false
                            } else {
                                firstTargetArea = true
                                mGoogleMap?.let { map ->
                                    val lat = userLocation!!.latitude
                                    val long = userLocation!!.longitude
                                    val latLngUser = LatLng(lat, long)
                                    map.moveCamera(
                                        CameraUpdateFactory.newLatLngZoom(
                                            latLngUser,
                                            11.0f
                                        )
                                    )
                                }
                            }
                            retrievePoi?.let {
                                poiLatitude = it.latitude
                                poiLongitude = it.longitude
                                radiusValue = it.radius * 10
                                var radiusToShow = it.radius / 100
                                km_value.text = radiusToShow.toString()

                            }


                            /**
                             * @param creationDate
                             * to establish if poi exist or not
                             */
                            if (retrievePoi != null) {
                                seekbar.progress = if (retrievePoi!!.active == false) {
                                    radiusValue.div(METERS_IN_KM)
                                } else {
                                    retrievePoi.radius / 100
                                }
                            } else {
                                seekbar.progress = 1
                            }

                            retrievePoi?.let { poi ->
                                isGeoActive = poi.active
                                isCheck = poi.active

                                if (isGeoActive == false) {
                                    setBottomLayoutOff(true)
                                    uiTargetAreaNoSelected()
                                } else {

                                    isGeoSwitchActive = true

                                    if (retrievePoi!!.active == false) {
                                        setBottomLayoutOff(true)
                                    } else {
                                        setBottomLayoutOn()

                                        retrievePoi?.let {
                                            Log.e("poi not null ", "***")
                                            poiLatitude?.let { lat ->
                                                poiLongitude?.let { long ->
                                                    Log.e("popolati", "***")
                                                    mGoogleMap?.let { map ->
                                                        addMarkerToMap(
                                                            map,
                                                            lat,
                                                            long,
                                                            R.drawable.markergeofence,
                                                            null,
                                                            0.5F,
                                                            0.5F,
                                                            context!!
                                                        )
                                                        poiCircle = addRadiusArea(
                                                            lat,
                                                            long,
                                                            map,
                                                            R.color.slate_transparent,
                                                            retrievePoi.radius * 10
                                                        )
                                                        val latLngUser = LatLng(lat, long)
                                                        map.moveCamera(
                                                            CameraUpdateFactory.newLatLngZoom(
                                                                latLngUser,
                                                                11.0f
                                                            )
                                                        )
                                                        if (isGeoSwitchActive) {
                                                            seekbar.thumb =
                                                                resources.getDrawable(R.drawable.custom_thumb)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Log.e("getTargetArea finish", "***")
                        }
                    }else {
                        if (response.code() == 404){
                            mapFragment!!.getMapAsync { map ->
                                if(userLocation!=null) {
                                    Log.e("userLocation non è null", "***")
                                    addMarkerToMap(
                                        map,
                                        userLocation!!.latitude,
                                        userLocation!!.longitude,
                                        R.drawable.my_position,
                                        null,
                                        -1f,
                                        -1f,
                                        context!!
                                    )
                                   /* poiCircle = addRadiusArea(
                                        userLocation!!.latitude,
                                        userLocation!!.longitude,
                                        map,
                                        R.color.slate_transparent_green,
                                        50
                                    )*/
                                    // map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngUser,11.0f))
                                    mGoogleMap?.let { map ->
                                        addMarkerToMap(
                                            map,
                                            userLocation!!.latitude,
                                            userLocation!!.longitude,
                                            R.drawable.markergeofence,
                                            null,
                                            0.5F,
                                            0.5F,
                                            context!!
                                        )
                                        poiCircle = addRadiusArea(
                                            userLocation!!.latitude,
                                            userLocation!!.longitude,
                                            map,
                                            R.color.slate_transparent,
                                            0
                                        )
                                        val latLngUser = LatLng( userLocation!!.latitude, userLocation!!.longitude)
                                        map.moveCamera(
                                            CameraUpdateFactory.newLatLngZoom(
                                                latLngUser,
                                                11.0f
                                            )
                                        )
                                        isGetTargetArea = true
                                        if (isGeoSwitchActive) {
                                            seekbar.thumb =
                                                resources.getDrawable(R.drawable.custom_thumb)
                                        }else {
                                            setBottomLayoutOff(true)
                                            seekbar.thumb =
                                                resources.getDrawable(R.drawable.custom_thumb_off)
                                        }
                                    }
                                }
                            }
                        }else {
                            if(response.code() != 404 && response.code() != 401) {
                                dialog = DialogUtils.Builder(dialog, activity!!)
                                    .title("Error")
                                    .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                    .content("Generic Error")
                                    .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                    .positiveText("Close")
                                    .show()
                            }
                        }
                    }
                 progress?.dismiss()()
                }

            }
        )*/
    }


    fun uiTargetAreaNoSelected(){
        mapFragment!!.getMapAsync { map ->
            if(userLocation!=null) {
                Log.e("userLocation non è null", "***")
                addMarkerToMap(
                        map,
                        userLocation!!.latitude,
                        userLocation!!.longitude,
                        R.drawable.my_position,
                        null,
                        -1f,
                        -1f,
                        context!!
                )
                /*poiCircle = addRadiusArea(
                        userLocation!!.latitude,
                        userLocation!!.longitude,
                        map,
                        R.color.slate_transparent_green,
                        50
                )*/
                // map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngUser,11.0f))
                mGoogleMap?.let { map ->
                    addMarkerToMap(
                            map,
                            userLocation!!.latitude,
                            userLocation!!.longitude,
                            R.drawable.pincarfinder,
                            null,
                            0.5F,
                            0.5F,
                            context!!
                    )
                    poiCircle = addRadiusArea(
                            userLocation!!.latitude,
                            userLocation!!.longitude,
                            map,
                            R.color.slate_transparent,
                            0
                    )
                    val latLngUser = LatLng( userLocation!!.latitude, userLocation!!.longitude)
                    map.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                    latLngUser,
                                    11.0f
                            )
                    )
                    isGetTargetArea = true
                    if (isGeoSwitchActive) {
                        seekbar.thumb =
                                resources.getDrawable(R.drawable.custom_thumb)
                    }else {
                        setBottomLayoutOff(true)
                        seekbar.thumb =
                                resources.getDrawable(R.drawable.custom_thumb_off)
                    }
                }
            }
        }
    }


    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            mGoogleApiClient?.disconnect()
        }
    }

    private fun buildAlertMessageNoGps() {

        val builder = AlertDialog.Builder(baseActivity)
        builder.setMessage(Html.fromHtml("<font color='#000000'>Abilitare il GPS?</font>"))
            .setCancelable(false)
            .setPositiveButton(Html.fromHtml("<font color='#000000'>Yes</font>"))
            { dialog, id ->
                dialog.cancel()
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setNegativeButton(Html.fromHtml("<font color='#000000'>No</font>"))
            { dialog, id ->
                dialog.cancel()
            }
        val alert = builder.create()
        alert.show()
    }

    private fun loaderSetData() {
     //TODO
      //  progress = DialogUtils.showProgressDialog(progress, context as BaseActivity)
    }

    private fun loaderGetData() {
        //TODO

       // progress = DialogUtils.showProgressDialog(progress, context as BaseActivity)
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {}

    override fun onConnectionSuspended(p0: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(
                    baseActivity,
                    900
                )
            } catch (e: IntentSender.SendIntentException) {
                e.printStackTrace()
            }
        } else {
            Log.i(
                "error",
                "Location services connection failed with code " + connectionResult.errorCode
            )
        }
    }
}