package com.octotelematics.installatori.fragments.interfaces

interface OnSnapPositionChangeListener {
    fun onSnapPositionChange(position: Int)
}