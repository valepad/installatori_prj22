package com.octotelematics.installatori.fragments.passwordManagment

import APIRepository
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.google.gson.Gson

import com.octotelematics.installatori.BuildConfig.TSC_ID
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.profile.ContactUsFragment
import com.octotelematics.installatori.models.accessTokenResponse.AccessToken
import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.DateUtils
import com.octotelematics.installatori.utils.NetworkUtils
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import okhttp3.ResponseBody
import retrofit2.Callback
import java.util.*
import java.util.regex.Pattern

/**
 * A simple [Fragment] subclass.
 */
class ForgotPasswordFragment : BaseFragment() {

    var dialog: DialogUtils? = null
    private var progress: DialogUtils? = null
    private lateinit var emailUser: EditText

    private val pattern = Pattern.compile("[a-zA-Z0-9._!-/]+@[a-z]+\\.+[a-z]+")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        emailUser = view.findViewById(R.id.forgot_pass_edit)
        var baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setLightToolbar(
            "",
            R.drawable.ic_octo_small_black,
            R.drawable.arrow_back,
            face
        )
        baseActivity.hideDarkToolbar()

        //listeners
        emailUser.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                forgot_pass_button.isEnabled = (emailUser.text.matches(pattern.toRegex()))
            }
        })
        forgot_pass_contact.setOnClickListener {
            addFragment(ContactUsFragment(), true, true, R.id.container)
        }

        forgot_pass_button.setOnClickListener(View.OnClickListener {
            val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
                .title(R.string.loading)
                .content("Please wait!")
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(resources.getColor(R.color.green_mint))
                .setProgressColor(resources.getColor(R.color.green_mint))
                .progress()
            progress = builder.show()
            DDNetworkClient.instance.resetAPIService()

            NetworkUtils.setupDDNetworkClient(activity!!)

            val bodyToken = NetworkUtils.getBodyAccessToken()
            APIRepository(context!!).getAccessToken(
                bodyToken,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                        progress?.dismiss()
                    }

                    override fun onResponse(
                        call: retrofit2.Call<ResponseBody>,
                        response: retrofit2.Response<ResponseBody>
                    ) {

                        if (response.isSuccessful) {
                            val customResponse = NetworkUtis.manageResponse(response)

                            var gson = Gson()
                            val tokenResponse = gson.fromJson(
                                customResponse.body.toString(),
                                AccessToken::class.java
                            )
                            val tokenRegistration = tokenResponse.access_token
                            APIRepository(context!!).resetPassword(
                                "Bearer $tokenRegistration",
                                TSC_ID,
                                emailUser.text.toString().trim(),
                                System.currentTimeMillis().toString(),
                                System.currentTimeMillis().toString(),
                                DateUtils.getDateString(
                                    Date(System.currentTimeMillis()),
                                    "yyyy-MM-dd'T'HH:mm:ss"
                                )!!,
                                object : Callback<ResponseBody> {
                                    override fun onFailure(
                                        call: retrofit2.Call<ResponseBody>,
                                        t: Throwable
                                    ) {
                                        Log.e("DEBUG", "ON FAILURE = " + t)
                                        progress?.dismiss()
                                        dialog = DialogUtils.Builder(dialog, activity!!)
                                            .title("ATTENZIONE")
                                            .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                            .content("Errore!")
                                            .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                            .positiveText("Close")
                                            .show()
                                    }

                                    override fun onResponse(
                                        call: retrofit2.Call<ResponseBody>,
                                        response: retrofit2.Response<ResponseBody>
                                    ) {

                                        //val customResponse = NetworkUtis.manageResponse(response)
                                        //Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                                        if (response.isSuccessful) {
                                            //non mostriamo errori perche in caso di hack. l hacker non deve sapere se esiste o meno la mail registrata.
                                            //quindi mostriamo comunque conferma

                                            progress?.dismiss()
                                            changeFragment(
                                                ConfirmChangeNewPasswordFragment(emailUser.text.toString().trim()),
                                                R.id.container
                                            )
                                            /*
                                            progress?.dismiss()
                                            dialog = DialogUtils.Builder(dialog, activity!!)
                                                .title("Digital Driver")
                                                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                                .content("Temporary passoword sent! \n Please check your email")
                                                .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                                .positiveText("Close")
                                                .show()
                                            val activityBase = activity as BaseActivity
                                            activityBase.onBackPressed()

                                             */
                                        } else {

                                            progress?.dismiss()

                                            changeFragment(
                                                ConfirmChangeNewPasswordFragment(emailUser.text.toString().trim()),
                                                R.id.container
                                            )
                                            /* progress?.dismiss()
                                            if (activity != null) {
                                                val activityBase = activity as BaseActivity
                                                activityBase!!.onBackPressed()

                                                dialog = DialogUtils.Builder(dialog, activity!!)
                                                    .title("SUCCESS")
                                                    .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                                    .content("E-mail sent")
                                                    .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                                    .positiveText("Close")
                                                    .show()
                                            }*/
                                        }
                                    }

                                }
                            )
                        } else {
                            progress?.dismiss()
                            if (response.code() != 401) {
                                if (activity != null) {
                                    val customResponse = NetworkUtis.manageResponse(response)

                                    val activityBase = activity as BaseActivity
                                    activityBase!!.onBackPressed()
                                    dialog = DialogUtils.Builder(dialog, activity!!)
                                        .title("SUCCESS")
                                        .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                        .content("Errore del server")
                                        .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                        .positiveText("Close")
                                        .show()
                                }
                            }
                        }
                    }

                }
            )
        })

    }

}
