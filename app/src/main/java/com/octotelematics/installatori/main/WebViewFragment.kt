package com.octotelematics.installatori.main


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.R
import kotlinx.android.synthetic.main.fragment_webview.*


/**
 * A simple [Fragment] subclass.
 */
class WebViewFragment : BaseFragment() {

    private var isFromSettings: Boolean = false
    private var title: String? = null
    private var url: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_webview, container, false)
    }

    companion object{
        fun newInstance(isFromSettings:Boolean, title: String?, url: String?): WebViewFragment?{
            val fragment: WebViewFragment = WebViewFragment()
            fragment.setValues(isFromSettings, title, url)
            return fragment
        }
    }

    fun setValues(isFromSettings:Boolean, title: String?, url: String?){
        this.isFromSettings = isFromSettings
        this.title = title
        this.url = url
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!isFromSettings){
            titletoolbar.text=title
            toolbarreg.visibility=View.VISIBLE
            val baseActivity = activity as BaseActivity
            webview_back.setOnClickListener {
                baseActivity.onBackPressed()
            }
        }else{
            toolbarreg.visibility=View.GONE
            setupToolbar()
        }


        if (url != null) {
            webview.settings.javaScriptEnabled = true
            webview.settings.loadWithOverviewMode = true
            webview.settings.useWideViewPort = true
            webview.isVerticalScrollBarEnabled = true
            webview.settings.cacheMode = WebSettings.LOAD_NO_CACHE
            webview.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
            webview.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY

            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)

            webview.loadUrl(url)

            webview.setWebViewClient(object : WebViewClient() {

                internal var authComplete = false
                internal var resultIntent = Intent()

                override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
                    super.onPageStarted(view, url, favicon)
                }

                override fun onPageFinished(view: WebView, url: String) {

                    super.onPageFinished(view, url)

                }
            })
        }
    }

    fun setupToolbar() {
        val baseActivity = activity as BaseActivity
        baseActivity.showToolbar(true)
        if (title != null) {

            baseActivity.setTextToobar(title!!,resources.getColor(R.color.dark_blue_text),false)
        }
        baseActivity.setImageToobarLeft( R.drawable.arrow_back, View.OnClickListener {
            baseActivity.onBackPressed()
        })

        baseActivity.setFontTextToolbar(context!!,resources.getString(R.string.custom_font_bold))
        baseActivity.showBottomNavigation(true)
    }

}
