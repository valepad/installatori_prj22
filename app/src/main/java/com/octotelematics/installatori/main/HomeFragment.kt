package com.octotelematics.installatori.main


import APIRepository
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.ArrayMap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SnapHelper

import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.BuildConfig.TSC_ID
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.profile.AccountFragment


import com.octotelematics.installatori.models.profileModels.Profile
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils

import com.octotelematics.installatori.utils.AppUtils.DEFAULT_LANGUAGE
import com.octotelematics.installatori.utils.NetworkUtils
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_DEVICE_TYPE
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_TOKEN
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_DD_TOKEN
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_EMAIL_USER
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_LANGUAGE
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_USER_ID
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_ID
import com.octotelematics.installatori.utils.ProfileBottomFragment
import com.octotelematics.installatori.utils.fcm.FirebaseConfig
import com.octotelematics.installatori.utils.fcm.MyFirebaseMessagingService
import kotlinx.android.synthetic.main.fragment_home.*

import okhttp3.ResponseBody
import retrofit2.Callback
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment() {

    val listArr: MutableList<Profile?> = ArrayList()
    var snapHelper: SnapHelper? = null
    private var progress: DialogUtils? = null
    lateinit var weekEndFirst: String
    private lateinit var type: Typeface
    private lateinit var typeNormal: Typeface
    private var ddToken: String? = null
    private var voucherID: String? = null
    var textViewDonut: TextView? = null
    private var callCounter = 0
    var dialog: DialogUtils? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?


    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")



//////////
        val handlerThread = HandlerThread("MyHandlerThread")
        handlerThread.start()
        val looper: Looper = handlerThread.getLooper()
        val handler = Handler(looper)

        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, activity!!)
            .title(R.string.loading)
            .content("Please wait!")
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(resources.getColor(R.color.green_mint))
            .setProgressColor(resources.getColor(R.color.green_mint))
            .progress()
        progress = builder.show()

        baseActivity.showBottomNavigation(true)

        snapHelper = PagerSnapHelper()



        if (context != null) {
            val fp = MyFirebaseMessagingService.getFirebasePreferences(context!!)
            if (!fp.getBoolean(FirebaseConfig.PREF_SENT, false)) {
                val token = fp.getString(FirebaseConfig.PREF_REG_ID, null)
                if (token != null && !token.isEmpty()) {
                    updateToken(token)
                }
            }
        }

        //addFragment(FinishRegistrationFragment(), true, true, R.id.container)
        checkTokenPushNotifications(context)

        loadData()


        type = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        typeNormal = Typeface.createFromAsset(activity!!.assets, "fonts/solomon_book.ttf")
        textViewDonut?.typeface = type

        PreferencesHelper.setBooleanValue(context!!, PreferencesHelper.PREF_REFRESH_TRIP_LIST, true)


        // CHECK ALL SECTIONS


        loadData()

        progress?.dismiss()

    }

    override fun onResume() {
        var baseActivity = activity as BaseActivity
     baseActivity.showToolbar(true)
     val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")

     baseActivity.setDarkToolbar("Home", R.drawable.organization_status_help_24, R.drawable.icons_24_ic_profile_on_dark, face)

        baseActivity.showBottomNavigation(true)
        baseActivity.getBottomNavigationInstance().getMenu().findItem(1).setChecked(true);
        super.onResume()
    }

    private fun profileBottom() {

        val bottomAlert = ProfileBottomFragment(activity as BaseActivity)
        if (fragmentManager != null) {
            val args = Bundle()
            args.putString("from", "profile")
            bottomAlert.arguments = args
            bottomAlert.isCancelable = true
            bottomAlert.show(fragmentManager!!, bottomAlert.tag)
        }
    }
    fun checkTokenPushNotifications(context: Context?) {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task: Task<InstanceIdResult?> ->
            if (!task.isSuccessful) {
                Objects.requireNonNull(task.exception)!!.printStackTrace()
            } else {
                val token = Objects.requireNonNull(task.result)?.token
                Log.e("DEBUG", "TOKEN = $token")
                val preferences: SharedPreferences =
                    MyFirebaseMessagingService.getFirebasePreferences(context!!)
                val editor = preferences.edit()
                editor.putString(FirebaseConfig.PREF_REG_ID, token)
                editor.apply()

                val jsonParamsToken: MutableMap<String?, Any?> =
                    ArrayMap()
                jsonParamsToken["deviceType"] = "Android"
                jsonParamsToken["token"] = token
                val body = ConvertRequestBody.responseBodyReturn(jsonParamsToken)
                if (activity != null && activity!!.applicationContext != null) {
                    APIRepository(activity!!.applicationContext).accessPushToken(
                        BuildConfig.TSC_ID,
                        body,
                        object : retrofit2.Callback<ResponseBody> {
                            override fun onFailure(
                                call: retrofit2.Call<ResponseBody>,
                                t: Throwable
                            ) {
                                Log.e("DEBUG", "ON FAILURE = " + t)
                                //   baseFragment!!.hideProgress()
                            }

                            override fun onResponse(
                                call: retrofit2.Call<ResponseBody>,
                                response: retrofit2.Response<ResponseBody>
                            ) {
                                if (response.isSuccessful) {
                                    if (activity != null) {
                                        if (activity!!.applicationContext != null) {
                                            FirebaseMessaging.getInstance().isAutoInitEnabled = true
                                            val pref =
                                                MyFirebaseMessagingService.getFirebasePreferences(
                                                    activity!!.applicationContext!!
                                                )
                                            val edit = pref.edit()
                                            edit.putBoolean(FirebaseConfig.PREF_SENT, true)
                                            edit.apply()
                                            Log.d("OctoLog", "WITH THE PUSH TOKEN IT IS ALL RIGHT")
                                        }
                                    }
                                }
                                //baseFragment!!. hideProgress()
                            }

                        }
                    )
                }
            }

        }
    }



    private var vouchers: Vouchers? = null
    fun loadData() {
        initVoucher()
        // listArrTrip.clear()
        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
            .title(R.string.loading)
            .content("Please wait!")
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(resources.getColor(R.color.mint))
            .setProgressColor(resources.getColor(R.color.mint))
            .progress()
        progress = builder.show()

        if (voucherItem != null) {

            setupNetwork()
            getVouchersListCall(PreferencesHelper.getStringValue(context!!, PREF_EMAIL_USER)!!)


        }
    }

    private fun getVouchersListCall(email: String) {
        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
            .title(R.string.loading)
            .content("Please wait!")
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(resources.getColor(R.color.dark_blue_text))
            .setProgressColor(resources.getColor(R.color.dark_blue_text))
            .progress()
        progress = builder.show()
        APIRepository(context!!).getVouchers(
            TSC_ID,
            PreferencesHelper.getStringValue(context!!, PREF_LANGUAGE) ?: DEFAULT_LANGUAGE,
            PreferencesHelper.getStringValue(
                context!!,
                PreferencesHelper.PREF_TEMPLATE
            ) ?: "",
            email.trim(),
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val customResponse = NetworkUtis.manageResponseArray(response)
                        var gson = Gson()
                        vouchers =
                            gson.fromJson(customResponse.body.toString(), Vouchers::class.java)
                        if (context != null) {
                            PreferencesHelper.setStringValue(
                                context!!,
                                PreferencesHelper.PREF_VOUCHER_LIST,
                                customResponse.body.toString()
                            )
                        }

                        if (customResponse.body.toString() != "[]") {
                            progress!!.dismiss()
                            var gson = Gson()
                            if (context != null) {
                                PreferencesHelper.setStringValue(
                                    context!!,
                                    PreferencesHelper.PREF_VOUCHER_LIST,
                                    customResponse.body.toString()
                                )
                            }
                        } else {
                            progress!!.dismiss()
                            dialog = DialogUtils.Builder(dialog, activity!!)
                                .title("ERROR")
                                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                .content("Empty vouchers list!")
                                .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                .positiveText("Close")
                                .show()
                            //addFragment(InfoCarRegistrationFragment(),true,true,R.id.container)
                        }
                    } else {
                        progress!!.dismiss()

                        if (response.code() != 401) {
                            PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_ID, "")
                            dialog = DialogUtils.Builder(dialog, activity!!)
                                .title("ERROR")
                                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                .content("Server error")
                                .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                .positiveText("Close")
                                .show()
                        }
                    }
                }

            }
        )
    }

    @SuppressLint("CheckResult")
    private fun updateToken(token: String) {
        setupNetwork()
        val jsonParams: MutableMap<String?, Any?> =
            ArrayMap()
        jsonParams[PARAMETER_DEVICE_TYPE] = "Android"
        jsonParams[PARAMETER_TOKEN] = token
        val body = ConvertRequestBody.responseBodyReturn(jsonParams)
        APIRepository(context!!).accessPushToken(
            TSC_ID,
            body,
            object : Callback<ResponseBody> {
                override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                }

                override fun onResponse(
                    call: retrofit2.Call<ResponseBody>,
                    response: retrofit2.Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        if (context != null) {
                            FirebaseMessaging.getInstance().isAutoInitEnabled = true
                            val pref =
                                MyFirebaseMessagingService.getFirebasePreferences(context!!)
                            val edit = pref.edit()
                            edit.putBoolean(FirebaseConfig.PREF_SENT, true)
                            edit.apply()
                            Log.d("OctoLog", "WITH THE PUSH TOKEN IT IS ALL RIGHT")
                        }
                    }
                    //baseFragment!!. hideProgress()
                }

            }
        )


    }


    fun callbackConnectionBLE() {
        Log.e("callbackConnectionBLE", "connessione avvenuta");
    }


    fun setupNetwork() {
        ddToken = PreferencesHelper.getStringValue(context!!, PREF_DD_TOKEN)
        voucherID = PreferencesHelper.getStringValue(context!!, PREF_VOUCHER_ID)

        DDNetworkClient.token = ddToken
        DDNetworkClient.instance.resetAPIService()

        NetworkUtils.setupDDNetworkClientDashboard(activity!!)
    }


    fun checkProgress() {
        callCounter--
        if (callCounter == 0) {
            progress?.dismiss()
        }
    }

    var voucherItem: VouchersItem? = null


    fun showprogressLoading() {
        val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
            .title(R.string.loading)
            .content("Please wait!")
            .setTitleFont("fonts/solomon_book.ttf")
            .setTitleColor(resources.getColor(R.color.mint))
            .setProgressColor(resources.getColor(R.color.mint))
            .progress()
        progress = builder.show()
    }


    private fun initVoucher() {

        if (PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) != null
        ) {
            voucherItem = PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) as VouchersItem

            if (voucherItem?.user != null) {
                PreferencesHelper.setStringValue(context!!, PREF_EMAIL_USER, voucherItem?.user?.clientEmail)
                PreferencesHelper.setStringValue(context!!, PREF_USER_ID, voucherItem?.user?.userId)
            }
            PreferencesHelper.setStringValue(context!!, "voucherID", voucherItem?.idVoucher)
            PreferencesHelper.setStringValue(context!!, "voucherFk", voucherItem?.voucherId)
            PreferencesHelper.setObjValue(context!!, "currentVoucher", voucherItem)
            PreferencesHelper.setObjValue(context!!, "currentUser", voucherItem?.user)

            nameDashboard.text = "Hi, " + voucherItem?.user?.clientFirstName

            hideProgress()


        }

    }
}
