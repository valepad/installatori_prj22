package com.octotelematics.installatori.main

import APIRepository
import android.Manifest
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.octotelematics.installatori.models.vouchers.Vouchers

import com.octotelematics.installatori.BuildConfig.*
import com.octotelematics.installatori.R

import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.DDNetworkClientTokenISAM
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.AppUtils.DEFAULT_LANGUAGE
import com.octotelematics.installatori.utils.NetworkUtils
import com.octotelematics.installatori.utils.NetworkUtils.GRANT_TYPE
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_EXPIRY
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_GATEWAY_SCOPES
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_ID_TOKEN
import com.octotelematics.installatori.utils.NetworkUtils.SCOPE
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_DD_TOKEN
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_EMAIL_USER
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_LANGUAGE
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_NEW_PASSWORD
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_TEMPLATE
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_ID
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_ITEM
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_LIST
import okhttp3.ResponseBody
import retrofit2.Callback
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
@Suppress("IMPLICIT_CAST_TO_ANY")
class SplashFragment : BaseFragment() {

    private var progress: DialogUtils? = null
    var dialog: DialogUtils? = null
    var isPermissionValid=false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val baseActivity = activity as BaseActivity
        baseActivity.setBarsColor(R.color.blue_bg)
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onResume() {

        super.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val type =
            Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")


        val permissions: ArrayList<String> =
            ArrayList()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
                  permissions.add(Manifest.permission.ACTIVITY_RECOGNITION)
        } else {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        val baseActivity = activity as BaseActivity
        baseActivity!!
        if (!baseActivity.checkDeniedPermission(permissions)) {
        isPermissionValid=true

        }



            PreferencesHelper.setStringValue(
                context!!,
                PREF_TEMPLATE,
                AppUtils.TemplateVouchers.templateUI.toString()
            )
        initLogin()
    }

    fun initLogin() {
        val handler = Handler()
        val ddToken = PreferencesHelper.getStringValue(context!!, PREF_DD_TOKEN)
        val voucherID = PreferencesHelper.getStringValue(context!!, PREF_VOUCHER_ID)
        val email = PreferencesHelper.getStringValue(context!!, PREF_EMAIL_USER) ?: ""
        val psw = PreferencesHelper.getStringValue(context!!, PREF_NEW_PASSWORD) ?: ""
        val any = if (ddToken.isNullOrEmpty()) {
            handler.postDelayed(Runnable {
                changeFragment(LoginFragment(), R.id.container)
            }, 3000)
        } else if (voucherID.isNullOrEmpty()) {

            val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
                .title(R.string.loading)
                .content("Please wait!")
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(resources.getColor(R.color.green_mint))
                .setProgressColor(resources.getColor(R.color.green_mint))
                .progress()
            progress = builder.show()
            DDNetworkClient.instance.resetAPIService()
            DDNetworkClientTokenISAM.instance.resetAPIService()

            NetworkUtils.setupDDNetworkClientNoUnauthorized(activity!!)

            APIRepository(context!!).loginISAMToken(
                CLIENT_ID,
                CLIENT_SECRET,
                GRANT_TYPE,
                SCOPE,
                email.trim(),
                psw.trim(),
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {
                    }

                    override fun onResponse(
                        call: retrofit2.Call<ResponseBody>,
                        response: retrofit2.Response<ResponseBody>
                    ) {
                        val customResponse = NetworkUtis.manageResponse(response)
                        if (customResponse.body.has("error_description")) {
                            if (customResponse.body.getString("error_description")
                                    .contains("HPDAA0295E")
                            ) {

                            }
                        }
                        if (customResponse.code == 200) {

                            NetworkUtils.setupDDNetworkClientToken(activity!!)

                            var idToken: String = customResponse.body.get("id_token") as String
                            val jsonParams: MutableMap<String?, Any?> =
                                ArrayMap()
                            jsonParams[PARAMETER_GATEWAY_SCOPES] = GATEWAY_SCOPE
                            jsonParams[PARAMETER_ID_TOKEN] = idToken
                            jsonParams[PARAMETER_EXPIRY] = "43199"

                            val body = ConvertRequestBody.responseBodyReturn(jsonParams)
                            APIRepository(context!!).loginAPIGWExchangeToken(
                                "Bearer " + idToken,
                                TSC_ID,
                                body,
                                object : Callback<ResponseBody> {
                                    override fun onFailure(
                                        call: retrofit2.Call<ResponseBody>,
                                        t: Throwable
                                    ) {
                                    }

                                    override fun onResponse(
                                        call: retrofit2.Call<ResponseBody>,
                                        response: retrofit2.Response<ResponseBody>
                                    ) {
                                        if (response.isSuccessful) {
                                            val customResponse =
                                                NetworkUtis.manageResponse(response)

                                            val ddToken =
                                                customResponse.body.get("accessToken").toString()

                                            PreferencesHelper.setStringValue(
                                                context!!,
                                                PREF_DD_TOKEN,
                                                ddToken
                                            )

                                            DDNetworkClient.token = ddToken
                                            DDNetworkClient.instance.resetAPIService()

                                            NetworkUtils.setupDDNetworkClientDashboard(activity!!)

                                            APIRepository(context!!).getVouchers(
                                                TSC_ID,
                                                PreferencesHelper.getStringValue(
                                                    context!!,
                                                    PREF_LANGUAGE
                                                ) ?: DEFAULT_LANGUAGE,
                                                PreferencesHelper.getStringValue(
                                                    context!!,
                                                    PreferencesHelper.PREF_TEMPLATE
                                                ) ?: "",
                                                email.trim(),
                                                object : Callback<ResponseBody> {
                                                    override fun onFailure(
                                                        call: retrofit2.Call<ResponseBody>,
                                                        t: Throwable
                                                    ) {
                                                    }

                                                    override fun onResponse(
                                                        call: retrofit2.Call<ResponseBody>,
                                                        response: retrofit2.Response<ResponseBody>
                                                    ) {
                                                        if (response.isSuccessful) {
                                                            val customResponse =
                                                                NetworkUtis.manageResponseArray(
                                                                    response
                                                                )

                                                            var gson = Gson()
                                                            var vouchers: Vouchers = gson.fromJson(
                                                                customResponse.body.toString(),
                                                                Vouchers::class.java
                                                            )

                                                            PreferencesHelper.setStringValue(
                                                                context!!,
                                                                PREF_VOUCHER_LIST,
                                                                customResponse.body.toString()
                                                            )
                                                            PreferencesHelper.removeValue(
                                                                context!!,
                                                                PREF_VOUCHER_ITEM
                                                            )


                                                                for (i in vouchers) {
                                                                    if (i.voucherStatus == AppUtils.VoucherStatus.CREATED.name || i.voucherStatus == AppUtils.VoucherStatus.REACTIVATED.name
                                                                        || i.voucherStatus == AppUtils.VoucherStatus.SUSPENDED.name || i.voucherStatus == AppUtils.VoucherStatus.DRAFT.name
                                                                    ) {

                                                                        PreferencesHelper.setObjValue(
                                                                            context!!,
                                                                            PREF_VOUCHER_ITEM,
                                                                            i
                                                                        )
                                                                        break
                                                                    }
                                                                }

                                                                       changeFragment(
                                                                            HomeFragment(),
                                                                            R.id.container
                                                                        )


                                                        } else {
                                                            //SE LA CHIAMATA VA MALE
                                                            progress!!.dismiss()

                                                            if (response.code() != 401) {
                                                                PreferencesHelper.setStringValue(
                                                                    context!!,
                                                                    PREF_VOUCHER_ID,
                                                                    ""
                                                                )
                                                                dialog = DialogUtils.Builder(
                                                                    dialog,
                                                                    activity!!
                                                                )
                                                                    .title("ERRORE")
                                                                    .setTitleColor(
                                                                        resources.getColor(
                                                                            R.color.dark_blue_text
                                                                        )
                                                                    )
                                                                    .content("Errore del server!")
                                                                    .setMessageColor(
                                                                        resources.getColor(
                                                                            R.color.dark_blue_text
                                                                        )
                                                                    )
                                                                    .positiveText("Close")
                                                                    .show()
                                                            }
                                                        }
                                                    }
                                                }
                                            )
                                        }
                                    }
                                }
                            )
                        } else {
                            progress!!.dismiss()
                            PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_ID, "")
                            dialog = DialogUtils.Builder(dialog, activity!!)
                                .title(resources.getString(R.string.error))
                                .setTitleColor(resources.getColor(R.color.dark_blue_text))
                                .content(resources.getString(R.string.wronguserpass))
                                .setMessageColor(resources.getColor(R.color.dark_blue_text))
                                .positiveText("Close")
                                .show()
                        }
                    }

                }
            )
        } else {

            continueToDashBoard()



        }
    }

    private fun continueToDashBoard() {

        changeFragment(HomeFragment(), R.id.container)





    }
}
