package com.octotelematics.installatori.main


import android.graphics.Typeface
import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment

import com.octotelematics.installatori.R
import com.octotelematics.installatori.utils.ProfileBottomFragment


/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment() : BaseFragment() {

    lateinit var baseActivity: BaseActivity


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            "Dashboard",
            R.drawable.organization_status_help_24,
            R.drawable.icons_24_ic_profile_on_dark,
            face
        )
        baseActivity.setImageToobarLeft(R.drawable.icons_24_ic_profile_on_dark, View.OnClickListener {
            profileBottom()
        })



    }


    private fun profileBottom() {

        val bottomAlert = ProfileBottomFragment(activity as BaseActivity)
        if (fragmentManager != null) {
            val args = Bundle()
            args.putString("from", "profile")
            bottomAlert.arguments = args
            bottomAlert.isCancelable = true
            bottomAlert.show(fragmentManager!!, bottomAlert.tag)
        }
    }
}
