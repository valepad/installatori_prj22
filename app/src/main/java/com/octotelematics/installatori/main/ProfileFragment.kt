package com.octotelematics.installatori.main

import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octo.core.DDSDK
import com.octotelematics.conTe.fragments.RoadsideAssistanceFragment
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.BuildConfig
import com.octotelematics.installatori.BuildConfig.FLAVOR

import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentProfileBinding
import com.octotelematics.installatori.fragments.profile.*

import com.octotelematics.installatori.fragments.vas.VasFragment
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.utils.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_profile.*


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment() {

    private lateinit var binding: FragmentProfileBinding
    var voucherItem: VouchersItem? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val baseActivity = activity as BaseActivity
        baseActivity.showToolbar(false)
        baseActivity.showBottomNavigation(false)
        if (PreferencesHelper.getObjValue(
                context!!,
                "voucherItem",
                VouchersItem::class.java
            ) != null
        ) {
            voucherItem = PreferencesHelper.getObjValue(
                context!!,
                "voucherItem",
                VouchersItem::class.java
            ) as VouchersItem
            nameprofile.text =
                voucherItem?.user?.clientFirstName + "\n" + voucherItem?.user?.clientLastName
        }

        profile_tab_account.setOnClickListener { baseActivity.addFragment(AccountFragment(), true, true, R.id.container) }
        profile_tab_settings.setOnClickListener { baseActivity.addFragment(AccountFragment(), true, true, R.id.container)  }
        profile_tab_anagrafic.setOnClickListener {  baseActivity.addFragment(AccountFragment(), true, true, R.id.container) }
        profile_tab_contacts.setOnClickListener {  baseActivity.addFragment(AccountFragment(), true, true, R.id.container) }
        profile_tab_timetables.setOnClickListener { baseActivity.addFragment(AccountFragment(), true, true, R.id.container)  }
        profile_tab_enable_profiles.setOnClickListener { baseActivity.addFragment(AccountFragment(), true, true, R.id.container)  }
        profile_tab_infostructure.setOnClickListener {  baseActivity.addFragment(AccountFragment(), true, true, R.id.container) }
        profile_tab_inventory.setOnClickListener { baseActivity.addFragment(AccountFragment(), true, true, R.id.container)  }
        close.setOnClickListener {
            baseActivity.onBackPressed()
        }

        help.setOnClickListener {
            baseActivity.addFragment(HelpFragment(), true, true, R.id.container)
        }
    }

}
