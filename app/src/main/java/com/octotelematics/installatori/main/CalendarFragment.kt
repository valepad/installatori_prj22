package com.octotelematics.installatori.main

import android.graphics.Typeface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment

import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentCalendarBinding
import com.octotelematics.installatori.fragments.adapters.CalendarPrenotationAdapter
import com.octotelematics.installatori.fragments.interfaces.OnItemClickCalendarPrenotation
import com.octotelematics.installatori.models.calendar.PrenotationItem

import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.ProfileBottomFragment
import kotlinx.android.synthetic.main.fragment_calendar.*


/**
 * A simple [Fragment] subclass.
 */
class CalendarFragment : BaseFragment() {

    private lateinit var binding: FragmentCalendarBinding
    var voucherItem: VouchersItem? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalendarBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val baseActivity = activity as BaseActivity
        baseActivity.showBottomNavigation(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        var baseActivity = activity as BaseActivity
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        baseActivity.setDarkToolbar(
            resources.getString(R.string.calendar),
            R.drawable.organization_status_help_24,
            R.drawable.icons_24_ic_profile_on_dark,
            face
        )

        baseActivity.setImageToobarLeft(
            R.drawable.icons_24_ic_profile_on_dark,
            View.OnClickListener {
                profileBottom()
            })
calendar.setOnDateChangeListener { calendarView, i, i2, i3 ->
  //TODO i è l anno, i2  mese-1 e i3 è il giorno
}
      mock()



    }


    fun mock(){
        val listArr: MutableList<PrenotationItem?> = ArrayList()
        listArr.clear()
        val prenotationResponse: String = "{\"hours\":\"17-16\",\"status\":\"iniziato\",\"data\":\"12 Dicembre 2021\"}"
        val prenotationResponse2: String = "{\"hours\":\"11-19\",\"status\":\"finot\",\"data\":\"12 Novembre 2021\"}"
        val prenotationResponse3: String = "{\"hours\":\"3-14\",\"status\":\"incorso\",\"data\":\"2 Dicembre 2041\"}"
        val gson = Gson()
        val mockPrenotation = gson.fromJson(prenotationResponse, PrenotationItem::class.java)
        val mockPrenotation2 = gson.fromJson(prenotationResponse2, PrenotationItem::class.java)
        val mockPrenotation3 = gson.fromJson(prenotationResponse3, PrenotationItem::class.java)
        listArr.add(mockPrenotation)
        listArr.add(mockPrenotation2)
        listArr.add(mockPrenotation3)
        setPrenotationAdapter(listArr)
    }


    private var adapter: CalendarPrenotationAdapter? = null
    private fun setPrenotationAdapter(prenotationResponse: MutableList<PrenotationItem?>) {
        if (PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) != null) {
            voucherItem = PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) as VouchersItem


        }
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        calendarRecyclerView?.layoutManager = layoutManager
        adapter = prenotationResponse?.let {
            CalendarPrenotationAdapter(
               it,
                object : OnItemClickCalendarPrenotation {
                    override fun onClick(position: Int) {
                    }

                })


        }

    calendarRecyclerView?.adapter = adapter
        calendarRecyclerView.adapter!!.notifyDataSetChanged();
    }
    private fun profileBottom() {

        val bottomAlert = ProfileBottomFragment(activity as BaseActivity)
        if (fragmentManager != null) {
            val args = Bundle()
            args.putString("from", "profile")
            bottomAlert.arguments = args
            bottomAlert.isCancelable = true
            bottomAlert.show(fragmentManager!!, bottomAlert.tag)
        }
    }
}
