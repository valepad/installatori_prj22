package com.octotelematics.installatori.main

import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.AppUtils
import com.octotelematics.installatori.utils.ConfigurationUtils
import com.octotelematics.installatori.utils.CustomApplication
import com.octotelematics.installatori.utils.PreferencesHelper
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_TEMPLATE



class MainActivity : BaseActivity() {


    private lateinit var container: FrameLayout
    var dialog: DialogUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //  getSoftButtonsBarSizePort(this)

        container = findViewById(R.id.container)

        // setScreenSize(applicationContext)
        showToolbar(false)
        CustomApplication.setContext(this)


        if (getBottomNavigationInstance().menu.size() != 0) {
            getBottomNavigationInstance().menu.clear()
        }

        PreferencesHelper.setStringValue(this, PREF_TEMPLATE, AppUtils.TemplateVouchers.templateUI.name)


        ConfigurationUtils.listDashboardTabBar.forEach {
            when (it) {
                ConfigurationUtils.DashboardSections.HOME -> {
                    addBottomNavigationItem(
                        1,
                        resources.getString(R.string.home),
                        R.drawable.icon_home_selector,
                        true,
                        MenuItem.OnMenuItemClickListener { item: MenuItem? ->
                            if (currentFragment == null) {
                                changeFragment(HomeFragment(), R.id.container)
                            } else {
                                if (!currentFragment!!::class.simpleName.equals(HomeFragment::class.simpleName)) {

                                    replaceFragment(HomeFragment(), false, R.id.container)
                                }
                            }
                            false
                        })
                }
 ConfigurationUtils.DashboardSections.DASHBOARD -> {
                    addBottomNavigationItem(
                        2,
                        "Dashboard",
                        R.drawable.icon_dashboard_selector,
                        true,
                        MenuItem.OnMenuItemClickListener { item: MenuItem? ->
                            if (currentFragment == null) {
                                changeFragment(DashboardFragment(), R.id.container)
                            } else {
                                if (!currentFragment!!::class.simpleName.equals(DashboardFragment::class.simpleName)) {

                                    replaceFragment(DashboardFragment(), false, R.id.container)
                                }
                            }
                            false
                        })
                }

                ConfigurationUtils.DashboardSections.CALENDAR -> {
                    addBottomNavigationItem(
                        3,
                        resources.getString(R.string.calendar),
                        R.drawable.icon_calendar_selector,
                        true,
                        MenuItem.OnMenuItemClickListener { item: MenuItem? ->
                            if (currentFragment == null) {
                                changeFragment(CalendarFragment(), R.id.container)
                            } else {
                                if (!currentFragment!!::class.simpleName.equals(CalendarFragment::class.simpleName)) {
                                    changeFragment(CalendarFragment(), R.id.container)
                                }
                            }
                            false
                        })
                }

            }
        }


        setBackgroundBottomNavigation(resources.getColor(R.color.bottomNavigationBackground))
        val menuView: BottomNavigationMenuView =
            getBottomNavigationInstance().getChildAt(0) as BottomNavigationMenuView
        for (i in 0 until menuView.childCount) {
            val iconView: View =
                menuView.getChildAt(i).findViewById(com.google.android.material.R.id.icon)
            val layoutParams: ViewGroup.LayoutParams = iconView.layoutParams
            val displayMetrics = resources.displayMetrics
            // set your height here
            layoutParams.height =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30f, displayMetrics).toInt()
            // set your width here
            layoutParams.width =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30f, displayMetrics).toInt()
            iconView.layoutParams = layoutParams
        }

        getBottomNavigationInstance().setItemIconTintList(null)
        showTextAndIconOnBottomNavigation(true)
        setTextColorIcon(
            resources.getColor(R.color.mint), resources.getColor(R.color.blue), resources.getColor(
                R.color.blue
            )
        )

        if (savedInstanceState == null) {
            addFragment(SplashFragment(), true, false, R.id.container)
        }
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        if (fragmentManager != null && fragmentManager.backStackEntryCount == 0) {
            if (!currentFragment!!::class.simpleName.equals(HomeFragment::class.simpleName)&&
                !currentFragment!!::class.simpleName.equals(LoginFragment::class.simpleName)
            ) {
                getBottomNavigationInstance()
                    .selectedItemId = 1
            } else {
                dialog = DialogUtils.Builder(dialog, this)
                    .title(R.string.confirmQuit)
                    .content(getString(R.string.reallyQuit, getString(R.string.app_name)))
                    .positiveText(R.string.quit)
                    .negativeText(R.string.cancel)
                    .onPositive(View.OnClickListener { super.onBackPressed() })
                    .show()

            }
        } else {
            super.onBackPressed()
        }
    }

}


