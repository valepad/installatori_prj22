package com.octotelematics.installatori.main

import APIRepository
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.util.ArrayMap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import co.infinum.goldfinger.Goldfinger
import com.google.gson.Gson
import com.octotelematics.installatori.BuildConfig.*
import com.octotelematics.installatori.R
import com.octotelematics.installatori.databinding.FragmentLoginBinding
import com.octotelematics.installatori.fragments.passwordManagment.ForgotPasswordFragment
import com.octotelematics.installatori.fragments.profile.BiometricFragment
import com.octotelematics.installatori.models.vouchers.Vouchers
import com.octotelematics.installatori.models.vouchers.VouchersItem
import com.octotelematics.installatori.network.DDNetworkClient
import com.octotelematics.installatori.network.DDNetworkClientTokenISAM
import com.octotelematics.installatori.network.utils.ConvertRequestBody
import com.octotelematics.installatori.network.utils.NetworkUtis
import com.octotelematics.installatori.uicore.base.BaseActivity
import com.octotelematics.installatori.uicore.base.BaseFragment
import com.octotelematics.installatori.uicore.utils.DialogUtils
import com.octotelematics.installatori.utils.*
import com.octotelematics.installatori.utils.NetworkUtils.GRANT_TYPE
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_EXPIRY
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_GATEWAY_SCOPES
import com.octotelematics.installatori.utils.NetworkUtils.PARAMETER_ID_TOKEN
import com.octotelematics.installatori.utils.NetworkUtils.SCOPE
import com.octotelematics.installatori.utils.PreferencesHelper.FINGER_DATA
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_DD_TOKEN
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_EMAIL_FINGER
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_EMAIL_USER
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_FINGER_DISABLED
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_NEW_PASSWORD
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_OLD_PASSWORD_TEMPORARY
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_PSW_FINGER
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_TEMPLATE
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_ID
import com.octotelematics.installatori.utils.PreferencesHelper.PREF_VOUCHER_LIST
import com.octotelematics.installatori.utils.PreferencesHelper.REMEMBERME
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_webview.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Callback
import java.util.regex.Pattern

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : BaseFragment() {

    private lateinit var binding: FragmentLoginBinding

    var dialog: DialogUtils? = null
    private var progress: DialogUtils? = null
    private var isTemporaryPsw: Boolean? = false
    private var fingerprintError = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("CommitPrefEdits")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setUpFromListener() //this listener controls if the edit text fits the needs
        val face = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        val baseActivity = activity as BaseActivity
        baseActivity.showBottomNavigation(false)


        baseActivity.setLightToolbar(
            resources.getString(R.string.login) ,
            R.drawable.ic_octo_small_black,
            R.drawable.arrow_back,
            face
        )
        baseActivity.showToolbar(true)


//TODO FIX DIALOG PROBLEM

        super.onViewCreated(view, savedInstanceState)

        val type = Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        val typeNormal =
            Typeface.createFromAsset(activity!!.assets, "fonts/inter-bold.ttf")
        binding.loginBtn.typeface = type

        binding.loginForgotPassword.setOnClickListener(View.OnClickListener {
            addFragment(ForgotPasswordFragment(), true, true, R.id.container)
        })
        binding.loginBtn.setOnClickListener(View.OnClickListener {
            var email = binding.loginEmailInput.text.toString()
            var password = binding.loginPasswordInput.text.toString()
            login(email, password)
        })


        info.setOnClickListener{
            if (popup.visibility == View.GONE)
                popup.visibility = View.VISIBLE
            else
                popup.visibility = View.GONE
        }
        remember_me_switch.isChecked = PreferencesHelper.getBooleanValue(context!!,FINGER_DATA, REMEMBERME)

        remember_me_switch.setOnCheckedChangeListener { view, isChecked ->

            PreferencesHelper.setBooleanValue(context!!, FINGER_DATA,REMEMBERME, isChecked)
        }
        val fingerPref: SharedPreferences =
            context!!.getSharedPreferences("FingerData", Context.MODE_PRIVATE)
        //  sign_in.typeface = type
        //  text_forgot_psw.typeface = type
        //  sign_in_btn.typeface = type
        PreferencesHelper.removeValue(context!!, "voucherItem")
        PreferencesHelper.removeValue(context!!, "currentUser")
        PreferencesHelper.removeValue(context!!, "voucherList")
        //    arrow_back.setOnClickListener(View.OnClickListener {
        //        activity!!.onBackPressed()
        //    })
        //    text_forgot_psw.setOnClickListener(View.OnClickListener {
        //        addFragment(ForgotPasswordFragment(), true, true, R.id.container)
        //    })
        val goldfinger: Goldfinger = Goldfinger.Builder(context).build()

        //controllo se l utente ha il finger attivo e ha un impronta registrata nel device
        if (goldfinger.hasEnrolledFingerprint() && ConfigurationUtils.FINGERPRINT &&
            !PreferencesHelper.getBooleanValue(context!!, FINGER_DATA, PREF_FINGER_DISABLED)
        ) {
            val email = PreferencesHelper.getStringValue(context!!, FINGER_DATA, PREF_EMAIL_FINGER)
            val psw = PreferencesHelper.getStringValue(context!!, FINGER_DATA, PREF_PSW_FINGER)
            //controllo se l utente ha gia credenziali salvate
            if (!email.isNullOrEmpty() && !psw.isNullOrEmpty()) {
                loginBiometric.visibility = View.VISIBLE
                loginBiometric.setOnClickListener {

                    authenticateWithBiometric()
                }
            } else {
                loginBiometric.alpha = 0.5f
                loginBiometric.visibility = View.GONE
            }
        } else {
            loginBiometric.visibility = View.GONE
        }
    }


    private fun authenticateWithBiometric() {
        val goldfinger: Goldfinger = Goldfinger.Builder(context).build()
        if (goldfinger.hasEnrolledFingerprint()) {
            val biometricFragment: BiometricFragment = BiometricFragment.Companion.newInstance(this)

            biometricFragment.show(activity!!.supportFragmentManager, "biometric")
            biometricFragment.isCancelable = false

            goldfinger.authenticate(object : Goldfinger.Callback() {

                override fun onError(error: co.infinum.goldfinger.Error?) {
                    try {
                        biometricFragment.biometricError();
                    } catch (e: java.lang.Exception) {
                    }
                }

                override fun onSuccess(value: String?) {

                    biometricFragment.biometricSuccess()
                    val email =
                        PreferencesHelper.getStringValue(context!!, FINGER_DATA, PREF_EMAIL_FINGER)
                            ?: ""

                    val psw =
                        PreferencesHelper.getStringValue(context!!, FINGER_DATA, PREF_PSW_FINGER)
                            ?: ""

                    val builder: DialogUtils.Builder = DialogUtils.Builder(progress, activity!!)
                        .title(R.string.loading)
                        .content("Please wait!")
                        .setTitleFont("fonts/solomon_book.ttf")
                        .setTitleColor(resources.getColor(R.color.green_mint))
                        .setProgressColor(resources.getColor(R.color.green_mint))
                        .progress()
                    progress = builder.show()
                    Handler().postDelayed(Runnable {
                        login(email.trim(), psw.trim())
                        biometricFragment.dismiss()
                    }, 1000)



                    fingerprintError = 0
                }


            })
        }
    }


    var voucherItem: VouchersItem? = null

    fun login(username: String, password: String) {
        // var baseActivity = activity as BaseActivity
        //baseActivity.showToolbar(false)
        if (username != "" && password != "") {
            val builder: DialogUtils.Builder = DialogUtils.Builder(progress, context!!)
                .title(R.string.loading)
                .content("Please wait!")
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(resources.getColor(R.color.green_mint))
                .setProgressColor(resources.getColor(R.color.green_mint))
                .progress()
            progress = builder.show()

            PreferencesHelper.setStringValue(context!!, PREF_EMAIL_USER, username)
            PreferencesHelper.setStringValue(context!!, PREF_NEW_PASSWORD, password)

            DDNetworkClient.instance.resetAPIService()
            DDNetworkClientTokenISAM.instance.resetAPIService()

            NetworkUtils.setupDDNetworkClientNoUnauthorized(activity!!)

            APIRepository(context!!).loginISAMToken(
                CLIENT_ID,
                CLIENT_SECRET,
                GRANT_TYPE,
                SCOPE,
                username,
                password,
                object : Callback<ResponseBody> {
                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) { Log.e("DEBUG", "ON FAILURE = " + t) }

                    override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                        val customResponse = NetworkUtis.manageResponse(response)
                        Log.e("DEBUG", "ON RESPONSE = " + customResponse.body)
                        if (customResponse.body.has("error_description")) {
                            if (customResponse.body.getString("error_description").contains("HPDAA0295E")) {
                                isTemporaryPsw = true
                                PreferencesHelper.setStringValue(context!!, PREF_EMAIL_USER, username)
                                PreferencesHelper.setStringValue(
                                    context!!,
                                    PREF_OLD_PASSWORD_TEMPORARY,
                                    password
                                )


                            }
                        }
                        if (customResponse.code == 200) {

                            NetworkUtils.setupDDNetworkClientToken(activity!!)

                            var idToken: String = customResponse.body.get("id_token") as String
                            val jsonParams: MutableMap<String?, Any?> =
                                ArrayMap()
                            jsonParams[PARAMETER_GATEWAY_SCOPES] = GATEWAY_SCOPE
                            jsonParams[PARAMETER_ID_TOKEN] = idToken
                            jsonParams[PARAMETER_EXPIRY] = "43199"

                            val body = ConvertRequestBody.responseBodyReturn(jsonParams)

                            APIRepository(context!!).loginAPIGWExchangeToken(
                                "Bearer " + idToken,
                                TSC_ID,
                                body,
                                object : Callback<ResponseBody> {
                                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) { Log.e("DEBUG", "ON FAILURE = " + t) }
                                    override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {

                                        if (response.isSuccessful) {
                                            val customResponse =
                                                NetworkUtis.manageResponse(response)
                                            try {
                                                PreferencesHelper.setStringValue(context!!, PREF_DD_TOKEN, customResponse.body.get("accessToken").toString())
                                                DDNetworkClient.token = customResponse.body.get("accessToken") as String? } catch (e: Exception) { }
                                            val ddToken = PreferencesHelper.getStringValue(context!!, PREF_DD_TOKEN)

                                            DDNetworkClient.token = ddToken
                                            DDNetworkClient.instance.resetAPIService()

                                            NetworkUtils.setupDDNetworkClientDashboard(activity!!)

                                            saveFingerCredentials(username, password)


                                                PreferencesHelper.setStringValue(context!!, PREF_TEMPLATE, AppUtils.TemplateVouchers.templateUI.toString())


                                            //GET VOUCHER
                                            APIRepository(context!!).getVouchers(TSC_ID, PreferencesHelper.getStringValue(context!!, PreferencesHelper.PREF_LANGUAGE) ?: AppUtils.DEFAULT_LANGUAGE,
                                                PreferencesHelper.getStringValue(
                                                    context!!,
                                                    PreferencesHelper.PREF_TEMPLATE
                                                ) ?: "",
                                                username.trim(),
                                                object : Callback<ResponseBody> {
                                                    override fun onFailure(call: retrofit2.Call<ResponseBody>, t: Throwable) {}
                                                    override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>
                                                    ) {
                                                        if (response.isSuccessful) {
                                                            val customResponse =
                                                                NetworkUtis.manageResponseArray(
                                                                    response
                                                                )
                                                            var gson = Gson()
                                                            var vouchers: Vouchers = gson.fromJson(customResponse.body.toString(), Vouchers::class.java)

                                                            PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_LIST, customResponse.body.toString())


                                                        //SE SONO VOUCHER PRESENTI

                                                                progress!!.dismiss()

                                                                // PRENDO SOLO I VOUCHER VALIDI

                                                                for (i in vouchers) {
                                                                    if (i.voucherStatus == AppUtils.VoucherStatus.CREATED.name || i.voucherStatus == AppUtils.VoucherStatus.REACTIVATED.name
                                                                        || i.voucherStatus == AppUtils.VoucherStatus.SUSPENDED.name || i.voucherStatus == AppUtils.VoucherStatus.DRAFT.name) {

                                                                            PreferencesHelper.setObjValue(context!!, "voucherItem", i)
                                                                        break
                                                                    }
                                                                }

                                                                //CONTROLLO SE CI SONO VOUCHER VALIDI


                                                                    voucherItem = PreferencesHelper.getObjValue(context!!, "voucherItem", VouchersItem::class.java) as VouchersItem

                                                                    //addFragment(VoucherListFragment(false), true, true, R.id.container)

                                                                    for (i in 0 until activity?.supportFragmentManager!!.backStackEntryCount) {
                                                                            activity?.supportFragmentManager!!.popBackStack()
                                                                        }
                                                                        progress!!.dismiss()
                                                                        changeFragment(HomeFragment(), R.id.container)





                                                        } else {
                                                            progress!!.dismiss()

                                                            if (response.code() != 401) {
                                                                PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_ID, "")

                                                                CustomAlertSingle(resources.getString(R.string.error_credentials_title),resources.getString(R.string.error_login_connection),resources.getString(R.string.retry))
                                                                    .show(requireFragmentManager(),"CustomAlert")

                                                            }
                                                        }
                                                    }
                                                }
                                            )
                                        }
                                    }

                                }
                            )
                        } else {
                            progress!!.dismiss()
                            if (isTemporaryPsw != true) {
                                val editor: SharedPreferences.Editor =
                                    context!!.getSharedPreferences(
                                        "DigitalDriver",
                                        Context.MODE_PRIVATE
                                    ).edit()
                                editor.putString("voucherID", "")
                                editor.apply()
                                PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_ID, "")
                                CustomAlertSingle(resources.getString(R.string.error_credentials_title),resources.getString(R.string.error_credentials_message),resources.getString(R.string.retry))
                                    .show(requireFragmentManager(),"CustomAlert")
                            } else {
                                isTemporaryPsw = false
                            }
                        }
                    }

                }
            )
        } else {
            if (isTemporaryPsw != true) {
                val editor: SharedPreferences.Editor =
                    context!!.getSharedPreferences("DigitalDriver", Context.MODE_PRIVATE).edit()
                editor.putString("voucherID", "")
                editor.apply()
                PreferencesHelper.setStringValue(context!!, PREF_VOUCHER_ID, "")
                dialog = DialogUtils.Builder(dialog, activity!!)
                    .title("ERRORE")
                    .setTitleColor(resources.getColor(R.color.dark_blue_text))
                    .content("Completa i campi username e password!")
                    .setMessageColor(resources.getColor(R.color.dark_blue_text))
                    .positiveText("Close")
                    .show()
            } else {
                isTemporaryPsw = false
            }
        }

    }


    fun saveFingerCredentials(username: String, password: String) {
        val fingerData: SharedPreferences.Editor =
            context!!.getSharedPreferences(
                "FingerData",
                Context.MODE_PRIVATE
            ).edit()
        fingerData.putString(PREF_EMAIL_FINGER, username)
        fingerData.putBoolean(PREF_FINGER_DISABLED, false)
        fingerData.apply()
        fingerData.apply()
        fingerData.putString(PREF_PSW_FINGER, password)
        fingerData.apply()

    }


    private fun setUpFromListener() {

        val email = MutableStateFlow("")
        val password = MutableStateFlow("")


        binding.loginEmailInput.addTextChangedListener {
            email.value = (it.toString())
        }
        binding.loginPasswordInput.addTextChangedListener {
            password.value = (it.toString())
        }

        val isLoginEnabled: Flow<Boolean> = combine(email, password) { email, password ->

            val pattern = Pattern.compile("[a-zA-Z0-9._!-/]+@[a-z]+\\.+[a-z]+")
            val emailCorrect = email.matches(pattern.toRegex())
            val passNotNull = password != ""

            //TODO implement min lenght for password

            return@combine emailCorrect and passNotNull
        }

        lifecycleScope.launch {
            isLoginEnabled.collect { value ->
                binding.loginBtn.isEnabled = value
            }
        }


        fun saveFingerCredentials(username: String, password: String) {

            PreferencesHelper.setStringValue(context!!, FINGER_DATA, PREF_EMAIL_FINGER, username)
            PreferencesHelper.setBooleanValue(context!!, FINGER_DATA, PREF_FINGER_DISABLED, false)
            PreferencesHelper.setStringValue(context!!, FINGER_DATA, PREF_PSW_FINGER, password)
        }


    }
}
