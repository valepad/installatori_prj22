package com.octotelematics.installatori.uicore.views;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import com.octotelematics.installatori.R;


/**
 * Created by iLabora on 14.09.2017.
 */

public class ProgressView extends View {

    public static final String TAG = "BigProgressView";

    //Sizes (with defaults)
    private int layoutHeight = 0;
    private int layoutWidth = 0;

    private float mBarWidth = 24f;
    private float mFiddleWidth = 9f;
    private float mFiddleBorderWidth = 2f;

    private boolean mFlatRim = true;
    private boolean mHasFiddle = false;
    private boolean mFiddleBorder = false;

    //Colors (with defaults)
    private int mProgressColor = 0xff8787;
    private int mRimColor = 0xffcc33;
    private int mShadowRimColor = 0x7f6619;
    private int mDarkShadowRimColor = 0x000000;
    private int mFiddleColor = 0xff8787;
    private int mBaseProgressColor = 0xff8787;
    private int mAnimationColor1 = 0xff8787;
    private int mAnimationColor2 = 0xffd462;
    private int mAnimationColor3 = 0x2fcd7c;


    //Padding (with defaults)
    private int paddingTop = 5;
    private int paddingBottom = 5;
    private int paddingLeft = 5;
    private int paddingRight = 5;
    private float mPadding = 0;

    //Rectangles
    private RectF mRimBounds = new RectF();
    private RectF mProgressBounds = new RectF();

    //Paints
    private Paint mCirclePaint = new Paint();
    private Paint mBarPaint = new Paint();
    private Paint mFiddlePaint = new Paint();
    private Paint mFiddleBorderPaint = new Paint();

    // Set percentage
    private int mPercentage = 0;

    // Set type
    private int mType = 1;


    public ProgressView(Context context) {
        super(context);
        init(null, 0);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }


    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.ProgressView, defStyle, 0);

        mFlatRim = a.getBoolean(R.styleable.ProgressView_flatRim, mFlatRim);
        mRimColor = a.getColor(R.styleable.ProgressView_rimColor, mRimColor);
        mShadowRimColor = a.getColor(R.styleable.ProgressView_shadowRimColor, mShadowRimColor);
        mDarkShadowRimColor = a.getColor(R.styleable.ProgressView_darkShadowRimColor, mDarkShadowRimColor);
        mBarWidth = a.getDimension(R.styleable.ProgressView_barWidth, mBarWidth);
        mProgressColor = a.getColor(R.styleable.ProgressView_progressColor, mProgressColor);
        mHasFiddle = a.getBoolean(R.styleable.ProgressView_hasFiddle, mHasFiddle);
        mFiddleColor = a.getColor(R.styleable.ProgressView_fiddleColor, mFiddleColor);
        mFiddleWidth = a.getDimension(R.styleable.ProgressView_fiddleWidth, mFiddleWidth);
        mFiddleBorder = a.getBoolean(R.styleable.ProgressView_fiddleBorder, mFiddleBorder);
        mFiddleBorderWidth = a.getDimension(R.styleable.ProgressView_fiddleBorderWidth, mFiddleBorderWidth);
        mBaseProgressColor = a.getColor(R.styleable.ProgressView_baseProgressColor, mProgressColor);
        mAnimationColor1 = a.getColor(R.styleable.ProgressView_animationColor1, mAnimationColor1);
        mAnimationColor2 = a.getColor(R.styleable.ProgressView_animationColor2, mAnimationColor2);
        mAnimationColor3 = a.getColor(R.styleable.ProgressView_animationColor3, mAnimationColor3);
        mPercentage = a.getInt(R.styleable.ProgressView_progress, mPercentage);
        mPadding = a.getDimension(R.styleable.ProgressView_padding, mPadding);
        if (isInEditMode()) {
            if ("gradient".equals(a.getString(R.styleable.ProgressView_barType))) {
                mType = 3;
            }
        }
        mType = a.getInteger(R.styleable.ProgressView_barType, mType);
        a.recycle();

        invalidate();
    }

    private void setupPaints() {
        mBarPaint.setColor(mProgressColor);
        mBarPaint.setAntiAlias(true);
        mBarPaint.setStyle(Paint.Style.STROKE);
        mBarPaint.setStrokeWidth(mBarWidth);
        mBarPaint.setStrokeCap(Paint.Cap.ROUND);

        if (mType == 3) {
            SweepGradient sweepShader = new SweepGradient(layoutWidth / 2, layoutHeight / 2,
                    new int[]{mBaseProgressColor, mProgressColor, mProgressColor},
                    new float[]{0, 1.0f * mPercentage / 360, 1});
            mBarPaint.setShader(sweepShader);
        }

        if (mFlatRim) {
            mCirclePaint.setColor(mRimColor);
        }
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mCirclePaint.setStrokeWidth(mBarWidth);
        mCirclePaint.setStrokeCap(Paint.Cap.ROUND);
        if (!mFlatRim) {
            float radius = mRimBounds.width() / 2 + mBarWidth;
            mCirclePaint.setShader(new RadialGradient(getWidth() / 2, getHeight() / 2, radius,
                    new int[]{Color.TRANSPARENT, mDarkShadowRimColor, mRimColor, mShadowRimColor}, new float[]{
                    0.0f / radius,
                    (radius - mBarWidth * 2) / radius,
                    (radius - mBarWidth) / radius,
                    radius / radius
            }, Shader.TileMode.CLAMP));
        }

        mFiddlePaint.setColor(mFiddleColor);
        mFiddlePaint.setAntiAlias(true);
        mFiddlePaint.setStyle(Paint.Style.FILL);

        mFiddleBorderPaint.setColor(mProgressColor);
        mFiddleBorderPaint.setAntiAlias(true);
        mFiddleBorderPaint.setStyle(Paint.Style.STROKE);
        mFiddleBorderPaint.setStrokeWidth(mFiddleBorderWidth);
        mFiddleBorderPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    private void setupBounds() {
        int minValue = Math.min(layoutWidth, layoutHeight);

        // Calc the Offset if needed
        int xOffset = layoutWidth - minValue;
        int yOffset = layoutHeight - minValue;

        // Offset
        paddingTop = (int) (mPadding + (yOffset / 2));
        paddingBottom = (int) (mPadding + (yOffset / 2));

        paddingLeft = (int) (mPadding + (xOffset / 2));
        paddingRight = (int) (mPadding + (xOffset / 2));

        int width = getWidth();
        int height = getHeight();


        mRimBounds = new RectF(
                paddingLeft + mBarWidth,
                paddingTop + mBarWidth,
                width - paddingRight - mBarWidth,
                height - paddingBottom - mBarWidth);
        mProgressBounds = new RectF(
                paddingLeft + mBarWidth,
                paddingTop + mBarWidth,
                width - paddingRight - mBarWidth,
                height - paddingBottom - mBarWidth);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutWidth = w;
        layoutHeight = h;
        setupBounds();
        setupPaints();

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawArc(mRimBounds, 0, 360, false, mCirclePaint);
        canvas.rotate(-93, layoutWidth / 2, layoutHeight / 2);
        canvas.drawArc(mProgressBounds, 3, mPercentage, false, mBarPaint);
        canvas.rotate(93, layoutWidth / 2, layoutHeight / 2);
        if (mHasFiddle) {
            float fiddleCX = (float) (getWidth() / 2.0 + Math.cos(Math.toRadians(mPercentage - 90)) * (mProgressBounds.width() / 2 - 0.75));
            float fiddleCY = (float) (getHeight() / 2.0 + Math.sin(Math.toRadians(mPercentage - 90)) * (mProgressBounds.height() / 2 - 0.75));
            canvas.drawCircle(fiddleCX, fiddleCY, mFiddleWidth, mFiddlePaint);
            if (mFiddleBorder) {
                canvas.drawCircle(fiddleCX, fiddleCY, mFiddleWidth, mFiddleBorderPaint);
            }
        }
    }

    private int percentToDegree(int degree) {
        Float value = (float) degree;
        value = (value * 360) / 100;
        return value.intValue();
    }

    private void startAnimation(int per) {
        int diff = per - mPercentage;

        int red = -1, orange = -1, green = -1;

        if (mType == 1) {
            if (per < percentToDegree(60)) {
                red = mAnimationColor1;
            } else if (per >= percentToDegree(60) && per < percentToDegree(80)) {
                red = mAnimationColor1;
                orange = mAnimationColor2;
            } else {
                red = mAnimationColor1;
                orange = mAnimationColor2;
                green = mAnimationColor3;
            }
        } else {
            red = mProgressColor;
        }

        ValueAnimator firstColorAnimator;
        ValueAnimator secondColorAnimator = null;
        ValueAnimator thirdColorAnimator = null;

        ValueAnimator valueAnimator = ValueAnimator
                .ofInt(mPercentage, mPercentage + diff)
                .setDuration(Math.abs(diff) * 5); //diff * 5 because every degree takes 5ms - which means 360 degrees takes 1800ms (~2s)
        valueAnimator.addUpdateListener(animation -> {
            mPercentage = (int) animation.getAnimatedValue();

            if (mType == 3) {
                SweepGradient sweepShader = new SweepGradient(layoutWidth / 2, layoutHeight / 2,
                        new int[]{mBaseProgressColor, mProgressColor, mProgressColor},
                        new float[]{0, 1.0f * mPercentage / 360, 1});
                mBarPaint.setShader(sweepShader);
            }

            invalidate();
        });

        if (mType != 1 || (red != -1 && orange == -1 && green == -1)) {             // score < 60%
            firstColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            red, red)
                    .setDuration(Math.abs(diff) * 3);
            firstColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });
        } else if (red != -1 && orange != -1 && green == -1) {      // score 60-80%
            firstColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            red, red)
                    .setDuration(Math.abs(diff) * 3);
            firstColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });

            secondColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            red, orange)
                    .setDuration(Math.abs(diff) * 2);
            secondColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });
        } else {       // score > 80%
            firstColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            red, red)
                    .setDuration(Math.abs(diff) * 3);
            firstColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });

            secondColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            red, orange)
                    .setDuration(Math.abs(diff));
            secondColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });

            thirdColorAnimator  = ValueAnimator
                    .ofObject(new ArgbEvaluator(),
                            orange, green)
                    .setDuration(Math.abs(diff));
            thirdColorAnimator.addUpdateListener(valueAnimator1 -> {
                mProgressColor = (int) valueAnimator1.getAnimatedValue();
                mBarPaint.setColor(mProgressColor);
                invalidate();
            });
        }



        if (secondColorAnimator == null) {      // < 60 %
            AnimatorSet set = new AnimatorSet();
            set.playTogether(firstColorAnimator, valueAnimator);
            set.start();
        } else if (thirdColorAnimator == null) {                                // 60 - 80 %
            AnimatorSet set = new AnimatorSet();
            AnimatorSet set1 = new AnimatorSet();
            set1.playSequentially(firstColorAnimator, secondColorAnimator);
            set.playTogether(set1, valueAnimator);
            set.start();
        } else {
            AnimatorSet set = new AnimatorSet();
            AnimatorSet set1 = new AnimatorSet();
            set1.playSequentially(firstColorAnimator, secondColorAnimator, thirdColorAnimator);
            set.playTogether(set1, valueAnimator);
            set.start();
        }
    }

    public void setProgress(int per) {
        startAnimation(percentToDegree(per));
    }

}
