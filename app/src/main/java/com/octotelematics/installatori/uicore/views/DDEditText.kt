package com.octotelematics.installatori.uicore.views


import android.graphics.Typeface
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.octotelematics.installatori.uicore.utils.UIUtils
import android.text.InputFilter
import android.text.method.DigitsKeyListener
import android.util.TypedValue
import androidx.core.widget.TextViewCompat
import com.octotelematics.installatori.R


class DDEditText : RelativeLayout {
    private var mLayoutInflater: LayoutInflater
    private lateinit var rootLayout: RelativeLayout
    lateinit var editText: EditText

    private lateinit var hintText: DDTextView

    private lateinit var imageEnd: ImageView
    private lateinit var textEnd: TextView
    var view: View? = null
    private lateinit var underline: View

    var isPasswordShown = false

    private var animationStatus = -1
    private val ANIMATION_UP = 0
    private val ANIMATION_DOWN = 1

    private val ANIMATION_DURATION_ISTANT: Long = 1
    private val ANIMATION_DURATION_NORMAL: Long = 100

    constructor(context: Context) : super(context) {
        mLayoutInflater = LayoutInflater.from(context)
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {
        mLayoutInflater = LayoutInflater.from(context)
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        mLayoutInflater = LayoutInflater.from(context)
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mLayoutInflater = LayoutInflater.from(context)
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        //view = mLayoutInflater.inflate(R.layout.dd_edit_text, this, true)

        view = View.inflate(context, R.layout.dd_edit_text, this)

        rootLayout = view!!.findViewById(R.id.rootLayout)
        editText = view!!.findViewById(R.id.editText)

        hintText = view!!.findViewById(R.id.textHintInner)

        imageEnd = view!!.findViewById(R.id.imageEnd)
        textEnd = view!!.findViewById(R.id.textEnd)
        underline = view!!.findViewById(R.id.underline)


        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.DDEditText)
            val fontName = a.getString(R.styleable.DDEditText_customFont)
            val imageOn = a.getResourceId(R.styleable.DDEditText_ddetImageOn, -1)
            val imageOff = a.getResourceId(R.styleable.DDEditText_ddetImageOff, -1)

            val textOn = a.getString(R.styleable.DDEditText_ddetTextOn)
            val textOff = a.getString(R.styleable.DDEditText_ddetTextOff)

            val textColorOn = a.getColor(R.styleable.DDEditText_ddetTextColorOn, 0)
            val textColorOff = a.getColor(R.styleable.DDEditText_ddetTextColorOff, 0)

            val imageWidth = a.getDimensionPixelSize(R.styleable.DDEditText_ddetImageWidth, -1)
            val imageHeight = a.getDimensionPixelSize(R.styleable.DDEditText_ddetImageHeight, -1)

            val textSize = a.getDimensionPixelSize(
                R.styleable.DDEditText_android_textSize,
                UIUtils.convertDpToPixel(15f, context).toInt()
            )
            val text = a.getString(R.styleable.DDEditText_android_text)
            val textColor = a.getColor(R.styleable.DDEditText_android_textColor, 0)
            val textColorHint = a.getColor(R.styleable.DDEditText_android_textColorHint, 0)

            val background = a.getColor(R.styleable.DDEditText_android_background, 0)

            var colorFocusOn = a.getColor(R.styleable.DDEditText_ddetColorFocusOn, 0)
            var colorFocusOff = a.getColor(R.styleable.DDEditText_ddetColorFocusOff, 0)

            val imeOption = a.getInt(R.styleable.DDEditText_android_imeOptions, -1)
            val inputType = a.getInt(R.styleable.DDEditText_android_inputType, -1)
            val maxLines = a.getInt(R.styleable.DDEditText_android_maxLines, -1)
            val maxLength = a.getInt(R.styleable.DDEditText_android_maxLength, -1)

            val hint = a.getString(R.styleable.DDEditText_android_hint)
            val scrollHorizontally =
                a.getBoolean(R.styleable.DDEditText_android_scrollHorizontally, true)

            val focusable = a.getBoolean(R.styleable.DDEditText_android_focusable, true)
            val focusableInTouchMode =
                a.getBoolean(R.styleable.DDEditText_android_focusableInTouchMode, true)

            val digits = a.getString(R.styleable.DDEditText_android_digits)

            val nextFocusForward = a.getResourceId(R.styleable.DDEditText_android_nextFocusForward, -1)

            try {

                editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())
                hintText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())

                if (imageWidth != -1) {
                    var params: LayoutParams = imageEnd.layoutParams as LayoutParams
                    if (params != null) {
                        params.width = imageWidth
                        imageEnd.layoutParams = params
                    }
                }

                if (imageHeight != -1) {
                    var params: LayoutParams = imageEnd.layoutParams as LayoutParams
                    if (params != null) {
                        params.height = imageHeight
                        imageEnd.layoutParams = params
                    }
                }

                if (background != 0) {
                    rootLayout.setBackgroundColor(background)
                }

                if (textColor != 0) {
                    editText.setTextColor(textColor)
                }

                if (textColorHint != 0) {
                    editText.setHintTextColor(textColorHint)
                }

                if (imeOption != -1) {
                    editText.imeOptions = imeOption
                }

                if (maxLines != -1) {
                    editText.maxLines = maxLines
                }

                if (maxLength != -1) {
                    val filterArray = arrayOfNulls<InputFilter>(1)
                    filterArray[0] = InputFilter.LengthFilter(maxLength)
                    editText.setFilters(filterArray)
                }

                if (fontName != null) {
                    val myTypeface = Typeface.createFromAsset(context.assets, fontName)
                    editText.typeface = myTypeface
                    hintText.typeface = myTypeface
                    textEnd.typeface = myTypeface
                }

                if (text != null) {
                    editText.setText(text)
                    if (!text.isEmpty()) {
                        doAnimation(ANIMATION_UP, ANIMATION_DURATION_ISTANT)
                    }
                }

                if (hint != null) {
                    hintText.text = hint
                }

                if (colorFocusOn == 0) {
                    colorFocusOn = context.resources.getColor(R.color.colorPrimary)
                } else {
                    underline.setBackgroundColor(colorFocusOn)
                    hintText.setTextColor(colorFocusOn)
                }

                if (colorFocusOff == 0) {
                    colorFocusOff = context.resources.getColor(R.color.colorPrimary)
                } else {
                    underline.setBackgroundColor(colorFocusOff)
                    hintText.setTextColor(colorFocusOff)
                }

                if (digits != null && digits.isNotEmpty()) {
                    editText.keyListener = DigitsKeyListener.getInstance(digits)
                }

                if (nextFocusForward != -1) {
                    editText.imeOptions = EditorInfo.IME_ACTION_NEXT
                    editText.nextFocusForwardId = nextFocusForward
                }

                editText.setHorizontallyScrolling(scrollHorizontally)

                editText.isFocusable = focusable
                editText.isFocusableInTouchMode = focusableInTouchMode

                // input type
                if (inputType != -1) {
                    editText.inputType = inputType
                    if (inputType == (EditorInfo.TYPE_TEXT_VARIATION_PASSWORD + EditorInfo.TYPE_CLASS_TEXT)) {
                        if (imageOn != -1) {
                            imageEnd.visibility = View.VISIBLE
                            textEnd.visibility = View.INVISIBLE
                            editText.setPadding(
                                0,
                                0,
                                UIUtils.convertDpToPixel(50f, context).toInt(),
                                0
                            )

                            hintText.setPadding(
                                0,
                                0,
                                UIUtils.convertDpToPixel(50f, context).toInt(),
                                0
                            )

                            imageEnd.setImageResource(imageOn)

                            imageEnd.setOnClickListener {
                                if (!isPasswordShown) {
                                    isPasswordShown = true
                                    imageEnd.setImageResource(imageOn)
                                    editText.transformationMethod =
                                        HideReturnsTransformationMethod.getInstance()
                                    editText.setSelection(editText.text.length)
                                } else {
                                    isPasswordShown = false
                                    imageEnd.setImageResource(imageOff)
                                    editText.transformationMethod =
                                        PasswordTransformationMethod.getInstance()
                                    editText.setSelection(editText.text.length)
                                }
                            }
                        } else {
                            imageEnd.visibility = View.INVISIBLE
                            textEnd.visibility = View.VISIBLE
                            editText.setPadding(
                                0,
                                0,
                                UIUtils.convertDpToPixel(50f, context).toInt(),
                                0
                            )
                            hintText.setPadding(
                                0,
                                0,
                                UIUtils.convertDpToPixel(50f, context).toInt(),
                                0
                            )
                            textEnd.setText(textOn)
                            textEnd.setTextColor(textColorOn)

                            textEnd.setOnClickListener {
                                if (!isPasswordShown) {
                                    isPasswordShown = true
                                    textEnd.setText(textOff)
                                    editText.transformationMethod =
                                        HideReturnsTransformationMethod.getInstance()
                                    editText.setSelection(editText.text.length)
                                } else {
                                    isPasswordShown = false
                                    textEnd.setText(textOn)
                                    editText.transformationMethod =
                                        PasswordTransformationMethod.getInstance()
                                    editText.setSelection(editText.text.length)
                                }
                            }
                        }

                    } else {
                        imageEnd.visibility = View.GONE
                        textEnd.visibility = View.GONE
                        editText.setPadding(0, 0, 0, 0)
                    }
                }

                TextViewCompat.setAutoSizeTextTypeWithDefaults(hintText, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM)

                //editText.addTextChangedListener(textWatcher)

                editText.setOnFocusChangeListener { _, hasFocus ->
                    if (editText.text.isEmpty()) {
                        // faccio partire l'animazione

                        doAnimation(if (hasFocus) ANIMATION_UP else ANIMATION_DOWN, ANIMATION_DURATION_NORMAL)

                    } else if (hasFocus && animationStatus != ANIMATION_UP) {
                        doAnimation(ANIMATION_UP, ANIMATION_DURATION_NORMAL)
                    }

                    underline.setBackgroundColor(if (hasFocus) colorFocusOn else colorFocusOff)

                    hintText.setTextColor(if (hasFocus) colorFocusOn else if (textColorHint != 0) textColorHint else colorFocusOff)

                }

                editText.addTextChangedListener(object : TextWatcher {

                    override fun afterTextChanged(s: Editable) {
                        if (animationStatus != ANIMATION_UP && s.toString().isNotEmpty()) {
                            doAnimation(ANIMATION_UP, ANIMATION_DURATION_NORMAL)
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence, start: Int,
                        count: Int, after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence, start: Int,
                        before: Int, count: Int
                    ) {
                    }
                })

            } catch (e: Exception) {
                e.printStackTrace()
            }

            a.recycle()
        }
    }

    fun doAnimation(type: Int, duration: Long) {
        val animation: Animation = AnimationUtils.loadAnimation(context, if (type == ANIMATION_UP) R.anim.from_big_to_small else R.anim.from_small_to_big)
        animation.fillAfter = true
        animation.duration = duration
        hintText.startAnimation(animation)
        animationStatus = type
    }

    fun doAnimation() {
        val animation: Animation = AnimationUtils.loadAnimation(context, R.anim.from_big_to_small)
        animation.fillAfter = true
        animation.duration = 1
        hintText.startAnimation(animation)
        animationStatus = ANIMATION_UP
    }

    fun doAnimationDown() {
        val animation: Animation = AnimationUtils.loadAnimation(context, R.anim.from_big_to_small)
        animation.fillAfter = false
        animation.duration = 1
        hintText.startAnimation(animation)
        animationStatus = ANIMATION_DOWN
    }

    fun setImageEnd(icon: Int, listener: OnClickListener) {
        imageEnd.visibility = View.VISIBLE
        editText.setPadding(0, 0, UIUtils.convertDpToPixel(50f, context).toInt(), 0)
        imageEnd.setImageResource(icon)
        imageEnd.layoutParams.width = 40
        imageEnd.layoutParams.height = 40
        imageEnd.setOnClickListener(listener)
    }

    fun setTextDate(text: String) {
        val animation: Animation = AnimationUtils.loadAnimation(context, R.anim.from_big_to_small)
        animation.fillAfter = true
        hintText.startAnimation(animation)
        editText.setText(text)
    }

    fun setText(text: String) {
        editText.setText(text)
    }

    fun getText(): String {
        return editText.text.toString()
    }

}