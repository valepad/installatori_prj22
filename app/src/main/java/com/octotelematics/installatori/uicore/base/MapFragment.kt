package com.octotelematics.installatori.uicore.base


import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.octotelematics.installatori.R

/**
 * A simple [Fragment] subclass.
 */
open class MapFragment : BaseFragment(), LocationListener {


    private var mapReady = false
    private var googleMap: GoogleMap? = null

    private val mContext: Context? = null

    // flag for GPS status
    var isGPSEnabled : Boolean = false

    // flag for network status
    var isNetworkEnabled : Boolean = false

    // flag for GPS status
    var canGetLocatio : Boolean = false

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null

    // location
    var location : Location? = null
    // latitude
    var lati : Double = 0.0
    // longitude
    var longi : Double = 0.0

    var myLocation : Location? = null
    var myLoca : Location? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myLocation = getLastKnownLocation()
        /*  val mapFragment = childFragmentManager.findFragmentById(
              R.id.mapFragment
          ) as SupportMapFragment?
          mapFragment!!.onCreate(savedInstanceState)
          mapFragment.getMapAsync { googleMap: GoogleMap ->
              mapReady = true
              this.googleMap = googleMap
          }*/
    }

    var mapFragment: SupportMapFragment? = null

    //region Lifecycle

    //region Lifecycle
    override fun onStart() {
        super.onStart()
        mapFragment?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapFragment?.onResume()
    }


    override fun onPause() {
        super.onPause()
        mapFragment?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapFragment?.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapFragment?.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapFragment?.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapFragment?.onDestroyView()
    }

    override fun onLocationChanged(p0: Location?) {

    }


    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

    }

    override fun onProviderEnabled(p0: String?) {

    }

    override fun onProviderDisabled(p0: String?) {

    }

    /**
     * Function to get latitude
     */
    open fun getLatitude(): Double {
        if (location != null) {
            lati = location!!.latitude
        }
        // return latitude
        return lati
    }

    /**
     * Function to get longitude
     */
    open fun getLongitude(): Double {
        if (location != null) {
            longi = location!!.longitude
        }
        // return longitude
        return longi
    }

    @SuppressLint("MissingPermission")
    open fun getLocationMap(minTimeToUpdates: Long, minDistanceChangeForUpdates: Float, context: Context): Location? {
        try {
            locationManager = context
                ?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            // getting GPS status
            isGPSEnabled = locationManager!!
                .isProviderEnabled(LocationManager.GPS_PROVIDER)
            // getting network status
            isNetworkEnabled = locationManager!!
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled && !isNetworkEnabled) { // no network provider is enabled
            } else {
                if (isNetworkEnabled) {
                    location = null
                    locationManager!!.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        minTimeToUpdates,
                        minDistanceChangeForUpdates,
                        this
                    )
                    if (locationManager != null) {
                        location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                        if (location != null) {
                            myLoca = location
                            lati = location!!.latitude
                            longi = location!!.longitude
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    location = null
                    if (location == null) {
                        locationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            minTimeToUpdates,
                            minDistanceChangeForUpdates,
                            this
                        )
                        if (locationManager != null) {
                            if (locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                                location = locationManager!!
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                lati = location!!.latitude
                                longi = location!!.longitude
                            } else {
                                location = getLastKnownLocation()
                                lati = myLocation!!.latitude
                                longi = myLocation!!.longitude
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return location
    }


    @SuppressLint("MissingPermission")
    fun getLastKnownLocation(): Location? {
        locationManager =
            activity!!.applicationContext.getSystemService(
                Context.LOCATION_SERVICE
            ) as LocationManager
        val providers: List<String> = locationManager!!.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l: Location = locationManager!!.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || l.accuracy < bestLocation.accuracy) { // Found best last known location: %s", l);
                bestLocation = l
            }
        }
        return bestLocation
    }

    /* fun addMarker(latitude: Double, longitude: Double, icon: Int, zoomCam:Float?, anchorX:Float, anchorY:Float) {

         val latLng = LatLng(latitude,longitude)
         val markerOptions =  if (anchorX!= -1f && anchorY != -1f){
             MarkerOptions().position(latLng).anchor(anchorX, anchorY).icon(BitmapDescriptorFactory.fromResource(icon))
         }else{
             MarkerOptions().position(latLng).anchor(0.5F, 0.5F).icon(BitmapDescriptorFactory.fromResource(icon))
         }
         googleMap?.apply {
             addMarker(markerOptions)
             zoomCam?.let {
                 moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,it))
             }
         }

     }

     fun addRadiusArea(latitude:Double,longitude:Double,map:GoogleMap,color:Int,radiusValue:Double, context: Context): Circle {
         val latLng = LatLng(latitude,longitude)
         return map.addCircle(
             CircleOptions()
                 .strokeColor(
                     ContextCompat.getColor(context,color))
                 .center(latLng)
                 .fillColor( ContextCompat.getColor(context,color))
                 .radius(radiusValue)
                 .strokeWidth(0f)
         )
     }*/


}
