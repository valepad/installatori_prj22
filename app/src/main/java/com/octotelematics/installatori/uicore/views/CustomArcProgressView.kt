package com.octotelematics.installatori.uicore.views


import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.octotelematics.installatori.R

class CustomArcProgressView : View {
    //Sizes (with defaults)
    private var layoutHeight = 0
    private var layoutWidth = 0
    private var mBarWidth = 24f
    private var mFlatRim = true
    //Colors (with defaults)
    private var mProgressColor = 0xff8787
    private var mRimColor = 0xffcc33
    private var mShadowRimColor = 0x7f6619
    private var mDarkShadowRimColor = 0x000000
    private var mFiddleColor = 0xff8787
    private var mBaseProgressColor = 0xff8787
    private var mAnimationColor1 = 0xff8787
    private var mAnimationColor2 = 0xffd462
    private var mAnimationColor3 = 0x2fcd7c
    //Padding (with defaults)
    private var paddingTopCustom = 5
    private var paddingBottomCustom = 5
    private var paddingLeftCustom = 5
    private var paddingRightCustom = 5
    private var mPadding = 0f
    //Rectangles
    private var mRimBounds = RectF()
    private var mProgressBounds = RectF()
    //Paints
    val circlePaint = Paint()
    private val mBarPaint = Paint()
    private val mFiddlePaint = Paint()
    // Set percentage
    private var mPercentage = 0
    // Set type
    private var mType = 1

    fun setFlatRim(enabled: Boolean) {
        mFlatRim = enabled
        invalidate()
    }

    fun setRimColor(mRimColor: Int) {
        this.mRimColor = mRimColor
        invalidate()
    }

    constructor(context: Context?) : super(context) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(
        attrs: AttributeSet?,
        defStyle: Int
    ) { // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.ProgressView, defStyle, 0
        )
        mFlatRim = a.getBoolean(R.styleable.ProgressView_flatRim, mFlatRim)
        mRimColor = a.getColor(R.styleable.ProgressView_rimColor, mRimColor)
        mShadowRimColor = a.getColor(R.styleable.ProgressView_shadowRimColor, mShadowRimColor)
        mDarkShadowRimColor =
            a.getColor(R.styleable.ProgressView_darkShadowRimColor, mDarkShadowRimColor)
        mBarWidth = a.getDimension(R.styleable.ProgressView_barWidth, mBarWidth)
        mProgressColor = a.getColor(R.styleable.ProgressView_progressColor, mProgressColor)
        mFiddleColor = a.getColor(R.styleable.ProgressView_fiddleColor, mProgressColor)
        mBaseProgressColor = a.getColor(R.styleable.ProgressView_baseProgressColor, mProgressColor)
        mAnimationColor1 = a.getColor(R.styleable.ProgressView_animationColor1, mAnimationColor1)
        mAnimationColor2 = a.getColor(R.styleable.ProgressView_animationColor2, mAnimationColor2)
        mAnimationColor3 = a.getColor(R.styleable.ProgressView_animationColor3, mAnimationColor3)
        mPercentage = a.getInt(R.styleable.ProgressView_progress, mPercentage)
        mPadding = a.getDimension(R.styleable.ProgressView_padding, mPadding)
        if (isInEditMode) {
            if ("gradient" == a.getString(R.styleable.ProgressView_barType)) {
                mType = 3
            }
        }
        mType = a.getInteger(R.styleable.ProgressView_barType, mType)
        a.recycle()
        invalidate()
    }

    private fun setupPaints() {
        mBarPaint.color = mProgressColor
        mBarPaint.isAntiAlias = true
        mBarPaint.style = Paint.Style.STROKE
        mBarPaint.strokeWidth = mBarWidth
        mBarPaint.strokeCap = Paint.Cap.ROUND
        if (mType == 3) {
            val sweepShader = SweepGradient(
                layoutWidth.toFloat() / 2.toFloat(),
                layoutHeight.toFloat() / 2.toFloat(),
                intArrayOf(mBaseProgressColor, mProgressColor, mProgressColor),
                floatArrayOf(0f, 1.0f * mPercentage / 360, 1f)
            )
            mBarPaint.shader = sweepShader
        }
        if (mFlatRim) {
            circlePaint.color = mRimColor
        }
        circlePaint.isAntiAlias = true
        circlePaint.style = Paint.Style.STROKE
        circlePaint.strokeWidth = mBarWidth
        circlePaint.strokeCap = Paint.Cap.ROUND
        if (!mFlatRim) {
            val radius = mRimBounds.width() / 2 + mBarWidth
            circlePaint.shader = RadialGradient(
                width.toFloat() / 2.toFloat(),
                height.toFloat() / 2.toFloat(),
                radius,
                intArrayOf(
                    Color.TRANSPARENT,
                    mDarkShadowRimColor,
                    mRimColor,
                    mShadowRimColor
                ),
                floatArrayOf(
                    0.0f / radius,
                    (radius - mBarWidth * 2) / radius,
                    (radius - mBarWidth) / radius,
                    radius / radius
                ),
                Shader.TileMode.CLAMP
            )
        }
        mFiddlePaint.color = mFiddleColor
        mFiddlePaint.isAntiAlias = true
        mFiddlePaint.style = Paint.Style.FILL
    }

    private fun setupBounds() {
        val minValue = Math.min(layoutWidth, layoutHeight)
        // Calc the Offset if needed
        val xOffset = layoutWidth - minValue
        val yOffset = layoutHeight - minValue
        // Offset
        paddingTopCustom = (mPadding + yOffset / 2).toInt()
        paddingBottomCustom = (mPadding + yOffset / 2).toInt()
        paddingLeftCustom = (mPadding + xOffset / 2).toInt()
        paddingRightCustom = (mPadding + xOffset / 2).toInt()
        val width = width
        val height = height
        mRimBounds = RectF(
            paddingLeft + mBarWidth,
            paddingTop + mBarWidth,
            width - paddingRight - mBarWidth,
            height - paddingBottom - mBarWidth
        )
        mProgressBounds = RectF(
            paddingLeft + mBarWidth,
            paddingTop + mBarWidth,
            width - paddingRight - mBarWidth,
            height - paddingBottom - mBarWidth
        )
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        layoutWidth = w
        layoutHeight = h
        setupBounds()
        setupPaints()
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawArc(mRimBounds, 0f, 360f, false, circlePaint)
        canvas.rotate(-93f, layoutWidth / 2.toFloat(), layoutHeight / 2.toFloat())
        canvas.drawArc(mProgressBounds, 3f, mPercentage.toFloat(), false, mBarPaint)
        canvas.rotate(93f, layoutWidth / 2.toFloat(), layoutHeight / 2.toFloat())
        canvas.drawCircle(
            width / 2 + (Math.cos(Math.toRadians(mPercentage - 90.toDouble())) * (mProgressBounds.width() / 2 - 0.75)).toFloat(),
            height / 2 + (Math.sin(Math.toRadians(mPercentage - 90.toDouble())) * (mProgressBounds.height() / 2 - 0.75)).toFloat(),
            mBarWidth / 2 - 3, mFiddlePaint
        )
    }

    private fun percentToDegree(degree: Int): Int {
        var value = degree.toFloat()
        value = value * 360 / 100
        return value.toInt()
    }

    private fun startAnimation(per: Int) {
        val diff = per - mPercentage
        var red = -1
        var orange = -1
        var green = -1
        if (mType == 1) {
            if (per < percentToDegree(60)) {
                red = mAnimationColor1
            } else if (per >= percentToDegree(60) && per < percentToDegree(80)) {
                red = mAnimationColor1
                orange = mAnimationColor2
            } else {
                red = mAnimationColor1
                orange = mAnimationColor2
                green = mAnimationColor3
            }
        } else {
            red = mProgressColor
        }
        val firstColorAnimator: ValueAnimator
        var secondColorAnimator: ValueAnimator? = null
        var thirdColorAnimator: ValueAnimator? = null
        val valueAnimator = ValueAnimator
            .ofInt(mPercentage, mPercentage + diff)
            .setDuration(Math.abs(diff) * 5.toLong()) //diff * 5 because every degree takes 5ms - which means 360 degrees takes 1800ms (~2s)
        valueAnimator.addUpdateListener { animation: ValueAnimator ->
            mPercentage = animation.animatedValue as Int
            if (mType == 3) {
                val sweepShader = SweepGradient(
                    layoutWidth.toFloat() / 2.toFloat(),
                    layoutHeight.toFloat() / 2.toFloat(),
                    intArrayOf(mBaseProgressColor, mProgressColor, mProgressColor),
                    floatArrayOf(0f, 1.0f * mPercentage / 360, 1f)
                )
                mBarPaint.shader = sweepShader
            }
            invalidate()
        }
        if (mType != 1 || red != -1 && orange == -1 && green == -1) { // score < 60%
            firstColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    red, red
                )
                .setDuration(Math.abs(diff) * 3.toLong())
            firstColorAnimator.addUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            }
        } else if (red != -1 && orange != -1 && green == -1) { // score 60-80%
            firstColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    red, red
                )
                .setDuration(Math.abs(diff) * 3.toLong())
            firstColorAnimator.addUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            }
            secondColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    red, orange
                )
                .setDuration(Math.abs(diff) * 2.toLong())
            secondColorAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            })
        } else { // score > 80%
            firstColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    red, red
                )
                .setDuration(Math.abs(diff) * 3.toLong())
            firstColorAnimator.addUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            }
            secondColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    red, orange
                )
                .setDuration(Math.abs(diff).toLong())
            secondColorAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            })
            thirdColorAnimator = ValueAnimator
                .ofObject(
                    ArgbEvaluator(),
                    orange, green
                )
                .setDuration(Math.abs(diff).toLong())
            thirdColorAnimator.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator1: ValueAnimator ->
                mProgressColor = valueAnimator1.animatedValue as Int
                mBarPaint.color = mProgressColor
                invalidate()
            })
        }
        if (secondColorAnimator == null) { // < 60 %
            val set = AnimatorSet()
            set.playTogether(firstColorAnimator, valueAnimator)
            set.start()
        } else if (thirdColorAnimator == null) { // 60 - 80 %
            val set = AnimatorSet()
            val set1 = AnimatorSet()
            set1.playSequentially(firstColorAnimator, secondColorAnimator)
            set.playTogether(set1, valueAnimator)
            set.start()
        } else {
            val set = AnimatorSet()
            val set1 = AnimatorSet()
            set1.playSequentially(firstColorAnimator, secondColorAnimator, thirdColorAnimator)
            set.playTogether(set1, valueAnimator)
            set.start()
        }
    }

    fun setProgress(per: Int) {
        startAnimation(percentToDegree(per))
    }

    companion object {
        const val TAG = "BigProgressView"
    }
}