
package com.octotelematics.installatori.uicore.utils


import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.base.BaseActivity

class DialogUtils private constructor(builder: Builder, showAtStart: Boolean) {
    private var dialogUtils: DialogUtils?
    private val context: Context
    var alertDialog: AlertDialog? = null
    var titleView: TextView? = null
    var imageView: ImageView? = null
    var additionalView: TextView? = null
    var progressView: ProgressBar? = null
    var contentView: TextView? = null
    var contentView1: TextView? = null
    var contentView2: TextView? = null
    var contentView3: TextView? = null
    var contentView4: TextView? = null
    var negativeView: TextView? = null
    var positiveView: TextView? = null
    var lineView : View? = null
    private val isProgressDialog: Boolean
    private val isMandatoryDialog: Boolean
    private val isCustomLayoutDialog: Boolean
    private val onProgressTimeout: DialogInterface.OnCancelListener? = null
    //region Custom Dialog
    var baseColor: Int
    var states: Array<IntArray>
    var colors: IntArray

    fun show() {
        if (isProgressDialog && onProgressTimeout != null) {
            Handler().postDelayed(
                { alertDialog!!.setCancelable(true) },
                PROGRESS_TIMEOUT.toLong()
            )
        }
        if (alertDialog != null) {
            alertDialog!!.show()
        }
    }

    fun dismiss() {
        if (alertDialog != null) {
         try{
            alertDialog!!.dismiss()}catch (e:Exception){

         }
        }
        dialogUtils = null
    }

    //endregion
//region Setup Progress Dialog methods
    private fun setupProgressDialog(builder: Builder): AlertDialog {
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(
                builder.context,
                R.style.Theme_AppCompat_Dialog
            )
        val view: View = inflater.inflate(R.layout.utils_dialog_progress, null)
        titleView = view.findViewById(R.id.title)
        val progress_bar = view.findViewById<ProgressBar>(R.id.progress_bar)
        progress_bar.indeterminateTintList = ColorStateList.valueOf(baseColor)
        if (builder.title == null && builder.titleColor == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title
            titleView!!.setTextColor(builder.titleColor!!)
        }
        if (builder.onProgressTimeout != null) {
            bui.setOnCancelListener(builder.onProgressTimeout)
        }
        if (builder.progressColor != null){
            progress_bar.indeterminateTintList = ColorStateList.valueOf(builder.progressColor!!)
        }
        if (builder.titleFont != null){
            val type =
                Typeface.createFromAsset(builder.context.assets, builder.titleFont!!)
            titleView!!.typeface = type
        }
        bui.setView(view)
        bui.setCancelable(!isMandatoryDialog)
        bui.setTitle(null)
        bui.setIcon(null)
        return bui.create()
    }

    private fun setupCustomLayoutDialog(builder: Builder): AlertDialog {
        val bui = AlertDialog.Builder(builder.context)

        if (builder.customLayoutID != null) {
            val inflater = (builder.context as Activity).layoutInflater

            val view: View = inflater.inflate(builder.customLayoutID!!, null)

            bui.setView(view)

            try {
                negativeView = view.findViewById(R.id.negative_btn)
                positiveView = view.findViewById(R.id.positive_btn)

                if (negativeView != null) {
                    if (builder.onNegative != null) {
                        negativeView!!.setOnClickListener(if (builder.onNegative == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onNegative)
                    } else {
                        negativeView!!.visibility = View.GONE
                    }
                }
                if (positiveView != null) {
                    if (builder.onPositive != null) {
                        positiveView!!.setOnClickListener(if (builder.onPositive == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onPositive)
                    } else {
                        positiveView!!.visibility = View.GONE
                    }
                }

            } catch (e: Exception) {}
        }
        return bui.create()
    }

    private fun setupSingleDialog(builder: Builder): AlertDialog {
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_dialog_single, null)
        titleView = view.findViewById(R.id.title)
        additionalView = view.findViewById(R.id.additional)
        lineView = view.findViewById(R.id.line_view)
        progressView = view.findViewById(R.id.progress)
        contentView = view.findViewById(R.id.content)
        positiveView = view.findViewById(R.id.positive)
        if (builder.title == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title

            if (builder.titleColor != null) {
                titleView!!.setTextColor(builder.titleColor!!)
            }

            if (builder.isTitleBold != null && builder.isTitleBold!!) {
                titleView!!.setTypeface(titleView!!.typeface, Typeface.BOLD)
            }
        }

        if (lineView != null && builder.separatorColor != null) {
            lineView!!.setBackgroundColor(builder.separatorColor!!)
        }

        if (builder.isMessageBold != null && builder.isMessageBold!!) {
            contentView!!.setTypeface(contentView!!.typeface, Typeface.BOLD)
        }

        contentView!!.text = builder.content
        positiveView!!.text = builder.positiveText

        if (builder.isPositiveTextBold != null && builder.isPositiveTextBold!!) {
            positiveView!!.setTypeface(positiveView!!.typeface, Typeface.BOLD)
        }

        //titleView!!.setTextColor(baseColor)
        bui.setView(view)
        bui.setCancelable(!isMandatoryDialog)
        bui.setTitle(null)
        bui.setIcon(null)
        if (!isMandatoryDialog || builder.onCancel != null) {
            bui.setOnCancelListener(if (builder.onCancel == null) DialogInterface.OnCancelListener { dismiss() } else builder.onCancel)
        }
        if (!isMandatoryDialog || builder.onPositive != null) {
            positiveView!!.setOnClickListener(if (builder.onPositive == null) View.OnClickListener { dismiss() } else builder.onPositive)
        } else {
            positiveView!!.visibility = View.GONE
        }
        return bui.create()
    }

    private fun setDialogMultiText(builder:Builder): AlertDialog{
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_dialog_multi_text, null)
        titleView = view.findViewById(R.id.title)
        //additionalView = view.findViewById(R.id.additional)
        //progressView = view.findViewById(R.id.progress)
        //lineView = view.findViewById(R.id.line_view)
        contentView = view.findViewById(R.id.description_subtitle)
        contentView1 = view.findViewById(R.id.description_subtitle_1)
        contentView2 = view.findViewById(R.id.description_subtitle_2)
        contentView3 = view.findViewById(R.id.description_subtitle_3)
        contentView4 = view.findViewById(R.id.description_subtitle_4)
        negativeView = view.findViewById(R.id.negative_btn)
        positiveView = view.findViewById(R.id.positive_btn)
        if (builder.title == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title
        }
        contentView!!.text = builder.content
        contentView1!!.text = builder.content1
        contentView2!!.text = builder.content2
        contentView3!!.text = builder.content3
        contentView4!!.text = builder.content4
        negativeView!!.text = builder.negativeText
        positiveView!!.text = builder.positiveText
        bui.setView(view)
        bui.setCancelable(!isMandatoryDialog)
        bui.setTitle(null)
        bui.setIcon(null)
        if (!isMandatoryDialog || builder.onCancel != null) {
            bui.setOnCancelListener(if (builder.onCancel == null) DialogInterface.OnCancelListener { dialogInterface: DialogInterface? -> dismiss() } else builder.onCancel)
        }
        if (!isMandatoryDialog || builder.onNegative != null) {
            negativeView!!.setOnClickListener(if (builder.onNegative == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onNegative)
        } else {
            negativeView!!.visibility = View.GONE
        }
        if (!isMandatoryDialog || builder.onPositive != null) {
            positiveView!!.setOnClickListener(if (builder.onPositive == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onPositive)
        } else {
            positiveView!!.visibility = View.GONE
        }
        return bui.create()
    }


    private fun setOkChangeValue(builder:Builder): AlertDialog{
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_ok_change, null)
        titleView = view.findViewById(R.id.change_value_text)
        imageView = view.findViewById(R.id.img_ok)
        //additionalView = view.findViewById(R.id.additional)
        //progressView = view.findViewById(R.id.progress)
        //lineView = view.findViewById(R.id.line_view)
        imageView!!.setImageDrawable(builder.imageChange)
        if (builder.title == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title
        }
        bui.setView(view)
        bui.setTitle(null)
        bui.setIcon(null)
        return bui.create()
    }

    private fun setDialogImage(builder:Builder): AlertDialog{
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_dialog_image, null)
        titleView = view.findViewById(R.id.title)
        imageView = view.findViewById(R.id.image)
        //additionalView = view.findViewById(R.id.additional)
        //progressView = view.findViewById(R.id.progress)
        //lineView = view.findViewById(R.id.line_view)
        contentView = view.findViewById(R.id.description_subtitle)
        negativeView = view.findViewById(R.id.negative_btn)
        positiveView = view.findViewById(R.id.positive_btn)
        imageView!!.setImageDrawable(builder.image)
        if (builder.title == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title
        }
        contentView!!.text = builder.content
        negativeView!!.text = builder.negativeText
        positiveView!!.text = builder.positiveText
        bui.setView(view)
        bui.setCancelable(!isMandatoryDialog)
        bui.setTitle(null)
        bui.setIcon(null)
        if (!isMandatoryDialog || builder.onCancel != null) {
            bui.setOnCancelListener(if (builder.onCancel == null) DialogInterface.OnCancelListener { dialogInterface: DialogInterface? -> dismiss() } else builder.onCancel)
        }
        if (!isMandatoryDialog || builder.onNegative != null) {
            negativeView!!.setOnClickListener(if (builder.onNegative == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onNegative)
        } else {
            negativeView!!.visibility = View.GONE
        }
        if (!isMandatoryDialog || builder.onPositive != null) {
            positiveView!!.setOnClickListener(if (builder.onPositive == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onPositive)
        } else {
            positiveView!!.visibility = View.GONE
        }
        return bui.create()
    }

    private fun setupMultiDialog(builder: Builder): AlertDialog {
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_dialog_multi, null)
        titleView = view.findViewById(R.id.title)
        additionalView = view.findViewById(R.id.additional)
        progressView = view.findViewById(R.id.progress)
        lineView = view.findViewById(R.id.line_view)
        contentView = view.findViewById(R.id.content)
        negativeView = view.findViewById(R.id.negative)
        positiveView = view.findViewById(R.id.positive)
        if (builder.title == null) {
            titleView!!.visibility = View.GONE
        } else {
            titleView!!.text = builder.title

            if (builder.titleColor != null) {
                titleView!!.setTextColor(builder.titleColor!!)
            }

            if (builder.isTitleBold != null && builder.isTitleBold!!) {
                titleView!!.setTypeface(titleView!!.typeface, Typeface.BOLD)
            }
        }

        if (lineView != null && builder.separatorColor != null) {
            lineView!!.setBackgroundColor(builder.separatorColor!!)
        }

        if (builder.isMessageBold != null && builder.isMessageBold!!) {
            contentView!!.setTypeface(contentView!!.typeface, Typeface.BOLD)
        }

        if (builder.isPositiveTextBold != null && builder.isPositiveTextBold!!) {
            positiveView!!.setTypeface(positiveView!!.typeface, Typeface.BOLD)
        }

        if (builder.isNegativeTextBold != null && builder.isNegativeTextBold!!) {
            negativeView!!.setTypeface(negativeView!!.typeface, Typeface.BOLD)
        }

        if (builder.positiveTextColor != null) {
            positiveView!!.setTextColor(builder.positiveTextColor!!)
        }

        if (builder.negativeTextColor != null) {
            negativeView!!.setTextColor(builder.negativeTextColor!!)
        }

        contentView!!.text = builder.content
        negativeView!!.text = builder.negativeText
        positiveView!!.text = builder.positiveText
        bui.setView(view)
        bui.setCancelable(!isMandatoryDialog)
        bui.setTitle(null)
        bui.setIcon(null)
        if (!isMandatoryDialog || builder.onCancel != null) {
            bui.setOnCancelListener(if (builder.onCancel == null) DialogInterface.OnCancelListener { dialogInterface: DialogInterface? -> dismiss() } else builder.onCancel)
        }
        if (!isMandatoryDialog || builder.onNegative != null) {
            negativeView!!.setOnClickListener(if (builder.onNegative == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onNegative)
        } else {
            negativeView!!.visibility = View.GONE
        }
        if (!isMandatoryDialog || builder.onPositive != null) {
            positiveView!!.setOnClickListener(if (builder.onPositive == null) View.OnClickListener { view1: View? -> dismiss() } else builder.onPositive)
        } else {
            positiveView!!.visibility = View.GONE
        }
        return bui.create()
    }


    private fun setupTripEditDialog(builder: Builder, image : Int?, colorBackgroundRoundImage: Int?,    iconDriver: Int?,
                                    iconPassager: Int?,
                                    colorDriverIcon: Int?,
                                    colorPassangerIcon: Int?,
                                    colorDriverText: Int?,
                                    colorPassangerText: Int?): AlertDialog {
        val inflater = (builder.context as Activity).layoutInflater
        val bui =
            AlertDialog.Builder(builder.context)
        val view: View = inflater.inflate(R.layout.utils_dialog_trip_edit, null)
        val background = view.findViewById<RelativeLayout>(R.id.background)
        val container = view.findViewById<FrameLayout>(R.id.con)
        val icon = view.findViewById<ImageView>(R.id.icon)
        val driverPassengerView: View =
            inflater.inflate(R.layout.utils_dialog_driver_passenger, null)
        setupDriverPassenger(inflater, container, driverPassengerView, builder, iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText)
        bui.setView(view)
        bui.setCancelable(true)
        bui.setTitle(null)
        bui.setIcon(null)
        icon.backgroundTintList = ColorStateList.valueOf(baseColor)
        if (image != null){
            icon.setImageResource(image)
        }
        if (colorBackgroundRoundImage != null){
            icon.setBackgroundColor(colorBackgroundRoundImage)
        }
        bui.setOnCancelListener(if (builder.onCancel == null) DialogInterface.OnCancelListener { dialogInterface: DialogInterface? -> dismiss() } else builder.onCancel)
        background.setOnClickListener { view1: View? -> dismiss() }
        return bui.create()
    }

    //endregion
    interface CancelListener {
        fun onProgressTimeout()
    }


    fun useSetupTripEditDialog(builder: Builder,image : Int?, colorBackgroundRoundImage: Int?,    iconDriver: Int?,
                               iconPassager: Int?,
                               colorDriverIcon: Int?,
                               colorPassangerIcon: Int?,
                               colorDriverText: Int?,
                               colorPassangerText: Int?){
        setupTripEditDialog(builder, image, colorBackgroundRoundImage, iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText )
    }

    //region Setup Trip Edit Sub Views
    private fun setupDriverPassenger(
        inflater: LayoutInflater,
        container: FrameLayout,
        currentView: View,
        builder: Builder,
        iconDriver: Int?,
        iconPassager: Int?,
        colorDriverIcon: Int?,
        colorPassangerIcon: Int?,
        colorDriverText: Int?,
        colorPassangerText: Int?
    ) {
        val driverPassengerView: View =
            inflater.inflate(R.layout.utils_dialog_driver_passenger, null)
        switchView(container, currentView, driverPassengerView)
        val driver = driverPassengerView.findViewById<LinearLayout>(R.id.driver)
        val driverImage =
            driverPassengerView.findViewById<ImageView>(R.id.driverImage)
        val passenger = driverPassengerView.findViewById<LinearLayout>(R.id.passenger)
        val passengerImage =
            driverPassengerView.findViewById<ImageView>(R.id.passengerImage)
        val close =
            driverPassengerView.findViewById<ImageView>(R.id.close)
        val confirm = driverPassengerView.findViewById<TextView>(R.id.confirm)
        val deleteJourneys = driverPassengerView.findViewById<TextView>(R.id.delete)
        val title = driverPassengerView.findViewById<TextView>(R.id.title)
        val driverText = driverPassengerView.findViewById<TextView>(R.id.driverText)
        val passengerText = driverPassengerView.findViewById<TextView>(R.id.passengerText)
        driverImage.isSelected = !builder.passengerTrip
        passenger.isSelected = builder.passengerTrip
        driver.setOnClickListener { view1: View? ->
            driverImage.isSelected = true
            passengerImage.isSelected = false
        }
        passenger.setOnClickListener { view1: View? ->
            passengerImage.isSelected = true
            driverImage.isSelected = false
        }
        title.setTextColor(baseColor)
        close.setColorFilter(baseColor)
        driverText.setTextColor(baseColor)
        passengerText.setTextColor(baseColor)
        confirm.backgroundTintList = ColorStateList.valueOf(baseColor)
        passengerImage.backgroundTintList = ColorStateList(states, colors)
        driverImage.backgroundTintList = ColorStateList(states, colors)
        passengerImage.imageTintList = ColorStateList(
            states,
            intArrayOf(builder.context.resources.getColor(R.color.white), baseColor)
        )
        driverImage.imageTintList = ColorStateList(
            states,
            intArrayOf(builder.context.resources.getColor(R.color.white), baseColor)
        )
        if (iconDriver != null){
            driverImage.setImageResource(iconDriver)
        }
        if (iconPassager != null){
            passengerImage.setImageResource(iconPassager)
        }
        if (colorDriverIcon != null){
            driverImage.imageTintList = ColorStateList( states,
                intArrayOf(colorDriverIcon, baseColor))
        }
        if (colorPassangerIcon != null){
            passengerImage.imageTintList = ColorStateList( states,
                intArrayOf(colorPassangerIcon, baseColor))
        }
        if (colorDriverText != null){
            driverText.setTextColor(colorDriverText)
        }
        if (colorPassangerText != null){
            passengerText.setTextColor(colorPassangerText)
        }
        close.setOnClickListener { view1: View? -> dismiss() }
        confirm.setOnClickListener { view1: View? ->
            if (driverImage.isSelected) {
                if (!builder.bleConnected) { //show sub alertDialog with car / motorcycle
                    setupCarMotorcycle(inflater, container, driverPassengerView, builder, iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText)
                } else { //edit trip (driver)
                    builder.onConfirmEdit!!.onClick(1)
                }
            } else if (passengerImage.isSelected) { //edit trip (passenger)
                builder.onConfirmEdit!!.onClick(0)
            }
        }
        deleteJourneys.setOnClickListener { view1: View? ->
            //show sub alertDialog with trip delete
            setupDeleteJourney(inflater, container, driverPassengerView, builder,  iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText)
        }
    }

    private fun setupCarMotorcycle(
        inflater: LayoutInflater,
        container: FrameLayout,
        currentView: View,
        builder: Builder,
        iconDriver: Int?,
        iconPassager: Int?,
        colorDriverIcon: Int?,
        colorPassangerIcon: Int?,
        colorDriverText: Int?,
        colorPassangerText: Int?
    ) {
        val carMotorcycleView: View =
            inflater.inflate(R.layout.utils_dialog_car_motorcycle, null)
        switchView(container, currentView, carMotorcycleView)
        val car = carMotorcycleView.findViewById<LinearLayout>(R.id.car)
        val carImage =
            carMotorcycleView.findViewById<ImageView>(R.id.carImage)
        val motorcycle = carMotorcycleView.findViewById<LinearLayout>(R.id.motorcycle)
        val motorcycleImage =
            carMotorcycleView.findViewById<ImageView>(R.id.motorcycleImage)
        val close =
            carMotorcycleView.findViewById<ImageView>(R.id.close)
        val confirm = carMotorcycleView.findViewById<TextView>(R.id.confirm)
        val cancel = carMotorcycleView.findViewById<TextView>(R.id.cancel)
        val title = carMotorcycleView.findViewById<TextView>(R.id.title)
        val carText = carMotorcycleView.findViewById<TextView>(R.id.carText)
        val motorcycleText = carMotorcycleView.findViewById<TextView>(R.id.motorcycleText)
        title.setTextColor(baseColor)
        close.setColorFilter(baseColor)
        carText.setTextColor(baseColor)
        motorcycleText.setTextColor(baseColor)
        confirm.backgroundTintList = ColorStateList.valueOf(baseColor)
        confirm.backgroundTintList = ColorStateList.valueOf(baseColor)
        carImage.backgroundTintList = ColorStateList(states, colors)
        motorcycleImage.backgroundTintList = ColorStateList(states, colors)
        carImage.imageTintList = ColorStateList(
            states,
            intArrayOf(builder.context.resources.getColor(R.color.white), baseColor)
        )
        motorcycleImage.imageTintList = ColorStateList(
            states,
            intArrayOf(builder.context.resources.getColor(R.color.white), baseColor)
        )
        car.setOnClickListener { view1: View? ->
            carImage.isSelected = true
            motorcycleImage.isSelected = false
            confirm.isEnabled = true
        }
        motorcycle.setOnClickListener { view1: View? ->
            motorcycleImage.isSelected = true
            carImage.isSelected = false
            confirm.isEnabled = true
        }
        close.setOnClickListener { view1: View? -> dismiss() }
        confirm.setOnClickListener { view1: View? ->
            if (carImage.isSelected) { //edit trip (driver car)
                builder.onConfirmEdit!!.onClick(2)
            } else if (motorcycleImage.isSelected) { //edit trip (driver motorcycle)
                builder.onConfirmEdit!!.onClick(3)
            }
        }
        cancel.setOnClickListener { view1: View? ->
            //show sub alertDialog with driver passenger again
            setupDriverPassenger(inflater, container, carMotorcycleView, builder, iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText)
        }
    }

    private fun setupDeleteJourney(
        inflater: LayoutInflater,
        container: FrameLayout,
        currentView: View,
        builder: Builder,
        iconDriver: Int?,
        iconPassager: Int?,
        colorDriverIcon: Int?,
        colorPassangerIcon: Int?,
        colorDriverText: Int?,
        colorPassangerText: Int?
    ) {
        val deleteJourneyView: View =
            inflater.inflate(R.layout.utils_dialog_delete_journey, null)
        switchView(container, currentView, deleteJourneyView)
        val close =
            deleteJourneyView.findViewById<ImageView>(R.id.close)
        val confirm = deleteJourneyView.findViewById<TextView>(R.id.confirm)
        val cancel = deleteJourneyView.findViewById<TextView>(R.id.cancel)
        val title = deleteJourneyView.findViewById<TextView>(R.id.title)
        title.setTextColor(baseColor)
        cancel.setTextColor(baseColor)
        close.setColorFilter(baseColor)
        close.setOnClickListener { view1: View? -> dismiss() }
        confirm.setOnClickListener { view1: View? -> builder.onDeleteTrip!!.onClick() }
        cancel.setOnClickListener { view1: View? ->
            //show sub alertDialog with driver passenger again
            setupDriverPassenger(inflater, container, deleteJourneyView, builder, iconDriver, iconPassager, colorDriverIcon, colorPassangerIcon, colorDriverText, colorPassangerText)
        }
    }

    private fun switchView(
        container: FrameLayout,
        fromView: View,
        toView: View
    ) {
        val fromViewAnimator = ObjectAnimator.ofFloat(fromView, "alpha", 0.0f)
        fromViewAnimator.duration = 500
        toView.alpha = 0.0f
        val toViewAnimator = ObjectAnimator.ofFloat(toView, "alpha", 1.0f)
        fromViewAnimator.duration = 500
        val set = AnimatorSet()
        set.playTogether(fromViewAnimator, toViewAnimator)
        set.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {
                container.addView(toView)
            }

            override fun onAnimationEnd(animator: Animator) {
                if (container.childCount > 1) container.removeViewAt(0)
            }

            override fun onAnimationCancel(animator: Animator) {}
            override fun onAnimationRepeat(animator: Animator) {}
        })
        set.start()
    }

    //endregion
//region Builder
    class Builder {
        var title: String? = null
        var content: String? = null
        var content1: String? = null
        var content2: String? = null
        var content3: String? = null
        var content4: String? = null
        var image: Drawable? = null
        var imageChange: Drawable? = null
        var positiveText: String? = null
        var negativeText: String? = null
        var onPositive: View.OnClickListener? = null
        var onNegative: View.OnClickListener? = null
        var onPositiveBack: View.OnClickListener? = null
        var onProgressTimeout: DialogInterface.OnCancelListener? = null
        var onConfirmEdit: OnConfirmEditClickListener? = null
        var onDeleteTrip: OnDeleteTripClickListener? = null
        var progress = false
        var mandatory = false
        var customLayout = false
        var bleConnected = false
        var passengerTrip = false
        var onCancel: DialogInterface.OnCancelListener? = null
        var dialog: DialogUtils? = null
        var context: Context
        var titleColor: Int?  = null
        var titleFont: String? = null
        var progressColor: Int? = null
        var messageColor: Int? = null
        var separatorColor: Int? = null
        var positiveBackgroundColor: Int? = null
        var positiveTextColor : Int? = null
        var negativeBackgroundColor: Int? = null
        var negativeTextColor : Int? = null
        var lineViewColor: Int? = null
        var closeImageColor: Int? = null
        var customLayoutID: Int? = null
        var isTitleBold: Boolean? = null
        var isMessageBold: Boolean? = null
        var isPositiveTextBold: Boolean? = null
        var isNegativeTextBold: Boolean? = null


        constructor(dialog: DialogUtils?, context: Context) {
            this.context = context
            if (dialog != null) {
                this.dialog = dialog
            }
        }

        fun mandatory(): Builder {
            mandatory = true
            return this
        }

        constructor(
            dialog: DialogUtils?,
            context: Context,
            passengerTrip: Boolean
        ) {
            this.context = context
            this.passengerTrip = passengerTrip
            if (dialog != null) {
                this.dialog = dialog
            }
        }

        fun progress(): Builder {
            mandatory = true // progress is also mandatory in whole app
            progress = true
            return this
        }

        fun customLayout(): Builder {
            customLayout = true
            return this
        }

        fun onProgressTimeout(onProgressTimeout: DialogInterface.OnCancelListener?): Builder {
            this.onProgressTimeout = onProgressTimeout
            return this
        }

        fun bleConnected(): Builder {
            bleConnected = true
            return this
        }

        fun title(title: String?): Builder {
            this.title = title
            return this
        }

        fun title(@StringRes title: Int): Builder {
            this.title = context.getString(title)
            return this
        }

        fun content(content: String?): Builder {
            this.content = content
            return this
        }

        fun content1(content1: String?): Builder {
            this.content1 = content1
            return this
        }

        fun content2(content2: String?): Builder {
            this.content2 = content2
            return this
        }

        fun content3(content3: String?): Builder {
            this.content3 = content3
            return this
        }

        fun content4(content4: String?): Builder {
            this.content4 = content4
            return this
        }

        fun content(@StringRes content: Int): Builder {
            this.content = context.getString(content)
            return this
        }

        fun positiveText(positiveText: String?): Builder {
            this.positiveText = positiveText
            return this
        }

        fun positiveText(@StringRes positiveText: Int): Builder {
            this.positiveText = context.getString(positiveText)
            return this
        }

        fun negativeText(negativeText: String?): Builder {
            this.negativeText = negativeText
            return this
        }

        fun negativeText(@StringRes negativeText: Int): Builder {
            this.negativeText = context.getString(negativeText)
            return this
        }

        fun onPositive(onPositive: View.OnClickListener?): Builder {
            this.onPositive = onPositive
            return this
        }

        fun onNegative(onNegative: View.OnClickListener?): Builder {
            this.onNegative = onNegative
            return this
        }

        fun onConfirmEdit(onConfirmEdit: OnConfirmEditClickListener?): Builder {
            this.onConfirmEdit = onConfirmEdit
            return this
        }

        fun onDeleteTrip(onDeleteTrip: OnDeleteTripClickListener?): Builder {
            this.onDeleteTrip = onDeleteTrip
            return this
        }

        fun onCancel(onCancel: DialogInterface.OnCancelListener?): Builder {
            this.onCancel = onCancel
            return this
        }

        fun setProgressColor(color: Int) : Builder {
            this.progressColor = color
            return this
        }

        fun setTitleColor(color: Int) : Builder {
            this.titleColor = color
            return this
        }

        fun setTitleFont(font: String) : Builder {
            this.titleFont = font
            return this
        }

        fun setSeparatorColor(color: Int) : Builder {
            this.separatorColor = color
            return this
        }

        fun setPositiveBackgroundColor(color: Int): Builder {
            this.positiveBackgroundColor = color
            return this
        }

        fun setMessageColor(color: Int) : Builder{
            this.messageColor = color
            return this
        }

        fun setPositiveTextColor(color: Int): Builder{
            this.positiveTextColor = color
            return this
        }


        fun setNegativeBackgroundColor(color: Int): Builder {
            this.negativeBackgroundColor = color
            return this
        }


        fun setNegativeTextColor(color: Int): Builder{
            this.negativeTextColor = color
            return this
        }

        fun setLineViewColor(color: Int): Builder{
            this.lineViewColor = color
            return this
        }

        fun setCloseImageColor(color: Int): Builder{
            this.closeImageColor = color
            return this
        }

        fun setImage (img: Drawable): Builder{
            this.image = img
            return this
        }

        fun setImageChange (img: Drawable): Builder{
            this.imageChange = img
            return this
        }

        fun setCustomLayoutID(customLayoutID: Int?): Builder {
            this.customLayoutID = customLayoutID
            return this
        }

        fun setIsTitleBold(isTitleBold: Boolean?): Builder {
            this.isTitleBold = isTitleBold
            return this
        }

        fun setIsMessageBold(isMessageBold: Boolean?): Builder {
            this.isMessageBold = isMessageBold
            return this
        }

        fun setIsPositiveTextBold(isPositiveTextBold: Boolean?): Builder {
            this.isPositiveTextBold = isPositiveTextBold
            return this
        }

        fun setIsNegativeTextBold(isNegativeTextBold: Boolean?): Builder {
            this.isNegativeTextBold = isNegativeTextBold
            return this
        }

        fun show(): DialogUtils {
            return if (dialog != null && dialog!!.alertDialog != null && dialog!!.alertDialog!!.isShowing) {
                dialog!!
            } else {
                dialog = DialogUtils(this, true)
                dialog!!
            }
        }

        fun build(): DialogUtils {
            dialog = DialogUtils(this, false)
            return dialog!!
        }
    }

    interface OnConfirmEditClickListener {
        /**
         * 0 - PASSENGER <br></br>
         * 1 - DRIVER <br></br>
         * 2 - DRIVER (CAR) <br></br>
         * 3 - DRIVER (MOTORCYCLE)
         *
         * @param currentModeOfTransport Int value representing transport mode
         */
        fun onClick(currentModeOfTransport: Int)
    }

    interface OnDeleteTripClickListener {
        fun onClick()
    } //endregion

    companion object {
        private const val PROGRESS_TIMEOUT = 30000 //30s

        fun makeDialog(dialogUtils: DialogUtils?, context: Context, title: String?, titleColor: Int?,
                       message: String?, messageColor: Int?, positiveText: String?, negativeText: String?): DialogUtils {
            var dialog = Builder(
                dialogUtils,
                context
            )

            if (title != null) {
                dialog.title(title)
            }
            if (titleColor != null) {
                dialog.setTitleColor(context.resources.getColor(titleColor))
            } else {
                dialog.setTitleColor(context.resources.getColor(R.color.colorPrimary))
            }
            if (message != null) {
                dialog.content(message)
            }
            if (messageColor != null) {
                dialog.setMessageColor(context.resources.getColor(messageColor))
            } else {
                dialog.setMessageColor(context.resources.getColor(R.color.colorPrimary))
            }
            if (positiveText != null) {
                dialog.positiveText(positiveText)
            }
            if (negativeText != null) {
                dialog.negativeText(negativeText)
            }

            return dialog.show()
        }

        fun makeDialog(dialogUtils: DialogUtils?, context: Context, title: String?, titleColor: Int?,
                       message: String?, messageColor: Int?, positiveText: String?, positiveListener: View.OnClickListener?,
                       negativeText: String?, negativeListener: View.OnClickListener?): DialogUtils {
            var dialog = Builder(
                dialogUtils,
                context
            )

            if (title != null) {
                dialog.title(title)
            }
            if (titleColor != null) {
                dialog.setTitleColor(context.resources.getColor(titleColor))
            } else {
                dialog.setTitleColor(context.resources.getColor(R.color.colorPrimary))
            }
            if (message != null) {
                dialog.content(message)
            }
            if (messageColor != null) {
                dialog.setMessageColor(context.resources.getColor(messageColor))
            } else {
                dialog.setMessageColor(context.resources.getColor(R.color.colorPrimary))
            }
            if (positiveText != null) {
                dialog.positiveText(positiveText)
            }
            if (positiveListener != null) {
                dialog.onPositive = positiveListener
            }
            if (negativeText != null) {
                dialog.negativeText(negativeText)
            }
            if (negativeListener != null) {
                dialog.onNegative = negativeListener
            }

            return dialog.show()
        }

        fun showProgressDialog(progress: DialogUtils?, activity: BaseActivity) : DialogUtils {
            val builder: Builder = Builder(progress, activity)
                .title(R.string.loading)
                .content("Please wait!")
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(activity.resources.getColor(R.color.green_mint))
                .setProgressColor(activity.resources.getColor(R.color.green_mint))
                .progress()
            return builder.show()
        }

        fun showProgressDialog(progress: DialogUtils?, activity: BaseActivity, title: String,
                               titleFont: String?, message: String) : DialogUtils {
            val builder: Builder = Builder(progress, activity)
                .title(title)
                .content(message)
                .setTitleColor(activity.resources.getColor(R.color.green_mint))
                .setProgressColor(activity.resources.getColor(R.color.green_mint))
                .progress()
            if (titleFont != null && titleFont.isNotEmpty()) {
                builder.setTitleFont(titleFont)
            }
            return builder.show()
        }

        fun showUnauthorizedDialog(dialog: DialogUtils?, activity: Activity) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(R.string.unauthorized_dialog_title)
                .content(R.string.unauthorized_dialog_message)
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(activity.resources.getColor(R.color.colorPrimary))
                .setProgressColor(activity.resources.getColor(R.color.colorPrimary))
                .positiveText(R.string.unauthorized_dialog_button)
            return builder.show()
        }

        fun showErrorDialog(dialog: DialogUtils?, activity: Activity, title: Int, message: Int, button: Int) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(title)
                .content(message)
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(activity.resources.getColor(R.color.colorPrimary))
                .setProgressColor(activity.resources.getColor(R.color.colorPrimary))
                .positiveText(button)
            return builder.show()
        }

        fun showErrorDialog(dialog: DialogUtils?, activity: Activity, title: Int, message: String, button: Int) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(title)
                .content(message)
                .setTitleFont("fonts/solomon_book.ttf")
                .setTitleColor(activity.resources.getColor(R.color.colorPrimary))
                .setProgressColor(activity.resources.getColor(R.color.colorPrimary))
                .positiveText(button)
            return builder.show()
        }

        fun showErrorDialog(dialog: DialogUtils?, activity: Activity, title: String, titleFont: String?,
                            message: String, button: String) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(title)
                .content(message)
                .setTitleColor(activity.resources.getColor(R.color.colorPrimary))
                .setProgressColor(activity.resources.getColor(R.color.colorPrimary))
                .positiveText(button)
            if (titleFont != null && titleFont.isNotEmpty()) {
                builder.setTitleFont(titleFont)
            }
            return builder.show()
        }

        fun showErrorDialog(dialog: DialogUtils?, activity: Activity, title: String, titleColor: Int?, isTitleBold: Boolean?, titleFont: String?,
                            message: String, isMessageBold: Boolean?, separatorColor: Int?, button: String) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(title)
                .content(message)
                .setTitleColor(activity.resources.getColor(titleColor?: R.color.colorPrimary))
                .setSeparatorColor(activity.resources.getColor(separatorColor?: R.color.colorPrimary))
                .setIsTitleBold(isTitleBold)
                .setIsMessageBold(isMessageBold)
                .positiveText(button)
            if (titleFont != null && titleFont.isNotEmpty()) {
                builder.setTitleFont(titleFont)
            }
            return builder.show()
        }

        fun showDefaultSmallErrorDialog(dialog: DialogUtils?, activity: Activity, title: String,
                            message: String, button: String) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .title(title)
                .content(message)
                .setTitleColor(activity.resources.getColor(R.color.black))
                .setSeparatorColor(activity.resources.getColor(R.color.black))
                .setIsTitleBold(true)
                .positiveText(button)
                .setIsPositiveTextBold(true)
            return builder.show()
        }

        fun showCustomLayoutDialog(dialog: DialogUtils?, activity: Activity, layoutID: Int?,
                                   onNegative: View.OnClickListener?, onPositive: View.OnClickListener?) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .customLayout()
                .setCustomLayoutID(layoutID)
                .onNegative(onNegative)
                .onPositive(onPositive)
            return builder.show()
        }

        fun showCustomLayoutDialog(dialog: DialogUtils?, activity: Activity, layoutID: Int?,
                                   onNegative: View.OnClickListener?, onPositive: View.OnClickListener?,
                                   show: Boolean) : DialogUtils {
            val builder: Builder = Builder(dialog, activity)
                .customLayout()
                .setCustomLayoutID(layoutID)
                .onNegative(onNegative)
                .onPositive(onPositive)
            return if (show) builder.show() else DialogUtils(builder, false)
        }
    }

    init {
        run {
            if (builder.dialog != null && builder.dialog!!.alertDialog != null && builder.dialog!!.alertDialog!!.isShowing) {
                return@run
            }
        }
    }

    init {
        baseColor = builder.context.resources.getColor(R.color.white)
        states = arrayOf(
            intArrayOf(android.R.attr.state_selected),
            intArrayOf(-android.R.attr.state_selected)
        )
        colors = intArrayOf(baseColor, builder.context.resources.getColor(R.color.white))
        context = builder.context
        isProgressDialog = builder.progress
        isMandatoryDialog = builder.mandatory
        isCustomLayoutDialog = builder.customLayout
        dialogUtils = builder.dialog
        val onConfirmEdit = builder.onConfirmEdit
        val onDeleteTrip = builder.onDeleteTrip
        alertDialog = if (isProgressDialog) {
            setupProgressDialog(builder)
        } else if (isCustomLayoutDialog) {
            setupCustomLayoutDialog(builder)
        } else if (onDeleteTrip != null && onConfirmEdit != null) {
            setupTripEditDialog(builder, null, null, null, null, null, null, null, null)
        } else if (builder.onNegative == null && builder.negativeText == null) {
            setupSingleDialog(builder)
        }  else if (builder.image != null) {
            setDialogImage(builder)
        }  else if (builder.imageChange != null) {
            setOkChangeValue(builder)
        } else if (builder.content1 != null) {
            setDialogMultiText(builder)
        }
        else {
            setupMultiDialog(builder)
        }
        if (alertDialog!!.window != null) {
            alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        if (isProgressDialog && onProgressTimeout != null) {
            Handler().postDelayed({
                if (alertDialog!!.isShowing) {
                    alertDialog!!.cancel()
                }
            }, PROGRESS_TIMEOUT.toLong())
        }
        if (showAtStart) {
            try{
            alertDialog?.show()}catch (e:Exception){

            }
        }
    }
}