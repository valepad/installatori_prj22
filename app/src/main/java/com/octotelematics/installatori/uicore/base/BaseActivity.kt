package com.octotelematics.installatori.uicore.base


import android.content.Context
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.octotelematics.installatori.R
import com.octotelematics.installatori.fragments.profile.HelpFragment
import com.octotelematics.installatori.main.LoginFragment
import com.octotelematics.installatori.main.ProfileFragment
import com.octotelematics.installatori.uicore.utils.UIUtils.Companion.hideKeyboard
import com.octotelematics.installatori.uicore.views.CustomProgress
import com.octotelematics.installatori.utils.BottomSheetFragment
import com.octotelematics.installatori.utils.ProfileBottomFragment
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlin.math.roundToInt


open class BaseActivity : AppCompatActivity() {
    lateinit var mTextViewScreenTitle: TextView  // digital-driver 2.0 ToolbarTitle
    lateinit var mTextLeft: TextView  // digital_driver 2.0 ToolbarleftText
    lateinit var mTextRight: TextView
    lateinit var mToolbar: Toolbar
    lateinit var mImageButtonBack: ImageButton // digital-driver 2.0 ToolbarBack
    lateinit var mImageRight: ImageButton      // digital-driver 2.0 ToolbarRightImage
    lateinit var mContainerToobar: RelativeLayout
    lateinit var mProgressDialog: CustomProgress
    lateinit var mImageCenter: ImageButton
    lateinit var mBottomNavigationView: BottomNavigationView
    lateinit var mContainerText: RelativeLayout
    lateinit var mSubtitleText: TextView
    var currentFragment: Fragment? = null
    lateinit var toolbarProgressBar: ProgressBar  // digital-driver 2.0 ToolbarProgressBar
    lateinit var divider: View // digital-driver 2.0 Toolbardivider

    lateinit var darkToolbarLayout: RelativeLayout   // digital_driver 2.0 ToolbarLayout dark theme
    lateinit var darkToolbarTitle: TextView          // digital_driver 2.0 ToolbarleftText dark theme
    lateinit var darkToolbarBack: ImageButton        // digital_driver 2.0 ToolbarBackBtn dark theme
    lateinit var darkToolbarLogo: ImageView         // digital_driver 2.0 ToolbarLogo dark theme

    // new toolbar
    lateinit var mTextViewScreenTitleNew: TextView
    lateinit var mTextLeftNew: TextView
    lateinit var mTextRightNew: TextView
    lateinit var mImageButtonBackNew: ImageButton
    lateinit var mImageRightNew: ImageButton
    lateinit var mImageCenterNew: ImageButton
    lateinit var mSubtitleTextNew: TextView
    lateinit var mNewToolbar: LinearLayout
    lateinit var mLayoutNewLeft: RelativeLayout
    lateinit var mLayoutNewCenter: RelativeLayout
    lateinit var mLayoutNewRight: RelativeLayout

    // new toolbar 2
    lateinit var mTextViewScreenTitleNew2: TextView
    lateinit var mTextLeftNew2: TextView
    lateinit var mTextRightNew2: TextView
    lateinit var mImageButtonBackNew2: ImageButton
    lateinit var mImageRightNew2: ImageButton
    lateinit var mImageCenterNew2: ImageButton
    lateinit var mSubtitleTextNew2: TextView
    lateinit var mNewToolbar2: RelativeLayout
    lateinit var mLayoutNewLeft2: RelativeLayout
    lateinit var mLayoutNewCenter2: RelativeLayout
    lateinit var mLayoutNewRight2: RelativeLayout
    lateinit var toolbarParams: ViewGroup.LayoutParams


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        try {
            mProgressDialog = CustomProgress.getInstance(this)
            mProgressDialog.setTextMessage("Loading")
            mProgressDialog.setCancelable(false)
        } catch (e: Exception) {
        }
    }

    override fun setContentView(layoutResID: Int) {
        try {

            var relativeLayout: RelativeLayout =
                layoutInflater.inflate(R.layout.activity_base, null) as RelativeLayout
            var activityContainer: FrameLayout = relativeLayout.findViewById(R.id.layout_container)
            mTextViewScreenTitle = relativeLayout.findViewById(R.id.text_screen_title) as TextView
            mToolbar = relativeLayout.findViewById(R.id.toolbar)
            mTextLeft = relativeLayout.findViewById(R.id.text_left) as TextView
            mTextRight = relativeLayout.findViewById(R.id.text_right) as TextView
            mImageButtonBack = relativeLayout.findViewById(R.id.image_left)
            mImageRight = relativeLayout.findViewById(R.id.image_right)
            mImageCenter = relativeLayout.findViewById(R.id.image_center)
            mContainerToobar = relativeLayout.findViewById(R.id.container_toolbar)
            mBottomNavigationView = relativeLayout.findViewById(R.id.bottomNavigation)
            mContainerText = relativeLayout.findViewById(R.id.containerTexts)
            mSubtitleText = relativeLayout.findViewById(R.id.subtext_screen_title) as TextView
            toolbarProgressBar = relativeLayout.findViewById(R.id.toolbar_progress_bar)
            divider = relativeLayout.findViewById(R.id.toolbar_margin)
            darkToolbarTitle = relativeLayout.findViewById(R.id.toolbar_text_left)
            darkToolbarBack = relativeLayout.findViewById(R.id.back_btn_dark_theme)
            darkToolbarLayout = relativeLayout.findViewById(R.id.dark_theme_toolbar)
            darkToolbarLogo = relativeLayout.findViewById(R.id.dark_toolbar_logo)

            // new toolbar
            mTextViewScreenTitleNew =
                relativeLayout.findViewById(R.id.text_screen_title_new) as TextView
            mTextLeftNew = relativeLayout.findViewById(R.id.text_left_new) as TextView
            mTextRightNew = relativeLayout.findViewById(R.id.text_right_new) as TextView
            mImageButtonBackNew = relativeLayout.findViewById(R.id.image_left_new)
            mImageRightNew = relativeLayout.findViewById(R.id.image_right_new)
            mImageCenterNew = relativeLayout.findViewById(R.id.image_center_new)
            mSubtitleTextNew =
                relativeLayout.findViewById(R.id.subtext_screen_title_new) as TextView
            mNewToolbar = relativeLayout.findViewById(R.id.new_toolbar)
            mLayoutNewLeft = relativeLayout.findViewById(R.id.layout_new_left)
            mLayoutNewCenter = relativeLayout.findViewById(R.id.layout_new_center)
            mLayoutNewRight = relativeLayout.findViewById(R.id.layout_new_right)

            // new toolbar 2
            mTextViewScreenTitleNew2 =
                relativeLayout.findViewById(R.id.text_screen_title_new_2) as TextView
            mTextLeftNew2 = relativeLayout.findViewById(R.id.text_left_new_2) as TextView
            mTextRightNew2 = relativeLayout.findViewById(R.id.text_right_new_2) as TextView
            mImageButtonBackNew2 = relativeLayout.findViewById(R.id.image_left_new_2)
            mImageRightNew2 = relativeLayout.findViewById(R.id.image_right_new_2)
            mImageCenterNew2 = relativeLayout.findViewById(R.id.image_center_new_2)
            mSubtitleTextNew2 =
                relativeLayout.findViewById(R.id.subtext_screen_title_new_2) as TextView
            mNewToolbar2 = relativeLayout.findViewById(R.id.new_toolbar_2)
            mLayoutNewLeft2 = relativeLayout.findViewById(R.id.layout_new_left_2)
            mLayoutNewCenter2 = relativeLayout.findViewById(R.id.layout_new_center_2)
            mLayoutNewRight2 = relativeLayout.findViewById(R.id.layout_new_right_2)
            toolbarParams = mToolbar.layoutParams

            layoutInflater.inflate(layoutResID, activityContainer, true)

            super.setContentView(relativeLayout)
        } catch (e: Exception) {
        }
    }

    companion object {
        val instance = BaseActivity()
    }


    fun addFragment(
        fragment: Fragment,
        replace: Boolean,
        addToBackStack: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragment.javaClass.name)
            }
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    open fun checkDeniedPermission(permissions: List<String?>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (p in permissions) {
                val res = this.checkCallingOrSelfPermission(p!!)
                if (res != PackageManager.PERMISSION_GRANTED) {
                    return true
                }
            }
        }
        return false
    }

    fun addFragmentModal(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_bottom, R.anim.slide_out_to_top,
                R.anim.slide_in_from_top, R.anim.slide_out_to_bottom
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun addFragmentLeft(
        fragment: Fragment,
        replace: Boolean,
        rootViewId: Int
    ) {

        try {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_left, R.anim.slide_out_to_right,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun addFragmentBottom(
        fragment: Fragment,
        replace: Boolean,
        rootViewId: Int
    ) {

        try {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom,
                R.anim.slide_out_to_bottom, R.anim.slide_in_from_bottom
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun addFragmentRight(
        fragment: Fragment,
        replace: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun replaceFragment(
        fragment: Fragment,
        addToBackStack: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )

            supportFragmentManager!!.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragment.javaClass.name)
            }

            fragmentTransaction.replace(rootViewId, fragment)

            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun changeFragment(
        fragment: Fragment,
        rootViewid: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootViewid, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun changeFragmentLeft(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_left, R.anim.slide_out_to_right,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }

    fun changeFragmentRight(
        fragment: Fragment,
        rootView: View
    ) {
        try {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootView.id, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun setScreenTitle(title: String) {
        try {
            mTextViewScreenTitle.text = title
            mImageCenter.visibility = View.GONE
            mTextViewScreenTitle.visibility = View.VISIBLE
            mSubtitleText.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextSubtitle(title: String, context: Context, font: String?, color: Int?, size: Float?) {
        try {
            mSubtitleText.text = title
            if (color != null) {
                mSubtitleText.setTextColor(color)
            }
            if (size != null) {
                mSubtitleText.textSize = size
            }
            val type =
                Typeface.createFromAsset(context.assets, font)
            mSubtitleText.typeface = type
            mSubtitleText.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun getBackButton(): ImageButton {
        return mImageButtonBack;
    }

    fun showProgress() {
        try {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.showProgress()
            }
        } catch (e: Exception) {
        }
    }

    fun hideProgress() {
        try {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hideProgress()
            }
        } catch (e: Exception) {
        }
    }

    fun hideAllLeft() {
        try {
            mImageButtonBack.visibility = View.GONE
            mTextLeft.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextLeft(text: String) {
        try {
            mTextLeft.text = text
            mImageButtonBack.visibility = View.GONE
            mTextLeft.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextLeft(text: String, color: Int?, style: Boolean?, size: Float?) {
        try {
            mTextLeft.text = text
            mImageButtonBack.visibility = View.GONE
            mTextLeft.visibility = View.VISIBLE
            if (color != null) {
                mTextLeft.setTextColor(color)
            }
            if (style == true) {
                mTextLeft.setTypeface(null, Typeface.BOLD)
            }
            if (size != null) {
                mTextLeft.textSize = size
            }
        } catch (e: Exception) {
        }
    }

    fun hideAllRight() {
        try {
            mImageRight.visibility = View.GONE
            mTextRight.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextRight(text: String, color: Int?, size: Float?, style: Boolean) {
        try {
            mTextRight.text = text
            mImageRight.visibility = View.GONE
            mTextRight.visibility = View.VISIBLE
            if (color != null) {
                mTextRight.setTextColor(color)
            }
            if (size != null) {
                mTextRight.textSize = size
            }
            if (style == true) {
                mTextRight.setTypeface(null, Typeface.BOLD)
            }
        } catch (e: Exception) {
        }
    }


    fun setTextRight(text: String) {
        try {
            mTextRight.text = text
            mImageRight.visibility = View.GONE
            mTextRight.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextRightListener(text: String, listener: View.OnClickListener?, color: Int?) {
        try {
            mTextRight.text = text
            mImageRight.visibility = View.GONE
            mTextRight.visibility = View.VISIBLE
            if (listener != null) {
                mTextRight.setOnClickListener(listener)
            }
            if (color != null) {
                mTextRight.setTextColor(color)
            }
        } catch (e: Exception) {
        }
    }


    fun setFontRightText(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextRight.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setImageToobarRight(image: Int, listener: View.OnClickListener?) {
        try {
            mImageRight.setImageResource(image)
            if (listener != null) {
                mImageRight.setOnClickListener(listener)
            }
            mImageRight.visibility = View.VISIBLE
            mTextRight.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setImageToolbarCenter(image: Int, listener: View.OnClickListener?) {
        try {
            mImageCenter.setImageResource(image)
            if (listener != null) {
                mImageCenter.setOnClickListener(listener)
            }
            mImageCenter.visibility = View.VISIBLE
            mTextViewScreenTitle.visibility = View.GONE
        } catch (e: Exception) {
        }
    }


    fun setImageToobarLeft(image: Int, listener: View.OnClickListener?) {
        try {
            mImageButtonBack.setImageResource(image)
            if (listener != null) {
                mImageButtonBack.setOnClickListener(listener)
            }
            darkToolbarBack.visibility = View.GONE
            mImageButtonBack.visibility = View.VISIBLE
            mTextLeft.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextToobar(text: String, color: Int?, style: Boolean?) {
        try {
            mTextViewScreenTitle.text = text
            mImageCenter.visibility = View.GONE
            mTextViewScreenTitle.visibility = View.VISIBLE
            if (color != null) {
                mTextViewScreenTitle.setTextColor(color)
            }
            if (style == true) {
                mTextViewScreenTitle.setTypeface(mTextViewScreenTitle.typeface, Typeface.BOLD)
            }
            mSubtitleText.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setFontTextToolbar(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextViewScreenTitle.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setBackgroundToobar(color: Int) {
        try {
            mContainerToobar.setBackgroundColor(color)
        } catch (e: Exception) {
        }
    }

    fun showToolbar(isShow: Boolean) {
        try {
            mToolbar.visibility = if (isShow) View.VISIBLE else View.GONE
            mNewToolbar.visibility =
                View.GONE // hide the new toolbar always, make it visible when necessary
            mNewToolbar2.visibility =
                View.GONE // hide the new toolbar always, make it visible when necessary
            darkToolbarLayout.visibility =
                View.GONE  // hide the new toolbar always, make it visible when necessary
        } catch (e: Exception) {
        }
    }

    fun addBottomNavigationItem(
        groupId: Int,
        id: Int,
        order: Int,
        title: String,
        icon: Int,
        isEnabled: Boolean,
        listener: MenuItem.OnMenuItemClickListener?
    ) {
        try {
            var menu: Menu = mBottomNavigationView.menu
            menu.add(groupId, id, order, title).setIcon(icon).setEnabled(isEnabled)
                .setOnMenuItemClickListener(listener)
        } catch (e: Exception) {
        }
    }

    fun getBottomNavigationInstance(): BottomNavigationView {
        return mBottomNavigationView
    }


    fun inflateMenuBottomNavigation(layout: Int) {
        try {
            mBottomNavigationView.inflateMenu(layout)
        } catch (e: Exception) {
        }
    }

    fun addBottomNavigationItem(
        id: Int,
        title: String,
        icon: Int,
        isEnabled: Boolean,
        listener: MenuItem.OnMenuItemClickListener?
    ) {
        try {
            var menu: Menu = mBottomNavigationView.menu
            menu.add(Menu.NONE, id, Menu.NONE, title).setIcon(icon).setEnabled(isEnabled)
                .setOnMenuItemClickListener(listener)
        } catch (e: Exception) {
        }

    }

    fun showBottomNavigation(isShow: Boolean) {
        try {
            mBottomNavigationView.visibility = if (isShow) View.VISIBLE else View.GONE
        } catch (e: Exception) {
        }

    }


    fun setBackgroundBottomNavigation(color: Int) {
        try {
            mBottomNavigationView.setBackgroundColor(color)
        } catch (e: Exception) {
        }
    }

    fun setIconColor(colorChecked: Int, colorEnable: Int, colorDisable: Int) {
        try {
            val states = arrayOf(
                intArrayOf(android.R.attr.state_checked), // checked
                intArrayOf(android.R.attr.state_enabled), // enable
                intArrayOf(-android.R.attr.state_enabled) // disable
            )

            val colors = intArrayOf(
                colorChecked,
                colorEnable,
                colorDisable
            )

            mBottomNavigationView.itemIconTintList = ColorStateList(states, colors)
        } catch (e: Exception) {
        }
    }

    fun setTextColorIcon(colorChecked: Int, colorEnable: Int, colorDisable: Int) {
        try {
            val states = arrayOf(
                intArrayOf(android.R.attr.state_checked), // checked
                intArrayOf(android.R.attr.state_enabled), // enable
                intArrayOf(-android.R.attr.state_enabled) // disable
            )

            val colors = intArrayOf(
                colorChecked,
                colorEnable,
                colorDisable
            )

            mBottomNavigationView.itemTextColor = ColorStateList(states, colors)
        } catch (e: Exception) {
        }
    }

    fun setIconSizeOnBottomNavigation(size: Int) {
        try {
            mBottomNavigationView.itemIconSize = size
        } catch (e: Exception) {
        }
    }

    fun setItemBackground() {
        try {
            mBottomNavigationView.itemBackgroundResource = R.drawable.item_selector_navigation
        } catch (e: Exception) {
        }
    }

    fun addPaddingOnRightImg() {
        try {
            image_right.setPadding(10, 10, 10, 10)
        } catch (e: Exception) {
        }
    }

    fun showTextAndIconOnBottomNavigation(isShow: Boolean) {
        try {
            mBottomNavigationView.labelVisibilityMode =
                if (isShow) LabelVisibilityMode.LABEL_VISIBILITY_LABELED else LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED
        } catch (e: Exception) {
        }
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }


    // NEW TOOLBAR

    fun showNewToolbar() {
        try {
            mNewToolbar.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setVisibilityLeftNew(visibility: Int) {
        try {
            mLayoutNewLeft.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setVisibilityCenterNew(visibility: Int) {
        try {
            mLayoutNewCenter.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setVisibilityRightNew(visibility: Int) {
        try {
            mLayoutNewRight.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setTextLeftNew(text: String) {
        try {
            mLayoutNewLeft.visibility = View.VISIBLE

            mTextLeftNew.text = text
            mImageButtonBackNew.visibility = View.GONE
            mTextLeftNew.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextLeftNew(text: String, color: Int?, style: Boolean?, size: Float?) {
        try {
            mLayoutNewLeft.visibility = View.VISIBLE

            mTextLeftNew.text = text
            mImageButtonBackNew.visibility = View.GONE
            mTextLeftNew.visibility = View.VISIBLE
            if (color != null) {
                mTextLeftNew.setTextColor(color)
            }
            if (style == true) {
                mTextLeftNew.setTypeface(null, Typeface.BOLD)
            }
            if (size != null) {
                mTextLeftNew.textSize = size
            }
        } catch (e: Exception) {
        }
    }

    fun setTextRightNew(text: String, color: Int?, size: Float?, style: Boolean) {
        try {
            mLayoutNewRight.visibility = View.VISIBLE

            mTextRightNew.text = text
            mImageRightNew.visibility = View.GONE
            mTextRightNew.visibility = View.VISIBLE
            if (color != null) {
                mTextRightNew.setTextColor(color)
            }
            if (size != null) {
                mTextRightNew.textSize = size
            }
            if (style == true) {
                mTextRightNew.setTypeface(null, Typeface.BOLD)
            }
        } catch (e: Exception) {
        }
    }


    fun setTextRightNew(text: String) {
        try {
            mLayoutNewRight.visibility = View.VISIBLE

            mTextRightNew.text = text
            mImageRightNew.visibility = View.GONE
            mTextRightNew.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextRightNewListener(text: String, listener: View.OnClickListener?, color: Int?) {
        try {
            mLayoutNewRight.visibility = View.VISIBLE

            mTextRightNew.text = text
            mImageRightNew.visibility = View.GONE
            mTextRightNew.visibility = View.VISIBLE
            if (listener != null) {
                mTextRightNew.setOnClickListener(listener)
            }
            if (color != null) {
                mTextRightNew.setTextColor(color)
            }
        } catch (e: Exception) {
        }
    }


    fun setFontRightTextNew(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextRightNew.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setImageToobarRightNew(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewRight.visibility = View.VISIBLE
            mImageRightNew.setImageResource(image)
            if (listener != null) {
                mImageRightNew.setOnClickListener(listener)
            }
            mImageRightNew.visibility = View.VISIBLE
            mTextRightNew.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setImageToolbarCenterNew(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewCenter.visibility = View.VISIBLE
            mImageCenterNew.setImageResource(image)
            if (listener != null) {
                mImageCenterNew.setOnClickListener(listener)
            }
            mImageCenterNew.visibility = View.VISIBLE
            mTextViewScreenTitleNew.visibility = View.GONE
        } catch (e: Exception) {
        }
    }


    fun setImageToobarLeftNew(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewLeft.visibility = View.VISIBLE
            mImageButtonBackNew.setImageResource(image)
            if (listener != null) {
                mImageButtonBackNew.setOnClickListener(listener)
            }
            mImageButtonBackNew.visibility = View.VISIBLE
            mTextLeftNew.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextToobarNew(text: String, color: Int?, style: Boolean?) {
        try {
            mLayoutNewCenter.visibility = View.VISIBLE
            mTextViewScreenTitleNew.text = text
            mImageCenterNew.visibility = View.GONE
            mTextViewScreenTitleNew.visibility = View.VISIBLE
            if (color != null) {
                mTextViewScreenTitleNew.setTextColor(color)
            }
            if (style == true) {
                mTextViewScreenTitleNew.setTypeface(mTextViewScreenTitleNew.typeface, Typeface.BOLD)
            }
            mSubtitleTextNew.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setFontTextToolbarNew(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextViewScreenTitleNew.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setBackgroundToobarNew(color: Int) {
        try {
            mNewToolbar.setBackgroundColor(color)
        } catch (e: Exception) {
        }
    }


    // NEW TOOLBAR 2

    fun showNewToolbar2() {
        try {
            mNewToolbar2.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setVisibilityLeftNew2(visibility: Int) {
        try {
            mLayoutNewLeft2.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setVisibilityCenterNew2(visibility: Int) {
        try {
            mLayoutNewCenter2.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setVisibilityRightNew2(visibility: Int) {
        try {
            mLayoutNewRight2.visibility = visibility
        } catch (e: Exception) {
        }
    }

    fun setTextLeftNew2(text: String) {
        try {
            mLayoutNewLeft2.visibility = View.VISIBLE

            mTextLeftNew2.text = text
            mImageButtonBackNew2.visibility = View.GONE
            mTextLeftNew2.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextLeftNew2(text: String, color: Int?, style: Boolean?, size: Float?) {
        try {
            mLayoutNewLeft2.visibility = View.VISIBLE

            mTextLeftNew2.text = text
            mImageButtonBackNew2.visibility = View.GONE
            mTextLeftNew2.visibility = View.VISIBLE
            if (color != null) {
                mTextLeftNew2.setTextColor(color)
            }
            if (style == true) {
                mTextLeftNew2.setTypeface(null, Typeface.BOLD)
            }
            if (size != null) {
                mTextLeftNew2.textSize = size
            }
        } catch (e: Exception) {
        }
    }

    fun setTextRightNew2(text: String, color: Int?, size: Float?, style: Boolean) {
        try {
            mLayoutNewRight2.visibility = View.VISIBLE

            mTextRightNew2.text = text
            mImageRightNew2.visibility = View.GONE
            mTextRightNew2.visibility = View.VISIBLE
            if (color != null) {
                mTextRightNew2.setTextColor(color)
            }
            if (size != null) {
                mTextRightNew2.textSize = size
            }
            if (style) {
                mTextRightNew2.setTypeface(null, Typeface.BOLD)
            }
        } catch (e: Exception) {
        }
    }


    fun setTextRightNew2(text: String) {
        try {
            mLayoutNewRight2.visibility = View.VISIBLE

            mTextRightNew2.text = text
            mImageRightNew2.visibility = View.GONE
            mTextRightNew2.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun setTextRightNewListener2(text: String, listener: View.OnClickListener?, color: Int?) {
        try {
            mLayoutNewRight2.visibility = View.VISIBLE

            mTextRightNew2.text = text
            mImageRightNew2.visibility = View.GONE
            mTextRightNew2.visibility = View.VISIBLE
            if (listener != null) {
                mTextRightNew2.setOnClickListener(listener)
            }
            if (color != null) {
                mTextRightNew2.setTextColor(color)
            }
        } catch (e: Exception) {
        }
    }


    fun setFontRightTextNew2(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextRightNew2.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setImageToobarRightNew2(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewRight2.visibility = View.VISIBLE
            mImageRightNew2.setImageResource(image)
            if (listener != null) {
                mImageRightNew2.setOnClickListener(listener)
            }
            mImageRightNew2.visibility = View.VISIBLE
            mTextRightNew2.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setImageToolbarCenterNew2(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewCenter2.visibility = View.VISIBLE
            mImageCenterNew2.setImageResource(image)
            if (listener != null) {
                mImageCenterNew2.setOnClickListener(listener)
            }
            mImageCenterNew2.visibility = View.VISIBLE
            mTextViewScreenTitleNew2.visibility = View.GONE
        } catch (e: Exception) {
        }
    }


    fun setImageToobarLeftNew2(image: Int, listener: View.OnClickListener?) {
        try {
            mLayoutNewLeft2.visibility = View.VISIBLE
            mImageButtonBackNew2.setImageResource(image)
            if (listener != null) {
                mImageButtonBackNew2.setOnClickListener(listener)
            }
            mImageButtonBackNew2.visibility = View.VISIBLE
            mTextLeftNew2.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setTextToobarNew2(text: String, color: Int?, style: Boolean?) {
        try {
            mLayoutNewCenter2.visibility = View.VISIBLE
            mTextViewScreenTitleNew2.text = text
            mImageCenterNew2.visibility = View.GONE
            mTextViewScreenTitleNew2.visibility = View.VISIBLE
            if (color != null) {
                mTextViewScreenTitleNew2.setTextColor(color)
            }
            if (style == true) {
                mTextViewScreenTitleNew2.setTypeface(
                    mTextViewScreenTitleNew.typeface,
                    Typeface.BOLD
                )
            }
            mSubtitleTextNew2.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setFontTextToolbarNew2(context: Context, font: String) {
        try {
            val type =
                Typeface.createFromAsset(context.assets, font)
            mTextViewScreenTitleNew2.typeface = type
        } catch (e: Exception) {
        }
    }

    fun setBackgroundToobarNew2(color: Int) {
        try {
            mNewToolbar2.setBackgroundColor(color)
        } catch (e: Exception) {
        }
    }

    //Toolbar in the login

    open fun dpToPx(dp: Int): Int {
        val density: Float = applicationContext.resources
            .displayMetrics.density
        return (dp.toFloat() * density).roundToInt()
    }

    fun setLightToolbar(title: String, rDrawable: Int, backDrawable: Int, face: Typeface) {
        //setting Height
        mToolbar.layoutParams = toolbarParams
        toolbarParams.height = dpToPx(58)
        //shows nad hides other TB
        showToolbar(true)
        toolbarProgressBar.visibility = View.GONE
        mLayoutNewCenter2.visibility = View.GONE
        //Set Title
        setScreenTitle(title)
        mTextViewScreenTitle.textSize = 17F
        mTextViewScreenTitle.typeface = face
        mTextViewScreenTitle.setTextColor(resources.getColor(R.color.dark_blue_text))
        //Set Back Image
        setImageToobarLeft(
            backDrawable,
            View.OnClickListener { supportFragmentManager.popBackStack() })
        //Set Right Image
        setImageToobarRight(rDrawable, null)
        setTextRight("")
        setImageToobarRight(rDrawable, View.OnClickListener {
            //
        })
        hideDarkToolbar()
        mToolbar.setBackgroundColor(resources.getColor(R.color.white))
        setBackgroundToobar(resources.getColor(R.color.white))
        divider.visibility = View.VISIBLE  // Divider
        //Bg color
        //    mContainerToobar.setBackgroundColor(resources.getColor(R.color.white))
        //mToolbar.setBackgroundColor(resources.getColor(R.color.white))

    }

    fun setDarkToolbar(title: String, rDrawable: Int, backDrawable: Int, face: Typeface) {
        //setting Height
        mToolbar.layoutParams = toolbarParams
        toolbarParams.height = dpToPx(58)
        //shows nad hides other TB
        showToolbar(true)
        toolbarProgressBar.visibility = View.GONE
        mLayoutNewCenter2.visibility = View.GONE
        //Set Title
        setScreenTitle(title)
        mTextViewScreenTitle.textSize = 17F
        mTextViewScreenTitle.typeface = face
        mTextViewScreenTitle.setTextColor(resources.getColor(R.color.white))
        //Set Back Image
        setImageToobarLeft(backDrawable,
            View.OnClickListener {
                if (backDrawable == R.drawable.arrow_back_white) {
                   onBackPressed()
                }
                if(backDrawable==R.drawable.icons_24_ic_profile_on_dark){
                  addFragmentBottom(ProfileFragment(), true, R.id.container)
                }
            })
                    //Set Right Image

        setImageToobarRight (rDrawable, null)
        setTextRight("")
        setImageToobarRight(rDrawable, View.OnClickListener {
          if(rDrawable==R.drawable.organization_status_help_24){
              addFragmentBottom(HelpFragment(), true, R.id.container)
          }
        })
        hideDarkToolbar()

        mToolbar.setBackgroundColor(resources.getColor(R.color.dark_blue_text))
        setBackgroundToobar(resources.getColor(R.color.dark_blue_text))
        divider.visibility = View.VISIBLE  // Divider
        //Bg color
        //    mContainerToobar.setBackgroundColor(resources.getColor(R.color.white))
        //mToolbar.setBackgroundColor(resources.getColor(R.color.white))

    }

    fun setBarsColor(color: Int) {
        window.navigationBarColor = resources.getColor(R.color.dark_blue_text)
        window.statusBarColor = resources.getColor(R.color.dark_blue_text)
    }

    fun setProgressBar(drawable: Int, progress: Int) {
        mImageRight.background = resources.getDrawable(drawable)
        mImageRight.setImageResource(drawable)
        mTextViewScreenTitle.visibility = View.GONE
        toolbarProgressBar.visibility = View.VISIBLE
        toolbarProgressBar.progress = progress
        mToolbar.layoutParams = toolbarParams
        toolbarParams.height = dpToPx(58)
        toolbarProgressBar.visibility = View.VISIBLE
        mLayoutNewCenter2.visibility = View.GONE
        mToolbar.setBackgroundColor(resources.getColor(R.color.white))
        setBackgroundToobar(resources.getColor(R.color.white))
        hideDarkToolbar()
    }

    fun hideProgressBar() {
        try {
            if (toolbarProgressBar.visibility == View.VISIBLE) {
                toolbarProgressBar.visibility = View.GONE
            }
        } catch (e: Exception) {
        }
    }

    fun setRoundedProgressBar(progressDrawable: Drawable?) {
        if (progressDrawable != null) {
            toolbarProgressBar.progressDrawable = progressDrawable
        }
    }

    //Toolbar End------------

    //Toolbar in the Profile
    fun showDarkToolbar() {
        try {
            mToolbar.visibility = View.VISIBLE
            mNewToolbar.visibility = View.GONE
            mNewToolbar2.visibility = View.GONE
            darkToolbarLayout.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun hideDarkToolbar() {
        try {
            mToolbar.visibility = View.VISIBLE
            mNewToolbar.visibility = View.GONE
            mNewToolbar2.visibility = View.GONE
            darkToolbarLayout.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    fun setDarkToolbar(
        title: String,
        isBackEnabled: Boolean,
        backDrawable: Int,
        logoDrawable: Int,
        face: Typeface
    ) {
        //Set TB Height
        toolbarParams.height = dpToPx(100)
        mToolbar.layoutParams = toolbarParams
        // shows dark TB
        mImageRight.visibility = View.GONE
        divider.visibility = View.VISIBLE
        showDarkToolbar()
        mTextLeft.visibility = View.GONE
        toolbarProgressBar.visibility = View.GONE
// set Title
        setLeftText(title)
        setScreenTitle("")
        setTextToobar("", null, false)
        darkToolbarTitle.typeface = face
        darkToolbarTitle.setTextColor(resources.getColor(R.color.white))

//back btn
        isbackEnabled(isBackEnabled, backDrawable)
//set logo
        darkToolbarLogo.setImageResource(logoDrawable)
        //set BG
        //   mContainerToobar.setBackgroundColor(resources.getColor(R.color.dark_blue_text))
        mToolbar.setBackgroundColor(resources.getColor(R.color.dark_blue_text))
        setBackgroundToobar(resources.getColor(R.color.dark_blue_text))
        //  window.statusBarColor = resources.getColor(R.color.dark_blue_text)
        // window.navigationBarColor = resources.getColor(R.color.white)


    }

    fun setLeftText(leftText: String) {
        darkToolbarTitle.visibility = View.VISIBLE
        darkToolbarTitle.setTypeface(mTextLeft.typeface, Typeface.BOLD)
        darkToolbarTitle.text = leftText
        darkToolbarTitle.setTextColor(resources.getColor(R.color.white))

    }

    fun isbackEnabled(isEnabled: Boolean, backDrawable: Int) {
        if (isEnabled) {
            darkToolbarBack.visibility = View.VISIBLE
            darkToolbarBack.setImageResource(backDrawable)
            darkToolbarBack.setOnClickListener(View.OnClickListener { supportFragmentManager.popBackStack() })
            mImageButtonBack.visibility = View.GONE
        } else {
            darkToolbarBack.visibility = View.GONE
            mImageButtonBack.visibility = View.GONE
        }
    }
}