package com.octotelematics.installatori.uicore.base


import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.utils.UIUtils.Companion.hideKeyboard
import java.lang.Exception
import java.text.DecimalFormat

/**
 * A simple [Fragment] subclass.
 */
open class BaseFragment : Fragment() {


    private var listenerCustom: onLocationChangedListener? = null
    private var listenerCustomMotion: onMotionChangedListener? = null
    private var listenerBle: onBLEChangedListener? = null
    private var applicationId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCurrentFragment(this)

    }



    fun setCurrentFragment(fragment: Fragment) {
        try {
            var baseActivity = activity as BaseActivity
            baseActivity.currentFragment = fragment
        } catch (e: Exception) { }
    }


    fun addFragment(
        fragment: Fragment,
        replace: Boolean,
        addToBackStack: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragment.javaClass.name)
            }
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun addFragmentModal(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_bottom, R.anim.slide_out_to_top,
                R.anim.slide_in_from_top, R.anim.slide_out_to_bottom
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun addFragmentLeft(
        fragment: Fragment,
        replace: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_left, R.anim.slide_out_to_right,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun addFragmentRight(
        fragment: Fragment,
        replace: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
            if (replace) {
                fragmentTransaction.replace(rootViewId, fragment)
            } else {
                fragmentTransaction.add(rootViewId, fragment)
                fragmentTransaction.show(fragment)
            }
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun changeFragment(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }
    fun  formatedKmsValue(mileage:String):String{
        var mile= DecimalFormat("##.#").format(mileage.toDouble() / 1000)
        var miles = mile.replace(",", ".")
        return  miles
    }
    fun changeFragmentLeft(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_left, R.anim.slide_out_to_right,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun changeFragmentRight(
        fragment: Fragment,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.replace(rootViewId, fragment)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun replaceFragment(
        fragment: Fragment,
        addToBackStack: Boolean,
        rootViewId: Int
    ) {
        try {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right
            )

            fragmentManager!!.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragment.javaClass.name)
            }

            fragmentTransaction.replace(rootViewId, fragment)

            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {}
    }

    fun showProgress() {
        try {
            if (activity != null && activity is BaseActivity) {
                var baseActivity = activity as BaseActivity
                baseActivity.showProgress()
            }
        } catch (e: Exception) {}
    }

    fun hideProgress() {
        try {
            if (activity != null && activity is BaseActivity) {
                var baseActivity = activity as BaseActivity
                baseActivity.hideProgress()
            }
        } catch (e: Exception) {}
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    fun getApplicationId(appID: String){
        applicationId = appID
    }

    interface onLocationChangedListener {
        fun setOnChangedLocation(bool: Boolean?)
    }

    interface onMotionChangedListener {
        fun setOnChangedValueMotion(bool: Boolean?)
    }

    interface onBLEChangedListener {
        fun setOnChangedValueBle(bool: Boolean?)
    }


    fun setCustomObjectListener(listener: onLocationChangedListener) {
        this.listenerCustom = listener;
    }

    fun  setCustomObjectListenerMotion(listener: onMotionChangedListener) {
        this.listenerCustomMotion = listener;
    }

    fun setResponseRequestBleListener(listener: onBLEChangedListener) {
        this.listenerBle = listener;
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e("CIACCIO", "CHIAMO ACTIVITYMAIN")
        when (requestCode) {
            1 -> {
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    /*Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();*/
                    if (listenerCustom != null) {
                        listenerCustom!!.setOnChangedLocation(true)
                    }
                } else {
                    //Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                    if (permissions.size > 0 && permissions != null) {
                        val permission = permissions[0]
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            val showRationale =
                                shouldShowRequestPermissionRationale(permission)
                            if (!showRationale) {
                                val intent =
                                    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri =
                                    Uri.fromParts("package", applicationId, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                        } else {
                            if (listenerCustom != null) {
                                listenerCustom!!.setOnChangedLocation(false)
                            }
                        }
                    }
                }
                return
            }
            99 -> {
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    /*Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();*/
                    if (listenerCustomMotion != null) {
                        listenerCustomMotion!!.setOnChangedValueMotion(true)
                    }
                } else {
                    if (permissions.size > 0 && permissions != null) {
                        val permission = permissions[0]
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            val showRationale =
                                shouldShowRequestPermissionRationale(permission)
                            if (!showRationale) {
                                val intent =
                                    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri =
                                    Uri.fromParts("package", applicationId, null)
                                intent.data = uri
                                startActivity(intent)
                            }
                        } else {
                            if (listenerCustom != null) {
                                listenerCustom!!.setOnChangedLocation(false)
                            }
                        }
                    }
                }
                return
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            51 -> {
                if (resultCode == 0) {
                    // bluetoothStatus = "Bluetooth Required";
                    if (listenerBle != null) {
                        listenerBle!!.setOnChangedValueBle(false)
                    }
                } else {
                    if (listenerBle != null) {
                        listenerBle!!.setOnChangedValueBle(true)
                    }
                }
            }
        }
    }
}
