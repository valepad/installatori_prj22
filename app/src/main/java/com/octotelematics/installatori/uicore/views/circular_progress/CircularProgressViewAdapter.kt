package com.octotelematics.installatori.uicore.views


/**
 * This class implements CircularProgressViewListener. Use this if you don't want to implement all methods for the listener.
 */
class CircularProgressViewAdapter : CircularProgressViewListener {

    override fun onProgressUpdate(currentProgress: Float) {

    }

    override fun onProgressUpdateEnd(currentProgress: Float) {

    }

    override fun onAnimationReset() {

    }

    override fun onModeChanged(isIndeterminate: Boolean) {

    }
}