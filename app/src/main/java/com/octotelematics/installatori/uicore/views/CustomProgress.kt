package com.octotelematics.installatori.uicore.views


import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.TextView
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.views.circular_progress.CircularProgressView


class CustomProgress {

    companion object {
        var customProgress: CustomProgress? = null

        fun getInstance(context: Context): CustomProgress {
            if (customProgress == null) {
                customProgress = CustomProgress()
                customProgress!!.init(context)
            }
            return customProgress as CustomProgress
        }
    }

    var mDialog: Dialog? = null
    var mProgressBar : CircularProgressView? = null
    var progressText: TextView? = null
    var context: Context? = null

    fun init(context: Context) {
        try {
            this.context = context
            mDialog = Dialog(context)
            // no tile for the dialog
            mDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            mDialog!!.setContentView(R.layout.dialog_progress_bar)
            mProgressBar = mDialog!!.findViewById(R.id.progress_bar)
            progressText = mDialog!!.findViewById(R.id.progress_text)
        } catch (e: Exception) {}
    }

    fun setTextMessage(message: String?) {
        try {
            if (message != null) progressText?.text = message
        } catch (e: Exception) {}
    }

    fun setCancelable(cancelable: Boolean) {
        try {
            mDialog!!.setCancelable(cancelable)
            mDialog!!.setCanceledOnTouchOutside(cancelable)
        } catch (e: Exception) {}
    }

    fun isShowing(): Boolean {
        try {
            return mDialog != null && mDialog!!.isShowing
        } catch (e: Exception) {}
        return false
    }

    fun getProgressDialog() : CircularProgressView? {
        return mProgressBar
    }

    fun showProgress() {
        showProgress(null, null)
    }

    fun showProgress(message: String) {
        showProgress(message, false)
    }

    fun showProgress(message: String?, cancelable: Boolean?) {

        try {
            if (message != null) {
                progressText!!.text = message
                progressText!!.visibility = View.VISIBLE
            }

            if (cancelable != null) {
                mDialog!!.setCancelable(cancelable)
                mDialog!!.setCanceledOnTouchOutside(cancelable)
            }

            mDialog!!.show()
        } catch (e: java.lang.Exception) {}
    }

    fun hideProgress() {
        try {
            if (mDialog != null) {
                mDialog!!.dismiss()
            }
        } catch (e: java.lang.Exception) {}
    }

    fun setColors(colorId: Int) {
        try {
            mProgressBar?.setColor(colorId)
            progressText?.setTextColor(context!!.resources.getColor(colorId))
        } catch (e: java.lang.Exception) {}
    }

    fun setProgressColor(colorId: Int) {
        try {
            mProgressBar?.setColor(colorId)
        } catch (e: java.lang.Exception) {}
    }

    fun setProgressTextColor(colorId: Int) {
        try {
            progressText?.setTextColor(context!!.resources.getColor(colorId))
        } catch (e: java.lang.Exception) {}
    }
}