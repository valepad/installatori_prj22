
package com.octotelematics.installatori.uicore.utils

interface DismissInterface {
    fun onDismissInterface()
}