package com.octotelematics.installatori.uicore.views


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.octotelematics.installatori.R
import kotlinx.android.synthetic.main.custom_bottom_sheet_close.*


open class CustomBottomSheetClose(layout: Int, isExpanded: Boolean) : BottomSheetDialogFragment() {

    private var layout: Int = layout
    private var isExpanded: Boolean = isExpanded
    private var container: View? = null
    private var mBehavior : BottomSheetBehavior<View>? = null
    private var img : ImageView? = null
    var frameLayout: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.container = View.inflate(getContext(), R.layout.custom_bottom_sheet_close, null)
        img = container!!.findViewById(R.id.image_close)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        this.container = View.inflate(getContext(), R.layout.custom_bottom_sheet_close, null)
        img = container!!.findViewById(R.id.image_close)
        if (isExpanded){
            val frameLayout : FrameLayout = container!!.findViewById(R.id.layout_container)
            val params : RelativeLayout.LayoutParams = frameLayout.layoutParams as RelativeLayout.LayoutParams
            params.height = getScreenHeight();
            frameLayout.layoutParams = params;
            dialog.setContentView(container!!)
            this.mBehavior = BottomSheetBehavior.from(container!!.parent as (View) )
            this.mBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(
                    bottomSheet: View,
                    newState: Int
                ) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
                    }
                }

                override fun onSlide(
                    bottomSheet: View,
                    slideOffset: Float
                ) {
                }
            })
        }else {
            dialog.setContentView(container!!)
            /*this.container!!.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            this.mBehavior = BottomSheetBehavior.from(container!!.parent as (View) )
            this.mBehavior!!.setPeekHeight(this.container!!.layoutParams.height)*/
        }
        return dialog
    }

    override fun onStart() {
        super.onStart()
        if (isExpanded){
            mBehavior!!.setState(BottomSheetBehavior.STATE_EXPANDED)
        }
    }

    open fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        frameLayout = container!!.findViewById(R.id.layout_container)
        val frameContainer = View.inflate(context, layout, null)
        frameLayout!!.addView(frameContainer)
        img?.setOnClickListener{
            dismiss()
        }

        if (!isExpanded) {
            frameLayout?.post {
                try {
                    this.mBehavior = BottomSheetBehavior.from(container!!.parent as (View) )
                    this.mBehavior!!.peekHeight = frameLayout!!.measuredHeight
                } catch (e: Exception) {}
            }
        }
    }
    fun setBackgroundColorImageClose(color: Int){
        image_close.setBackgroundColor(color)
    }

    fun setBackgroundCustomBottom(color: Int){
        container_custom_bottom.setBackgroundColor(color)
    }

    fun getLayout() : FrameLayout?{
        return frameLayout
    }

    fun setIconClose(icon: Int){
        img!!.setImageResource(icon)
    }

    fun setVisibilityCloseIcon(boolean: Boolean){
        if (!boolean){
            img!!.visibility = View.INVISIBLE
        }else {
            img!!.visibility = View.VISIBLE
        }
    }

    fun setTintImageClose(color: Int){
        image_close.setColorFilter(color)
    }

    fun setSizeIconClose(width: Int, height: Int){
        if (image_close.layoutParams != null){
            image_close.layoutParams.height = height
            image_close.layoutParams.width = width
        }
    }

    fun setListenerClose(listener: View.OnClickListener){
        image_close.setOnClickListener(listener)
    }
}