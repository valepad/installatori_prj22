package com.octotelematics.installatori.uicore.views


import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.octotelematics.installatori.R
import com.octotelematics.installatori.uicore.utils.DismissInterface


class CustomTemporaryDialog {

    companion object {
        var customProgress: CustomTemporaryDialog? = null

        fun getInstance(context: Context, layoutID: Int, duration: Long?): CustomTemporaryDialog {
            if (customProgress == null) {
                customProgress = CustomTemporaryDialog()
                customProgress!!.init(context, layoutID, duration)
            }
            return customProgress as CustomTemporaryDialog
        }
    }

    private val DEFAULT_DURATION = 3000L // 3 seconds

    var mDialog: Dialog? = null
    var textMessage: TextView? = null
    var context: Context? = null
    var duration: Long? = null

    var handler: Handler = Handler()
    var runnable: Runnable = Runnable {
        if (mDialog != null && mDialog!!.isShowing) {
            mDialog!!.dismiss()
        }
    }

    fun init(context: Context, layoutID: Int, duration: Long?) {
        this.context = context
        this.duration = duration
        mDialog = Dialog(context)
        // no tile for the dialog
        mDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog!!.setContentView(layoutID)
        mDialog!!.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        mDialog!!.window?.setBackgroundDrawableResource(android.R.color.transparent)

        textMessage = mDialog!!.findViewById(R.id.message)

        mDialog!!.setOnDismissListener {
            handler.removeCallbacks(runnable)
        }
    }

    fun setDismissListener(dismissInterface: DismissInterface) {
        try {
            mDialog!!.setOnDismissListener {
                handler.removeCallbacks(runnable)
                dismissInterface.onDismissInterface()
            }

        } catch (e: Exception) {}
    }

    fun setTextMessage(message: String?) {
        if (message != null) textMessage?.text = message
    }

    fun setCancelable(cancelable: Boolean) {
        mDialog!!.setCancelable(cancelable)
        mDialog!!.setCanceledOnTouchOutside(cancelable)
    }

    fun isShowing(): Boolean {
        return mDialog != null && mDialog!!.isShowing
    }

    fun show() {
        show(null, null)
    }

    fun show(message: String) {
        show(message, false)
    }

    fun show(message: String?, cancelable: Boolean?) {

        if (message != null) {
            textMessage!!.text = message
            textMessage!!.visibility = View.VISIBLE
        }

        if (cancelable != null) {
            mDialog!!.setCancelable(cancelable)
            mDialog!!.setCanceledOnTouchOutside(cancelable)
        }

        mDialog!!.show()

        if (duration == null) {
            duration = DEFAULT_DURATION
        }
        handler.postDelayed(runnable, duration!!)
    }

    fun hide() {
        if (mDialog != null) {
            mDialog!!.dismiss()
        }
    }

    fun setProgressTextColor(colorId: Int) {
        textMessage?.setTextColor(context!!.resources.getColor(colorId))
    }
}