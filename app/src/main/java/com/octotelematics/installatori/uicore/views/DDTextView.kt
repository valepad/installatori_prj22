package com.octotelematics.installatori.uicore.views


import android.graphics.Typeface
import android.content.Context
import android.util.AttributeSet
import com.octotelematics.installatori.R

class DDTextView : androidx.appcompat.widget.AppCompatTextView {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int): super(context, attrs, defStyle) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.DDTextView)
            val fontName = a.getString(R.styleable.DDTextView_customFont)

            try {
                if (fontName != null) {
                    val myTypeface = Typeface.createFromAsset(context.assets, fontName)
                    typeface = myTypeface
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            a.recycle()
        }
    }

    public fun setCustomFont(fontName: String?) {
        try {
            if (fontName != null) {
                val myTypeface = Typeface.createFromAsset(context.assets, fontName)
                typeface = myTypeface
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}