package com.octotelematics.installatori.models.profileModels

import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("province")
    val code: String,
    @SerializedName("province")
    val flagImageUrl: String,
    @SerializedName("province")
    val name: String
)