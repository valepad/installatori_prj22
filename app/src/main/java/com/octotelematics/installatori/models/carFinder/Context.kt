package com.octotelematics.installatori.models.carFinder

data class Context(
    val PNL: String,
    val PWS: String
)