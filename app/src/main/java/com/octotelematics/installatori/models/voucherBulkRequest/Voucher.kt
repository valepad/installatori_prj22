package com.octotelematics.installatori.models.voucherBulkRequest


import com.octotelematics.installatori.models.voucherBulkResponseCreate.User
import com.octotelematics.installatori.models.vouchers.Motor

data class Voucher(
    val motor: Motor?,
    val policy: Policy?,
    val user: User?,
    val type: String?,
    val voucherStatus: String?

)