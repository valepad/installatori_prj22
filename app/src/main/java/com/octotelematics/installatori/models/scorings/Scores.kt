package com.octotelematics.ddcore.models.scorings

data class Scores(
    val CS: Double,
    val S2: Double
)