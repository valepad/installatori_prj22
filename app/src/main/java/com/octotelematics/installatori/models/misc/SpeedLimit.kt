package com.octotelematics.installatori.models.misc

data class SpeedLimit(
    val active: Boolean,
    val inProgress: Boolean,
    val threshold: Int
)