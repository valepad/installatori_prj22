package com.octotelematics.installatori.models.voucherBulkResponseCreate

import com.octotelematics.installatori.models.vouchers.Motor

data class voucherBulkResponseItem(
    val agencyId: Any,
    val bulkId: String,
    val closureReason: Any,
    val contractType: Any,
    val customerId: Any,
    val device: Any,
    val endDate: Any,
    val id: Int,
    val idVoucher: Any,
    val motor: Motor,
    val packageId: Any,
    val pet: Any,
    val resultCode: Any,
    val sequence: Int,
    val startDate: Any,
    val type: String,
    val user: User,
    val userId: String,
    val voucherId: String,
    val voucherRequestsGroupId: Any,
    val voucherStatus: String
)