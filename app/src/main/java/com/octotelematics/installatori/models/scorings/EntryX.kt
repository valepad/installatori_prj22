package com.octotelematics.installatori.models.scorings

import com.octotelematics.ddcore.models.scorings.ScoreDetails
import com.octotelematics.ddcore.models.scorings.Scores

data class EntryX(
    val scoreDetails: ScoreDetails,
    val scores: Scores
)