package com.octotelematics.installatori.models.userRegistration

data class Tsc(
    val id: Int,
    val language: Language,
    val name: String
)