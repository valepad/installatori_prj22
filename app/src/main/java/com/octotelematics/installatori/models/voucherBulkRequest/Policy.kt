package com.octotelematics.installatori.models.voucherBulkRequest

data class Policy(
    val policyNumber: String
)