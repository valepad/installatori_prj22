package com.octotelematics.installatori.models.carFinder

data class carFinderPosition(
    val context: Context,
    val deviceId: String,
    val heading: Double,
    val latitude: Double,
    val longitude: Double,
    val quality: Int,
    val satellites: Int,
    val speed: Double,
    val timestamp: String,
    val voucherid: String
)