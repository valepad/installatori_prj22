package com.octotelematics.installatori.models.calendar

import com.octotelematics.installatori.models.profileModels.Device
import com.octotelematics.installatori.models.voucherBulkResponseCreate.User


data class PrenotationItem(

    val hours: String,
    val status: String,
    val data: String
)