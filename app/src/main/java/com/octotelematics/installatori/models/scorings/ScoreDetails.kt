package com.octotelematics.ddcore.models.scorings

import com.octotelematics.installatori.models.scorings.CS

data class ScoreDetails(
    val CS: CS,
    val S2: S2
)