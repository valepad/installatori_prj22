package com.octotelematics.installatori.models.voucherBulkResponseCreate

import com.octotelematics.installatori.models.vouchers.UserOptions

data class User(
    var birthDate: String?,
    var clientAddressCity: Any?,
    var clientAddressProvince: Any?,
    var clientAddressStreet: Any?,
    var clientAddressZip: Any?,
    var clientCountry: String?,
    var clientEmail: String?,
    var clientFirstName: String?,
    var clientLastName: String?,
    var clientPhone: String?,
    var companyFlag: String?,
    var companyName: String?,
    var fax: Any?,
    var id: Int?,
    var isEmailVerified: Any?,
    var language: Any?,
    var phone2: Any?,
    var phone3: Any?,
    var registrationStep: Any?,
    var ssnnin: Any?,
    var taxId: Any?,
    var tsc: String?,
    var userId: String?,
    var userStatus: String?,
    var userOptions: UserOptions?

)