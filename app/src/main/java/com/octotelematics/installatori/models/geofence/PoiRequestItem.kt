package com.octotelematics.installatori.models.geofence

data class PoiRequestItem(
    val active: Boolean,
    val id: Int,
    val latitude: Double,
    val longitude: Double,
    val notificationType: String,
    val radius: Int,
    val radiusHysteresis: Int
)