package com.octotelematics.installatori.models.scorings

import com.octotelematics.installatori.models.scorings.EntryX

data class Entry(
    val entry: EntryX
)