package com.octotelematics.ddcore.models.scorings

import com.octotelematics.installatori.models.scorings.Entry

data class Scorings(
    val entries: List<Entry>
)