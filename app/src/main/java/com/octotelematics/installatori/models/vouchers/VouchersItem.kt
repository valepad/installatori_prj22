package com.octotelematics.installatori.models.vouchers

import com.octotelematics.installatori.models.profileModels.Device
import com.octotelematics.installatori.models.voucherBulkResponseCreate.User


data class VouchersItem(
    val bulkId: String,
    val device: Device?,
    val endDate: String,
    val id: Int,
    val idVoucher: String,
    var motor: Motor?,
    val packageId: String,
    val policy: Policy,
    val sequence: Int,
    val startDate: String,
    val type: String,
    val user: User?,
    val userId: String,
    val voucherId: String,
    val voucherRequestsGroupId: String,
    val voucherStatus: String
)