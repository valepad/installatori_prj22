package com.octotelematics.installatori.models.vouchers

data class Policy(
    val closureDate: String,
    val creationTimestamp: Int,
    val id: Int,
    val policyNumber: String
)