package com.octotelematics.installatori.models.voucherBulkRequest

data class voucherBulkRequest(
    val voucherList: List<Voucher>
)