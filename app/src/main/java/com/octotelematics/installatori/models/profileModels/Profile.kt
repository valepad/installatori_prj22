package com.octotelematics.installatori.models.profileModels

import com.google.gson.annotations.SerializedName

class Profile {
    @SerializedName("id")
    val id: String? = null

    @SerializedName("value")
    val value: Int? = null

    @SerializedName("level")
    val level: String? = null

    @SerializedName("nextLevel")
    val nextLevel: String? = null

    @SerializedName("nextLevelValue")
    val nextLevelValue: Int? = null

    @SerializedName("journeys")
    val journeys: String? = null

    @SerializedName("duration")
    val duration: String? = null

    @SerializedName("kms")
    val kms: String? = null
}