package com.octotelematics.installatori.models.userRegistration

data class UserOptions(
    val id: Int,
    val language: Language
)