package com.octotelematics.ddcore.models.scorings

data class Component(
    val componentId: String,
    val currentValue: Double,
    val duration: Int,
    val mileage: Int,
    val penalty: Double

)