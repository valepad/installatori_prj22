package com.octotelematics.installatori.models.vouchers

data class Tsc(
    val id: Int,
    val language: Language,
    val name: String
)