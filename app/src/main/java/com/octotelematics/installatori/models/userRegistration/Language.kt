package com.octotelematics.installatori.models.userRegistration

data class Language(
    val id: Int,
    val name: String
)