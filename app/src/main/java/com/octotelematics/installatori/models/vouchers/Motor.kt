package com.octotelematics.installatori.models.vouchers

data class Motor(
    var colour: String,
    var id: Int,
    var plate: String,
    var type: String,
    var vehicleMake: String,
    var vehicleModel: String,
    var vehicleRegistrationDate: String,
    var vehicleVIN: String
)