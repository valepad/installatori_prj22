package com.octotelematics.installatori.models.vouchers

data class RegistrationCode(
    val registrationRequestCode: String
)