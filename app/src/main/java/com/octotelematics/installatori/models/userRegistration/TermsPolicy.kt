package com.octotelematics.installatori.models.userRegistration

data class TermsPolicy(

    val privacyText: String,
    val termsAndConditionsText: String
)