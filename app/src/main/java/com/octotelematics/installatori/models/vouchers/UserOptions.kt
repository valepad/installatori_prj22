package com.octotelematics.installatori.models.vouchers

import com.octotelematics.installatori.models.vouchers.Language

data class UserOptions(
    val id: Int,
    val language: Language
)