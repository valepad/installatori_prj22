package com.octotelematics.installatori.models.profileModels

import com.google.gson.annotations.SerializedName

data class ModelProfile(
    @SerializedName("applicationName")
    val applicationName: String,
    @SerializedName("avatarUrl")
    val avatarUrl: String,
    @SerializedName("birthDate")
    val birthDate: Any,
    @SerializedName("bleActivated")
    val bleActivated: Boolean,
    @SerializedName("car")
    val car: Any,
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: Country,
    @SerializedName("countryCode")
    val countryCode: String,
    @SerializedName("debugToken")
    val debugToken: Any,
    @SerializedName("email")
    val email: String,
    @SerializedName("facebookId")
    val facebookId: Any,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("fixedPhoneNumber")
    val fixedPhoneNumber: Any,
    @SerializedName("gender")
    val gender: Any,
    @SerializedName("id")
    val id: String,
    @SerializedName("identityCardNumber")
    val identityCardNumber: Any,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("insurancePolicyExpiryDate")
    val insurancePolicyExpiryDate: Any,
    @SerializedName("language")
    val language: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("licensePlate")
    val licensePlate: Any,
    @SerializedName("mobilePhoneNumber")
    val mobilePhoneNumber: String,
    @SerializedName("musicGenres")
    val musicGenres: List<Any>,
    @SerializedName("ngpVoucher")
    val ngpVoucher: Any,
    @SerializedName("nickname")
    val nickname: String,
    @SerializedName("numberOfFriends")
    val numberOfFriends: Int,
    @SerializedName("numberOfWonChallenges")
    val numberOfWonChallenges: Int,
    @SerializedName("passwordTemporary")
    val passwordTemporary: Boolean,
    @SerializedName("placeOfBirth")
    val placeOfBirth: Any,
    @SerializedName("region")
    val region: String,
    @SerializedName("registrationDate")
    val registrationDate: String,
    @SerializedName("roles")
    val roles: List<String>,
    @SerializedName("street")
    val street: String,
    @SerializedName("tripCreationAllowed")
    val tripCreationAllowed: Boolean,
    @SerializedName("uCoinsCount")
    val uCoinsCount: Int,
    @SerializedName("uStarsCount")
    val uStarsCount: Int,
    @SerializedName("ucoinsCount")
    val ucoinsCount: Int,
    @SerializedName("ustarsCount")
    val ustarsCount: Int,
    @SerializedName("voucher")
    val voucher: Voucher,
    @SerializedName("zipCode")
    val zipCode: String

)