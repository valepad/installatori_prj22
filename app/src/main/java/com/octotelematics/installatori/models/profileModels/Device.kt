package com.octotelematics.installatori.models.profileModels



import com.google.gson.annotations.SerializedName
import com.octotelematics.installatori.models.voucherBulkResponseCreate.User

data class Device(
    @SerializedName("id")
    var id: Int,
    @SerializedName("deviceId")
    var deviceId: String,
    @SerializedName("deviceStatus")
    var deviceStatus: String?,
    @SerializedName("installationDate")
    var installationDate: String,
    @SerializedName("driver")
    var driver: User,

    @SerializedName("deviceType")
    val deviceType: String
)