package com.octotelematics.installatori.models.scorings

data class CS(
    val duration: Double,
    val mileage: Int
)