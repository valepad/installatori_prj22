package com.octotelematics.installatori.models.profileModels

import com.google.gson.annotations.SerializedName

data class Voucher(
    @SerializedName("contractEndTimestamp")
    val contractEndTimestamp: Int,
    @SerializedName("contractStartTimestamp")
    val contractStartTimestamp: Int,
    @SerializedName("contractType")
    val contractType: ContractType,
    @SerializedName("evaluationPeriodEndTimestamp")
    val evaluationPeriodEndTimestamp: Int,
    @SerializedName("id")
    val id: String,
    @SerializedName("remainingAssessmentDays")
    val remainingAssessmentDays: Int
)