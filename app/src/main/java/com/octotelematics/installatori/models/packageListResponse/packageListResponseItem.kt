package com.octotelematics.installatori.models.packageListResponse

data class packageListResponseItem(
    val description: String,
    val id: Int,
    val name: String,
    val packageId: String,
    val price: Double,
    val template: String,
    val duration: String,
    val interval: String,
    val isActive: Boolean,
    var note: String?

)