package com.octotelematics.installatori.models.overallHabits

data class Counters(
    val crashes: Int,
    val parks: Int,
    val trips: Int
)