package com.octotelematics.installatori.models.overallHabits

data class overallHabits(
    val statistics: List<Statistic>
)