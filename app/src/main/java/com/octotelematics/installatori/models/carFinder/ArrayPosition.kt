package com.octotelematics.installatori.models.carFinder

data class ArrayPosition(
    val positions: List<carFinderPosition>
)