package com.octotelematics.installatori.models.geofence

data class PoiItem(
    val active: Boolean,
    val id: Int,
    val inProgress: Boolean,
    val latitude: Double,
    val longitude: Double,
    val notificationType: String,
    val radius: Int,
    val radiusHysteresis: Int
)