package com.octotelematics.installatori.models.userRegistration

data class UserRegistration(
    val birthDate: String,
    val clientCountry: String,
    val clientEmail: String,
    val clientFirstName: String,
    val clientLastName: String,
    val clientPhone: String,
    val companyFlag: String,
    val companyName: String,
    val id: Int,
    val tsc: Tsc,
    val userId: String,
    val userOptions: UserOptions
)