package com.octotelematics.installatori.models.profileModels

import com.google.gson.annotations.SerializedName

data class ContractType(
    @SerializedName("id")
    val id: String,
    @SerializedName("province")
    val province: Any
)