package com.octotelematics.installatori.models.overallHabits

data class Statistic(
    val computationDate: String,
    val counters: Counters,
    val customerId: String,
    val deviceId: String,
    val endUserContractId: String,
    val endUserId: String,
    val from: String,
    val geographicalStatistics: List<Any>,
    val mileage: Int,
    val mobilityTime: Int,
    val parkedTime: Int,
    val to: String,
    val totalDays: Int,
    val tscId: String
)