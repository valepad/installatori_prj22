package com.octotelematics.ddcore.models.scorings

data class S2(
    val components: List<Component>,
    val duration: Double,
    val mileage: Int,
    val penalty: Double
)