package com.octotelematics.installatori.models.vouchers

data class Language(
    val id: Int,
    val name: String
)