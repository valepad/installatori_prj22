package com.octotelematics.installatori.models.misc

data class Faq(
    var id: Int,
    var title: String,
    var description: String,
    var shortDescription: String,
    var lang: String,
    var category: String,
    var order: Int
)